DELETE FROM therapy_unit;
ALTER TABLE therapy_unit DROP COLUMN therapy_plan;
ALTER TABLE therapy_unit ADD COLUMN therapy INTEGER NOT NULL;
ALTER TABLE therapy_unit ADD FOREIGN KEY (therapy) REFERENCES therapy(ID);
