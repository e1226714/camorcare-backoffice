ALTER TABLE therapy_event DROP COLUMN therapy_time;
ALTER TABLE therapy_event ADD COLUMN therapy_time INTEGER ;
ALTER TABLE therapy_event ADD FOREIGN KEY (therapy_time) REFERENCES time_block(ID);
ALTER TABLE therapy_event DROP COLUMN therapy_room;
ALTER TABLE therapy_event ADD COLUMN therapy_room INTEGER;
ALTER TABLE therapy_event ADD FOREIGN KEY (therapy_room) REFERENCES therapy_room(ID);
