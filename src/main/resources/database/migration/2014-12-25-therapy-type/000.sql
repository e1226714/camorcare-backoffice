ALTER TABLE THERAPY_TYPE ADD COLUMN INDIVIDUAL_THERAPY BOOLEAN DEFAULT FALSE AFTER NAME;
ALTER TABLE THERAPY_TYPE ADD COLUMN INDIVIDUAL_THERAPY_DURATION INTEGER(2) DEFAULT 45 AFTER INDIVIDUAL_THERAPY;
ALTER TABLE THERAPY_TYPE ADD COLUMN INDIVIDUAL_THERAPIST_FIXED BOOLEAN DEFAULT FALSE AFTER INDIVIDUAL_THERAPY_DURATION;

ALTER TABLE THERAPY_TYPE ADD COLUMN GROUP_THERAPY BOOLEAN DEFAULT FALSE AFTER INDIVIDUAL_THERAPIST_FIXED;
ALTER TABLE THERAPY_TYPE ADD COLUMN GROUP_THERAPY_DURATION INTEGER(2) DEFAULT 45 AFTER GROUP_THERAPY;
ALTER TABLE THERAPY_TYPE ADD COLUMN GROUP_THERAPIST_FIXED BOOLEAN DEFAULT FALSE AFTER GROUP_THERAPY_DURATION;
ALTER TABLE THERAPY_TYPE ADD COLUMN GROUP_THERAPY_MIN_SIZE INTEGER DEFAULT 0 AFTER GROUP_THERAPIST_FIXED;
ALTER TABLE THERAPY_TYPE ADD COLUMN GROUP_THERAPY_MAX_SIZE INTEGER DEFAULT 0 AFTER GROUP_THERAPY_MIN_SIZE;


