CREATE TABLE TBC_THERAPY_TYPE_OCCURRENCE
(
  ID             INTEGER        PRIMARY KEY AUTO_INCREMENT,
  THERAPIST      INTEGER        NOT NULL REFERENCES THERAPIST (ID),
  THERAPY_TYPE   INTEGER        NOT NULL REFERENCES THERAPY_TYPE (ID),
  MAX_PER_DAY    INTEGER        NOT NULL,
  TIME_CREATED   TIMESTAMP      NOT NULL,
  TIME_MODIFIED  TIMESTAMP      NOT NULL,
  TIME_DELETED   TIMESTAMP      NULL DEFAULT NULL
);
