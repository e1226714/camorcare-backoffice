
ALTER TABLE therapy_event ADD COLUMN therapy_plan INTEGER AFTER ID;
ALTER TABLE therapy_event ADD FOREIGN KEY (therapy_plan) REFERENCES therapy_plan(ID);

ALTER TABLE therapy_event DROP COLUMN therapist;
ALTER TABLE therapy_event ADD COLUMN therapist INTEGER NULL DEFAULT NULL AFTER therapy_type;
ALTER TABLE therapy_event ADD FOREIGN KEY (therapist) REFERENCES therapist(ID);

ALTER TABLE therapy_event ADD COLUMN therapy_mode VARCHAR(6) AFTER therapy_type;
ALTER TABLE therapy_event ADD FOREIGN KEY (therapy_mode) REFERENCES therapy_mode(name);

ALTER TABLE therapy_event DROP COLUMN therapy_room;
ALTER TABLE therapy_event ADD COLUMN therapy_room INTEGER NULL DEFAULT NULL AFTER therapy_mode;
ALTER TABLE therapy_event ADD FOREIGN KEY (therapy_room) REFERENCES therapy_room(ID);

ALTER TABLE therapy_event DROP COLUMN therapy_time;
ALTER TABLE therapy_event ADD COLUMN therapy_time DATETIME NULL DEFAULT NULL AFTER therapy_room;

ALTER TABLE therapy_event ADD COLUMN therapy_time_block INTEGER NULL DEFAULT NULL AFTER therapy_time;
ALTER TABLE therapy_event ADD FOREIGN KEY (therapy_time_block) REFERENCES time_block(ID);


CREATE TABLE JOIN_THERAPY_EVENT_THERAPY
(
  THERAPY_EVENT_ID    INTEGER        NOT NULL REFERENCES THERAPY_EVENT (ID),
  THERAPY_ID          INTEGER        NOT NULL REFERENCES THERAPY (ID),
  PRIMARY KEY(THERAPY_EVENT_ID, THERAPY_ID)
);