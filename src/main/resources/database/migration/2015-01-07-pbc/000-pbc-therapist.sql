CREATE TABLE PBC_THERAPIST
(
  ID             INTEGER        PRIMARY KEY AUTO_INCREMENT,
  PATIENT        INTEGER        NOT NULL REFERENCES PATIENT (ID),
  THERAPIST      INTEGER        NOT NULL REFERENCES THERAPIST (ID),
  THERAPY_TYPE   INTEGER        NOT NULL REFERENCES THERAPY_TYPE (ID),
  TIME_CREATED   TIMESTAMP      NOT NULL,
  TIME_MODIFIED  TIMESTAMP      NOT NULL,
  TIME_DELETED   TIMESTAMP      NULL DEFAULT NULL
);
