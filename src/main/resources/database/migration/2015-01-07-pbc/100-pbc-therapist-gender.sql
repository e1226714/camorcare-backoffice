CREATE TABLE PBC_THERAPIST_GENDER
(
  ID             INTEGER        PRIMARY KEY AUTO_INCREMENT,
  PATIENT        INTEGER        NOT NULL REFERENCES PATIENT (ID),
  THERAPY_TYPE   INTEGER        NOT NULL REFERENCES THERAPY_TYPE (ID),
  GENDER         VARCHAR(6)     NOT NULL,
  TIME_CREATED   TIMESTAMP      NOT NULL,
  TIME_MODIFIED  TIMESTAMP      NOT NULL,
  TIME_DELETED   TIMESTAMP      NULL DEFAULT NULL
);