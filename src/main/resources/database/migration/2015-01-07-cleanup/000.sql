DROP TABLE THERAPY_HEALTH_INSURANCE;
DROP TABLE HEALTH_INSURANCE;
DROP TABLE MEDICATION;
DROP TABLE MEDICINE;
DROP TABLE PATIENT_PATIENT_CONTACT;

ALTER TABLE PATIENT_DUTY RENAME TO JOIN_PATIENT_DUTY;
ALTER TABLE PATIENT_ALLERGY RENAME TO JOIN_PATIENT_ALLERGY;
ALTER TABLE THERAPIST_PROFESSION RENAME TO JOIN_THERAPIST_THERAPY_TYPE;
