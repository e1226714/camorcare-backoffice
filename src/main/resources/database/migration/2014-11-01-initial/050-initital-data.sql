INSERT INTO ADDRESS
(STREET, ZIP, CITY, COUNTRY, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  ('Straße 1', '1234', 'Stadt 1', 'CH', NOW(), NOW(), NULL),
  ('Straße 2', '2234', 'Stadt 1', 'CH', NOW(), NOW(), NULL),
  ('Straße 3', '3234', 'Stadt 2', 'CH', NOW(), NOW(), NULL),
  ('Straße 2', '4234', 'Stadt', 'AT', NOW(), NOW(), NULL),
  ('Straße 1', '5234', 'Stadt', 'AT', NOW(), NOW(), NULL),
  ('Straße 1', '6234', 'Stadt', 'CH', NOW(), NOW(), NULL);

INSERT INTO PATIENT_CONTACT
(FIRST_NAME, LAST_NAME, PHONE, EMAIL, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  ('VName 1', 'NName 1', '+43123456', 'contact1@email.com', NOW(), NOW(), NULL),
  ('VName 2', 'NName 2', '+44123456', 'contact2@email.com', NOW(), NOW(), NULL),
  ('VName 3', 'NName 2', '+45123456', 'contact3@email.com', NOW(), NOW(), NULL),
  ('VName 1', 'NName 3', '+46123456', 'contact4@email.com', NOW(), NOW(), NULL),
  ('VName 1', 'NName 3', '+47123456', 'contact1@email.com', NOW(), NOW(), NULL),
  ('VName 2', 'NName 4', '+43123456', 'contact1@email.com', NOW(), NOW(), NULL),
  ('VName 2', 'NName 5', '+43123456', 'contact1@email.com', NOW(), NOW(), NULL);

INSERT INTO PATIENT
(FIRST_NAME, LAST_NAME, GENDER, BIRTHDATE, SSN, PHONE, EMAIL, ADDRESS, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  ('VName1', 'NName1', 'male', PARSEDATETIME('1982-01-07', 'yyyy-dd-MM'), '1234010782', '+431234567',
   'patient1@mail.com', 1, NOW(), NOW(), NULL),
  ('VName1', 'NName2', 'female', PARSEDATETIME('1983-01-07', 'yyyy-dd-MM'), '1234010783', '+432234567',
   'patient2@mail.com', 2, NOW(), NOW(), NULL),
  ('VName2', 'NName2', 'male', PARSEDATETIME('1984-01-07', 'yyyy-dd-MM'), '1234010783', '+433234567',
   'patient3@mail.com', 3, NOW(), NOW(), NULL),
  ('VName3', 'NName2', 'female', PARSEDATETIME('1982-01-07', 'yyyy-dd-MM'), '2234010782', '+434234567',
   'patient4@mail.com', 4, NOW(), NOW(), NULL),
  ('VName3', 'NName3', 'male', PARSEDATETIME('1985-01-07', 'yyyy-dd-MM'), '1234010785', '+431234567',
   'patient5@mail.com', 5, NOW(), NOW(), NULL),
  ('VName4', 'NName3', 'female', PARSEDATETIME('1986-01-07', 'yyyy-dd-MM'), '1234010786', '+431234567',
   'patient6@mail.com', 6, NOW(), NOW(), NULL);


INSERT INTO PATIENT_COMMENT
(PATIENT, CONTENT, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, 'Comment 1', NOW(), NOW(), NULL),
  (1, 'Comment 2', NOW(), NOW(), NULL),
  (2, 'Comment 3', NOW(), NOW(), NULL),
  (3, 'Comment 4', NOW(), NOW(), NULL),
  (4, 'Comment 5', NOW(), NOW(), NULL),
  (5, 'Comment 6', NOW(), NOW(), NULL),
  (5, 'Comment 7', NOW(), NOW(), NULL);

INSERT INTO PATIENT_PATIENT_CONTACT
(PATIENT, CONTACT)
VALUES
  (1, 1),
  (1, 2),
  (2, 3),
  (3, 4),
  (4, 5),
  (5, 6),
  (5, 1);


INSERT INTO PATIENT_ALLERGY
(PATIENT, NAME, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, 'Allergie 1', now(), now(), NULL),
  (2, 'Allergie 1', now(), now(), NULL),
  (4, 'Allergie 2', now(), now(), NULL),
  (4, 'Allergie 3', now(), now(), NULL),
  (5, 'Allergie 1', now(), now(), NULL),
  (5, 'Allergie 2', now(), now(), NULL),
  (5, 'Allergie 3', now(), now(), NULL);


INSERT INTO DUTY_CATEGORY
(NAME, DURATION, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  ('Gartendienst', 2, NOW(), NOW(), NULL),
  ('Küchendienst', 1, NOW(), NOW(), NULL),
  ('Tee kochen', 1, NOW(), NOW(), NULL),
  ('Therapieräume aufräumen', 2, NOW(), NOW(), NULL);

INSERT INTO DUTY
(CATEGORY, SCHEDULE_DATE, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (1, PARSEDATETIME('2015-04-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (2, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (2, PARSEDATETIME('2015-02-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (2, PARSEDATETIME('2015-03-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (2, PARSEDATETIME('2015-04-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (2, PARSEDATETIME('2015-05-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (2, PARSEDATETIME('2015-06-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (2, PARSEDATETIME('2015-07-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (3, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (3, PARSEDATETIME('2015-02-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (3, PARSEDATETIME('2015-03-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (3, PARSEDATETIME('2015-04-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (3, PARSEDATETIME('2015-05-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (3, PARSEDATETIME('2015-06-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (3, PARSEDATETIME('2015-07-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL);

INSERT INTO PATIENT_DUTY
(DUTY, PATIENT, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, 1, NOW(), NOW(), NULL),
  (2, 2, NOW(), NOW(), NULL),
  (3, 3, NOW(), NOW(), NULL),
  (4, 4, NOW(), NOW(), NULL),
  (5, 5, NOW(), NOW(), NULL),
  (6, 5, NOW(), NOW(), NULL),
  (7, 5, NOW(), NOW(), NULL),
  (8, 1, NOW(), NOW(), NULL),
  (9, 2, NOW(), NOW(), NULL),
  (10, 3, NOW(), NOW(), NULL),
  (11, 4, NOW(), NOW(), NULL),
  (12, 5, NOW(), NOW(), NULL);

INSERT INTO PATIENT_ROOM_CATEGORY
(NAME, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  ('standard', NOW(), NOW(), NULL),
  ('komfort', NOW(), NOW(), NULL),
  ('luxus', NOW(), NOW(), NULL),
  ('super', NOW(), NOW(), NULL);

INSERT INTO PATIENT_ROOM_CATEGORY_PRICE
(CATEGORY, INSURANCE_LEVEL, CENTS, DATE_FROM, DATE_UNTIL, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, 'dummy', 9900, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), PARSEDATETIME('2015-01-08', 'yyyy-dd-MM'), NOW(),
   NOW(), NULL),
  (1, 'dummy', 9900, PARSEDATETIME('2015-01-08', 'yyyy-dd-MM'), PARSEDATETIME('2016-01-01', 'yyyy-dd-MM'), NOW(),
   NOW(), NULL),
  (2, 'dummy', 19900, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), PARSEDATETIME('2015-01-08', 'yyyy-dd-MM'), NOW(),
   NOW(), NULL),
  (3, 'dummy', 29900, PARSEDATETIME('2015-01-08', 'yyyy-dd-MM'), PARSEDATETIME('2016-01-01', 'yyyy-dd-MM'), NOW(),
   NOW(), NULL);

INSERT INTO PATIENT_ROOM
(NAME, CATEGORY, CAPACITY, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  ('Königssuite', 3, 2, NOW(), NOW(), NULL),
  ('Zimmer 4', 2, 1, NOW(), NOW(), NULL),
  ('Zimmer 3', 1, 3, NOW(), NOW(), NULL),
  ('Zimmer 2', 1, 4, NOW(), NOW(), NULL),
  ('Zimmer 1', 1, 2, NOW(), NOW(), NULL);

INSERT INTO THERAPY
(PATIENT, PATIENT_ROOM, DATE_SICK_LEAVE, DATE_FROM, DATE_UNTIL, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, 1, PARSEDATETIME('2015-01-05,13', 'yyyy-dd-MM,HH'), PARSEDATETIME('2015-01-06,13', 'yyyy-dd-MM,HH'),
   PARSEDATETIME('2015-19-06,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (2, 2, PARSEDATETIME('2015-01-05,13', 'yyyy-dd-MM,HH'), PARSEDATETIME('2015-01-06,13', 'yyyy-dd-MM,HH'),
   PARSEDATETIME('2015-19-06,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (3, 3, PARSEDATETIME('2015-01-05,13', 'yyyy-dd-MM,HH'), PARSEDATETIME('2015-01-06,13', 'yyyy-dd-MM,HH'),
   PARSEDATETIME('2015-19-06,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (4, 3, PARSEDATETIME('2015-01-05,13', 'yyyy-dd-MM,HH'), PARSEDATETIME('2015-01-06,13', 'yyyy-dd-MM,HH'),
   PARSEDATETIME('2015-19-06,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (5, 3, PARSEDATETIME('2015-01-05,13', 'yyyy-dd-MM,HH'), PARSEDATETIME('2015-01-06,13', 'yyyy-dd-MM,HH'),
   PARSEDATETIME('2015-19-06,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (5, 4, PARSEDATETIME('2015-01-05,13', 'yyyy-dd-MM,HH'), PARSEDATETIME('2016-01-06,13', 'yyyy-dd-MM,HH'),
   PARSEDATETIME('2016-19-06,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (5, 4, PARSEDATETIME('2015-01-05,13', 'yyyy-dd-MM,HH'), PARSEDATETIME('2017-01-06,13', 'yyyy-dd-MM,HH'),
   PARSEDATETIME('2017-19-06,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL);

INSERT INTO HEALTH_INSURANCE
(NAME, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  ('Versicherung 1', NOW(), NOW(), NULL),
  ('Versicherung 2', NOW(), NOW(), NULL),
  ('Versicherung 3', NOW(), NOW(), NULL),
  ('Versicherung 4', NOW(), NOW(), NULL),
  ('Versicherung 5', NOW(), NOW(), NULL),
  ('Versicherung 6', NOW(), NOW(), NULL),
  ('Versicherung 7', NOW(), NOW(), NULL);

INSERT INTO THERAPY_HEALTH_INSURANCE
(THERAPY, HEALTH_INSURANCE, HEALTH_INSURANCE_LEVEL, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, 1, 'dummy', now(), now(), NULL),
  (2, 2, 'dummy', now(), now(), NULL),
  (3, 3, 'dummy', now(), now(), NULL),
  (4, 4, 'dummy', now(), now(), NULL),
  (5, 5, 'dummy', now(), now(), NULL),
  (6, 6, 'dummy', now(), now(), NULL),
  (6, 1, 'dummy', now(), now(), NULL),
  (6, 2, 'dummy', now(), now(), NULL);

INSERT INTO MEDICINE
(NAME, UNIT, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
    VALUES
      ('Medizin 1', 'milliliter', now(), now(), NULL ),
      ('Medizin 2', 'milliliter', now(), now(), NULL ),
      ('Medizin 2', 'pill', now(), now(), NULL ),
      ('Medizin 3', 'pill', now(), now(), NULL ),
      ('Medizin 4', 'milliliter', now(), now(), NULL ),
      ('Medizin 5', 'pill', now(), now(), NULL );

INSERT INTO MEDICATION
(THERAPY, MEDICINE, AMOUNT, TIME_GIVEN, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
    VALUES
      (1,1,'amount 1', now(), now(), now(), NULL),
      (1,2,'amount 2', now(), now(), now(), NULL),
      (2,1,'amount 3', now(), now(), now(), NULL),
      (3,2,'amount 4', now(), now(), now(), NULL),
      (4,3,'amount 1', now(), now(), now(), NULL),
      (5,4,'amount 1', now(), now(), now(), NULL),
      (4,1,'amount 2', now(), now(), now(), NULL),
      (3,2,'amount 2', now(), now(), now(), NULL),
      (2,3,'amount 1', now(), now(), now(), NULL),
      (1,1,'amount 1', now(), now(), now(), NULL);
INSERT INTO THERAPIST
(FIRST_NAME,LAST_NAME,GENDER,EMAIL, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  ('Hans', 'Huber', 'male', 'therapeut1@mail.com', NOW(), NOW(), NULL ),
  ('Susanne', 'Schmidt', 'female', 'therapeut2@mail.com', NOW(), NOW(), NULL ),
  ('Maria', 'Mayer', 'female','therapeut3@mail.com', NOW(), NOW(), NULL ),
  ('Peter', 'Prechtl', 'male', 'therapeut4@mail.com', NOW(), NOW(), NULL ),
  ('Petra', 'Prechtl', 'female', 'therapeut5@mail.com', NOW(), NOW(), NULL );

INSERT INTO THERAPY_TYPE
(NAME,TIME_CREATED,TIME_MODIFIED, TIME_DELETED)
VALUES
  ('Gestalttherapie', NOW(), NOW(), NULL),
  ('Integrative Therapie', NOW(), NOW(), NULL),
  ('Körperpsychotherapie', NOW(), NOW(), NULL),
  ('Logotherapie und Existenzanalyse', NOW(), NOW(), NULL),
  ('Musiktherapie', NOW(), NOW(), NULL),
  ('Psychoanalyse', NOW(), NOW(), NULL),
  ('Verhaltenstherapie', NOW(), NOW(), NULL),
  ('Yoga', NOW(), NOW(), NULL);


INSERT INTO THERAPIST_PROFESSION
(THERAPIST, THERAPY_TYPE)
VALUES
  (1, 2),
  (1, 3),
  (2, 4),
  (2, 7),
  (3, 8),
  (3, 1),
  (4, 6),
  (4, 5);

INSERT INTO THERAPY_ROOM
(NAME, CAPACITY, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  ('Therapieraum A', 20, NOW(), NOW(), NULL),
  ('Therapieraum B', 10, NOW(), NOW(), NULL),
  ('Therapieraum C', 30, NOW(), NOW(), NULL);

INSERT INTO THERAPY_EVENT
(THERAPIST, THERAPY_TYPE, THERAPY_ROOM, THERAPY_TIME, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, 1, 1, PARSEDATETIME('2015-01-06,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (1, 2, 1, PARSEDATETIME('2015-01-06,15', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (1, 1, 1, PARSEDATETIME('2015-01-07,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (1, 2, 1, PARSEDATETIME('2015-01-07,15', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (1, 3, 2, PARSEDATETIME('2015-01-06,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (1, 4, 2, PARSEDATETIME('2015-01-06,15', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL),
  (1, 3, 2, PARSEDATETIME('2015-01-07,10', 'yyyy-dd-MM,HH'), NOW(), NOW(), NULL);

INSERT INTO THERAPY_PLAN
(THERAPY, DATE_FROM, DATE_UNTIL, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), PARSEDATETIME('2015-07-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (1, PARSEDATETIME('2015-08-06', 'yyyy-dd-MM'), PARSEDATETIME('2015-15-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (2, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), PARSEDATETIME('2015-07-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (3, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), PARSEDATETIME('2015-07-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (4, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), PARSEDATETIME('2015-07-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL),
  (5, PARSEDATETIME('2015-01-06', 'yyyy-dd-MM'), PARSEDATETIME('2015-07-06', 'yyyy-dd-MM'), NOW(), NOW(), NULL);

INSERT INTO THERAPY_UNIT
(THERAPY_EVENT, THERAPY_PLAN, TIME_CREATED, TIME_MODIFIED, TIME_DELETED)
VALUES
  (1, 1, NOW(), NOW(), NULL),
  (1, 3, NOW(), NOW(), NULL),
  (1, 4, NOW(), NOW(), NULL),
  (1, 5, NOW(), NOW(), NULL),
  (2, 1, NOW(), NOW(), NULL),
  (2, 3, NOW(), NOW(), NULL),
  (3, 1, NOW(), NOW(), NULL),
  (4, 1, NOW(), NOW(), NULL),
  (5, 1, NOW(), NOW(), NULL);

INSERT INTO INVOICE
(THERAPY,ADDRESS,VAT_INCLUDED, TIME_CREATED, TIME_MODIFIED , TIME_DELETED)
VALUES
  (1,4, TRUE , now(), now(), NULL ),
  (2,3, TRUE , now(), now(), NULL ),
  (3,2, TRUE , now(), now(), NULL ),
  (4,1, TRUE , now(), now(), NULL );

  	  

