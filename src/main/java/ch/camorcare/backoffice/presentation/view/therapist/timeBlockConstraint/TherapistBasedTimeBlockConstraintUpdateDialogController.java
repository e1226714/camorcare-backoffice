package ch.camorcare.backoffice.presentation.view.therapist.timeBlockConstraint;

import ch.camorcare.backoffice.adapter.TherapistAdapter;
import ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint;
import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.TherapistConstraintManager;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.*;

/**
 * Controller for the TherapistBasedTimeBlockConstraint update dialog window
 */
public class TherapistBasedTimeBlockConstraintUpdateDialogController implements Initializable {

    @FXML
    private CheckBox blockMONDAY1;
    @FXML
    private CheckBox blockMONDAY2;
    @FXML
    private CheckBox blockMONDAY3;
    @FXML
    private CheckBox blockMONDAY4;
    @FXML
    private CheckBox blockMONDAY5;
    @FXML
    private CheckBox blockMONDAY6;
    @FXML
    private CheckBox blockTUESDAY1;
    @FXML
    private CheckBox blockTUESDAY2;
    @FXML
    private CheckBox blockTUESDAY3;
    @FXML
    private CheckBox blockTUESDAY4;
    @FXML
    private CheckBox blockTUESDAY5;
    @FXML
    private CheckBox blockTUESDAY6;
    @FXML
    private CheckBox blockWEDNESDAY1;
    @FXML
    private CheckBox blockWEDNESDAY2;
    @FXML
    private CheckBox blockWEDNESDAY3;
    @FXML
    private CheckBox blockWEDNESDAY4;
    @FXML
    private CheckBox blockWEDNESDAY5;
    @FXML
    private CheckBox blockWEDNESDAY6;
    @FXML
    private CheckBox blockTHURSDAY1;
    @FXML
    private CheckBox blockTHURSDAY2;
    @FXML
    private CheckBox blockTHURSDAY3;
    @FXML
    private CheckBox blockTHURSDAY4;
    @FXML
    private CheckBox blockTHURSDAY5;
    @FXML
    private CheckBox blockTHURSDAY6;
    @FXML
    private CheckBox blockFRIDAY1;
    @FXML
    private CheckBox blockFRIDAY2;
    @FXML
    private CheckBox blockFRIDAY3;
    @FXML
    private CheckBox blockFRIDAY4;
    @FXML
    private CheckBox blockFRIDAY5;
    @FXML
    private CheckBox blockFRIDAY6;

    /**
     *
     */
    private Stage dialogStage;

    /**
     *
     */
    private TherapistAdapter therapist;

    /**
     *
     */
    private TherapistConstraintManager therapistConstraintManager;

    /**
     *
     */
    private Map<Day, List<TimeBlock>> timeBlocks;

    /**
     *
     */
    private Map<TimeBlock, CheckBox> timeBlockToCheckBox;

    /**
     *
     */
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(400);
    }

    public void setTherapistConstraintManager(TherapistConstraintManager therapistConstraintManager) {
        this.therapistConstraintManager = therapistConstraintManager;
    }

    /**
     * Sets the therapist to be edited in the dialog.
     *
     * @param therapist
     */
    public void setTherapist(TherapistAdapter therapist) {
        this.therapist = therapist;
        List<TherapistBasedTimeBlockConstraint> constraints = new ArrayList<>();

        try {
            timeBlockToCheckBox = new HashMap<>();
            timeBlocks = therapistConstraintManager.findAllTimeBlocksGroupedByDay();

            timeBlockToCheckBox.put(timeBlocks.get(Day.MONDAY).get(0), blockMONDAY1);
            timeBlockToCheckBox.put(timeBlocks.get(Day.MONDAY).get(1), blockMONDAY2);
            timeBlockToCheckBox.put(timeBlocks.get(Day.MONDAY).get(2), blockMONDAY3);
            timeBlockToCheckBox.put(timeBlocks.get(Day.MONDAY).get(3), blockMONDAY4);
            timeBlockToCheckBox.put(timeBlocks.get(Day.MONDAY).get(4), blockMONDAY5);
            timeBlockToCheckBox.put(timeBlocks.get(Day.MONDAY).get(5), blockMONDAY6);

            timeBlockToCheckBox.put(timeBlocks.get(Day.TUESDAY).get(0), blockTUESDAY1);
            timeBlockToCheckBox.put(timeBlocks.get(Day.TUESDAY).get(1), blockTUESDAY2);
            timeBlockToCheckBox.put(timeBlocks.get(Day.TUESDAY).get(2), blockTUESDAY3);
            timeBlockToCheckBox.put(timeBlocks.get(Day.TUESDAY).get(3), blockTUESDAY4);
            timeBlockToCheckBox.put(timeBlocks.get(Day.TUESDAY).get(4), blockTUESDAY5);
            timeBlockToCheckBox.put(timeBlocks.get(Day.TUESDAY).get(5), blockTUESDAY6);

            timeBlockToCheckBox.put(timeBlocks.get(Day.WEDNESDAY).get(0), blockWEDNESDAY1);
            timeBlockToCheckBox.put(timeBlocks.get(Day.WEDNESDAY).get(1), blockWEDNESDAY2);
            timeBlockToCheckBox.put(timeBlocks.get(Day.WEDNESDAY).get(2), blockWEDNESDAY3);
            timeBlockToCheckBox.put(timeBlocks.get(Day.WEDNESDAY).get(3), blockWEDNESDAY4);
            timeBlockToCheckBox.put(timeBlocks.get(Day.WEDNESDAY).get(4), blockWEDNESDAY5);
            timeBlockToCheckBox.put(timeBlocks.get(Day.WEDNESDAY).get(5), blockWEDNESDAY6);

            timeBlockToCheckBox.put(timeBlocks.get(Day.THURSDAY).get(0), blockTHURSDAY1);
            timeBlockToCheckBox.put(timeBlocks.get(Day.THURSDAY).get(1), blockTHURSDAY2);
            timeBlockToCheckBox.put(timeBlocks.get(Day.THURSDAY).get(2), blockTHURSDAY3);
            timeBlockToCheckBox.put(timeBlocks.get(Day.THURSDAY).get(3), blockTHURSDAY4);
            timeBlockToCheckBox.put(timeBlocks.get(Day.THURSDAY).get(4), blockTHURSDAY5);
            timeBlockToCheckBox.put(timeBlocks.get(Day.THURSDAY).get(5), blockTHURSDAY6);

            timeBlockToCheckBox.put(timeBlocks.get(Day.FRIDAY).get(0), blockFRIDAY1);
            timeBlockToCheckBox.put(timeBlocks.get(Day.FRIDAY).get(1), blockFRIDAY2);
            timeBlockToCheckBox.put(timeBlocks.get(Day.FRIDAY).get(2), blockFRIDAY3);
            timeBlockToCheckBox.put(timeBlocks.get(Day.FRIDAY).get(3), blockFRIDAY4);
            timeBlockToCheckBox.put(timeBlocks.get(Day.FRIDAY).get(4), blockFRIDAY5);
            timeBlockToCheckBox.put(timeBlocks.get(Day.FRIDAY).get(5), blockFRIDAY6);

        } catch (ServiceException e) {
            e.printStackTrace();
        }

        try {
            constraints = therapistConstraintManager.findAllTherapistBasedTimeBlockConstraintsByTherapist(therapist.getEntity());
        } catch (ServiceException e) {
            // TODO
        }

        for (TherapistBasedTimeBlockConstraint constraint : constraints) {
            for (Map.Entry<TimeBlock, CheckBox> entry : timeBlockToCheckBox.entrySet()) {
                TimeBlock timeBlock = entry.getKey();
                CheckBox checkBox = entry.getValue();

                if (constraint.getTimeBlock().getId().equals(timeBlock.getId())) {
                    checkBox.setSelected(true);
                }
            }
        }
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        List<TherapistBasedTimeBlockConstraint> constraints = new ArrayList<>();

        for (Map.Entry<TimeBlock, CheckBox> entry : timeBlockToCheckBox.entrySet()) {
            TimeBlock timeBlock = entry.getKey();
            CheckBox checkBox = entry.getValue();

            if (checkBox.isSelected()) {
                TherapistBasedTimeBlockConstraint constraint = new TherapistBasedTimeBlockConstraint();
                constraint.setTherapist(therapist.getEntity());
                constraint.setTimeBlock(timeBlock);

                constraints.add(constraint);
            }
        }

        try {
            therapistConstraintManager.create(constraints, therapist.getEntity());
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (ValidationException e) {
            e.printStackTrace();
        }

        dialogStage.close();
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
}
