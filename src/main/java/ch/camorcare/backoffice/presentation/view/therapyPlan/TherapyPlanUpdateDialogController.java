package ch.camorcare.backoffice.presentation.view.therapyPlan;

import ch.camorcare.backoffice.adapter.TherapyPlanAdapter;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;
import org.joda.time.DateTime;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the TherapyPlan update dialog window
 */
public class TherapyPlanUpdateDialogController implements Initializable {

    @FXML
    private DatePicker fieldDateFrom;

    private Stage dialogStage;

    private TherapyPlanAdapter therapyPlan;

    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(180);
    }

    /**
     * Sets the therapy room to be edited in the dialog.
     *
     * @param therapyPlan
     */
    public void setTherapyPlan(TherapyPlanAdapter therapyPlan) {
        this.therapyPlan = therapyPlan;
        fieldDateFrom.setValue(DateTimeConverter.dateTimeToLocalDate(therapyPlan.getStartDateProperty().getValue()));
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            DateTime startDate = DateTimeConverter.localDateToDateTime(fieldDateFrom.getValue());

            therapyPlan.setStartDate(startDate);
            therapyPlan.setEndDate(startDate.plusDays(4));

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        // TODO

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Bitte korrigieren Sie ihre Eingabe")
                    .message(errorMessage)
                    .showError();
            return false;
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
}
