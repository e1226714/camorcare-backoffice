package ch.camorcare.backoffice.presentation.view.therapyPlan;

import ch.camorcare.backoffice.adapter.*;
import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.presentation.MainApp;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.PatientManager;
import ch.camorcare.backoffice.service.impl.TherapistManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.input.MouseEvent;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.*;
import org.joda.time.DateTime;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the TherapyPlan overview
 */
public class TherapyPlanOverviewController implements Initializable {

    /**
     * Therapy plan information.
     */
    @FXML
    private TableView<TherapyPlanAdapter> therapyPlanTable;
    private ObservableList<TherapyPlanAdapter> therapyPlanTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TherapyPlanAdapter, DateTime> therapyPlanTableColumnYear;

    @FXML
    private TableColumn<TherapyPlanAdapter, DateTime> therapyPlanTableColumnWeek;

    @FXML
    private TableColumn<TherapyPlanAdapter, DateTime> therapyPlanTableColumnStartDate;

    @FXML
    private TableColumn<TherapyPlanAdapter, DateTime> therapyPlanTableColumnEndDate;

    @FXML
    private Label labelYear;

    @FXML
    private Label labelWeek;

    @FXML
    private Label labelStartDate;

    @FXML
    private Label labelEndDate;

    @FXML
    private TextField filterField;

    /**
     * Patient information.
     */
    @FXML
    private TableView<TherapyAdapter> patientTable;
    private ObservableList<TherapyAdapter> patientTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TherapyAdapter, String> patientTableColumnFirstName;

    @FXML
    private TableColumn<TherapyAdapter, String> patientTableColumnLastName;

    /**
     * Therapist information.
     */
    @FXML
    private TableView<TherapistAdapter> therapistTable;
    private ObservableList<TherapistAdapter> therapistTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TherapistAdapter, String> therapistTableColumnFirstName;

    @FXML
    private TableColumn<TherapistAdapter, String> therapistTableColumnLastName;

    /**
     * Therapy room information.
     */
    @FXML
    private TableView<TherapyRoomAdapter> therapyRoomTable;
    private ObservableList<TherapyRoomAdapter> therapyRoomTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TherapyRoomAdapter, String> therapyRoomTableColumnName;

    /**
     * Main application.
     */
    private MainApp mainApp;

    /**
     * Therapy Manager.
     */
    private TherapyManager therapyManager;

    /**
     * Therapist Manager.
     */
    private TherapistManager therapistManager;

    /**
     * Patient Manager.
     */
    private PatientManager patientManager;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setTherapistManager(TherapistManager therapistManager) {
        this.therapistManager = therapistManager;
    }

    public void setPatientManager(PatientManager patientManager) {
        this.patientManager = patientManager;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        /**
         * Therapy plan table
         */
        therapyPlanTableColumnYear.setCellValueFactory(cellData -> cellData.getValue().getStartDateProperty());
        therapyPlanTableColumnWeek.setCellValueFactory(cellData -> cellData.getValue().getStartDateProperty());
        therapyPlanTableColumnStartDate.setCellValueFactory(cellData -> cellData.getValue().getStartDateProperty());
        therapyPlanTableColumnEndDate.setCellValueFactory(cellData -> cellData.getValue().getEndDateProperty());


        therapyPlanTableColumnYear.setCellFactory(column -> {
            return new TableCell<TherapyPlanAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(""+item.getYear());
                    }
                }
            };
        });

        therapyPlanTableColumnWeek.setCellFactory(column -> {
            return new TableCell<TherapyPlanAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(""+item.getWeekOfWeekyear());
                    }
                }
            };
        });

        therapyPlanTableColumnStartDate.setCellFactory(column -> {
            return new TableCell<TherapyPlanAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(DateTimeConverter.dateTimeToString(item));
                    }
                }
            };
        });

        therapyPlanTableColumnEndDate.setCellFactory(column -> {
            return new TableCell<TherapyPlanAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(DateTimeConverter.dateTimeToString(item));
                    }
                }
            };
        });

        /**
         * Details: Patient table.
         */
        patientTableColumnFirstName.setCellValueFactory(cellData -> cellData.getValue().getPatientAdapter().getFirstNameProperty());
        patientTableColumnLastName.setCellValueFactory(cellData -> cellData.getValue().getPatientAdapter().getLastNameProperty());

        patientTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (t.getClickCount() == 2 && patientTable.getSelectionModel().getSelectedIndex() >= 0) {
                    handleOpenTherapyPlan();
                }
            }
        });

        /**
         * Details: Therapist table.
         */
        therapistTableColumnFirstName.setCellValueFactory(cellData -> cellData.getValue().getFirstNameProperty());
        therapistTableColumnLastName.setCellValueFactory(cellData -> cellData.getValue().getLastNameProperty());

        therapistTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (t.getClickCount() == 2 && therapistTable.getSelectionModel().getSelectedIndex() >= 0) {
                    handleOpenWorkingPlan();
                }
            }
        });

        /**
         * Details: Room table.
         */
        therapyRoomTableColumnName.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());

        therapyRoomTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (t.getClickCount() == 2 && therapyRoomTable.getSelectionModel().getSelectedIndex() >= 0) {
                    handleOpenRoomPlan();
                }
            }
        });


        // Clear details.
        showTherapyPlanDetails(null);

        // Listen for selection changes and show details when changed.
        therapyPlanTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showTherapyPlanDetails(newValue));

        // Fills therapy plan table with data
        initTherapyPlanTable();
    }

    private void initTherapyPlanTable() {
        try {
            therapyPlanTableData.clear();
            List<TherapyPlan> therapyPlans = therapyManager.findAllTherapyPlans();

            for (TherapyPlan therapyPlan : therapyPlans) {
                therapyPlanTableData.add(new TherapyPlanAdapter(therapyPlan));
            }

            therapyPlanTable.setItems(therapyPlanTableData);

            // Wrap the ObservableList in a FilteredList (initially display all data).
            FilteredList<TherapyPlanAdapter> filteredData = new FilteredList<>(therapyPlanTableData, p -> true);

            // Set the filter Predicate whenever the filter changes.

            filterField
                .textProperty()
                .addListener((observable, oldValue, newValue) -> {
                    filteredData.setPredicate(therapyPlanAdapter -> {
                        // If filter text is empty, display all
                        if (newValue == null || newValue.isEmpty()) {
                            return true;
                        }

                        // filter text.
                        String lowerCaseFilter = newValue.toLowerCase();


                        if(DateTimeConverter.dateTimeToString(therapyPlanAdapter.getStartDateProperty().get()).toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }

                        if(DateTimeConverter.dateTimeToString(therapyPlanAdapter.getEndDateProperty().get()).toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }

                        if((therapyPlanAdapter.getStartDateProperty().get().getWeekOfWeekyear()+"").equals(lowerCaseFilter)) {
                            return true;
                        }

                        return false;
                    });
                });


            // Wrap the FilteredList in a SortedList.
            SortedList<TherapyPlanAdapter> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(therapyPlanTable.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            therapyPlanTable.setItems(sortedData);

        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialiseren der Therapieplanübersicht")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    /**
     * @param therapyPlan the therapy plan or null
     */
    private void showTherapyPlanDetails(TherapyPlanAdapter therapyPlan) {
        if (therapyPlan != null) {
            labelYear.setText(""+therapyPlan.getStartDateProperty().getValue().getYear());
            labelWeek.setText(""+therapyPlan.getStartDateProperty().getValue().getWeekOfWeekyear());
            labelStartDate.setText(DateTimeConverter.dateTimeToString(therapyPlan.getStartDateProperty().get()));
            labelEndDate.setText(DateTimeConverter.dateTimeToString(therapyPlan.getEndDateProperty().get()));
        } else {
            labelYear.setText("");
            labelWeek.setText("");
            labelStartDate.setText("");
            labelEndDate.setText("");
        }

        initTherapyPlanDetails(therapyPlan);
    }

    /**
     *
     * @param therapyPlan
     */
    private void initTherapyPlanDetails(TherapyPlanAdapter therapyPlan) {

        if(therapyPlan != null) {

            /**
             * Look up relevant patients.
             */
            try {
                patientTableData.clear();
                List<Therapy> therapies = therapyManager.findAllTherapiesByTherapyPlan(therapyPlan.getEntity());

                for (Therapy therapy : therapies) {
                    //Patient patient = patientManager.findOnePatientById(therapy.getPatient());
                    //patientTableData.add(new PatientAdapter(patient));
                    patientTableData.add(new TherapyAdapter(therapy));
                }

                patientTable.setItems(patientTableData);
            } catch (ServiceException e) {
                Dialogs.create().title("Fehler beim Initialiseren der Therapieplandetails")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }

            /**
             * Look up relevant therapists.
             */
            try {
                therapistTableData.clear();
                List<Therapist> therapists = therapistManager.findAllTherapistsByTherapyPlan(therapyPlan.getEntity());

                for (Therapist therapist : therapists) {
                    therapist = therapistManager.findOneById(therapist);
                    therapistTableData.add(new TherapistAdapter(therapist));
                }

                therapistTable.setItems(therapistTableData);
            } catch (ServiceException e) {
                e.printStackTrace();
            }

            /**
             * Look up relevant rooms.
             */
            try {
                therapyRoomTableData.clear();
                List<TherapyRoom> therapyRooms = therapyManager.findAllTherapyRoomsByTherapyPlan(therapyPlan.getEntity());

                for (TherapyRoom therapyRoom : therapyRooms) {
                    therapyRoomTableData.add(new TherapyRoomAdapter(therapyRoom));
                }

                therapyRoomTable.setItems(therapyRoomTableData);
            } catch (ServiceException e) {
                e.printStackTrace();
            }

        } else {
            patientTableData.clear();
            therapistTableData.clear();
            therapyRoomTableData.clear();
        }
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new therapy room.
     */
    @FXML
    private void handleCreate() {
        TherapyPlan therapyPlan = new TherapyPlan();
        boolean okClicked = mainApp.showTherapyPlanEditDialog(new TherapyPlanAdapter(therapyPlan));

        if (okClicked) {
            if(therapyPlan.getDateFrom().getDayOfWeek() != 1) {
                Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Start-Datum des Therapieplans")
                    .message("Ein Therapieplan darf nur an einem Montag beginnen.")
                    .showWarning();
            } else {
                try {
                    boolean exists = false;

                    for (TherapyPlanAdapter tpAdapter : therapyPlanTableData) {
                        if (tpAdapter.getEntity().getDateFrom().equals(therapyPlan.getDateFrom())) {
                            exists = true;
                            break;
                        }
                    }

                    if (exists) {
                        Dialogs.create()
                                .title("Ungültige Eingabe")
                                .masthead("Therapieplan wurde bereits erstellt")
                                .message("Für diese Woche wurde bereits ein Therapieplan erstellt.")
                                .showWarning();
                    }
                    else {
                        therapyPlan = therapyManager.create(therapyPlan);
                        TherapyPlanAdapter adapter = new TherapyPlanAdapter(therapyPlan);
                        therapyPlanTableData.add(adapter);
                        therapyPlanTable.getSelectionModel().select(adapter);
                    }
                } catch (ServiceException | ValidationException e) {
                    Dialogs.create().title("Fehler beim Anlegen eines Therapieplans")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected therapy room.
     */
    @FXML
    private void handleUpdate() {
        /*
        TherapyPlanAdapter tra = therapyPlanTable.getSelectionModel().getSelectedItem();
        if (tra != null) {
            boolean okClicked = mainApp.showTherapyRoomEditDialog(tra);
            if (okClicked) {
                try {
                    showTherapyPlanDetails(tra);
                    therapyManager.update(tra.getEntity());
                } catch (ServiceException | ValidationException e) {
                    Dialogs.create().title("Fehler beim Aktualisieren eines Therapieplans")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            // Nothing selected.
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Keinen Raum ausgewählt")
                    .message("Bitte wählen Sie einen Raum aus der Tabelle aus.")
                    .showWarning();
        }
        */
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDelete() {
        int selectedIndex = therapyPlanTable.getSelectionModel().getSelectedIndex();

        if (selectedIndex >= 0) {
            Action response = Dialogs.create().title("Bestätigungsdialog")
                    .masthead("Therapieplan löschen")
                    .message("Möchten Sie diesen Therapieplan wirklich löschen?")
                    .actions(org.controlsfx.dialog.Dialog.ACTION_YES, org.controlsfx.dialog.Dialog.ACTION_NO)
                    .showConfirm();
            if (response == org.controlsfx.dialog.Dialog.ACTION_YES) {
                try {
                    therapyManager.delete(therapyPlanTableData.get(selectedIndex).getEntity());
                    therapyPlanTableData.remove(selectedIndex);
                    Dialogs.create().title("Informationsdialog")
                            .masthead(null).message("Therapieplan wurde erfolgreich gelöscht.")
                            .showInformation();
                } catch (ServiceException e) {
                    Dialogs.create().title("Fehler beim Löschen eines Therapieplans")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            Dialogs.create().title("Ungültige Auswahl")
                .masthead("Keinen Therapieplan ausgewählt")
                .message("Bitte wählen Sie einen Therapieplan aus der Tabelle aus.")
                .showWarning();
        }
    }

    @FXML
    public void handleGoToGroupTherapyEvents() {
        TherapyPlanAdapter tpa = therapyPlanTable.getFocusModel().getFocusedItem();
        mainApp.showEventCreator(tpa);
    }

    @FXML
    public void handleGoToPlanOverview() {
        TherapyPlanAdapter tpa = therapyPlanTable.getFocusModel().getFocusedItem();
        mainApp.showPlanOverview(tpa);
    }

    private void handleOpenTherapyPlan() {
            TherapyPlanAdapter tpa = therapyPlanTable.getFocusModel().getFocusedItem();
            TherapyAdapter ta = patientTable.getFocusModel().getFocusedItem();
            mainApp.showPlanOverview(tpa.getEntity(), ta.getEntity(), null, null);
    }

    private void handleOpenWorkingPlan() {
            TherapyPlanAdapter tpa = therapyPlanTable.getFocusModel().getFocusedItem();
            TherapistAdapter ta = therapistTable.getFocusModel().getFocusedItem();
            mainApp.showPlanOverview(tpa.getEntity(), null, ta.getEntity(), null);
    }

    private void handleOpenRoomPlan() {
            TherapyPlanAdapter tpa = therapyPlanTable.getFocusModel().getFocusedItem();
            TherapyRoomAdapter tra = therapyRoomTable.getFocusModel().getFocusedItem();
            mainApp.showPlanOverview(tpa.getEntity(), null, null, tra.getEntity());
    }
}
