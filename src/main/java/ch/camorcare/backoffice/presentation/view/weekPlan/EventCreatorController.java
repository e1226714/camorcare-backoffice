package ch.camorcare.backoffice.presentation.view.weekPlan;

import ch.camorcare.backoffice.adapter.TherapistAdapter;
import ch.camorcare.backoffice.adapter.TherapyAdapter;
import ch.camorcare.backoffice.adapter.TherapyEventAdapter;
import ch.camorcare.backoffice.adapter.TherapyPlanAdapter;
import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.entities.TherapyEvent;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.presentation.MainApp;
import ch.camorcare.backoffice.service.TherapyScheduleService;
import ch.camorcare.backoffice.service.exception.PlanningException;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.PatientManager;
import ch.camorcare.backoffice.service.impl.TherapistConstraintManager;
import ch.camorcare.backoffice.service.impl.TherapistManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Controller for event creator
 */
public class EventCreatorController implements Initializable{

    @FXML
    private TableView<TherapyEventAdapter> therapyEventTable;

    @FXML
    private TableColumn<TherapyEventAdapter, Number> participantColumn;

    @FXML
    private TableColumn<TherapyEventAdapter, String> therapistColumn;

    @FXML
    private TableColumn<TherapyEventAdapter, String> therapyColumn;

    @FXML
    private TextField filterField;

    @FXML
    private ChoiceBox<TherapistAdapter> therapistChoiceBox;

    @FXML
    private TableView<TherapyAdapter> patientTable;

    @FXML
    private TableColumn<TherapyAdapter, String> patientColumn;

    @FXML
    private ChoiceBox<TherapyAdapter> patientChoiceBox;

    private TherapyEventAdapter therapyEventAdapter;

    /**
     *
     */
    private MainApp mainApp;

    /**
     *
     */
    private TherapyManager therapyManager;

    /**
     *
     */
    private TherapistManager therapistManager;

    /**
     *
     */
    private TherapistConstraintManager therapistConstraintManager;

    /**
     *
     */
    private PatientManager patientManager;

    private TherapyScheduleService therapyScheduleService;

    private Stage stage;

    /**
     *
     */
    private TherapyPlanAdapter therapyPlanAdapter;

    /**
     * List containing all Events to show
     */
    private ObservableList<TherapyEventAdapter> therapyEvents = FXCollections.observableArrayList();

    /**
     * List containing all therapists to show
     */
    private ObservableList<TherapistAdapter> therapists = FXCollections.observableArrayList();

    /**
     * List containing all patients to show in patients table
     */
    private ObservableList<TherapyAdapter> therapiesForPatientsTable = FXCollections.observableArrayList();

    /**
     * List containing all patients to show in patient choice box
     */
    private ObservableList<TherapyAdapter> therapiesForPatientsChoiceBox = FXCollections.observableArrayList();

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(EventCreatorController.class.getName());

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // initializes with setTherapyPlanAdapter();

    }

    private void InitTherapyEventTable() {
        try {
            therapyEvents.clear();
            List<TherapyEvent> tEvents = therapyManager.findAllTherapyEventsByTherapyPlan(therapyPlanAdapter.getEntity());

            for (TherapyEvent te : tEvents){
                if(te.getTherapyMode().equals(TherapyMode.GROUP)) {
                    te.setTherapies(therapyManager.getParticipants(te));
                    TherapyEventAdapter newTherapyEventAdapter = new TherapyEventAdapter(te);
                    therapyEvents.add(newTherapyEventAdapter);
                    if(therapyEventAdapter!=null && te.getId().equals(therapyEventAdapter.getEntity().getId())){
                        therapyEventAdapter = newTherapyEventAdapter;
                    }
                }
            }

            therapyEventTable.setItems(therapyEvents);
            if(therapyEventAdapter != null)
                therapyEventTable.getSelectionModel().select(therapyEventAdapter);
            therapyEventAdapter = null;

            // Wrap the ObservableList in a FilteredList (initially display all
            // data).
            FilteredList<TherapyEventAdapter> filteredData = new FilteredList<>(therapyEvents,
                    p -> true);

            // Set the filter Predicate whenever the filter changes.
            filterField
                    .textProperty()
                    .addListener(
                            (observable, oldValue, newValue) -> {
                                filteredData.setPredicate(event -> {
                                    // If filter text is empty, display all patients.
                                    if (newValue == null || newValue.isEmpty()) {
                                        return true;
                                    }

                                    // Compare name and participant number of every
                                    // therapy event with filter text.
                                    String lowerCaseFilter = newValue.toLowerCase();

                                    if (event.getTherapyName().get().toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;

                                    } else if (Integer.toString(event.getParticipantNumber().get()).toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;
                                    }
                                    return false; // Does not match.
                                });
                            });
            // Wrap the FilteredList in a SortedList.
            SortedList<TherapyEventAdapter> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(therapyEventTable.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            therapyEventTable.setItems(sortedData);

        } catch (ServiceException | ValidationException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Eventübersicht")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    private void showTherapyEventDetails(TherapyEventAdapter therapyEventAdapter) {

       /*
            therapistChoiceBox.getItems().clear();
            patientTable.getItems().clear();
            patientChoiceBox.getItems().clear();
       */

        if (therapyEventAdapter != null) {
            try {

                //fill choice box for therapists
                therapists.clear();
                List<Therapist> therapistsList = therapistConstraintManager.findAllTherapistsByTherapyType(therapyEventAdapter.getEntity().getTherapyType());

                if (therapistsList.size() > 0) {
                    Therapist th = null;
                    TherapistAdapter therapistAdapter = null;
                    if (therapyEventAdapter.getEntity().getTherapist() != null) {
                        th = therapyEventAdapter.getEntity().getTherapist();
                        therapistAdapter = new TherapistAdapter(th);
                        therapists.add(therapistAdapter);
                    }


                    for (Therapist t : therapistsList) {
                        if(!t.equals(th))
                            therapists.add(new TherapistAdapter(t));
                    }

                    therapistChoiceBox.setItems(therapists);

                    if(therapistAdapter != null)
                        therapistChoiceBox.setValue(therapistAdapter);

                    // listener to save changes in choicebox automatically ...dont use now, heeds changes in class
                    /*
                    therapistChoiceBox
                            .getSelectionModel()
                            .selectedItemProperty()
                            .addListener(
                                (observable, oldValue, newValue) -> handleSave());
                                */


                    // fill table for patients
                    patientColumn.setCellValueFactory(cellData -> cellData.getValue().getPatientAdapter().getNameProperty());

                    therapiesForPatientsTable.clear();

                    List<Therapy> therapyList = therapyManager.getParticipants(therapyEventAdapter.getEntity());
                    therapyEventAdapter.getEntity().setTherapies(therapyList);

                    for (Therapy t : therapyList) {
                        therapiesForPatientsTable.add(new TherapyAdapter(t));
                    }

                    patientTable.setItems(therapiesForPatientsTable);

                    // fill choice box for patients
                    therapiesForPatientsChoiceBox.clear();

                    List<Therapy> notSelectedTherapies = therapyManager.findAllTherapiesByTherapyPlan(therapyPlanAdapter.getEntity());

                    for (Therapy t : notSelectedTherapies) {
                        therapiesForPatientsChoiceBox.add(new TherapyAdapter(t));
                    }

                    patientChoiceBox.setItems(therapiesForPatientsChoiceBox);
                    patientChoiceBox.setValue(therapiesForPatientsChoiceBox.get(0));

                }
            } catch(ServiceException | ValidationException e){
                Dialogs.create().title("Fehler beim Initialisieren der Therapieeventdetails")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    public void setMainApp(MainApp mainApp) { this.mainApp = mainApp; }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setTherapistManager(TherapistManager therapistManager) { this.therapistManager = therapistManager; }

    public void setPatientManager(PatientManager patientManager) { this.patientManager = patientManager; }

    public void setTherapistConstraintManager(TherapistConstraintManager therapistConstraintManager) {
        this.therapistConstraintManager = therapistConstraintManager;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        this.stage.setMinWidth(1024);
        this.stage.setMinHeight(768);
    }

    public void setTherapyPlanAdapter(TherapyPlanAdapter therapyPlanAdapter) {

        if(therapyPlanAdapter == null)
            return;

        this.therapyPlanAdapter = therapyPlanAdapter;

        // Initialize the patient table with the three columns.
        therapyColumn.setCellValueFactory(cellData -> cellData.getValue().getTherapyName());
        therapistColumn.setCellValueFactory(cellData -> cellData.getValue().getTherapistProperty().getNameProperty());
        participantColumn.setCellValueFactory(cellData -> cellData.getValue().getParticipantNumber());

        therapistColumn.setCellFactory(column -> {
            return new TableCell<TherapyEventAdapter, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    }
                    else {
                        if (item == " ") {
                            setText("Nicht zugewiesen");
                        } else {
                            setText(item);
                            }
                    }
                }
            };
        });

        // Clear patient details.
        //showTherapyEventDetails(null);

        // Listen for selection changes and show the patient details when changed.
        therapyEventTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showTherapyEventDetails(newValue));

        // Fills the therapy event table with data
        InitTherapyEventTable();

    }

    public void setTherapyScheduleService(TherapyScheduleService therapyScheduleService) {
        this.therapyScheduleService = therapyScheduleService;
    }

    @FXML
    public void handleCreateEvent(){

        mainApp.showCreateGroupTherapyEvent(therapyPlanAdapter);
        InitTherapyEventTable();

    }

    @FXML
    public void handleDeleteEvent() {

        Action response = Dialogs.create()
                .owner(stage)
                .title("Bestätigungsdialog")
                .masthead("Therapie löschen")
                .message("Möchten Sie diese Therapie wirklich löschen?")
                .actions(Dialog.ACTION_YES, Dialog.ACTION_NO)
                .showConfirm();

        if (response == Dialog.ACTION_YES) {

            TherapyEvent te = therapyEventTable.getFocusModel().getFocusedItem().getEntity();
            try {
                therapyManager.delete(te);
            } catch (ServiceException e) {
                Dialogs.create().title("Fehler beim Löschen eines Therapieevents")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }

            InitTherapyEventTable();

            Dialogs.create()
                    .title("Informationsdialog")
                    .masthead(null)
                    .message("Therapie erfolgreich gelöscht!")
                    .showInformation();
        } else {
            // ... user chose CANCEL, or closed the dialog
        }

    }

    @FXML
    public void handleEventCreationAutomatically() {
        log.debug("create TherapyEvents automatically");
        try {
            therapyScheduleService.deleteAllEventsWithParticipants(therapyPlanAdapter.getEntity());
            Map<TherapyEvent, List<Therapy>> eventsWithParticipants= therapyScheduleService.createEventsForWeek(therapyPlanAdapter.getEntity());
            therapyScheduleService.createAllEventsWithParticipants(eventsWithParticipants);
        } catch (ValidationException | ServiceException |PlanningException e) {
            Dialogs.create().title("Fehler beim automatischen Erzeugen der Therapieevents")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
        InitTherapyEventTable();

        Dialogs.create().title("Informationsdialog")
                .masthead(null)
                .message("Die Therapien wurden erfolgreich erstellt")
                .showInformation();
    }

    @FXML
    public void handleAddPatient(){

        TherapyEventAdapter selectedTherapyEventAdapter = therapyEventTable.getFocusModel().getFocusedItem();
        TherapyEvent selectedTherapyEvent = selectedTherapyEventAdapter.getEntity();
        Therapy t = patientChoiceBox.getSelectionModel().getSelectedItem().getEntity();

        try {
            therapyManager.createParticipation(selectedTherapyEvent, t);
           // therapyManager.update(selectedTherapyEvent);

        } catch (ServiceException | ValidationException e) {
            Dialogs.create().title("Fehler beim Hinzufügen eines Patienten")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        //showTherapyEventDetails(selectedTherapyEventAdapter);
        therapyEventAdapter = selectedTherapyEventAdapter;
        InitTherapyEventTable();
        therapyEventTable.getSelectionModel().select(selectedTherapyEventAdapter);

    }

    @FXML
    public void handleRemovePatient() {

        Action response = Dialogs.create()
                .owner(stage)
                .title("Bestätigungsdialog")
                .masthead("Patient entfernen")
                .message("Möchten Sie diesen Patienten von der Therapie entfernen?")
                .actions(Dialog.ACTION_YES, Dialog.ACTION_NO)
                .showConfirm();

        if (response == Dialog.ACTION_YES) {

            TherapyEventAdapter selectedTherapyEventAdapter = therapyEventTable.getFocusModel().getFocusedItem();
            TherapyEvent selectedTherapyEvent = selectedTherapyEventAdapter.getEntity();

            Therapy selectedTherapy = patientTable.getFocusModel().getFocusedItem().getEntity();

            try {
                therapyManager.deleteParticipation(selectedTherapyEvent, selectedTherapy);
            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Entfernen eines Patienten")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
                return;
            }

            //showTherapyEventDetails(selectedTherapyEventAdapter);
            therapyEventAdapter = selectedTherapyEventAdapter;
            InitTherapyEventTable();
            therapyEventTable.getSelectionModel().select(selectedTherapyEventAdapter);

            Dialogs.create().title("Informationsdialog")
                    .masthead(null)
                    .message("Patient erfolgreich entfernt!")
                    .showInformation();

        } else {
            // ... user chose CANCEL, or closed the dialog
        }

    }

    @FXML
    public void handleSave() {
        TherapistAdapter therapist = therapistChoiceBox.getSelectionModel().getSelectedItem();
        TherapyEventAdapter selectedTherapyEventAdapter = therapyEventTable.getFocusModel().getFocusedItem();
        TherapyEvent selectedTE = selectedTherapyEventAdapter.getEntity();
        try {
            selectedTE.setTherapist(therapist.getEntity());
            therapyManager.update(selectedTE);
            int index = therapyEvents.indexOf(selectedTherapyEventAdapter);
            selectedTherapyEventAdapter.setTherapist(therapist);
            therapyEvents.set(index, selectedTherapyEventAdapter);
            therapyEventTable.getFocusModel().focus(index);
        } catch (ServiceException | ValidationException e) {
            Dialogs.create().title("Fehler beim Speichern des Therapeuten")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        therapyEventAdapter = selectedTherapyEventAdapter;
        //InitTherapyEventTable();

        Dialogs.create().title("Informationsdialog")
                .masthead(null)
                .message("Therapeut erfolgreich geändert!")
                .showInformation();

    }

    @FXML
    public void handleGoToPlanOverview() {
        mainApp.showPlanOverview(therapyPlanAdapter.getEntity(), null, null, null);
        stage.close();
    }
}
