package ch.camorcare.backoffice.presentation.view.patient;

import ch.camorcare.backoffice.adapter.AddressAdapter;
import ch.camorcare.backoffice.adapter.PatientAdapter;
import ch.camorcare.backoffice.entities.Address;
import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;
import org.joda.time.DateTime;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the Patient overview
 */
@SuppressWarnings("deprecation")
public class PatientUpdateDialogController implements Initializable {

    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField socialSecurityNumberField;
    @FXML
    private TextField streetField;
    @FXML
    private TextField zipField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField phoneField;
    @FXML
    private TextField emailField;
    @FXML
    private TextField allergyField;
    @FXML
    private TextField commentField;

    @FXML
    private RadioButton maleRadio;
    @FXML
    private RadioButton femaleRadio;

    @FXML
    private DatePicker birthdayPicker;

    private Stage dialogStage;
    private PatientAdapter patient;
    private boolean okClicked = false;
    private EntityValidator<Patient> validator;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(400);
    }

    public void setValidator(EntityValidator<Patient> validator) {
        this.validator = validator;
    }

    /**
     * Sets the patient to be edited in the dialog.
     *
     * @param patient
     */
    public void setPatient(PatientAdapter patient) {
        this.patient = patient;

        firstNameField.setText(patient.getFirstNameProperty().getValue());
        lastNameField.setText(patient.getLastNameProperty().getValue());
        if (patient.getGenderProperty().getValue() == null || patient.getGenderProperty().getValue() == Gender.MALE) {
            maleRadio.setSelected(true);
        } else {
            femaleRadio.setSelected(true);
        }
        birthdayPicker.setValue(DateTimeConverter.dateTimeToLocalDate(patient.getBirthdayProperty().getValue()));
        birthdayPicker.setPromptText("dd.mm.yyyy");
        socialSecurityNumberField.setText(patient.getSocialSecurityNumberProperty().getValue());
        AddressAdapter address = patient.getAddressAdapter();
        if (address.getEntity() != null) {
            streetField.setText(address.getStreetProperty().getValue());
            zipField.setText(address.getZipProperty().getValue());
            cityField.setText(address.getCityProperty().getValue());
        }
        phoneField.setText(patient.getPhoneProperty().getValue());
        emailField.setText(patient.getEmailProperty().getValue());
        /*
        String allergies = "";
        List<Allergy> allergyList = patient.getAllergyList();
        if (allergyList != null && allergyList.size() >= 1) {
            for (Allergy a : allergyList) {
                allergies += a.getName() + ", ";
            }
            allergies = allergies.substring(0, allergies.length() - 2);
        }
        allergyField.setText(allergies);
        allergyField.setPromptText("Allergie1, Allergie2, Allergie3");
        */

        /*
        PatientComment comment = patient.getPatientComment();
        if (comment != null) {
            commentField.setText(comment.getContent());
        }
        */
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            patient.setFirstName(firstNameField.getText());
            patient.setLastName(lastNameField.getText());
            if (maleRadio.isSelected()) {
                patient.setGender(Gender.MALE);
            } else {
                patient.setGender(Gender.FEMALE);
            }
            patient.setBirthday(DateTimeConverter.localDateToDateTime(birthdayPicker.getValue()));
            patient.setSocialSecurityNumber(socialSecurityNumberField.getText());
            AddressAdapter address = patient.getAddressAdapter();
            if (address.getEntity() == null) {
                patient.getEntity().setAddress(new Address());
                address = new AddressAdapter(patient.getEntity().getAddress());
            }
            address.setStreet(streetField.getText());
            address.setZip(zipField.getText());
            address.setCity(cityField.getText());
            //ToDo: include country code in ui
            address.setCountryCode("CH");
            patient.setAddress(address.getEntity());
            patient.setPhone(phoneField.getText());
            patient.setEmail(emailField.getText());

            /*
            String allergyText = allergyField.getText();
            if (!allergyText.isEmpty()) {
                String[] allergies = allergyField.getText().split(", ");
                List<Allergy> allergyList = new ArrayList<Allergy>();
                for (String s : allergies) {
                    Allergy pa = new Allergy();
                    pa.setName(s);
                    allergyList.add(pa);
                }
                patient.setAllergyList(allergyList);
            } else {
                patient.setAllergyList(null);
            }
            */

            /*
            String commentText = commentField.getText();
            if (!commentText.isEmpty()) {
                PatientComment comment = patient.getPatientComment();
                if (comment == null) {
                    comment = new PatientComment();
                }
                comment.setContent(commentText);
                patient.setPatientComment(comment);
            }
            */

            okClicked = true;
            dialogStage.close();

            /*try {
                validator.validate(patient.getEntity());
                okClicked = true;
                dialogStage.close();
            } catch (ValidationException e) {
                // Show the error message.
                Dialogs.create()
                        .title("Ungültige Eingabe")
                        .masthead("Bitte korrigieren Sie Ihre Eingabe.")
                        .message(e.getMessage())
                        .showError();
            }*/
        }
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (firstNameField.getText() == null || firstNameField.getText().length() == 0) {
            errorMessage += "Ungültiger Vorname!\n";
        }

        if (lastNameField.getText() == null || lastNameField.getText().length() == 0) {
            errorMessage += "Ungültiger Nachname!\n";
        }

        if (birthdayPicker.getValue() == null) {
            errorMessage += "Ungültiges Geburtsdatum!\n";
        } else {
            try {
                new DateTime(birthdayPicker.getValue().getYear(), birthdayPicker.getValue().getMonthValue(), birthdayPicker.getValue().getDayOfMonth(), 0, 0);
            } catch (Exception e) {
                errorMessage += "Ungültiges Geburtsdatum. Bitte verwenden Sie das Format: dd.mm.yyyy!\n";
            }
        }


        if (socialSecurityNumberField.getText() == null || socialSecurityNumberField.getText().length() == 0) {
            errorMessage += "Ungültige Versicherungsnummer!\n";
        }

        if (streetField.getText() == null || streetField.getText().length() == 0) {
            errorMessage += "Ungültige Straße!\n";
        }

        if (zipField.getText() == null || zipField.getText().length() == 0) {
            errorMessage += "Ungültige Postleitzahl!\n";
        }

        if (cityField.getText() == null || cityField.getText().length() == 0) {
            errorMessage += "Ungültiger Ort!\n";
        }

        if (phoneField.getText() == null || phoneField.getText().length() == 0) {
            errorMessage += "Ungültige Telefonnummer!\n";
        }

        if (emailField.getText() == null || emailField.getText().length() == 0 || !emailField.getText().contains("@")) {
            errorMessage += "Ungültige E-Mail-Adresse!\n";
        } 


        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Bitte korrigieren Sie Ihre Eingabe.")
                    .message(errorMessage)
                    .showError();
            return false;
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
}
