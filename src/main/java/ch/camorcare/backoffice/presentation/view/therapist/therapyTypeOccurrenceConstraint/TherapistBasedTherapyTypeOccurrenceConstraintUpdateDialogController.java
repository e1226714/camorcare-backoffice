package ch.camorcare.backoffice.presentation.view.therapist.therapyTypeOccurrenceConstraint;

import ch.camorcare.backoffice.adapter.PatientBasedTherapyTypeOccurrenceConstraintAdapter;
import ch.camorcare.backoffice.adapter.TherapistAdapter;
import ch.camorcare.backoffice.adapter.TherapistBasedTherapyTypeOccurrenceConstraintAdapter;
import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.TherapistConstraintManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.stage.Stage;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the TherapistBasedTherapyTypeOccurrenceConstraint update dialog window
 */
public class TherapistBasedTherapyTypeOccurrenceConstraintUpdateDialogController implements Initializable {

    @FXML
    private TableView<TherapistBasedTherapyTypeOccurrenceConstraintAdapter> therapistTherapyTypeTable;
    private ObservableList<TherapistBasedTherapyTypeOccurrenceConstraintAdapter> therapistTherapyTypeTableData = FXCollections.observableArrayList();
    private ObservableList<TherapyType> therapyTypes = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, String> therapistTherapyTypeTableTypeColumn;

    @FXML
    private TableColumn<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, TherapyMode> therapistTherapyTypeTableModeColumn;

    @FXML
    private TableColumn<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, Number> therapistTherapyTypeTableMaxPerDayColumn;

    @FXML
    private ComboBox<TherapyType> therapyTypeCombo;

    @FXML
    private ComboBox<TherapyMode> therapyModeCombo;

    @FXML
    private ComboBox<Number> maxPerDayCombo;

    private Stage dialogStage;

    private TherapistAdapter therapist;

    private TherapyManager therapyManager;

    private TherapistConstraintManager therapistConstraintManager;

    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        therapistTherapyTypeTableTypeColumn.setCellValueFactory(cellData -> cellData.getValue().getTherapyTypeAdapter().getNameProperty());
        therapistTherapyTypeTableModeColumn.setCellValueFactory(cellData -> cellData.getValue().getTherapyModeProperty());
        therapistTherapyTypeTableMaxPerDayColumn.setCellValueFactory(cellData -> cellData.getValue().getMaxOccurrencePerDayProperty());
        //therapistTherapyTypeTableMaxPerDayColumn.setCellFactory(TextFieldTableCell.<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, Number>forTableColumn(new NumberStringConverter()));

        ObservableList<Number> timeSlots = FXCollections.observableArrayList();
        timeSlots.add(1);
        timeSlots.add(2);
        timeSlots.add(3);
        timeSlots.add(4);
        timeSlots.add(5);
        timeSlots.add(6);
        therapistTherapyTypeTableMaxPerDayColumn.setCellFactory(ComboBoxTableCell.forTableColumn(timeSlots));
        maxPerDayCombo.setItems(timeSlots);
        maxPerDayCombo.getSelectionModel().select(5);

        therapistTherapyTypeTableMaxPerDayColumn.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, Number>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, Number> t) {
                        ((TherapistBasedTherapyTypeOccurrenceConstraintAdapter) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setMaxOccurrencePerDayProperty((Integer) t.getNewValue());
                    }
                }
        );

        ObservableList<TherapyMode> therapyModes = FXCollections.observableArrayList();
        therapyModes.add(TherapyMode.SINGLE);
        therapyModes.add(TherapyMode.GROUP);

        therapistTherapyTypeTableModeColumn.setCellFactory(ComboBoxTableCell.forTableColumn(therapyModes));
        therapyModeCombo.setItems(therapyModes);
        therapyModeCombo.getSelectionModel().select(0);

        therapistTherapyTypeTableModeColumn.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, TherapyMode>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, TherapyMode> t) {
                        ((TherapistBasedTherapyTypeOccurrenceConstraintAdapter) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setTherapyModeProperty(t.getNewValue());
                    }
                }
        );

        therapyTypeCombo.setButtonCell(new ListCell<TherapyType>() {
            @Override
            protected void updateItem(TherapyType item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText("");
                } else {
                    setText(item.getName());
                }
            }
        });

        therapyTypeCombo.setCellFactory(cell -> {
            return new ComboBoxListCell<TherapyType>() {
                @Override
                public void updateItem(TherapyType item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(item.getName());
                    }
                }
            };
        });

        therapyTypeCombo.valueProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
                if(newValue != null) {
                    therapyModeCombo.disableProperty().setValue(false);
                    ObservableList<TherapyMode> modes = FXCollections.observableArrayList();

                    if(therapyTypeCombo.getValue().isSuitableForIndividuals()) {
                        modes.add(TherapyMode.SINGLE);
                    }

                    if(therapyTypeCombo.getValue().isSuitableForGroups()) {
                        modes.add(TherapyMode.GROUP);
                    }

                    therapyModeCombo.getItems().clear();
                    therapyModeCombo.setItems(modes);

                    if(therapyModeCombo.getItems().size() > 0) {
                        therapyModeCombo.getSelectionModel().select(0);
                        therapyModeCombo.getSelectionModel().select(0);
                        maxPerDayCombo.getSelectionModel().select(5);
                    }

                } else {
                    therapyModeCombo.getSelectionModel().clearSelection();
                    therapyModeCombo.disableProperty().setValue(true);
                    maxPerDayCombo.getSelectionModel().clearSelection();
                    maxPerDayCombo.disableProperty().setValue(true);
                }
            }
        });

        try {
            List<TherapyType> allTherapyTypes = therapyManager.findAllTherapyTypes();

            for (TherapyType t : allTherapyTypes) {
                if(t.isSuitableForGroups() || t.isSuitableForIndividuals()) {
                    therapyTypes.add(t);
                }
            }

            therapyTypeCombo.setItems(therapyTypes);
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Arbeitszeiteinteilung")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        therapyTypeCombo.getSelectionModel().select(0);
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(350);
    }

    /**
     * Sets the therapist to be edited in the dialog.
     *
     * @param therapist
     */
    public void setTherapist(TherapistAdapter therapist) {
        this.therapist = therapist;

        try {
            therapistTherapyTypeTableData.clear();

            for (TherapistBasedTherapyTypeOccurrenceConstraint constraint : therapistConstraintManager.findAllTherapistBasedTherapyTypeOccurrenceConstraintsByTherapist(therapist.getEntity())) {
                constraint.setTherapyType(therapyManager.findTherapyTypeById(constraint.getTherapyType()));
                therapistTherapyTypeTableData.add(new TherapistBasedTherapyTypeOccurrenceConstraintAdapter(constraint));
            }

            therapistTherapyTypeTable.setItems(therapistTherapyTypeTableData);
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Setzen des Therapeuten")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleAdd() {
        boolean exists = false;

        TherapistBasedTherapyTypeOccurrenceConstraint constraint = new TherapistBasedTherapyTypeOccurrenceConstraint();
        constraint.setTherapyType(therapyTypeCombo.getSelectionModel().getSelectedItem());
        constraint.setTherapist(this.therapist.getEntity());
        constraint.setTherapyMode(therapyModeCombo.getSelectionModel().getSelectedItem());
        constraint.setMaxOccurrencePerDay((Integer)maxPerDayCombo.getSelectionModel().getSelectedItem());

        for(TherapistBasedTherapyTypeOccurrenceConstraintAdapter adapter : therapistTherapyTypeTableData) {
            if(constraint.getTherapyType().getId().equals(adapter.getEntity().getTherapyType().getId())) {
                if(constraint.getTherapyMode().equals(adapter.getEntity().getTherapyMode())) {
                    exists = true;
                    break;
                }
            }
        }

        if(exists) {
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Diese Therapieform wurde bereits zugeteilt")
                    .message("Bitte überprüfen Sie Ihre Auswahl.")
                    .showWarning();
        } else {
            therapistTherapyTypeTableData.add(new TherapistBasedTherapyTypeOccurrenceConstraintAdapter(constraint));
        }
    }

    @FXML
    private void handleDelete() {
        int selectedIndex = therapistTherapyTypeTable.getSelectionModel()
                .getSelectedIndex();
        if (selectedIndex >= 0) {
            Action response = Dialogs.create()
                    .title("Bestätigungsdialog")
                    .masthead("Eintrag löschen")
                    .message("Möchten Sie diesen Eintrag wirklich löschen?")
                    .actions(Dialog.ACTION_YES, Dialog.ACTION_NO)
                    .showConfirm();
            if (response == Dialog.ACTION_YES) {
                try {
                    if (therapistTherapyTypeTable.getSelectionModel().getSelectedItem().getEntity().getId() != null) {
                        therapistConstraintManager.delete(therapistTherapyTypeTable.getSelectionModel().getSelectedItem().getEntity());
                    }
                    therapistTherapyTypeTableData.remove(selectedIndex);

                    Dialogs.create()
                            .title("Informationsdialog")
                            .masthead(null)
                            .message("Eintrag gelöscht!")
                            .showInformation();
                } catch (ServiceException e) {
                    Dialogs.create().title("Fehler beim Löschen von Arbeitszeiten")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            Dialogs.create()
                    .title("Ungültige Auswahl")
                    .masthead("Kein Eintrag ausgewählt")
                    .message("Bitte wählen Sie einen Eintrag aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        try {
            List<TherapistBasedTherapyTypeOccurrenceConstraint> constraints = new ArrayList<>();
            for (TherapistBasedTherapyTypeOccurrenceConstraintAdapter constraint : therapistTherapyTypeTableData) {
                constraints.add(constraint.getEntity());
            }
            therapistConstraintManager.createTherapyTypeOccurrenceConstraints(constraints, therapist.getEntity());
        } catch(ServiceException | ValidationException e){
            Dialogs.create().title("Fehler beim Schreiben von Arbeitszeiten")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
            }
            dialogStage.close();
        }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setTherapistConstraintManager(TherapistConstraintManager therapistConstraintManager) {
        this.therapistConstraintManager = therapistConstraintManager;
    }
}


