package ch.camorcare.backoffice.presentation.view.therapyRoom;

import ch.camorcare.backoffice.adapter.TherapyRoomAdapter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the TherapyRoom update dialog window
 */
@SuppressWarnings("deprecation")
public class TherapyRoomUpdateDialogController implements Initializable {

    @FXML
    private TextField nameField;
    @FXML
    private TextField capacityField;

    private Stage dialogStage;
    private TherapyRoomAdapter therapyRoom;
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(125);
    }

    /**
     * Sets the therapy room to be edited in the dialog.
     *
     * @param therapyRoom
     */
    public void setTherapyRoom(TherapyRoomAdapter therapyRoom) {
        this.therapyRoom = therapyRoom;

        nameField.setText(therapyRoom.getNameProperty().getValue());
        capacityField.setText(Integer.toString(therapyRoom.getCapacityProperty().getValue()));
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            therapyRoom.setName(nameField.getText());
            therapyRoom.setCapacity(Integer.parseInt(capacityField.getText()));

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (nameField.getText() == null || nameField.getText().length() == 0) {
            errorMessage += "Ungültiger Name!\n";
        }

        if (capacityField.getText() == null || capacityField.getText().length() == 0) {
            errorMessage += "Ungültige Kapazität!\n";
        } else {
            try {
                Integer.parseInt(capacityField.getText());
                if (Integer.parseInt(capacityField.getText()) <= 0) {
                    errorMessage += "Ungültige Kapazität (muss größer als 0 sein)!\n";
                }
            } catch (NumberFormatException e) {
                errorMessage += "Ungültige Kapazität (nur ganze Zahlen sind erlaubt)!\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Bitte korrigieren Sie Ihre Eingabe.")
                    .message(errorMessage)
                    .showError();
            return false;
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
}
