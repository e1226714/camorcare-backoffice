package ch.camorcare.backoffice.presentation.view.therapyType;

import ch.camorcare.backoffice.adapter.TherapyTypeAdapter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the TherapyType update dialog window
 */
@SuppressWarnings("deprecation")
public class TherapyTypeUpdateDialogController implements Initializable {

    @FXML
    private TextField fieldName;

    @FXML
    private CheckBox fieldIsIndividual;

    @FXML
    private CheckBox fieldIsGroup;

    @FXML
    private TextField fieldGroupSizeMin;

    @FXML
    private TextField fieldGroupSizeMax;

    @FXML
    private Label labelGroupSizeDash;

    @FXML
    private Label labelGroupSizePersons;

    @FXML
    private ObservableList<String> durations = FXCollections.observableArrayList();

    /**
     *
     */
    private Stage dialogStage;

    /**
     *
     */
    private TherapyTypeAdapter therapyType;

    /**
     *
     */
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(330);
    }

    /**
     * Sets the therapy type to be edited in the dialog.
     *
     * @param therapyType
     */
    public void setTherapyType(TherapyTypeAdapter therapyType) {
        this.therapyType = therapyType;

        /**
         * @TODO use map between minutes (45) and description ("45 Minuten")
         */
        durations.add("45 Minuten");
        durations.add("90 Minuten");

        fieldName.setText(therapyType.getNameProperty().getValue());
        fieldIsIndividual.setSelected(therapyType.getEntity().isSuitableForIndividuals());

        fieldIsGroup.setSelected(therapyType.getEntity().isSuitableForGroups());
        fieldGroupSizeMin.setText(Integer.toString(therapyType.getEntity().getMinGroupSize()));
        fieldGroupSizeMax.setText(Integer.toString(therapyType.getEntity().getMaxGroupSize()));

        updateFieldIsIndividual();
        updateFieldIsGroup();
    }

    @FXML
    public void updateFieldIsIndividual() {
        /*
        if (!fieldIsIndividual.isSelected()) {
            fieldIsIndividualTherapistFixed.setDisable(true);
            fieldIndividualDuration.setDisable(true);
        } else {
            fieldIsIndividualTherapistFixed.setDisable(false);
            fieldIndividualDuration.setDisable(false);
        }
        */
    }

    @FXML
    public void updateFieldIsGroup() {
        if (!fieldIsGroup.isSelected()) {
            //fieldIsGroupTherapistFixed.setDisable(true);
            //fieldGroupDuration.setDisable(true);
            fieldGroupSizeMin.setDisable(true);
            fieldGroupSizeMax.setDisable(true);
            labelGroupSizePersons.setStyle("-fx-text-fill: #b8b8b8");
            labelGroupSizeDash.setStyle("-fx-text-fill: #b8b8b8");
        } else {
            //fieldIsGroupTherapistFixed.setDisable(false);
            //fieldGroupDuration.setDisable(false);
            fieldGroupSizeMin.setDisable(false);
            fieldGroupSizeMax.setDisable(false);
            labelGroupSizePersons.setStyle("-fx-text-fill: #000000");
            labelGroupSizeDash.setStyle("-fx-text-fill: #000000");
        }
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            therapyType.setName(fieldName.getText());
            therapyType.setIndividualTherapy(fieldIsIndividual.isSelected());
            therapyType.setGroupTherapy(fieldIsGroup.isSelected());
            therapyType.getEntity().setMinGroupSize(Integer.parseInt(fieldGroupSizeMin.getText()));
            therapyType.getEntity().setMaxGroupSize(Integer.parseInt(fieldGroupSizeMax.getText()));

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (fieldName.getText() == null || fieldName.getText().length() == 0) {
            errorMessage += "Ungültiger Name!\n";
        }

        if (fieldIsGroup.isSelected() && (fieldGroupSizeMin.getText() == null || fieldGroupSizeMin.getText().length() == 0 || fieldGroupSizeMax.getText() == null || fieldGroupSizeMax.getText().length() == 0)) {
            errorMessage += "Ungültige Gruppengröße!\n";
        } else if (fieldIsGroup.isSelected()) {
            boolean errorFlag = false;
            try {
                Integer.parseInt(fieldGroupSizeMin.getText());
                if (Integer.parseInt(fieldGroupSizeMin.getText()) <= 0) {
                    errorMessage += "Ungültige Gruppengröße (min.) - (muss größer als 0 sein)!\n";
                }
            } catch (NumberFormatException e) {
                errorMessage += "Ungültige Gruppengröße (min.) - (nur ganze Zahlen sind erlaubt)!\n";
                errorFlag = true;
            }
            try {
                Integer.parseInt(fieldGroupSizeMax.getText());
                if (Integer.parseInt(fieldGroupSizeMax.getText()) <= 0) {
                    errorMessage += "Ungültige Gruppengröße (max.) - (muss größer als 0 sein)!\n";
                }
            } catch (NumberFormatException e) {
                errorMessage += "Ungültige Gruppengröße (max.) - (nur ganze Zahlen sind erlaubt)!\n";
                errorFlag = true;
            }
            if (errorFlag == false && Integer.parseInt(fieldGroupSizeMin.getText()) > Integer.parseInt(fieldGroupSizeMax.getText())) {
                errorMessage += "Gruppengröße (max.) darf nicht kleiner als Gruppengröße (min.) sein!\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Bitte korrigieren Sie Ihre Eingabe.")
                    .message(errorMessage)
                    .showError();
            return false;
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
}
