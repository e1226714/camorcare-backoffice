package ch.camorcare.backoffice.presentation.view.patient.therapistGenderConstraint;

import ch.camorcare.backoffice.adapter.PatientAdapter;
import ch.camorcare.backoffice.adapter.PatientBasedTherapistConstraintAdapter;
import ch.camorcare.backoffice.adapter.PatientBasedTherapistGenderConstraintAdapter;
import ch.camorcare.backoffice.adapter.PatientBasedTherapyTypeOccurrenceConstraintAdapter;
import ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint;
import ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint;
import ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.PatientConstraintManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.stage.Stage;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the PatientBasedTherapistGenderConstraint update dialog window
 */
public class PatientBasedTherapistGenderConstraintUpdateDialogController implements Initializable {

    @FXML
    private TableView<PatientBasedTherapistGenderConstraintAdapter> constraintTable;
    private ObservableList<PatientBasedTherapistGenderConstraintAdapter> constraintTableData = FXCollections.observableArrayList();
    private ObservableList<TherapyType> therapyTypes = FXCollections.observableArrayList();

    @FXML
    private TableColumn<PatientBasedTherapistGenderConstraintAdapter, String> constraintTableColumnTherapyType;

    @FXML
    private TableColumn<PatientBasedTherapistGenderConstraintAdapter, Gender> constraintTableColumnGender;

    @FXML
    private ComboBox<TherapyType> therapyTypeCombo;

    @FXML
    private ComboBox<Gender> genderCombo;

    private Stage dialogStage;

    private PatientAdapter patientAdapter;

    private TherapyManager therapyManager;

    private PatientConstraintManager patientConstraintManager;

    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        constraintTableColumnTherapyType.setCellValueFactory(cellData -> cellData.getValue().getTherapyTypeAdapter().getNameProperty());
        constraintTableColumnGender.setCellValueFactory(cellData -> cellData.getValue().getGenderProperty());

        ObservableList<Gender> genders = FXCollections.observableArrayList();
        genders.add(Gender.FEMALE);
        genders.add(Gender.MALE);
        genderCombo.setItems(genders);
        genderCombo.getSelectionModel().select(0);

        therapyTypeCombo.setCellFactory(cell -> {
            return new ComboBoxListCell<TherapyType>() {
                @Override
                public void updateItem(TherapyType item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    }
                    else {
                        setText(item.getName());
                    }
                }
            };
        });
    }

    private void setTherapyTypeSelection(List<TherapyType> therapyTypeSelection) {
        therapyTypes.clear();

        for (TherapyType t : therapyTypeSelection) {
            therapyTypes.add(t);
        }

        therapyTypeCombo.setItems(therapyTypes);

        if(therapyTypes.size() > 0) {
            therapyTypeCombo.getSelectionModel().select(0);
        }
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(350);
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleAdd() {
        boolean exists = false;

        PatientBasedTherapistGenderConstraint constraint = new PatientBasedTherapistGenderConstraint();
        constraint.setPatient(patientAdapter.getEntity());
        constraint.setTherapyType(therapyTypeCombo.getSelectionModel().getSelectedItem());
        constraint.setGender(genderCombo.getSelectionModel().getSelectedItem());

        for(PatientBasedTherapistGenderConstraintAdapter adapter : constraintTableData) {
            if(constraint.getTherapyType().getId().equals(adapter.getEntity().getTherapyType().getId())) {
                exists = true;
            }
        }

        if(exists) {
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Diese Therapieform wurde bereits zugeteilt")
                    .message("Bitte überprüfen Sie Ihre Auswahl.")
                    .showWarning();
        } else {
            boolean genderConstraintExists = false;

            try {
                List<PatientBasedTherapistConstraint> otherConstraints = patientConstraintManager.findAllTherapistConstraintsByPatient(patientAdapter.getEntity());

                for(PatientBasedTherapistConstraint otherConstraint : otherConstraints) {
                    if(constraint.getTherapyType().getId().equals(otherConstraint.getTherapyType().getId())) {
                        genderConstraintExists = true;
                        break;
                    }
                }

                if(genderConstraintExists) {
                    Dialogs.create()
                            .title("Ungültige Eingabe")
                            .masthead("Diese Therapieform wurde bereits zugeteilt")
                            .message("Diese Therapieform wurde bereits für eine spezifische Therapeutenpräferenz verwendet.")
                            .showWarning();
                } else {
                    constraintTableData.add(new PatientBasedTherapistGenderConstraintAdapter(constraint));
                }

            } catch (ServiceException |ValidationException e) {
                Dialogs.create()
                        .title("Fehler beim Hinzufügen einer Geschlechterpräferenz")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    @FXML
    private void handleDelete() {
        int selectedIndex = constraintTable.getSelectionModel().getSelectedIndex();

        if (selectedIndex >= 0) {
            Action response = Dialogs.create()
                    .title("Bestätigungsdialog")
                    .masthead("Eintrag löschen")
                    .message("Möchten Sie diesen Eintrag wirklich löschen?")
                    .actions(org.controlsfx.dialog.Dialog.ACTION_YES, org.controlsfx.dialog.Dialog.ACTION_NO)
                    .showConfirm();
            if (response == org.controlsfx.dialog.Dialog.ACTION_YES) {
                constraintTableData.remove(selectedIndex);

                Dialogs.create()
                        .title("Informationsdialog")
                        .masthead(null)
                        .message("Eintrag gelöscht!")
                        .showInformation();
            }
        }
        else {
            Dialogs.create()
                    .title("Ungültige Auswahl")
                    .masthead("Kein Eintrag ausgewählt")
                    .message("Bitte wählen Sie einen Eintrag aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {

        try {
            List<PatientBasedTherapyTypeOccurrenceConstraint> occurrenceConstraints = patientConstraintManager.findAllPatientBasedTherapyTypeOccurrenceConstraintsByPatient(patientAdapter.getEntity());
            List<PatientBasedTherapistGenderConstraint> constraints = new ArrayList<>();

            for (PatientBasedTherapistGenderConstraintAdapter constraint : constraintTableData) {
                constraints.add(constraint.getEntity());
            }

            patientConstraintManager.create(constraints, patientAdapter.getEntity());
        } catch (ServiceException | ValidationException e) {
            Dialogs.create()
                    .title("Fehler beim Schreiben der Geschlechterpräferenzen")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        okClicked = true;
        dialogStage.close();
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setPatientConstraintManager(PatientConstraintManager patientConstraintManager) {
        this.patientConstraintManager = patientConstraintManager;
    }

    public void setPatientAdapter(PatientAdapter patientAdapter) {
        this.patientAdapter = patientAdapter;
        this.constraintTableData.clear();

        try {
            List<TherapyType> therapyTypeSelection = new ArrayList<>();
            for(PatientBasedTherapyTypeOccurrenceConstraint oc : patientConstraintManager.findAllPatientBasedTherapyTypeOccurrenceConstraintsByPatient(patientAdapter.getEntity())) {
                therapyTypeSelection.add(oc.getTherapyType());
            }
            setTherapyTypeSelection(therapyTypeSelection);

            List<PatientBasedTherapistGenderConstraint> constraints = patientConstraintManager.findAllTherapistGenderConstraintsByPatient(patientAdapter.getEntity());
            for (PatientBasedTherapistGenderConstraint constraint : constraints) {
                this.constraintTableData.add(new PatientBasedTherapistGenderConstraintAdapter(constraint));
            }
        } catch (ServiceException | ValidationException e) {
            Dialogs.create()
                    .title("Fehler beim Initialisieren der Geschlechterpräferenzen")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        this.constraintTable.setItems(constraintTableData);
    }
}
