package ch.camorcare.backoffice.presentation.view.therapy;

import ch.camorcare.backoffice.adapter.TherapyAdapter;
import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.entities.TherapyEvent;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.pdftools.PdfBillGenerator;
import ch.camorcare.backoffice.util.pdftools.PdfPrinter;
import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class PrintBillController implements Initializable {

    @FXML
    private TextField stayPriceTextField;

    @FXML
    private TextField therapyPriceTextField;

    @FXML
    private Label therapyNameLabel;

    /**
     *
     */
    private TherapyAdapter therapyAdapter;

    private Therapy therapy;

    private Patient patient;

    /**
     *
     */
    private Stage dialogStage;



    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(250);
    }

    public void setTherapy(TherapyAdapter therapyAdapter) {
        this.therapyAdapter = therapyAdapter;
        this.therapy = therapyAdapter.getEntity();
        this.patient = therapy.getPatient();

        therapyNameLabel.setText(therapyAdapter.toString());
    }

    @FXML
    public void handleOk() {

        String addressLine1 = patient.getFirstName() + " " + patient.getLastName();
        String addressLine2 = patient.getAddress().getStreet();
        String addressLine3 = patient.getAddress().getZip() + " " + patient.getAddress().getCity();

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date d = new Date();
        String date = dateFormat.format(d);

        String price1 = stayPriceTextField.getText();
        String price2 = therapyPriceTextField.getText();

        Double stayPrice = 0D;
        Double therapyPrice = 0D;

        try {
            stayPrice = Double.parseDouble(price1);
            therapyPrice = Double.parseDouble(price2);
        } catch (NumberFormatException e) {
            Dialogs.create()
                    .owner(dialogStage)
                    .title("Information")
                    .masthead(null)
                    .message("Bitte in die Preisfelder gültige Zahlen eingeben! Nachkommastellen sind mit einem Punkt zu trennen")
                    .showInformation();
            return;
        }



        dateFormat = new SimpleDateFormat("yyyyMMdd");
        String fileDate = dateFormat.format(d);
        String outfile = System.getProperty("user.home") + "/" + fileDate + addressLine1 + ".PDF";

        try {
            PdfBillGenerator.generatePDF(addressLine1, addressLine2, addressLine3, date, stayPrice, therapyPrice, outfile);
            PdfPrinter.silentPrintPDF(outfile);
        } catch (PdfException e) {
            Dialogs.create().title("Fehler")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        Dialogs.create()
                .owner(dialogStage)
                .title("Erfolg")
                .masthead(null)
                .message("Die Rechnung wurde erfolgreich gedruckt!")
                .showInformation();
        dialogStage.close();
    }

    @FXML
    public void handleCancel() {

        Action response = Dialogs.create()
                .title("Bestätigungsdialog")
                .masthead("Drucken abbrechen")
                .message("Wollen Sie das Drucken wirklich abbrechen?")
                .actions(Dialog.ACTION_YES, Dialog.ACTION_NO)
                .showConfirm();

        if (response == Dialog.ACTION_YES) {

            dialogStage.close();

        } else {
            // ... user chose CANCEL, or closed the dialog
        }

    }
}
