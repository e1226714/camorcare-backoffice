package ch.camorcare.backoffice.presentation.view.therapist;

import ch.camorcare.backoffice.adapter.TherapistAdapter;
import ch.camorcare.backoffice.entities.type.Gender;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the Therapist update dialog window
 */
@SuppressWarnings("deprecation")
public class TherapistUpdateDialogController implements Initializable {

    @FXML
    private TextField firstNameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField emailField;

    @FXML
    private RadioButton maleRadio;

    @FXML
    private RadioButton femaleRadio;

    /**
     *
     */
    private Stage dialogStage;

    /**
     *
     */
    private TherapistAdapter therapist;

    /**
     *
     */
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(200);
    }

    /**
     * Sets the patient to be edited in the dialog.
     *
     * @param therapist
     */
    public void setTherapist(TherapistAdapter therapist) {
        this.therapist = therapist;

        firstNameField.setText(therapist.getFirstNameProperty().getValue());
        lastNameField.setText(therapist.getLastNameProperty().getValue());
        emailField.setText(therapist.getEmailProperty().getValue());

        if (therapist.getEntity().getGender() == null || therapist.getEntity().getGender() == Gender.MALE) {
            maleRadio.setSelected(true);
        } else {
            femaleRadio.setSelected(true);
        }
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            therapist.setFirstName(firstNameField.getText());
            therapist.setLastName(lastNameField.getText());
            therapist.setEmail(emailField.getText());

            if (maleRadio.isSelected()) {
                therapist.getEntity().setGender(Gender.MALE);
            } else {
                therapist.getEntity().setGender(Gender.FEMALE);
            }

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (firstNameField.getText() == null || firstNameField.getText().length() == 0) {
            errorMessage += "Ungültiger Vorname!\n";
        }

        if (lastNameField.getText() == null || lastNameField.getText().length() == 0) {
            errorMessage += "Ungültiger Nachname!\n";
        }

        if (emailField.getText() == null || emailField.getText().length() == 0 || !emailField.getText().contains("@")) {
            errorMessage += "Ungültige E-Mail-Adresse!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Bitte korrigieren Sie ihre Eingabe")
                    .message(errorMessage)
                    .showError();
            return false;
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
}
