package ch.camorcare.backoffice.presentation.view.therapy;

import ch.camorcare.backoffice.adapter.TherapyAdapter;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the Therapy update dialog window
 */
@SuppressWarnings("deprecation")
public class TherapyUpdateDialogController implements Initializable {

    @FXML
    private Label labelPatientName;

    @FXML
    private DatePicker fieldDateSickLeave;

    @FXML
    private DatePicker fieldDateFrom;

    @FXML
    private DatePicker fieldDateUntil;

    /**
     *
     */
    private Stage dialogStage;

    /**
     *
     */
    private TherapyAdapter therapy;

    /**
     *
     */
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(300);
    }

    /**
     * Sets the therapy to be edited in the dialog.
     *
     * @param therapy
     */
    public void setTherapy(TherapyAdapter therapy) {
        this.therapy = therapy;

        if(therapy != null) {
            labelPatientName.setText(therapy.getPatientAdapter().getFirstNameProperty().getValue() + " " + therapy.getPatientAdapter().getLastNameProperty().getValue());
            fieldDateSickLeave.setValue(DateTimeConverter.dateTimeToLocalDate(therapy.getDateSickLeaveProperty().getValue()));
            fieldDateFrom.setValue(DateTimeConverter.dateTimeToLocalDate(therapy.getDateFromProperty().getValue()));
            fieldDateUntil.setValue(DateTimeConverter.dateTimeToLocalDate(therapy.getDateUntilProperty().getValue()));
        }
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            therapy.setDateFrom(DateTimeConverter.localDateToDateTime(fieldDateFrom.getValue()));
            therapy.setDateUntil(DateTimeConverter.localDateToDateTime(fieldDateUntil.getValue()));
            therapy.setDateSickLeave(DateTimeConverter.localDateToDateTime(fieldDateSickLeave.getValue()));

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        /*
        if (fieldName.getText() == null || fieldName.getText().length() == 0) {
            errorMessage += "Ungültiger Name!\n";
        }
        */

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Bitte korrigieren Sie ihre Eingabe")
                    .message(errorMessage)
                    .showError();
            return false;
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
}
