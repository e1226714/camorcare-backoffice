package ch.camorcare.backoffice.presentation.view.patient;

import ch.camorcare.backoffice.adapter.*;
import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.presentation.ScreenController;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.PatientConstraintManager;
import ch.camorcare.backoffice.service.impl.PatientManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.Callback;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.joda.time.DateTime;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the Patient overview
 */
@SuppressWarnings("deprecation")
public class PatientOverviewController extends ScreenController implements Initializable {

    /**
     * Patient table.
     */
    @FXML
    private TableView<PatientAdapter> patientTable;
    private ObservableList<PatientAdapter> patientTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<PatientAdapter, String> nameColumn;

    @FXML
    private TableColumn<PatientAdapter, DateTime> birthdayColumn;

    @FXML
    private TableColumn<PatientAdapter, String> socialSecurityNumberColumn;

    /**
     * Therapy table.
     */
    @FXML
    private TableView<TherapyAdapter> patientTherapyTable;
    private ObservableList<TherapyAdapter> patientTherapyTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TherapyAdapter, DateTime> patientTherapyTableColumnFrom;

    @FXML
    private TableColumn<TherapyAdapter, DateTime> patientTherapyTableColumnUntil;


    /**
     * Therapy type occurrence constraint table.
     */
    @FXML
    private TableView<PatientBasedTherapyTypeOccurrenceConstraintAdapter> patientTherapyTypeOccurrenceConstraintTable;
    private ObservableList<PatientBasedTherapyTypeOccurrenceConstraintAdapter> patientTherapyTypeOccurrenceConstraintTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<PatientBasedTherapyTypeOccurrenceConstraintAdapter, String> patientTherapyTypeOccurrenceConstraintTableColumnTherapyType;

    @FXML
    private TableColumn<PatientBasedTherapyTypeOccurrenceConstraintAdapter, TherapyMode> patientTherapyTypeOccurrenceConstraintTableColumnTherapyMode;

    @FXML
    private TableColumn<PatientBasedTherapyTypeOccurrenceConstraintAdapter, Number> patientTherapyTypeOccurrenceConstraintTableColumnOccurrence;


    /**
     * Therapist gender constraint table.
     */
    @FXML
    private TableView<PatientBasedTherapistGenderConstraintAdapter> patientTherapistGenderConstraintTable;
    private ObservableList<PatientBasedTherapistGenderConstraintAdapter> patientTherapistGenderConstraintTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<PatientBasedTherapistGenderConstraintAdapter, String> patientTherapistGenderConstraintTableColumnTherapyType;

    @FXML
    private TableColumn<PatientBasedTherapistGenderConstraintAdapter, Gender> patientTherapistGenderConstraintTableColumnGender;


    /**
     * Therapist constraint table.
     */
    @FXML
    private TableView<PatientBasedTherapistConstraintAdapter> patientTherapistConstraintTable;
    private ObservableList<PatientBasedTherapistConstraintAdapter> patientTherapistConstraintTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<PatientBasedTherapistConstraintAdapter, String> patientTherapistConstraintTableColumnTherapyType;

    @FXML
    private TableColumn<PatientBasedTherapistConstraintAdapter, String> patientTherapistConstraintTableColumnTherapist;


    @FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;
    @FXML
    private Label genderLabel;
    @FXML
    private Label birthdayLabel;
    @FXML
    private Label socialSecurityNumberLabel;
    @FXML
    private Label streetLabel;
    @FXML
    private Label zipLabel;
    @FXML
    private Label cityLabel;
    @FXML
    private Label phoneLabel;
    @FXML
    private Label emailLabel;

    @FXML
    private TextField filterField;

    /**
     *
     */
    private PatientManager patientManager;

    /**
     *
     */
    private PatientConstraintManager patientConstraintManager;

    /**
     *
     */
    private TherapyManager therapyManager;


    public void setPatientManager(PatientManager patientManager) {
        this.patientManager = patientManager;
    }

    public void setPatientConstraintManager(PatientConstraintManager patientConstraintManager) {
        this.patientConstraintManager = patientConstraintManager;
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize the patient table with the three columns.
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
        birthdayColumn.setCellValueFactory(cellData -> cellData.getValue().getBirthdayProperty());
        socialSecurityNumberColumn.setCellValueFactory(cellData -> cellData.getValue().getSocialSecurityNumberProperty());

        patientTherapyTableColumnFrom.setCellValueFactory(cellData -> cellData.getValue().getDateFromProperty());
        patientTherapyTableColumnUntil.setCellValueFactory(cellData -> cellData.getValue().getDateUntilProperty());

        patientTherapyTypeOccurrenceConstraintTableColumnTherapyType.setCellValueFactory(cellData -> cellData.getValue().getTherapyTypeAdapter().getNameProperty());
        patientTherapyTypeOccurrenceConstraintTableColumnTherapyMode.setCellValueFactory(cellData -> cellData.getValue().getTherapyModeProperty());
        patientTherapyTypeOccurrenceConstraintTableColumnOccurrence.setCellValueFactory(cellData -> cellData.getValue().getMaxOccurrencePerWeekProperty());

        patientTherapistGenderConstraintTableColumnTherapyType.setCellValueFactory(cellData -> cellData.getValue().getTherapyTypeAdapter().getNameProperty());
        patientTherapistGenderConstraintTableColumnGender.setCellValueFactory(cellData -> cellData.getValue().getGenderProperty());

        patientTherapistConstraintTableColumnTherapyType.setCellValueFactory(cellData -> cellData.getValue().getTherapyTypeAdapter().getNameProperty());
        patientTherapistConstraintTableColumnTherapist.setCellValueFactory(cellData -> cellData.getValue().getTherapistAdapter().getNameProperty());

        // Custom rendering of the table cell.
        birthdayColumn.setCellFactory(column -> {
            return new TableCell<PatientAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(DateTimeConverter.dateTimeToString(item));
                    }
                }
            };
        });

        patientTherapyTable.setRowFactory(new Callback<TableView<TherapyAdapter>, TableRow<TherapyAdapter>>() {

            public TableRow<TherapyAdapter> call(TableView<TherapyAdapter> tableView) {
                final TableRow<TherapyAdapter> row = new TableRow<>();
                final ContextMenu rowMenu = new ContextMenu();

                MenuItem detailsItem = new MenuItem("Details anzeigen");
                detailsItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        showTherapy(row.getItem());
                    }
                });

                MenuItem editItem = new MenuItem("Bearbeiten");
                editItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        updateTherapy(row.getItem());
                    }
                });

                MenuItem removeItem = new MenuItem("Löschen");
                removeItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        deleteTherapy(row.getItem());
                    }
                });

                rowMenu.getItems().addAll(detailsItem, editItem, removeItem);

                // only display context menu for non-null items:
                row.contextMenuProperty().bind(
                        Bindings.when(Bindings.isNotNull(row.itemProperty()))
                                .then(rowMenu)
                                .otherwise((ContextMenu)null));

                return row;
            }
        });

        // Custom rendering of the table cell.
        patientTherapyTableColumnFrom.setCellFactory(column -> {
            return new TableCell<TherapyAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    }
                    else {
                        setText(DateTimeConverter.dateTimeToString(item));
                    }
                }
            };
        });

        // Custom rendering of the table cell.
        patientTherapyTableColumnUntil.setCellFactory(column -> {
            return new TableCell<TherapyAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(DateTimeConverter.dateTimeToString(item));
                    }
                }
            };
        });

        // Clear patient details.
        showPatientDetails(null);

        // Listen for selection changes and show the patient details when changed.
        patientTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showPatientDetails(newValue));

        // Fills patient table with data
        InitPatientTable();
    }

    private void InitPatientTable() {
        try {
            patientTableData.clear();
            List<Patient> patients = patientManager.findAllPatients();

            for (Patient p : patients) {
                patientTableData.add(new PatientAdapter(p));
            }

            patientTable.setItems(patientTableData);

            // Wrap the ObservableList in a FilteredList (initially display all
            // data).
            FilteredList<PatientAdapter> filteredData = new FilteredList<>(patientTableData, p -> true);

            // Set the filter Predicate whenever the filter changes.
            filterField
                    .textProperty()
                    .addListener(
                            (observable, oldValue, newValue) -> {
                                filteredData.setPredicate(patient -> {
                                    // If filter text is empty, display all patients.
                                    if (newValue == null || newValue.isEmpty()) {
                                        return true;
                                    }

                                    // Compare name, birthday and social security number of every
                                    // patient with filter text.
                                    String lowerCaseFilter = newValue.toLowerCase();

                                    if (patient.getNameProperty().get().toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;
                                    } else if (DateTimeConverter.dateTimeToString(patient.getBirthdayProperty().get())
                                            .toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;
                                    } else if (patient.getSocialSecurityNumberProperty().get().toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;
                                    }
                                    return false; // Does not match.
                                });
                            });

            // Wrap the FilteredList in a SortedList.
            SortedList<PatientAdapter> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(patientTable.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            patientTable.setItems(sortedData);
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Patientenübersicht")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    /**
     * Fills all text fields to show details about the patient. If the specified
     * patient is null, all text fields are cleared.
     *
     * @param patient
     *            the patient or null
     */
    private void showPatientDetails(PatientAdapter patient) {
        if (patient != null) {
            showPatientDetailsBasic(patient);
            showPatientDetailsTherapy(patient);
            showPatientDetailsTherapyTypeOccurrenceConstraint(patient);
            showPatientDetailsTherapistGenderConstraint(patient);
            showPatientDetailsTherapistConstraint(patient);
        } else {
            hidePatientDetails();
        }
    }

    private void showPatientDetailsBasic(PatientAdapter patient) {
        firstNameLabel.setText(patient.getFirstNameProperty().get());
        lastNameLabel.setText(patient.getLastNameProperty().get());
        genderLabel.setText(patient.getEntity().getGender().toString());
        birthdayLabel.setText(DateTimeConverter.dateTimeToString(patient.getBirthdayProperty().get()));
        socialSecurityNumberLabel.setText(patient.getSocialSecurityNumberProperty().get());
        streetLabel.setText(patient.getAddressAdapter().getStreetProperty().get());
        zipLabel.setText(patient.getAddressAdapter().getZipProperty().get());
        cityLabel.setText(patient.getAddressAdapter().getCityProperty().get());
        phoneLabel.setText(patient.getPhoneProperty().get());
        emailLabel.setText(patient.getEmailProperty().get());
    }

    /**
     * Update therapist constraint table.
     */
    private void showPatientDetailsTherapistConstraint(PatientAdapter patient) {
        patientTherapistConstraintTableData.clear();

        try {
            for (PatientBasedTherapistConstraint constraint : patientConstraintManager.findAllTherapistConstraintsByPatient(patient.getEntity())) {
                patientTherapistConstraintTableData.add(new PatientBasedTherapistConstraintAdapter(constraint));
            }
        } catch (ServiceException | ValidationException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapeutenpräferenz")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        patientTherapistConstraintTable.setItems(patientTherapistConstraintTableData);
    }

    /**
     * Update therapist gender constraint table.
     */
    private void showPatientDetailsTherapistGenderConstraint(PatientAdapter patient) {
        patientTherapistGenderConstraintTableData.clear();

        try {
            for (PatientBasedTherapistGenderConstraint constraint : patientConstraintManager.findAllTherapistGenderConstraintsByPatient(patient.getEntity())) {
                patientTherapistGenderConstraintTableData.add(new PatientBasedTherapistGenderConstraintAdapter(constraint));
            }
        } catch (ServiceException | ValidationException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Geschlechterpräferenz")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        patientTherapistGenderConstraintTable.setItems(patientTherapistGenderConstraintTableData);
    }

    /**
     * Update therapy type occurrence constraint table.
     */
    private void showPatientDetailsTherapyTypeOccurrenceConstraint(PatientAdapter patient) {
        patientTherapyTypeOccurrenceConstraintTableData.clear();

        try {
            for (PatientBasedTherapyTypeOccurrenceConstraint constraint : patientConstraintManager.findAllPatientBasedTherapyTypeOccurrenceConstraintsByPatient(patient.getEntity())) {
                patientTherapyTypeOccurrenceConstraintTableData.add(new PatientBasedTherapyTypeOccurrenceConstraintAdapter(constraint));
            }
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der verodneten Therapiezeiten")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        patientTherapyTypeOccurrenceConstraintTable.setItems(patientTherapyTypeOccurrenceConstraintTableData);
    }

    /**
     * Update therapy table.
     */
    private void showPatientDetailsTherapy(PatientAdapter patient) {
        patientTherapyTableData.clear();

        try {
            for (Therapy therapy : therapyManager.findAllTherapiesByPatient(patient.getEntity())) {
                patientTherapyTableData.add(new TherapyAdapter(therapy));
            }
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapien für den Patienten")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        patientTherapyTable.setItems(patientTherapyTableData);
    }

    /**
     * Clear all fields.
     */
    private void hidePatientDetails() {
        firstNameLabel.setText("");
        lastNameLabel.setText("");
        genderLabel.setText("");
        birthdayLabel.setText("");
        socialSecurityNumberLabel.setText("");
        streetLabel.setText("");
        zipLabel.setText("");
        cityLabel.setText("");
        phoneLabel.setText("");
        emailLabel.setText("");

        patientTherapyTableData.clear();
        patientTherapyTable.setItems(patientTherapyTableData);

        patientTherapyTypeOccurrenceConstraintTableData.clear();
        patientTherapyTypeOccurrenceConstraintTable.setItems(patientTherapyTypeOccurrenceConstraintTableData);

        patientTherapistGenderConstraintTableData.clear();
        patientTherapistGenderConstraintTable.setItems(patientTherapistGenderConstraintTableData);

        patientTherapistConstraintTableData.clear();
        patientTherapistConstraintTable.setItems(patientTherapistConstraintTableData);
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new patient.
     */
    @FXML
    private void handleCreate() {
        Patient tempPatient = new Patient();
        boolean okClicked = this.getMainApp().showPatientEditDialog(new PatientAdapter(tempPatient));
        if (okClicked) {
            try {
                tempPatient = patientManager.create(tempPatient);
                PatientAdapter adapter = new PatientAdapter(tempPatient);
                patientTableData.add(adapter);
                patientTable.getSelectionModel().select(adapter);
            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Erzeugen eines neuen Patienten")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    private boolean hasTimeOverlap(TherapyAdapter therapyAdapter) {
        for(TherapyAdapter ta : patientTherapyTableData) {
            if(therapyAdapter.getEntity().getId() != null && therapyAdapter.getEntity().getId().equals(ta.getEntity().getId())) {
                continue;
            }

            if(therapyAdapter.getEntity().getDateFrom().equals(ta.getEntity().getDateFrom())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateFrom().equals(ta.getEntity().getDateUntil())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateUntil().equals(ta.getEntity().getDateFrom())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateUntil().equals(ta.getEntity().getDateUntil())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateFrom().isAfter(ta.getEntity().getDateFrom()) && therapyAdapter.getEntity().getDateFrom().isBefore(ta.getEntity().getDateUntil())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateUntil().isAfter(ta.getEntity().getDateFrom()) && therapyAdapter.getEntity().getDateUntil().isBefore(ta.getEntity().getDateUntil())) {
                return true;
            }
        }

        return false;
    }

    @FXML
    private void handleCreateTherapy() {
        PatientAdapter pa = patientTable.getSelectionModel().getSelectedItem();
        Therapy tempTherapy = new Therapy();
        tempTherapy.setPatient(pa.getEntity());

        TherapyAdapter therapyAdapter = new TherapyAdapter(tempTherapy);
        boolean okClicked = this.getMainApp().showTherapyUpdateDialog(therapyAdapter);

        if (okClicked) {
            try {
                if(hasTimeOverlap(therapyAdapter)) {
                    Dialogs.create()
                        .title("Ungültige Eingabe")
                        .masthead("Terminüberlappung")
                        .message("Für diesen Zeitraum wurde bereits eine Therapie gebucht.")
                        .showWarning();
                } else {
                    tempTherapy = therapyManager.create(tempTherapy);
                    patientTherapyTableData.add(new TherapyAdapter(tempTherapy));
                }

            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Erzeugen eines Aufenthalts")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    private void showTherapy(TherapyAdapter therapyAdapter) {
        this.getLayoutController().handleShowTherapyOverview(therapyAdapter);
    }

    private void updateTherapy(TherapyAdapter therapyAdapter) {
        boolean okClicked = this.getMainApp().showTherapyUpdateDialog(therapyAdapter);

        if (okClicked) {
            try {
                if(hasTimeOverlap(therapyAdapter)) {
                    Dialogs.create()
                        .title("Ungültige Eingabe")
                        .masthead("Terminüberlappung")
                        .message("Für diesen Zeitraum wurde bereits eine Therapie gebucht.")
                        .showWarning();

                    showPatientDetailsTherapy(patientTable.getSelectionModel().getSelectedItem());
                } else {
                    therapyManager.update(therapyAdapter.getEntity());
                }
            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Aktualisieren eines Aufenthalts")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    private void deleteTherapy(TherapyAdapter item) {
        Action response = Dialogs.create().title("Bestätigungsdialog")
                .masthead("Aufenthalt löschen")
                .message("Möchten Sie diesen Aufenthalt wirklich löschen?")
                .actions(org.controlsfx.dialog.Dialog.ACTION_YES, org.controlsfx.dialog.Dialog.ACTION_NO)
                .showConfirm();

        if (response == Dialog.ACTION_YES) {
            try {
                therapyManager.delete(item.getEntity());
                patientTherapyTableData.remove(item);

                Dialogs.create()
                        .title("Informationsdialog")
                        .masthead(null).message("Aufenthalt wurde erfolgreich gelöscht.")
                        .showInformation();
            } catch (ServiceException e) {
                Dialogs.create().title("Fehler beim Löschen eines Aufenthalts")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    @FXML
    private void handleEditTherapistGenderConstraints() {
        PatientAdapter pa = patientTable.getSelectionModel().getSelectedItem();

        if (pa != null) {
            boolean okClicked = this.getMainApp().showPatientBasedTherapistGenderConstraintUpdateDialog(pa);

            if (okClicked) {
                showPatientDetailsTherapistGenderConstraint(pa);
            }
        } else {
            // Nothing selected.
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Kein Patient ausgewählt")
                    .message("Bitte wählen Sie einen Patienten aus der Tabelle aus.")
                    .showWarning();
        }
    }

    @FXML
    private void handleEditTherapistTypeOccurrenceConstraints() {
        PatientAdapter pa = patientTable.getSelectionModel().getSelectedItem();

        if (pa != null) {
            boolean okClicked = this.getMainApp().showPatientBasedTherapyTypeOccurrenceConstraintUpdateDialog(pa);

            if (okClicked) {
                showPatientDetailsTherapyTypeOccurrenceConstraint(pa);
            }
        } else {
            // Nothing selected.
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Kein Patient ausgewählt")
                    .message("Bitte wählen Sie einen Patienten aus der Tabelle aus.")
                    .showWarning();
        }
    }

    @FXML
    private void handleEditTherapistConstraints() {
        PatientAdapter pa = patientTable.getSelectionModel().getSelectedItem();

        if (pa != null) {
            boolean okClicked = this.getMainApp().showPatientBasedTherapistConstraintUpdateDialog(pa);

            if (okClicked) {
                showPatientDetailsTherapistConstraint(pa);
            }
        } else {
            // Nothing selected.
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Kein Patient ausgewählt")
                    .message("Bitte wählen Sie einen Patienten aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected patient.
     */
    @FXML
    private void handleUpdate() {
        PatientAdapter pa = patientTable.getSelectionModel().getSelectedItem();
        if (pa != null) {
            boolean okClicked = this.getMainApp().showPatientEditDialog(pa);
            if (okClicked) {
                try {
                    showPatientDetailsBasic(pa);
                    patientManager.update(pa.getEntity());
                } catch (ServiceException | ValidationException e) {
                    Dialogs.create().title("Fehler beim Aktualisieren eines Patienten")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            // Nothing selected.
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Kein Patient ausgewählt")
                    .message("Bitte wählen Sie einen Patienten aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDelete() {
        int selectedIndex = patientTable.getSelectionModel().getSelectedIndex();

        if (selectedIndex >= 0) {
            Action response = Dialogs.create().title("Bestätigungsdialog")
                    .masthead("Patient löschen")
                    .message("Möchten Sie diesen Patienten wirklich löschen?")
                    .actions(org.controlsfx.dialog.Dialog.ACTION_YES, org.controlsfx.dialog.Dialog.ACTION_NO)
                    .showConfirm();

            if (response == Dialog.ACTION_YES) {
                try {
                    patientManager.delete(patientTableData.get(selectedIndex).getEntity());
                    patientTableData.remove(selectedIndex);

                    Dialogs.create()
                        .title("Informationsdialog")
                        .masthead(null).message("Patient gelöscht!")
                        .showInformation();
                } catch (ServiceException e) {
                    Dialogs.create().title("Fehler beim Löschen eines Patienten")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            Dialogs.create()
                .title("Ungültige Auswahl")
                .masthead("Kein Patient ausgewählt")
                .message("Bitte wählen Sie einen Patienten aus der Tabelle aus.")
                .showWarning();
        }
    }
}
