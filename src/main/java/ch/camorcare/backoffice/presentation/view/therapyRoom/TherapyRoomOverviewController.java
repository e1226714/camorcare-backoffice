package ch.camorcare.backoffice.presentation.view.therapyRoom;

import ch.camorcare.backoffice.adapter.TherapyRoomAdapter;
import ch.camorcare.backoffice.entities.TherapyRoom;
import ch.camorcare.backoffice.presentation.MainApp;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the TherapyRoom overview
 */
@SuppressWarnings("deprecation")
public class TherapyRoomOverviewController implements Initializable {

    @FXML
    private TableView<TherapyRoomAdapter> therapyRoomTable;
    @FXML
    private TableColumn<TherapyRoomAdapter, String> nameColumn;
    @FXML
    private TableColumn<TherapyRoomAdapter, Number> capacityColumn;

    @FXML
    private Label nameLabel;
    @FXML
    private Label capacityLabel;

    @FXML
    private TextField filterField;

    /**
     * Therapy Manager.
     */
    private TherapyManager therapyManager;

    private MainApp mainApp;
    private ObservableList<TherapyRoomAdapter> therapyRoomData = FXCollections.observableArrayList();

    public void setMainApp(MainApp mainApp) { this.mainApp = mainApp; }

    public TherapyManager getTherapyManager() {
        return therapyManager;
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize the duty category table with the two columns.
        nameColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getNameProperty());
        capacityColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getCapacityProperty());

        // Clear duty category details.
        showTherapyRoomDetails(null);

        // Listen for selection changes and show the therapy room details when changed.
        therapyRoomTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showTherapyRoomDetails(newValue));

        // Fills therapy room table with data
        InitTherapyRoomTable();
    }

    private void InitTherapyRoomTable() {
        try {
            therapyRoomData.clear();
            List<TherapyRoom> therapyRooms = therapyManager.findAllTherapyRooms();

            for (TherapyRoom tr : therapyRooms) {
                therapyRoomData.add(new TherapyRoomAdapter(tr));
            }

            therapyRoomTable.setItems(therapyRoomData);

            // Wrap the ObservableList in a FilteredList (initially display all
            // data).
            FilteredList<TherapyRoomAdapter> filteredData = new FilteredList<>(therapyRoomData, p -> true);

            // Set the filter Predicate whenever the filter changes.
            filterField
                    .textProperty()
                    .addListener(
                            (observable, oldValue, newValue) -> {
                                filteredData.setPredicate(dutyCategory -> {
                                    // If filter text is empty, display all therapy types.
                                    if (newValue == null || newValue.isEmpty()) {
                                        return true;
                                    }

                                    // Compare name and duration of every duty category with filter text.
                                    String lowerCaseFilter = newValue.toLowerCase();

                                    if (dutyCategory.getNameProperty().get().toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;
                                    } else if (dutyCategory.getCapacityProperty().getValue().toString().toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;
                                    }
                                    return false; // Does not match.
                                });
                            });

            // Wrap the FilteredList in a SortedList.
            SortedList<TherapyRoomAdapter> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(therapyRoomTable.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            therapyRoomTable.setItems(sortedData);
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapieraumübersicht")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    /**
     * Fills all text fields to show details about the therapy room. If the specified
     * therapy room is null, all text fields are cleared.
     *
     * @param therapyRoom the therapy room or null
     */
    private void showTherapyRoomDetails(TherapyRoomAdapter therapyRoom) {
        if (therapyRoom != null) {
            nameLabel.setText(therapyRoom.getNameProperty().get());
            capacityLabel.setText(therapyRoom.getCapacityProperty().getValue().toString());
        } else {
            // Therapy room is null, remove all the text.
            nameLabel.setText("");
            capacityLabel.setText("");
        }
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new therapy room.
     */
    @FXML
    private void handleCreate() {
        TherapyRoom tempTherapyRoom = new TherapyRoom();
        boolean okClicked = mainApp.showTherapyRoomEditDialog(new TherapyRoomAdapter(tempTherapyRoom));
        if (okClicked) {
            try {
                tempTherapyRoom = therapyManager.create(tempTherapyRoom);
                TherapyRoomAdapter adapter = new TherapyRoomAdapter(tempTherapyRoom);
                therapyRoomData.add(adapter);
                therapyRoomTable.getSelectionModel().select(adapter);
            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Anlegen eines Therapieraums")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected therapy room.
     */
    @FXML
    private void handleUpdate() {
        TherapyRoomAdapter tra = therapyRoomTable.getSelectionModel().getSelectedItem();
        if (tra != null) {
            boolean okClicked = mainApp.showTherapyRoomEditDialog(tra);
            if (okClicked) {
                try {
                    showTherapyRoomDetails(tra);
                    therapyManager.update(tra.getEntity());
                } catch (ServiceException | ValidationException e) {
                    Dialogs.create().title("Fehler beim Update eines Therapieraums")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            // Nothing selected.
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Keinen Therapieraum ausgewählt")
                    .message("Bitte wählen Sie einen Therapieraum aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDelete() {
        int selectedIndex = therapyRoomTable.getSelectionModel()
                .getSelectedIndex();
        if (selectedIndex >= 0) {
            Action response = Dialogs.create().title("Bestätigungsdialog")
                    .masthead("Therapieraum löschen")
                    .message("Möchten Sie diesen Therapieraum wirklich löschen?")
                    .actions(Dialog.ACTION_YES, Dialog.ACTION_NO)
                    .showConfirm();
            if (response == org.controlsfx.dialog.Dialog.ACTION_YES) {
                try {
                    therapyManager.delete(therapyRoomData
                            .get(selectedIndex).getEntity());
                    therapyRoomData.remove(selectedIndex);
                    Dialogs.create().title("Informationsdialog")
                            .masthead(null).message("Therapieraum gelöscht!")
                            .showInformation();
                } catch (ServiceException e) {
                    Dialogs.create().title("Fehler beim Löschen eines Therapieraums")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Keinen Therapieraum ausgewählt")
                    .message("Bitte wählen Sie einen Therapieraum aus der Tabelle aus.")
                    .showWarning();
        }
    }
}
