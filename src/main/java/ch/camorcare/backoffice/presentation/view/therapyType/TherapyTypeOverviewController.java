package ch.camorcare.backoffice.presentation.view.therapyType;

import ch.camorcare.backoffice.adapter.TherapyTypeAdapter;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.presentation.MainApp;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the TherapyType overview
 */
@SuppressWarnings("deprecation")
public class TherapyTypeOverviewController implements Initializable {

    @FXML
    private TableView<TherapyTypeAdapter> table;

    @FXML
    private TableColumn<TherapyTypeAdapter, String> nameColumn;

    @FXML
    private TableColumn<TherapyTypeAdapter, Boolean> isIndividualColumn;

    @FXML
    private TableColumn<TherapyTypeAdapter, Boolean> isGroupColumn;

    @FXML
    private Label nameLabel;

    @FXML
    private Label isIndividualLabel;

    @FXML
    private Label isGroupLabel;

    @FXML
    private Label groupSizeMinLabel;

    @FXML
    private Label groupSizeMaxLabel;

    @FXML
    private TextField filterField;

    /**
     * Therapy Manager.
     */
    private TherapyManager therapyManager;

    /**
     *
     */
    private MainApp mainApp;

    /**
     *
     */
    private ObservableList<TherapyTypeAdapter> therapyTypeData = FXCollections.observableArrayList();

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize the therapy type table with the one column.
        nameColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getNameProperty());

        isIndividualColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getIndividualTherapyProperty());

        isGroupColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getGroupTherapyProperty());

        // Custom rendering of the table cell.
        isIndividualColumn.setCellFactory(column -> {
            return new TableCell<TherapyTypeAdapter, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(item ? "Ja" : "Nein");
                    }
                }
            };
        });
        isGroupColumn.setCellFactory(column -> {
            return new TableCell<TherapyTypeAdapter, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(item ? "Ja" : "Nein");
                    }
                }
            };
        });

        // Clear therapy type details.
        handleOverview(null);

        // Listen for selection changes and show the therapy type details when changed.
        table
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> handleOverview(newValue));

        // Fills therapy type table with data
        initTable();
    }

    private void initTable() {
        try {
            therapyTypeData.clear();
            List<TherapyType> therapyTypes = therapyManager.findAllTherapyTypes();

            for (TherapyType tt : therapyTypes) {
                therapyTypeData.add(new TherapyTypeAdapter(tt));
            }

            table.setItems(therapyTypeData);

            // Wrap the ObservableList in a FilteredList (initially display all
            // data).
            FilteredList<TherapyTypeAdapter> filteredData = new FilteredList<>(therapyTypeData, p -> true);

            // Set the filter Predicate whenever the filter changes.
            filterField
                    .textProperty()
                    .addListener(
                            (observable, oldValue, newValue) -> {
                                filteredData.setPredicate(therapyType -> {
                                    // If filter text is empty, display all therapy types.
                                    if (newValue == null || newValue.isEmpty()) {
                                        return true;
                                    }

                                    // Compare name of every therapy type with filter text.
                                    String lowerCaseFilter = newValue.toLowerCase();

                                    if (therapyType.getNameProperty().get().toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;
                                    }
                                    return false; // Does not match.
                                });
                            }
                    );

            // Wrap the FilteredList in a SortedList.
            SortedList<TherapyTypeAdapter> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(table.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            table.setItems(sortedData);
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapietypübersicht")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    /**
     * Fills all text fields to show details about the therapy type. If the specified
     * therapy type is null, all text fields are cleared.
     *
     * @param therapyType the therapy type or null
     */
    private void handleOverview(TherapyTypeAdapter therapyType) {
        if (therapyType != null) {
            nameLabel.setText(therapyType.getNameProperty().get());
            if(therapyType.getEntity().isSuitableForIndividuals()) {
                isIndividualLabel.setText("Ja");
            } else {
                isIndividualLabel.setText("Nein");
            }
            //isIndividualTherapistFixedLabel.setText(therapyType.getEntity().isIndividualTherapistFixed() ? "Ja" : "Nein");
            //individualDurationLabel.setText(therapyType.getEntity().getIndividualTherapyDuration() + " Minuten");
            isGroupLabel.setText(therapyType.getEntity().isSuitableForGroups() ? "Ja" : "Nein");
            //isGroupTherapistFixedLabel.setText(therapyType.getEntity().isGroupTherapistFixed() ? "Ja" : "Nein");
            //groupDurationLabel.setText(therapyType.getEntity().getGroupTherapyDuration() + " Minuten");
            groupSizeMinLabel.setText(therapyType.getEntity().getMinGroupSize().toString());
            groupSizeMaxLabel.setText(therapyType.getEntity().getMaxGroupSize().toString());
        } else {
            // Therapy type is null, remove all the text.
            nameLabel.setText("");
            isIndividualLabel.setText("");
            //isIndividualTherapistFixedLabel.setText("");
            //individualDurationLabel.setText("");
            isGroupLabel.setText("");
            //isGroupTherapistFixedLabel.setText("");
            //groupDurationLabel.setText("");
            groupSizeMinLabel.setText("");
            groupSizeMaxLabel.setText("");
        }
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new therapy type.
     */
    @FXML
    private void handleCreate() {
        TherapyType tempTherapyType = new TherapyType();
        boolean okClicked = mainApp.showTherapyTypeEditDialog(new TherapyTypeAdapter(tempTherapyType));
        if (okClicked) {
            try {
                tempTherapyType = therapyManager.create(tempTherapyType);
                TherapyTypeAdapter adapter = new TherapyTypeAdapter(tempTherapyType);
                therapyTypeData.add(adapter);
                table.getSelectionModel().select(adapter);
            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Anlegen eines Therapietypen")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected therapy type.
     */
    @FXML
    private void handleUpdate() {
        TherapyTypeAdapter tta = table.getSelectionModel().getSelectedItem();
        if (tta != null) {
            boolean okClicked = mainApp.showTherapyTypeEditDialog(tta);
            if (okClicked) {
                try {
                    handleOverview(tta);
                    therapyManager.update(tta.getEntity());
                } catch (ServiceException | ValidationException e) {
                    Dialogs.create().title("Fehler beim Aktualisieren eines Therapietypen")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            // Nothing selected.
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Keine Therapieform ausgewählt")
                    .message("Bitte wählen Sie eine Therapieform aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDelete() {
        int selectedIndex = table.getSelectionModel()
                .getSelectedIndex();
        if (selectedIndex >= 0) {
            Action response = Dialogs.create().title("Bestätigungsdialog")
                    .masthead("Therapieform löschen")
                    .message("Möchten Sie diese Therapieform wirklich löschen?")
                    .actions(Dialog.ACTION_YES, Dialog.ACTION_NO)
                    .showConfirm();
            if (response == Dialog.ACTION_YES) {
                try {
                    therapyManager.delete(therapyTypeData
                            .get(selectedIndex).getEntity());
                    therapyTypeData.remove(selectedIndex);
                    Dialogs.create().title("Informationsdialog")
                            .masthead(null).message("Therapieform gelöscht!")
                            .showInformation();
                } catch (ServiceException e) {
                    Dialogs.create().title("Fehler beim Löschen eines Therapietypen")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Keine Therapieform ausgewählt")
                    .message("Bitte wählen Sie eine Therapieform aus der Tabelle aus.")
                    .showWarning();
        }
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

}
