package ch.camorcare.backoffice.presentation.view.patient.therapistConstraint;

import ch.camorcare.backoffice.adapter.PatientAdapter;
import ch.camorcare.backoffice.adapter.PatientBasedTherapistConstraintAdapter;
import ch.camorcare.backoffice.adapter.PatientBasedTherapistGenderConstraintAdapter;
import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.PatientConstraintManager;
import ch.camorcare.backoffice.service.impl.TherapistConstraintManager;
import ch.camorcare.backoffice.service.impl.TherapistManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the PatientBasedTherapistConstraint update dialog window
 */
public class PatientBasedTherapistConstraintUpdateDialogController implements Initializable {

    @FXML
    private TableView<PatientBasedTherapistConstraintAdapter> constraintTable;
    private ObservableList<PatientBasedTherapistConstraintAdapter> constraintTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<PatientBasedTherapistConstraintAdapter, String> constraintTableColumnTherapyType;

    @FXML
    private TableColumn<PatientBasedTherapistConstraintAdapter, String> constraintTableColumnTherapist;

    @FXML
    private ComboBox<TherapyType> therapyTypeCombo;
    private ObservableList<TherapyType> therapyTypes = FXCollections.observableArrayList();

    @FXML
    private ComboBox<Therapist> therapistCombo;
    private ObservableList<Therapist> therapists = FXCollections.observableArrayList();

    private Stage dialogStage;

    private PatientAdapter patientAdapter;

    private TherapyManager therapyManager;

    private TherapistManager therapistManager;

    private PatientConstraintManager patientConstraintManager;

    private TherapistConstraintManager therapistConstraintManager;

    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        constraintTableColumnTherapyType.setCellValueFactory(cellData -> cellData.getValue().getTherapyTypeAdapter().getNameProperty());
        constraintTableColumnTherapist.setCellValueFactory(cellData -> cellData.getValue().getTherapistAdapter().getNameProperty());


        therapyTypeCombo.valueProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
                if(newValue != null) {
                    therapistCombo.disableProperty().setValue(false);
                    List<Therapist> therapistList = new ArrayList<>();

                    try {
                        therapistList = therapistConstraintManager.findAllTherapistsByTherapyType(therapyTypeCombo.getSelectionModel().getSelectedItem());
                    } catch (ServiceException e) {
                        Dialogs.create().title("Fehler beim initialisieren der Therapeutenauswahl")
                                .masthead(null)
                                .message(e.getMessage())
                                .showWarning();
                    }

                    setTherapistSelection(therapistList);
                } else {
                    therapistCombo.getSelectionModel().clearSelection();
                    therapistCombo.disableProperty().setValue(true);
                }
            }
        });

    }

    /**
     * Updates the therapist type dropdown.
     *
     * @param therapyTypeSelection
     */
    private void setTherapyTypeSelection(List<TherapyType> therapyTypeSelection) {
        therapyTypes.clear();

        for (TherapyType t : therapyTypeSelection) {
            therapyTypes.add(t);
        }

        therapyTypeCombo.setItems(therapyTypes);

        if(therapyTypes.size() > 0) {
            therapyTypeCombo.getSelectionModel().select(0);
        }
    }

    /**
     * Updates the therapist dropdown.
     *
     * @param therapistSelection
     */
    private void setTherapistSelection(List<Therapist> therapistSelection) {
        therapists.clear();

        for (Therapist t : therapistSelection) {
            therapists.add(t);
        }

        therapistCombo.setItems(therapists);

        if(therapistSelection.size() > 0) {
            therapistCombo.getSelectionModel().select(0);
        }
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(350);
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleAdd() {
        boolean exists = false;

        PatientBasedTherapistConstraint constraint = new PatientBasedTherapistConstraint();
        constraint.setPatient(patientAdapter.getEntity());
        constraint.setTherapyType(therapyTypeCombo.getSelectionModel().getSelectedItem());
        constraint.setTherapist(therapistCombo.getSelectionModel().getSelectedItem());

        for(PatientBasedTherapistConstraintAdapter adapter : constraintTableData) {
            if(constraint.getTherapyType().getId().equals(adapter.getEntity().getTherapyType().getId())) {
                exists = true;
                break;
            }
        }

        if(exists) {
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Diese Therapieform wurde bereits zugeteilt")
                    .message("Bitte überprüfen Sie Ihre Auswahl.")
                    .showWarning();
        } else {
            boolean genderConstraintExists = false;

            try {
                List<PatientBasedTherapistGenderConstraint> otherConstraints = patientConstraintManager.findAllTherapistGenderConstraintsByPatient(patientAdapter.getEntity());

                for(PatientBasedTherapistGenderConstraint otherConstraint : otherConstraints) {
                    if(constraint.getTherapyType().getId().equals(otherConstraint.getTherapyType().getId())) {
                        genderConstraintExists = true;
                        break;
                    }
                }

                if(genderConstraintExists) {
                    Dialogs.create()
                        .title("Ungültige Eingabe")
                        .masthead("Diese Therapieform wurde bereits zugeteilt")
                        .message("Für diese Therapieform wurde bereits eine Geschlechterpräferenz angegeben.")
                        .showWarning();
                } else {
                    constraintTableData.add(new PatientBasedTherapistConstraintAdapter(constraint));
                }

            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Hinzufügen einer Therapeutenpräferenz")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    @FXML
    private void handleDelete() {
        int selectedIndex = constraintTable.getSelectionModel().getSelectedIndex();

        if (selectedIndex >= 0) {
            Action response = Dialogs.create()
                    .title("Bestätigungsdialog")
                    .masthead("Eintrag löschen")
                    .message("Möchten Sie diesen Eintrag wirklich löschen?")
                    .actions(org.controlsfx.dialog.Dialog.ACTION_YES, org.controlsfx.dialog.Dialog.ACTION_NO)
                    .showConfirm();
            if (response == org.controlsfx.dialog.Dialog.ACTION_YES) {
                constraintTableData.remove(selectedIndex);

                Dialogs.create()
                        .title("Informationsdialog")
                        .masthead(null)
                        .message("Eintrag gelöscht!")
                        .showInformation();
            }
        }
        else {
            Dialogs.create()
                    .title("Ungültige Auswahl")
                    .masthead("Kein Eintrag ausgewählt")
                    .message("Bitte wählen Sie einen Eintrag aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {

        try {
            List<PatientBasedTherapistConstraint> constraints = new ArrayList<>();

            for (PatientBasedTherapistConstraintAdapter constraint : constraintTableData) {
                constraints.add(constraint.getEntity());
            }

            patientConstraintManager.createPatientBasedTherapistGenderConstraints(constraints, patientAdapter.getEntity());
        } catch (ServiceException | ValidationException e) {
            Dialogs.create().title("Fehler beim Schreiben der Therapeutenpräferenzen")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        okClicked = true;
        dialogStage.close();
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setTherapistManager(TherapistManager therapistManager) {
        this.therapistManager = therapistManager;
    }

    public void setPatientConstraintManager(PatientConstraintManager patientConstraintManager) {
        this.patientConstraintManager = patientConstraintManager;
    }

    public void setPatientAdapter(PatientAdapter patientAdapter) {
        this.patientAdapter = patientAdapter;
        this.constraintTableData.clear();

        try {
            List<TherapyType> therapyTypeSelection = new ArrayList<>();
            for(PatientBasedTherapyTypeOccurrenceConstraint oc : patientConstraintManager.findAllPatientBasedTherapyTypeOccurrenceConstraintsByPatient(patientAdapter.getEntity())) {
                therapyTypeSelection.add(oc.getTherapyType());
            }
            setTherapyTypeSelection(therapyTypeSelection);

            List<PatientBasedTherapistConstraint> constraints = patientConstraintManager.findAllTherapistConstraintsByPatient(patientAdapter.getEntity());
            for (PatientBasedTherapistConstraint constraint : constraints) {
                this.constraintTableData.add(new PatientBasedTherapistConstraintAdapter(constraint));
            }
        } catch (ServiceException | ValidationException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapeutenpräferenzen")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

        this.constraintTable.setItems(constraintTableData);
    }

    public void setTherapistConstraintManager(TherapistConstraintManager therapistConstraintManager) {
        this.therapistConstraintManager = therapistConstraintManager;
    }
}
