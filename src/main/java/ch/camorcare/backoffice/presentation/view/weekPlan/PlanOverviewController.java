package ch.camorcare.backoffice.presentation.view.weekPlan;

import ch.camorcare.backoffice.adapter.TherapistAdapter;
import ch.camorcare.backoffice.adapter.TherapyAdapter;
import ch.camorcare.backoffice.adapter.TherapyEventAdapter;
import ch.camorcare.backoffice.adapter.TherapyRoomAdapter;
import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.presentation.MainApp;
import ch.camorcare.backoffice.service.TherapyScheduleService;
import ch.camorcare.backoffice.service.exception.PlanningException;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.PatientManager;
import ch.camorcare.backoffice.service.impl.TherapistConstraintManager;
import ch.camorcare.backoffice.service.impl.TherapistManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.email.EmailService;
import ch.camorcare.backoffice.util.pdftools.PdfPlanDateConverter;
import ch.camorcare.backoffice.util.pdftools.PdfPlanGenerator;
import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.dialog.Dialogs;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.mail.MailSendException;

import javax.mail.MessagingException;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Controller for plan overview
 */
public class PlanOverviewController implements Initializable {

	@FXML
	private ChoiceBox<String> typeChoiceBox;

	@FXML
	private ChoiceBox<Object> unitChoiceBox;

	@FXML
	private TableView<TherapyEventAdapter> therapyEventTable;

	@FXML
	private TableColumn<TherapyEventAdapter, String> eventColumn;

	@FXML
	private TableColumn<TherapyEventAdapter, Boolean> allocatedColumn;

	// saves the therapyEvent to be shown after reload of therapyeventTable;
	private TherapyEvent actualTherapyEvent = null;

	@FXML
	private Label mo1Label;
	@FXML
	private Label mo2Label;
	@FXML
	private Label mo3Label;
	@FXML
	private Label mo4Label;
	@FXML
	private Label mo5Label;
	@FXML
	private Label mo6Label;

	@FXML
	private Label di1Label;
	@FXML
	private Label di2Label;
	@FXML
	private Label di3Label;
	@FXML
	private Label di4Label;
	@FXML
	private Label di5Label;
	@FXML
	private Label di6Label;

	@FXML
	private Label mi1Label;
	@FXML
	private Label mi2Label;
	@FXML
	private Label mi3Label;
	@FXML
	private Label mi4Label;
	@FXML
	private Label mi5Label;
	@FXML
	private Label mi6Label;

	@FXML
	private Label do1Label;
	@FXML
	private Label do2Label;
	@FXML
	private Label do3Label;
	@FXML
	private Label do4Label;
	@FXML
	private Label do5Label;
	@FXML
	private Label do6Label;

	@FXML
	private Label fr1Label;
	@FXML
	private Label fr2Label;
	@FXML
	private Label fr3Label;
	@FXML
	private Label fr4Label;
	@FXML
	private Label fr5Label;
	@FXML
	private Label fr6Label;

	@FXML
	private Button mo1SetButton;
	@FXML
	private Button mo2SetButton;
	@FXML
	private Button mo3SetButton;
	@FXML
	private Button mo4SetButton;
	@FXML
	private Button mo5SetButton;
	@FXML
	private Button mo6SetButton;

	@FXML
	private Button di1SetButton;
	@FXML
	private Button di2SetButton;
	@FXML
	private Button di3SetButton;
	@FXML
	private Button di4SetButton;
	@FXML
	private Button di5SetButton;
	@FXML
	private Button di6SetButton;

	@FXML
	private Button mi1SetButton;
	@FXML
	private Button mi2SetButton;
	@FXML
	private Button mi3SetButton;
	@FXML
	private Button mi4SetButton;
	@FXML
	private Button mi5SetButton;
	@FXML
	private Button mi6SetButton;

	@FXML
	private Button do1SetButton;
	@FXML
	private Button do2SetButton;
	@FXML
	private Button do3SetButton;
	@FXML
	private Button do4SetButton;
	@FXML
	private Button do5SetButton;
	@FXML
	private Button do6SetButton;

	@FXML
	private Button fr1SetButton;
	@FXML
	private Button fr2SetButton;
	@FXML
	private Button fr3SetButton;
	@FXML
	private Button fr4SetButton;
	@FXML
	private Button fr5SetButton;
	@FXML
	private Button fr6SetButton;


	@FXML
	private Button printButton;
	@FXML
	private Button printAndSendButton;

    // details information
    @FXML
    private ChoiceBox<Therapist> detailsTherapistChoiceBox;
    private ObservableList<Therapist> therapistData = FXCollections.observableArrayList();
    @FXML
    private ChoiceBox<TherapyRoom> detailsRoomChoiceBox;
    private ObservableList<TherapyRoom> therapyRoomData = FXCollections.observableArrayList();
    @FXML
    private Label detailsTimeLabel;
    @FXML
    private ListView detailsParticipantsListView;
    private ObservableList<String> participantNames = FXCollections.observableArrayList();

	/**
	 *
	 */
	private MainApp mainApp;

	/**
	 *
	 */
	private TherapyPlan therapyPlan;

	private TherapyAdapter therapyAdapter;
	private TherapistAdapter therapistAdapter;
	private TherapyRoomAdapter therapyRoomAdapter;

	/**
	 *
	 */
	private TherapyManager therapyManager;

	/**
	 *
	 */
	private TherapistManager therapistManager;

	/**
	 *
	 */
	private PatientManager patientManager;

	/**
	 *
	 */
	private TherapistConstraintManager therapistConstraintManager;

	private TherapyScheduleService therapyScheduleService;

	private EmailService emailService;

	/**
	 *
	 */
	private Stage stage;

	/**
	 *
	 */
	private Map<Day, List<TimeBlock>> timeBlocks;

	/**
	 *
	 */
	private ObservableList<TherapyEventAdapter> therapyEventData = FXCollections.observableArrayList();

	private String patientString = "Patient";
	private String therapistString = "Therapeut";
	private String roomString = "Raum";
	/**
	 *
	 */
	private ObservableList<String> typeChoices = FXCollections.observableArrayList(
			patientString, therapistString, roomString);

	/**
	 *
	 */
	private ObservableList<Object> unitChoices = FXCollections.observableArrayList();

	/**
	 * Provides logging functionality
	 */
	private Logger log = LogManager.getLogger(EventCreatorController.class.getName());

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		try {
			timeBlocks = therapistConstraintManager.findAllTimeBlocksGroupedByDay();
		} catch (ServiceException e) {
			Dialogs.create().title("Fehler beim Initialisieren der Zeitblöcke")
					.masthead(null)
					.message(e.getMessage())
					.showWarning();
		}

		cleanGrid();
		printAndSendButton.setDisable(true);
		// init with setTherapyPlanAdapter()


	}

	private void initTherapyEventTable(Object object) {
		cleanDetails();

		if(object == null)
			return;

		try{

			// listener to show therapy event details
			therapyEventTable
					.getSelectionModel()
					.selectedItemProperty()
					.addListener(
							(observable, oldValue, newValue) -> showTherapyEventDetails(newValue));

			// listener to paint selected therapy event red in grid
			therapyEventTable
					.getSelectionModel()
					.selectedItemProperty()
					.addListener(
							(observable, oldValue, newValue) -> highlineTherapyEvent(oldValue, newValue));

			// listener to show/hide the placeEvent buttons
			therapyEventTable
					.getSelectionModel()
					.selectedItemProperty()
					.addListener(
							(observable, oldValue, newValue) -> showPlaceEventButtons(newValue));

			cleanGrid();

			therapyEventData.clear();
			List<TherapyEvent> tEvents = null;

			// fills therapy event list for patient
			if(typeChoiceBox.getSelectionModel().getSelectedItem().equals(patientString)){
				TherapyAdapter ta = (TherapyAdapter)unitChoiceBox.getSelectionModel().getSelectedItem();
				tEvents = therapyManager.getTherapyEventsByTherapyPlanAndPatient(therapyPlan, ta.getEntity().getPatient());
			}
			// fills therapy event list for therapist
			else if(typeChoiceBox.getSelectionModel().getSelectedItem().equals(therapistString)){
				TherapistAdapter ta = (TherapistAdapter)unitChoiceBox.getSelectionModel().getSelectedItem();
				tEvents = therapyManager.getTherapyEventsByTherapyPlanAndTherapist(therapyPlan, ta.getEntity());
			}
			// fills therapy event list for room
			else if(typeChoiceBox.getSelectionModel().getSelectedItem().equals(roomString)){
				TherapyRoomAdapter tra = (TherapyRoomAdapter)unitChoiceBox.getSelectionModel().getSelectedItem();
				tEvents = therapyManager.getTherapyEventsByTherapyPlanAndTherapyRoom(therapyPlan, tra.getEntity());
			}

			// fills therapy event list with all data for testing purpose
			// tEvents = therapyManager.findAllTherapyEvents();
			TherapyEventAdapter therapyEventAdapter = null;

			for (TherapyEvent te : tEvents){
				TherapyEventAdapter tea = new TherapyEventAdapter(te);
				therapyEventData.add(tea);

				if(actualTherapyEvent != null && actualTherapyEvent.getId().equals(te.getId()))
					therapyEventAdapter = tea;

				// shows the therapy event in the grid
				placeTherapyEvent(te, false);
			}

			// Wrap the therapyEventData in a SortedList.
			SortedList<TherapyEventAdapter> sortedData = new SortedList<>(therapyEventData);

			// Bind the SortedList comparator to the TableView comparator.
			sortedData.comparatorProperty().bind(therapyEventTable.comparatorProperty());
			therapyEventTable.setItems(therapyEventData);
			if(therapyEventAdapter != null)
				therapyEventTable.getSelectionModel().select(therapyEventAdapter);

			actualTherapyEvent = null;

		} catch (ServiceException | ValidationException e) {
			Dialogs.create().title("Fehler beim Initialisieren der Therapieeinheitenübersicht")
					.masthead(null)
					.message(e.getMessage())
					.showWarning();
		}
	}

	private void initTypeChoiceBox() {

		typeChoiceBox.
				getSelectionModel().
				selectedItemProperty().
				addListener(
						(observable, oldValue, newValue) -> initUnitChoiceBox(newValue));

		// shows or hides button to send email
		typeChoiceBox
				.getSelectionModel()
				.selectedItemProperty()
				.addListener(
						(observable, oldValue, newValue) -> showOrHideEmailButton(newValue));


		typeChoiceBox.setItems(typeChoices);
		if(therapistAdapter != null)
			typeChoiceBox.setValue(typeChoices.get(1));
		else if(therapyRoomAdapter != null)
			typeChoiceBox.setValue(typeChoices.get(2));
		else
			typeChoiceBox.setValue(typeChoices.get(0));

	}

	private void showOrHideEmailButton(String type){
		if(type.equals(therapistString))
			printAndSendButton.setDisable(false);
		else
			printAndSendButton.setDisable(true);
	}

	private void initUnitChoiceBox(String typeSelection) {
		// clears the choice box
		unitChoiceBox.getItems().clear();
		unitChoices.clear();

		// sets listener to change therapy event table for selected unit
		unitChoiceBox.
				getSelectionModel().
				selectedItemProperty().
				addListener(
						(observable, oldValue, newValue) -> initTherapyEventTable(newValue));



		// inits unitChoiceBox with the choosen type of data
		// patients
		if (typeSelection.equals(patientString)){

			try {
				List<Therapy> lt = therapyManager.findAllTherapiesByTherapyPlan(therapyPlan);

				for (Therapy t : lt){
					TherapyAdapter ta = new TherapyAdapter(t);
					unitChoices.add(ta);
					if(therapyAdapter != null && t.getId().equals(therapyAdapter.getEntity().getId()))
						therapyAdapter = ta;
				}

				unitChoiceBox.setItems(unitChoices);
				if(therapyAdapter != null) {
					unitChoiceBox.setValue(therapyAdapter);
					therapyAdapter = null;
				}
				else if (unitChoices.size() > 0)
					unitChoiceBox.setValue(unitChoices.get(0));

			} catch (ServiceException e) {
				Dialogs.create().title("Fehler beim Initialisieren der Patientenauswahl")
						.masthead(null)
						.message(e.getMessage())
						.showWarning();
			}

		}
		// therapists
		else if (typeSelection.equals(therapistString)){

			try {
				List<Therapist> lt = therapistManager.findAllTherapistsByTherapyPlan(therapyPlan);

				for (Therapist t : lt){
					TherapistAdapter ta = new TherapistAdapter(t);
					unitChoices.add(ta);
					if(therapistAdapter != null && t.getId().equals(therapistAdapter.getEntity().getId()))
						therapistAdapter = ta;
				}

				unitChoiceBox.setItems(unitChoices);
				if(therapistAdapter != null) {
					unitChoiceBox.setValue(therapistAdapter);
					therapistAdapter = null;
				}
				else if (unitChoices.size() > 0)
					unitChoiceBox.setValue(unitChoices.get(0));
			} catch (ServiceException e) {
				Dialogs.create().title("Fehler beim Initialisieren der Therapeutenauswahl")
						.masthead(null)
						.message(e.getMessage())
						.showWarning();
			}

		}
		// rooms
		else if (typeSelection.equals(roomString))  {

			try {
				List<TherapyRoom> lt = therapyManager.findAllTherapyRooms();

				for (TherapyRoom tr : lt){
					TherapyRoomAdapter tra = new TherapyRoomAdapter(tr);
					unitChoices.add(tra);
					if(therapyRoomAdapter != null && tr.getId().equals(therapyRoomAdapter.getEntity().getId()))
						therapyRoomAdapter = tra;
				}

				unitChoiceBox.setItems(unitChoices);
				if(therapyRoomAdapter != null) {
					unitChoiceBox.setValue(therapyRoomAdapter);
					therapyRoomAdapter = null;
				}
				else if (unitChoices.size() > 0)
					unitChoiceBox.setValue(unitChoices.get(0));
			} catch (ServiceException e) {
				Dialogs.create().title("Fehler beim Initialisieren der Raumauswahl")
						.masthead(null)
						.message(e.getMessage())
						.showWarning();
			}

		}

	}

	/**
	 * prints the name of therapy event in the fitting slot if clean == false.
	 * cleans the therapy slot where the event takes place if clean == true.
	 *
	 * @param therapyEvent
	 * @param clean
	 */
	private void placeTherapyEvent(TherapyEvent therapyEvent, Boolean clean) {

		if(therapyEvent.getTherapyTimeBlock() == null)
			return;

		TimeBlock timeBlock = therapyEvent.getTherapyTimeBlock();

		DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("HH:mm");

		String startTime = dateTimeFormatter.print(timeBlock.getTimeStart());
		String therapyName = "";
		if(!clean)
			therapyName = therapyEvent.getName();

		if (timeBlock.getDay().equals(Day.MONDAY)){

			if (startTime.equals("09:30")) {
				mo1Label.setText(therapyName);
			} else if (startTime.equals("10:15")) {
				mo2Label.setText(therapyName);
			} else if (startTime.equals("11:10")) {
				mo3Label.setText(therapyName);
			} else if (startTime.equals("11:55")) {
				mo4Label.setText(therapyName);
			} else if (startTime.equals("16:30")) {
				mo5Label.setText(therapyName);
			} else if (startTime.equals("17:15")) {
				mo6Label.setText(therapyName);
			}

		} else if (timeBlock.getDay().equals(Day.TUESDAY)) {

			if (startTime.equals("09:30")) {
				di1Label.setText(therapyName);
			} else if (startTime.equals("10:15")) {
				di2Label.setText(therapyName);
			} else if (startTime.equals("11:10")) {
				di3Label.setText(therapyName);
			} else if (startTime.equals("11:55")) {
				di4Label.setText(therapyName);
			} else if (startTime.equals("16:30")) {
				di5Label.setText(therapyName);
			} else if (startTime.equals("17:15")) {
				di6Label.setText(therapyName);
			}

		} else if (timeBlock.getDay().equals(Day.WEDNESDAY)) {

			if (startTime.equals("09:30")) {
				mi1Label.setText(therapyName);
			} else if (startTime.equals("10:15")) {
				mi2Label.setText(therapyName);
			} else if (startTime.equals("11:10")) {
				mi3Label.setText(therapyName);
			} else if (startTime.equals("11:55")) {
				mi4Label.setText(therapyName);
			} else if (startTime.equals("16:30")) {
				mi5Label.setText(therapyName);
			} else if (startTime.equals("17:15")) {
				mi6Label.setText(therapyName);
			}

		} else if (timeBlock.getDay().equals(Day.THURSDAY)) {

			if (startTime.equals("09:30")) {
				do1Label.setText(therapyName);
			} else if (startTime.equals("10:15")) {
				do2Label.setText(therapyName);
			} else if (startTime.equals("11:10")) {
				do3Label.setText(therapyName);
			} else if (startTime.equals("11:55")) {
				do4Label.setText(therapyName);
			} else if (startTime.equals("16:30")) {
				do5Label.setText(therapyName);
			} else if (startTime.equals("17:15")) {
				do6Label.setText(therapyName);
			}

		} else if (timeBlock.getDay().equals(Day.FRIDAY)) {

			if (startTime.equals("09:30")) {
				fr1Label.setText(therapyName);
			} else if (startTime.equals("10:15")) {
				fr2Label.setText(therapyName);
			} else if (startTime.equals("11:10")) {
				fr3Label.setText(therapyName);
			} else if (startTime.equals("11:55")) {
				fr4Label.setText(therapyName);
			} else if (startTime.equals("16:30")) {
				fr5Label.setText(therapyName);
			} else if (startTime.equals("17:15")) {
				fr6Label.setText(therapyName);
			}

		}

	}

	private void showTherapyEventDetails(TherapyEventAdapter therapyEventAdapter) {

		if (therapyEventAdapter != null) {

			try {
				// therapist choice box
				detailsTherapistChoiceBox.getItems().clear();

				List<Therapist> lt = therapistConstraintManager.findAllTherapistsByTherapyType(therapyEventAdapter.getEntity().getTherapyType());

				therapistData.clear();
				Therapist therapist = null;
				if(therapyEventAdapter.getEntity().getTherapist() != null) {
					therapist = therapyEventAdapter.getEntity().getTherapist();
					therapistData.add(therapist);
				}

				if (lt.size() > 0) {

					for (Therapist t : lt) {
						if(!t.equals(therapist))
							therapistData.add(t);
					}

					detailsTherapistChoiceBox.setItems(therapistData);
					if(therapist != null)
						detailsTherapistChoiceBox.setValue(therapist);

				}

				// listener to save changes when therapist was changed
				// dont use now, class need changes before!
				/*
				detailsTherapistChoiceBox.
						getSelectionModel().
						selectedItemProperty().
						addListener(
								(observable, oldValue, newValue) -> handleSaveDetails());
								*/



				// room choice box
				detailsRoomChoiceBox.getItems().clear();

				List<TherapyRoom> ltr;
				if(therapyEventAdapter.getEntity().getTherapyTimeBlock() == null)
					ltr = therapyManager.findAllTherapyRoomsByTherapyEvent(therapyEventAdapter.getEntity());
				else
					ltr = therapyManager.findAllTherapyRoomsByTherapyEventAndTimeBlock(therapyEventAdapter.getEntity(), therapyEventAdapter.getEntity().getTherapyTimeBlock());

				therapyRoomData.clear();
				TherapyRoom therapyRoom = null;
				if(therapyEventAdapter.getEntity().getTherapyRoom() != null){
					therapyRoom = therapyEventAdapter.getEntity().getTherapyRoom();
					therapyRoomData.add(therapyRoom);
				}

				if(ltr.size() > 0){

					for (TherapyRoom tr : ltr){
						if(!tr.equals(therapyRoom))
							therapyRoomData.add(tr);
					}

					detailsRoomChoiceBox.setItems(therapyRoomData);
					if(therapyRoom != null)
						detailsRoomChoiceBox.setValue(therapyRoom);
				}

				// listener to save changes when therapy room was changed
				// dont use now, class need changes before!
				/*
				detailsRoomChoiceBox.
						getSelectionModel().
						selectedItemProperty().
						addListener(
								(observable, oldValue, newValue) -> handleSaveDetails());
								*/



				// therapy time
				if(therapyEventAdapter.getEntity().getTherapyTimeBlock() == null){
					detailsTimeLabel.setText("Noch kein Zeitslot zugewiesen");
				} else {
					TimeBlock timeBlock = therapyEventAdapter.getEntity().getTherapyTimeBlock();
					DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("HH:mm");
					String startTime = dateTimeFormatter.print(timeBlock.getTimeStart());
					String day = timeBlock.getDay().toString();
					String time = day + " " + startTime;
					detailsTimeLabel.setText(time);
				}


				// participants
				detailsParticipantsListView.getItems().clear();
				participantNames.clear();

				//List<Therapy> lTherapy = therapyEventAdapter.getEntity().getTherapies();
				List<Therapy> lTherapy = therapyManager.getParticipants(therapyEventAdapter.getEntity());
				for(Therapy t : lTherapy) {
					participantNames.add(t.getPatient().getLastName() + " " + t.getPatient().getFirstName());
				}

				detailsParticipantsListView.setItems(participantNames);


			} catch (ServiceException | ValidationException e) {
				Dialogs.create().title("Fehler beim Initialisieren der Therapieeventdetails")
						.masthead(null)
						.message(e.getMessage())
						.showWarning();
			}

		}
	}

	private void showPlaceEventButtons(TherapyEventAdapter therapyEventAdapter){

		try {
			List<TimeBlock> listTimeBlocksAvailable = therapyManager.findAllAvailableTimeBlocksByTherapyEvent(therapyEventAdapter.getEntity());

			//monday
			TimeBlock timeBlock = timeBlocks.get(Day.MONDAY).get(0);
			Boolean disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mo1SetButton.setDisable(true);
			else mo1SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.MONDAY).get(1);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mo2SetButton.setDisable(true);
			else mo2SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.MONDAY).get(2);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mo3SetButton.setDisable(true);
			else mo3SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.MONDAY).get(3);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mo4SetButton.setDisable(true);
			else mo4SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.MONDAY).get(4);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mo5SetButton.setDisable(true);
			else mo5SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.MONDAY).get(5);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mo6SetButton.setDisable(true);
			else mo6SetButton.setDisable(false);


			//tuesday
			timeBlock = timeBlocks.get(Day.TUESDAY).get(0);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) di1SetButton.setDisable(true);
			else di1SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.TUESDAY).get(1);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) di2SetButton.setDisable(true);
			else di2SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.TUESDAY).get(2);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) di3SetButton.setDisable(true);
			else di3SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.TUESDAY).get(3);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) di4SetButton.setDisable(true);
			else di4SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.TUESDAY).get(4);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) di5SetButton.setDisable(true);
			else di5SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.TUESDAY).get(5);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) di6SetButton.setDisable(true);
			else di6SetButton.setDisable(false);


			//wednesday
			timeBlock = timeBlocks.get(Day.WEDNESDAY).get(0);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mi1SetButton.setDisable(true);
			else mi1SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.WEDNESDAY).get(1);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mi2SetButton.setDisable(true);
			else mi2SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.WEDNESDAY).get(2);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mi3SetButton.setDisable(true);
			else mi3SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.WEDNESDAY).get(3);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mi4SetButton.setDisable(true);
			else mi4SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.WEDNESDAY).get(4);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mi5SetButton.setDisable(true);
			else mi5SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.WEDNESDAY).get(5);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) mi6SetButton.setDisable(true);
			else mi6SetButton.setDisable(false);


			//thursday
			timeBlock = timeBlocks.get(Day.THURSDAY).get(0);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) do1SetButton.setDisable(true);
			else do1SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.THURSDAY).get(1);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) do2SetButton.setDisable(true);
			else do2SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.THURSDAY).get(2);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) do3SetButton.setDisable(true);
			else do3SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.THURSDAY).get(3);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) do4SetButton.setDisable(true);
			else do4SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.THURSDAY).get(4);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) do5SetButton.setDisable(true);
			else do5SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.THURSDAY).get(5);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) do6SetButton.setDisable(true);
			else do6SetButton.setDisable(false);


			//friday
			timeBlock = timeBlocks.get(Day.FRIDAY).get(0);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) fr1SetButton.setDisable(true);
			else fr1SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.FRIDAY).get(1);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) fr2SetButton.setDisable(true);
			else fr2SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.FRIDAY).get(2);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) fr3SetButton.setDisable(true);
			else fr3SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.FRIDAY).get(3);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) fr4SetButton.setDisable(true);
			else fr4SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.FRIDAY).get(4);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) fr5SetButton.setDisable(true);
			else fr5SetButton.setDisable(false);

			timeBlock = timeBlocks.get(Day.FRIDAY).get(5);
			disable = buttonToDisable(timeBlock, listTimeBlocksAvailable);
			if(disable) fr6SetButton.setDisable(true);
			else fr6SetButton.setDisable(false);



		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (ValidationException e) {
			e.printStackTrace();
		}
	}

	/**
	 *
	 * @param timeBlockToCheck
	 * @param listTimeBlocksAvailable
	 * @return false if timeBlockToCheck should not be disabled, true if timeBlockToCheck should be disabled
	 */
	private boolean buttonToDisable(TimeBlock timeBlockToCheck, List<TimeBlock> listTimeBlocksAvailable){
		for(TimeBlock tb : listTimeBlocksAvailable){
			if(tb.equals(timeBlockToCheck))
				return false;
		}
		return true;
	}

	private void highlineTherapyEvent(TherapyEventAdapter oldValue, TherapyEventAdapter newValue) {

		if(newValue == null)
			return;

		TimeBlock timeBlock = newValue.getEntity().getTherapyTimeBlock();

		DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("HH:mm");

		String startTime = dateTimeFormatter.print(timeBlock.getTimeStart());

		if (timeBlock.getDay().equals(Day.MONDAY)){

			if (startTime.equals("09:30")) {
				mo1Label.setTextFill(Color.RED);
			} else if (startTime.equals("10:15")) {
				mo2Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:10")) {
				mo3Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:55")) {
				mo4Label.setTextFill(Color.RED);
			} else if (startTime.equals("16:30")) {
				mo5Label.setTextFill(Color.RED);
			} else if (startTime.equals("17:15")) {
				mo6Label.setTextFill(Color.RED);
			}

		} else if (timeBlock.getDay().equals(Day.TUESDAY)) {

			if (startTime.equals("09:30")) {
				di1Label.setTextFill(Color.RED);
			} else if (startTime.equals("10:15")) {
				di2Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:10")) {
				di3Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:55")) {
				di4Label.setTextFill(Color.RED);
			} else if (startTime.equals("16:30")) {
				di5Label.setTextFill(Color.RED);
			} else if (startTime.equals("17:15")) {
				di6Label.setTextFill(Color.RED);
			}

		} else if (timeBlock.getDay().equals(Day.WEDNESDAY)) {

			if (startTime.equals("09:30")) {
				mi1Label.setTextFill(Color.RED);
			} else if (startTime.equals("10:15")) {
				mi2Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:10")) {
				mi3Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:55")) {
				mi4Label.setTextFill(Color.RED);
			} else if (startTime.equals("16:30")) {
				mi5Label.setTextFill(Color.RED);
			} else if (startTime.equals("17:15")) {
				mi6Label.setTextFill(Color.RED);
			}

		} else if (timeBlock.getDay().equals(Day.THURSDAY)) {

			if (startTime.equals("09:30")) {
				do1Label.setTextFill(Color.RED);
			} else if (startTime.equals("10:15")) {
				do2Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:10")) {
				do3Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:55")) {
				do4Label.setTextFill(Color.RED);
			} else if (startTime.equals("16:30")) {
				do5Label.setTextFill(Color.RED);
			} else if (startTime.equals("17:15")) {
				do6Label.setTextFill(Color.RED);
			}

		} else if (timeBlock.getDay().equals(Day.FRIDAY)) {

			if (startTime.equals("09:30")) {
				fr1Label.setTextFill(Color.RED);
			} else if (startTime.equals("10:15")) {
				fr2Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:10")) {
				fr3Label.setTextFill(Color.RED);
			} else if (startTime.equals("11:55")) {
				fr4Label.setTextFill(Color.RED);
			} else if (startTime.equals("16:30")) {
				fr5Label.setTextFill(Color.RED);
			} else if (startTime.equals("17:15")) {
				fr6Label.setTextFill(Color.RED);
			}

		}


		if(oldValue == null)
			return;

		// set old value back to black
		timeBlock = oldValue.getEntity().getTherapyTimeBlock();

		startTime = dateTimeFormatter.print(timeBlock.getTimeStart());

		if (timeBlock.getDay().equals(Day.MONDAY)){

			if (startTime.equals("09:30")) {
				mo1Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("10:15")) {
				mo2Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:10")) {
				mo3Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:55")) {
				mo4Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("16:30")) {
				mo5Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("17:15")) {
				mo6Label.setTextFill(Color.BLACK);
			}

		} else if (timeBlock.getDay().equals(Day.TUESDAY)) {

			if (startTime.equals("09:30")) {
				di1Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("10:15")) {
				di2Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:10")) {
				di3Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:55")) {
				di4Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("16:30")) {
				di5Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("17:15")) {
				di6Label.setTextFill(Color.BLACK);
			}

		} else if (timeBlock.getDay().equals(Day.WEDNESDAY)) {

			if (startTime.equals("09:30")) {
				mi1Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("10:15")) {
				mi2Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:10")) {
				mi3Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:55")) {
				mi4Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("16:30")) {
				mi5Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("17:15")) {
				mi6Label.setTextFill(Color.BLACK);
			}

		} else if (timeBlock.getDay().equals(Day.THURSDAY)) {

			if (startTime.equals("09:30")) {
				do1Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("10:15")) {
				do2Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:10")) {
				do3Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:55")) {
				do4Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("16:30")) {
				do5Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("17:15")) {
				do6Label.setTextFill(Color.BLACK);
			}

		} else if (timeBlock.getDay().equals(Day.FRIDAY)) {

			if (startTime.equals("09:30")) {
				fr1Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("10:15")) {
				fr2Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:10")) {
				fr3Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("11:55")) {
				fr4Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("16:30")) {
				fr5Label.setTextFill(Color.BLACK);
			} else if (startTime.equals("17:15")) {
				fr6Label.setTextFill(Color.BLACK);
			}

		}

	}

	public void setTherapyManager(TherapyManager therapyManager) {
		this.therapyManager = therapyManager;
	}

	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	public void setTherapistManager(TherapistManager therapistManager) {
		this.therapistManager = therapistManager;
	}

	public void setPatientManager(PatientManager patientManager) {
		this.patientManager = patientManager;
	}

	public void setTherapistConstraintManager(TherapistConstraintManager therapistConstraintManager) {
		this.therapistConstraintManager = therapistConstraintManager;
	}

	public void setTherapyScheduleService(TherapyScheduleService therapyScheduleService) {
		this.therapyScheduleService = therapyScheduleService;
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
		this.stage.setMinWidth(1024);
		this.stage.setMinHeight(768);
	}

	public void setTherapyPlan(TherapyPlan therapyPlan) {

		this.therapyPlan = therapyPlan;

		// initialize the therapy event table
		eventColumn.setCellValueFactory(cellData -> cellData.getValue().getTherapyName());
		allocatedColumn.setCellValueFactory(cellData -> cellData.getValue().isAllocated());

		// Custom rendering of the table cell.
		allocatedColumn.setCellFactory(column -> {
			return new TableCell<TherapyEventAdapter, Boolean>() {
				@Override
				protected void updateItem(Boolean item, boolean empty) {
					super.updateItem(item, empty);

					if (item == null || empty) {
						setText(null);
						setStyle("");
					}
					else {
						setText(item ? "Ja" : "Nein");
					}
				}
			};
		});

		initTypeChoiceBox();
	}

	public void setTherapy(Therapy therapy) {
		if(therapy == null)
			this.therapyAdapter = null;
		else
			this.therapyAdapter = new TherapyAdapter(therapy);
	}

	public  void setTherapist(Therapist therapist) {
		if(therapist == null)
			this.therapistAdapter = null;
		else
			this.therapistAdapter = new TherapistAdapter(therapist);
	}

	public void setTherapyRoom(TherapyRoom therapyRoom) {
		if(therapyRoom == null)
			this.therapyRoomAdapter = null;
		else
			this.therapyRoomAdapter = new TherapyRoomAdapter(therapyRoom);
	}

	private void cleanDetails() {
		if(detailsTherapistChoiceBox.getItems() != null)
			detailsTherapistChoiceBox.getItems().clear();

		if(detailsRoomChoiceBox.getItems() != null)
			detailsRoomChoiceBox.getItems().clear();

		detailsTimeLabel.setText("");

		if(detailsParticipantsListView.getItems() != null)
			detailsParticipantsListView.getItems().clear();
	}

	private void cleanGrid() {
		mo1Label.setText(""); mo1Label.setTextFill(Color.BLACK);
		mo2Label.setText(""); mo2Label.setTextFill(Color.BLACK);
		mo3Label.setText(""); mo3Label.setTextFill(Color.BLACK);
		mo4Label.setText(""); mo4Label.setTextFill(Color.BLACK);
		mo5Label.setText(""); mo5Label.setTextFill(Color.BLACK);
		mo6Label.setText(""); mo6Label.setTextFill(Color.BLACK);

		di1Label.setText(""); di1Label.setTextFill(Color.BLACK);
		di2Label.setText(""); di2Label.setTextFill(Color.BLACK);
		di3Label.setText(""); di3Label.setTextFill(Color.BLACK);
		di4Label.setText(""); di4Label.setTextFill(Color.BLACK);
		di5Label.setText(""); di5Label.setTextFill(Color.BLACK);
		di6Label.setText(""); di6Label.setTextFill(Color.BLACK);

		mi1Label.setText(""); mi1Label.setTextFill(Color.BLACK);
		mi2Label.setText(""); mi2Label.setTextFill(Color.BLACK);
		mi3Label.setText(""); mi3Label.setTextFill(Color.BLACK);
		mi4Label.setText(""); mi4Label.setTextFill(Color.BLACK);
		mi5Label.setText(""); mi5Label.setTextFill(Color.BLACK);
		mi6Label.setText(""); mi6Label.setTextFill(Color.BLACK);

		do1Label.setText(""); do1Label.setTextFill(Color.BLACK);
		do2Label.setText(""); do2Label.setTextFill(Color.BLACK);
		do3Label.setText(""); do3Label.setTextFill(Color.BLACK);
		do4Label.setText(""); do4Label.setTextFill(Color.BLACK);
		do5Label.setText(""); do5Label.setTextFill(Color.BLACK);
		do6Label.setText(""); do6Label.setTextFill(Color.BLACK);

		fr1Label.setText(""); fr1Label.setTextFill(Color.BLACK);
		fr2Label.setText(""); fr2Label.setTextFill(Color.BLACK);
		fr3Label.setText(""); fr3Label.setTextFill(Color.BLACK);
		fr4Label.setText(""); fr4Label.setTextFill(Color.BLACK);
		fr5Label.setText(""); fr5Label.setTextFill(Color.BLACK);
		fr6Label.setText(""); fr6Label.setTextFill(Color.BLACK);

		mo1SetButton.setDisable(true);
		mo2SetButton.setDisable(true);
		mo3SetButton.setDisable(true);
		mo4SetButton.setDisable(true);
		mo5SetButton.setDisable(true);
		mo6SetButton.setDisable(true);

		di1SetButton.setDisable(true);
		di2SetButton.setDisable(true);
		di3SetButton.setDisable(true);
		di4SetButton.setDisable(true);
		di5SetButton.setDisable(true);
		di6SetButton.setDisable(true);

		mi1SetButton.setDisable(true);
		mi2SetButton.setDisable(true);
		mi3SetButton.setDisable(true);
		mi4SetButton.setDisable(true);
		mi5SetButton.setDisable(true);
		mi6SetButton.setDisable(true);

		do1SetButton.setDisable(true);
		do2SetButton.setDisable(true);
		do3SetButton.setDisable(true);
		do4SetButton.setDisable(true);
		do5SetButton.setDisable(true);
		do6SetButton.setDisable(true);

		fr1SetButton.setDisable(true);
		fr2SetButton.setDisable(true);
		fr3SetButton.setDisable(true);
		fr4SetButton.setDisable(true);
		fr5SetButton.setDisable(true);
		fr6SetButton.setDisable(true);
	}


	private void assignTimeBlockToTherapyEvent(TimeBlock timeBlock){
		try {

			if(therapyEventTable.getFocusModel().getFocusedItem() == null) {
				Dialogs.create()
						.owner(stage)
						.title("Fehler")
						.masthead(null)
						.message("Es wurde kein Event zum zuteilen ausgewählt!")
						.showWarning();
				return;
			}


			TherapyEvent therapyEvent = therapyEventTable.getFocusModel().getFocusedItem().getEntity();
			List<TimeBlock> listTimeBlock = therapyManager.findAllAvailableTimeBlocksByTherapyEvent(therapyEvent);

			// checks if the chosen time block is available for this event
			Boolean containsTimeBlock = false;
			for(TimeBlock tb : listTimeBlock){
				if(tb.equals(timeBlock))
					containsTimeBlock = true;
			}

			if(!containsTimeBlock){
				Dialogs.create()
						.owner(stage)
						.title("Fehler")
						.masthead(null)
						.message("Der ausgewählte Zeitslot ist für dieses Event leider nicht verfügbar!")
						.showWarning();
				return;
			}



			// clean old slot of therapy event in plan
			placeTherapyEvent(therapyEvent, true);

			// assign new time block to therapy event
			therapyEvent.setTherapyTimeBlock(timeBlock);

			// assign dateTime from time block to therapy event
			DateTime dateTime = DateTimeConverter.dateTimeFromTherapyPlanAndTimeBlock(therapyPlan, timeBlock);
			therapyEvent.setTherapyTime(dateTime);

			// persists therapy event
			therapyManager.update(therapyEvent);

			// shows therapy event in plan
			placeTherapyEvent(therapyEvent, false);

			Object unit = unitChoiceBox.getSelectionModel().getSelectedItem();
			actualTherapyEvent = therapyEvent;
			initTherapyEventTable(unit);

		} catch (ServiceException | ValidationException e) {
			Dialogs.create().title("Fehler beim Initialisieren der Therapieeventdetails")
					.masthead(null)
					.message(e.getMessage())
					.showWarning();
		}


	}

	@FXML
	public void handleSetMo1(){
		TimeBlock timeBlock = timeBlocks.get(Day.MONDAY).get(0);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMo2(){
		TimeBlock timeBlock = timeBlocks.get(Day.MONDAY).get(1);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMo3(){
		TimeBlock timeBlock = timeBlocks.get(Day.MONDAY).get(2);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMo4(){
		TimeBlock timeBlock = timeBlocks.get(Day.MONDAY).get(3);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMo5(){
		TimeBlock timeBlock = timeBlocks.get(Day.MONDAY).get(4);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMo6(){
		TimeBlock timeBlock = timeBlocks.get(Day.MONDAY).get(5);
		assignTimeBlockToTherapyEvent(timeBlock);
	}

	@FXML
	public void handleSetDi1(){
		TimeBlock timeBlock = timeBlocks.get(Day.TUESDAY).get(0);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDi2(){
		TimeBlock timeBlock = timeBlocks.get(Day.TUESDAY).get(1);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDi3(){
		TimeBlock timeBlock = timeBlocks.get(Day.TUESDAY).get(2);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDi4(){
		TimeBlock timeBlock = timeBlocks.get(Day.TUESDAY).get(3);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDi5(){
		TimeBlock timeBlock = timeBlocks.get(Day.TUESDAY).get(4);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDi6(){
		TimeBlock timeBlock = timeBlocks.get(Day.TUESDAY).get(5);
		assignTimeBlockToTherapyEvent(timeBlock);
	}

	@FXML
	public void handleSetMi1(){
		TimeBlock timeBlock = timeBlocks.get(Day.WEDNESDAY).get(0);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMi2(){
		TimeBlock timeBlock = timeBlocks.get(Day.WEDNESDAY).get(1);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMi3(){
		TimeBlock timeBlock = timeBlocks.get(Day.WEDNESDAY).get(2);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMi4(){
		TimeBlock timeBlock = timeBlocks.get(Day.WEDNESDAY).get(3);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMi5(){
		TimeBlock timeBlock = timeBlocks.get(Day.WEDNESDAY).get(4);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetMi6(){
		TimeBlock timeBlock = timeBlocks.get(Day.WEDNESDAY).get(5);
		assignTimeBlockToTherapyEvent(timeBlock);
	}

	@FXML
	public void handleSetDo1(){
		TimeBlock timeBlock = timeBlocks.get(Day.THURSDAY).get(0);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDo2(){
		TimeBlock timeBlock = timeBlocks.get(Day.THURSDAY).get(1);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDo3(){
		TimeBlock timeBlock = timeBlocks.get(Day.THURSDAY).get(2);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDo4(){
		TimeBlock timeBlock = timeBlocks.get(Day.THURSDAY).get(3);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDo5(){
		TimeBlock timeBlock = timeBlocks.get(Day.THURSDAY).get(4);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetDo6(){
		TimeBlock timeBlock = timeBlocks.get(Day.THURSDAY).get(5);
		assignTimeBlockToTherapyEvent(timeBlock);
	}

	@FXML
	public void handleSetFr1(){
		TimeBlock timeBlock = timeBlocks.get(Day.FRIDAY).get(0);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetFr2(){
		TimeBlock timeBlock = timeBlocks.get(Day.FRIDAY).get(1);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetFr3(){
		TimeBlock timeBlock = timeBlocks.get(Day.FRIDAY).get(2);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetFr4(){
		TimeBlock timeBlock = timeBlocks.get(Day.FRIDAY).get(3);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetFr5(){
		TimeBlock timeBlock = timeBlocks.get(Day.FRIDAY).get(4);
		assignTimeBlockToTherapyEvent(timeBlock);
	}
	@FXML
	public void handleSetFr6(){
		TimeBlock timeBlock = timeBlocks.get(Day.FRIDAY).get(5);
		assignTimeBlockToTherapyEvent(timeBlock);
	}


	@FXML
	private void handleSaveDetails() {
		if(therapyEventTable.getFocusModel().getFocusedItem() == null){
			Dialogs.create().title("Informationsdialog")
					.masthead(null)
					.message("Kein Therapieevent ausgewählt")
					.showWarning();
			return;
		}
		TherapyEvent therapyEvent = therapyEventTable.getFocusModel().getFocusedItem().getEntity();

		// set new therapist
		if(detailsTherapistChoiceBox.getSelectionModel().getSelectedItem() != null) {
			Therapist therapist = detailsTherapistChoiceBox.getSelectionModel().getSelectedItem();
			therapyEvent.setTherapist(therapist);
		}

		// set new therapy room
		if(detailsRoomChoiceBox.getSelectionModel().getSelectedItem() != null) {
			TherapyRoom therapyRoom = detailsRoomChoiceBox.getSelectionModel().getSelectedItem();
			therapyEvent.setTherapyRoom(therapyRoom);
		}

		try {
			therapyManager.update(therapyEvent);
		} catch (ServiceException | ValidationException e) {
			Dialogs.create().title("Fehler beim Speichern der Therapieeventdetails")
					.masthead(null)
					.message(e.getMessage())
					.showWarning();
			return;
		}

		Object unit = null;
		actualTherapyEvent = therapyEvent;
		if(unitChoiceBox.getSelectionModel().getSelectedItem() != null)
			unit = unitChoiceBox.getSelectionModel().getSelectedItem();
		initTherapyEventTable(unit);
		Dialogs.create().title("Informationsdialog")
				.masthead(null)
				.message("Änderungen erfolgreich gespeichert")
				.showInformation();
	}

	@FXML
	public void handleScheduleAutomatically() {
		log.debug("schedule TherapyEvents automatically");
		try {
			List<TherapyEvent> therapyEvents = therapyScheduleService.schedule(therapyPlan);
			for (TherapyEvent therapyEvent: therapyEvents){
				therapyManager.update(therapyEvent);
			}
			initTherapyEventTable(unitChoiceBox.getSelectionModel().getSelectedItem());
		} catch (ServiceException | PlanningException | ValidationException e) {
			Dialogs.create().title("Fehler beim automatischen Planen der Therapieeinheiten")
					.masthead(null)
					.message(e.getMessage())
					.showWarning();
			return;
		}
		Dialogs.create().title("Informationsdialog")
				.masthead(null)
				.message("Therapien erfolgreich eingeteilt!")
				.showInformation();
	}

    @FXML
    public void handlePrint() {
        String typeSelection = typeChoiceBox.getValue();

        List<TherapyEventAdapter> adapterList = new ArrayList<>(therapyEventTable.getItems());
        List<TherapyEvent> outputList = new ArrayList<>();
        for (TherapyEventAdapter adapter : adapterList) {
            outputList.add(adapter.getEntity());
        }

        String outputPath;
        String pdfTitle;

        // Output PDF of TherapyPlan for Patient
        if (typeSelection.equals(patientString)) {
            TherapyAdapter patient = (TherapyAdapter) unitChoiceBox.getSelectionModel().getSelectedItem();
            String patientName = patient.getEntity().getPatient().getLastName() + ", " + patient.getEntity().getPatient().getFirstName();

            outputPath = System.getProperty("user.home") + "/" + patientName + "_" + "Wochenplan_" + therapyPlan.getDateFrom().getWeekOfWeekyear() + ".pdf";
            pdfTitle = patientName + " " + therapyPlan.getDateFrom().getDayOfMonth() + "." + therapyPlan.getDateFrom().getMonthOfYear() + " - " + therapyPlan.getDateUntil().getDayOfMonth() + "." + therapyPlan.getDateUntil().getMonthOfYear() + "." + therapyPlan.getDateFrom().getYearOfCentury();

            try {
                // Generate PDF
                PdfPlanGenerator.generatePDF(pdfTitle, PdfPlanDateConverter.convert(outputList, 1), outputPath);
                Desktop desktop = Desktop.getDesktop();
                // Open the user's home directory to let user delete the new file easier
                desktop.open(new File(System.getProperty("user.home")));
                // Open the file so the user can work with the file further
                desktop.open(new File(outputPath));
            } catch (PdfException | IOException e) {
                log.error(e.getMessage());
                Dialogs.create().title("Fehler beim Erstellen der PDF")
                        .masthead(null)
                        .message("Die PDF konnte nicht erstellt werden, bitte versuchen Sie es erneut.")
                        .showWarning();
            }
        }
        // Output PDF of TherapyPlan for Therapist
        else if (typeSelection.equals(therapistString)) {
            TherapistAdapter therapist = (TherapistAdapter) unitChoiceBox.getSelectionModel().getSelectedItem();
            String therapistName = therapist.getEntity().getLastName() + ", " + therapist.getEntity().getFirstName();

            outputPath = System.getProperty("user.home") + "/" + therapistName + "_" + "Wochenplan_" + therapyPlan.getDateFrom().getWeekOfWeekyear() + ".pdf";
            pdfTitle = therapistName + " " + therapyPlan.getDateFrom().getDayOfMonth() + "." + therapyPlan.getDateFrom().getMonthOfYear() + " - " + therapyPlan.getDateUntil().getDayOfMonth() + "." + therapyPlan.getDateUntil().getMonthOfYear() + "." + therapyPlan.getDateFrom().getYearOfCentury();

            try {
                // Generate PDF
                PdfPlanGenerator.generatePDF(pdfTitle, PdfPlanDateConverter.convert(outputList, 1), outputPath);
                Desktop desktop = Desktop.getDesktop();
                // Open the user's home directory to let user delete the new file easier
                desktop.open(new File(System.getProperty("user.home")));
                // Open the file so the user can work with the file further
                desktop.open(new File(outputPath));
            } catch (PdfException | IOException e) {
                log.error(e.getMessage());
                Dialogs.create().title("Fehler beim Erstellen der PDF")
                        .masthead(null)
                        .message("Die PDF konnte nicht erstellt werden, bitte versuchen Sie es erneut.")
                        .showWarning();
            }
        }
        // Output PDF of TherapyPlan for TherapyRoom
        else if (typeSelection.equals(roomString)) {
            TherapyRoomAdapter therapyRoom = (TherapyRoomAdapter) unitChoiceBox.getSelectionModel().getSelectedItem();
            String roomName = therapyRoom.getEntity().getName();

            outputPath = System.getProperty("user.home") + "/" + roomName + "_" + "Wochenplan_" + therapyPlan.getDateFrom().getWeekOfWeekyear() + ".pdf";
            pdfTitle = roomName + " " + therapyPlan.getDateFrom().getDayOfMonth() + "." + therapyPlan.getDateFrom().getMonthOfYear() + " - " + therapyPlan.getDateUntil().getDayOfMonth() + "." + therapyPlan.getDateUntil().getMonthOfYear() + "." + therapyPlan.getDateFrom().getYearOfCentury();

            try {
                // Generate PDF
                PdfPlanGenerator.generatePDF(pdfTitle, PdfPlanDateConverter.convert(outputList, 1), outputPath);
                Desktop desktop = Desktop.getDesktop();
                // Open the user's home directory to let user delete the new file easier
                desktop.open(new File(System.getProperty("user.home")));
                // Open the file so the user can work with the file further
                desktop.open(new File(outputPath));
            } catch (PdfException | IOException e) {
                log.error(e.getMessage());
                Dialogs.create().title("Fehler beim Erstellen der PDF")
                        .masthead(null)
                        .message("Die PDF konnte nicht erstellt werden, bitte versuchen Sie es erneut.")
                        .showWarning();
            }
        }
    }

    @FXML
    public void handlePrintAndSend() {
        String typeSelection = typeChoiceBox.getValue();

        List<TherapyEventAdapter> adapterList = new ArrayList<>(therapyEventTable.getItems());
        List<TherapyEvent> outputList = new ArrayList<>();
        for (TherapyEventAdapter adapter : adapterList) {
            outputList.add(adapter.getEntity());
        }

        String outputPath;
        String pdfTitle;
        String email;

        // Output PDF of TherapyPlan for Patient
        if (typeSelection.equals(patientString)) {
            TherapyAdapter patient = (TherapyAdapter) unitChoiceBox.getSelectionModel().getSelectedItem();
            String patientName = patient.getEntity().getPatient().getLastName() + ", " + patient.getEntity().getPatient().getFirstName();

            email = patient.getEntity().getPatient().getEmail();
            outputPath = System.getProperty("user.home") + "/" + patientName + "_" + "Wochenplan_" + therapyPlan.getDateFrom().getWeekOfWeekyear() + ".pdf";
            pdfTitle = patientName + " " + therapyPlan.getDateFrom().getDayOfMonth() + "." + therapyPlan.getDateFrom().getMonthOfYear() + " - " + therapyPlan.getDateUntil().getDayOfMonth() + "." + therapyPlan.getDateUntil().getMonthOfYear() + "." + therapyPlan.getDateFrom().getYearOfCentury();

            // Generate PDF
            try {
                PdfPlanGenerator.generatePDF(pdfTitle, PdfPlanDateConverter.convert(outputList, 1), outputPath);
                Desktop desktop = Desktop.getDesktop();
                // Open the user's home directory to let user delete the new file easier
                desktop.open(new File(System.getProperty("user.home")));
                // Open the file so the user can work with the file further
                desktop.open(new File(outputPath));
            } catch (PdfException | IOException e) {
                log.error(e.getMessage());
				Dialogs.create().title("Fehler beim Erstellen der PDF")
						.masthead(null)
						.message("Die PDF konnte nicht erstellt werden, bitte versuchen Sie es erneut.")
                        .showWarning();
            }
            // Send the email
            try {
                if (email != null) {
                    log.debug("Trying to send an email to: " + email);
                    emailService.sendTherapyPlan(email, therapyPlan.getDateFrom().getWeekOfWeekyear(), outputPath);
                }
                else {
                    log.error("Email address was null");
                    Dialogs.create().title("Fehler beim Versenden der PDF")
                            .masthead(null)
                            .message("Email kann nicht versendet werden, da keine Email-Adresse vorhanden ist!")
                            .showWarning();
                }
            } catch (MessagingException | MailSendException e) {
                log.error(e.getMessage());
				Dialogs.create().title("Fehler beim Versenden der PDF")
						.masthead(null)
						.message("Email kann zur Zeit nicht versendet werden, bitte überprüfen Sie Ihre Verbindung und versuchen Sie es erneut.")
                        .showWarning();
            }

        }
        // Output PDF of TherapyPlan for Therapist
        else if (typeSelection.equals(therapistString)) {
            TherapistAdapter therapist = (TherapistAdapter) unitChoiceBox.getSelectionModel().getSelectedItem();
            String therapistName = therapist.getEntity().getLastName() + ", " + therapist.getEntity().getFirstName();

            email = therapist.getEntity().getEmail();
            outputPath = System.getProperty("user.home") + "/" + therapistName + "_" + "Wochenplan_" + therapyPlan.getDateFrom().getWeekOfWeekyear() + ".pdf";
            pdfTitle = therapistName + " " + therapyPlan.getDateFrom().getDayOfMonth() + "." + therapyPlan.getDateFrom().getMonthOfYear() + " - " + therapyPlan.getDateUntil().getDayOfMonth() + "." + therapyPlan.getDateUntil().getMonthOfYear() + "." + therapyPlan.getDateFrom().getYearOfCentury();

            // Generate PDF
            try {
                PdfPlanGenerator.generatePDF(pdfTitle, PdfPlanDateConverter.convert(outputList, 2), outputPath);
                Desktop desktop = Desktop.getDesktop();
                // Open the user's home directory to let user delete the new file easier
                desktop.open(new File(System.getProperty("user.home")));
                // Open the file so the user can work with the file further
                desktop.open(new File(outputPath));
            } catch (PdfException | IOException e) {
                log.error(e.getMessage());
				Dialogs.create().title("Fehler beim Erstellen der PDF")
						.masthead(null)
						.message("Die PDF konnte nicht erstellt werden, bitte versuchen Sie es erneut.")
                        .showWarning();
            }
            // Send the email
            try {
                if (email != null) {
                    emailService.sendTherapyPlan(email, therapyPlan.getDateFrom().getWeekOfWeekyear(), outputPath);
                }
                else {
                    log.error("Email address was null");
                    Dialogs.create().title("Fehler beim Versenden der PDF")
                            .masthead(null)
                            .message("Email kann nicht versendet werden, da keine Email-Adresse vorhanden ist!")
                            .showWarning();
                }
            } catch (MessagingException | MailSendException e) {
                log.error(e.getMessage());
                Dialogs.create().title("Fehler beim Versenden der PDF")
                        .masthead(null)
                        .message("Email kann zur Zeit nicht versendet werden, bitte überprüfen Sie Ihre Verbindung und versuchen Sie es erneut.")
                        .showWarning();
            }
        }
    }

}
