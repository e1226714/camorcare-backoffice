package ch.camorcare.backoffice.presentation.view;

import ch.camorcare.backoffice.adapter.TherapyAdapter;
import ch.camorcare.backoffice.presentation.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for layout
 */
public class LayoutController implements Initializable {

    private MainApp mainApp;

    private BorderPane layout;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setLayout(BorderPane layout) {
        this.layout = layout;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Show overview and select item.
     *
     * @param therapyAdapter
     */
    @FXML
    public void handleShowTherapyOverview(TherapyAdapter therapyAdapter) {
        SplitPane screen = (SplitPane) mainApp.getLoader().load("/fxml/therapy/overview.fxml");
        AnchorPane anchorPane = (AnchorPane) screen.getItems().get(0);

        for(Node node : anchorPane.getChildren()) {
            if(node.getId() != null && node.getId().equals("therapyTable")) {
                TableView tableView = (TableView) node;
                tableView.getSelectionModel().select(therapyAdapter);
            }
        }

        layout.setCenter(screen);
    }

    @FXML
    private void handleShowTherapyOverview() {
        mainApp.showTherapyOverview();
    }

    @FXML
    private void handleShowTherapyPlanOverview() {
        mainApp.showTherapyPlanOverview();
    }

    @FXML
    private void handleShowDutyCategoryOverview() {
        mainApp.showDutyCategoryOverview();
    }

    @FXML
    private void handleShowPatientOverview() {
        mainApp.showPatientOverview();
    }

    @FXML
    private void handleShowTherapistOverview() {
        mainApp.showTherapistOverview();
    }

    @FXML
    private void handleShowTherapyRoomOverview() {
        mainApp.showTherapyRoomOverview();
    }

    @FXML
    private void handleShowTherapyTypeOverview() {
        mainApp.showTherapyTypeOverview();
    }

    /**
     * Closes the application.
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }
}