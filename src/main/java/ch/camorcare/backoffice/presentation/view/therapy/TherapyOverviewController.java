package ch.camorcare.backoffice.presentation.view.therapy;

import ch.camorcare.backoffice.adapter.TherapyAdapter;
import ch.camorcare.backoffice.adapter.TherapyTypeAdapter;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.presentation.MainApp;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.PatientManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;
import org.joda.time.DateTime;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the Therapy overview
 */
@SuppressWarnings("deprecation")
public class TherapyOverviewController implements Initializable {

    @FXML
    private TableView<TherapyAdapter> therapyTable;

    @FXML
    private TableColumn<TherapyAdapter, DateTime> columnDateFrom;

    @FXML
    private TableColumn<TherapyAdapter, DateTime> columnDateUntil;

    @FXML
    private TableColumn<TherapyAdapter, String> columnPatient;

    @FXML
    private Label labelPatientName;

    @FXML
    private Label labelDateSickleave;

    @FXML
    private Label labelDateFrom;

    @FXML
    private Label labelDateUntil;

    @FXML
    private TextField filterField;

    /**
     *
     */
    private TherapyManager therapyManager;

    /**
     *
     */
    private PatientManager patientManager;

    /**
     *
     */
    private MainApp mainApp;

    /**
     *
     */
    private ObservableList<TherapyAdapter> therapyData = FXCollections.observableArrayList();

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        columnDateFrom.setCellValueFactory(cellData -> cellData.getValue().getDateFromProperty());
        columnDateUntil.setCellValueFactory(cellData -> cellData.getValue().getDateUntilProperty());
        columnPatient.setCellValueFactory(cellData -> cellData.getValue().getPatientAdapter().getNameProperty());

        columnDateFrom.setCellFactory(column -> {
            return new TableCell<TherapyAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(DateTimeConverter.dateTimeToString(item));
                    }
                }
            };
        });

        columnDateUntil.setCellFactory(column -> {
            return new TableCell<TherapyAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(DateTimeConverter.dateTimeToString(item));
                    }
                }
            };
        });

        showDetails(null);

        // Listen for selection changes and show the patient details when changed.
        therapyTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> showDetails(newValue));

        initTable();

    }

    private void initTable() {
        try {
            therapyData.clear();
            List<Therapy> therapies = therapyManager.findAllTherapies();

            for (Therapy therapy : therapies) {
                therapy.setPatient(patientManager.findOnePatientById(therapy.getPatient()));
                therapyData.add(new TherapyAdapter(therapy));
            }

            therapyTable.setItems(therapyData);

            // Wrap the ObservableList in a FilteredList (initially display all data).
            FilteredList<TherapyAdapter> filteredData = new FilteredList<>(therapyData, p -> true);

            // Set the filter Predicate whenever the filter changes.
            filterField
                .textProperty()
                .addListener((observable, oldValue, newValue) -> {
                    filteredData.setPredicate(therapy -> {
                        // If filter text is empty, display all therapys.
                        if (newValue == null || newValue.isEmpty()) {
                            return true;
                        }

                        // Compare first and last name of every therapy with filter text.
                        String lowerCaseFilter = newValue.toLowerCase();

                        if(therapy.getPatientAdapter().getNameProperty().get().toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }

                        if(DateTimeConverter.dateTimeToString(therapy.getDateFromProperty().get()).toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }

                        if(therapy.getDateUntilProperty().get() != null) {
                            if (DateTimeConverter.dateTimeToString(therapy.getDateUntilProperty().get()).toLowerCase().contains(lowerCaseFilter)) {
                                return true;
                            }
                        }

                        return false;
                    });
                });

            // Wrap the FilteredList in a SortedList.
            SortedList<TherapyAdapter> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(therapyTable.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            therapyTable.setItems(sortedData);

        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapieübersicht")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    /**
     * Fills all text fields to show details about the therapy. If the specified
     * therapy is null, all text fields are cleared.
     *
     * @param therapy the therapy or null
     */
    private void showDetails(TherapyAdapter therapy) {
        if (therapy != null) {
            labelPatientName.setText(therapy.getPatientAdapter().getNameProperty().getValue());
            labelDateSickleave.setText(DateTimeConverter.dateTimeToString(therapy.getEntity().getDateSickLeave()));
            labelDateFrom.setText(DateTimeConverter.dateTimeToString(therapy.getEntity().getDateFrom()));
            labelDateUntil.setText(DateTimeConverter.dateTimeToString(therapy.getEntity().getDateUntil()));
        } else {
            labelPatientName.setText("");
            labelDateSickleave.setText("");
            labelDateFrom.setText("");
            labelDateUntil.setText("");
        }
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new therapy.
     */
    @FXML
    private void handleCreate() {
        Therapy tempTherapy = new Therapy();
        boolean okClicked = mainApp.showTherapyUpdateDialog(new TherapyAdapter(tempTherapy));

        if (okClicked) {
            try {
                tempTherapy = therapyManager.create(tempTherapy);
                therapyData.add(new TherapyAdapter(tempTherapy));
                initTable();
            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Anlegen einer Therapie")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected patient.
     */
    @FXML
    private void handleUpdate() {
        TherapyAdapter ta = therapyTable.getSelectionModel().getSelectedItem();

        if (ta != null) {
            boolean okClicked = mainApp.showTherapyUpdateDialog(ta);

            if (okClicked) {
                try {
                    if(hasTimeOverlap(ta, therapyData)) {
                        Dialogs.create()
                                .title("Ungültige Eingabe")
                                .masthead("Terminüberlappung")
                                .message("Für diesen Zeitraum wurde bereits eine Therapie gebucht.")
                                .showWarning();
                    } else {
                        therapyManager.update(ta.getEntity());
                    }

                    showDetails(ta);
                    initTable();
                } catch (ServiceException | ValidationException e) {
                    Dialogs.create().title("Fehler beim Aktualisieren einer Therapie")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            Dialogs.create()
                .title("Ungültige Auswahl")
                .masthead("Keine Therapie ausgewählt")
                .message("Bitte wählen Sie einen Therapieeintrag aus der Tabelle aus.")
                .showWarning();
        }
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDelete() {
        int selectedIndex = therapyTable.getSelectionModel()
                .getSelectedIndex();
        if (selectedIndex >= 0) {
            Action response = Dialogs.create()
                    .title("Bestätigungsdialog")
                    .masthead("Therapie löschen")
                    .message("Möchten Sie diese Therapie wirklich löschen?")
                    .actions(org.controlsfx.dialog.Dialog.ACTION_YES, org.controlsfx.dialog.Dialog.ACTION_NO)
                    .showConfirm();
            if (response == org.controlsfx.dialog.Dialog.ACTION_YES) {
                try {
                    therapyManager.delete(therapyData.get(selectedIndex).getEntity());
                    therapyData.remove(selectedIndex);

                    Dialogs.create()
                            .title("Informationsdialog")
                            .masthead(null)
                            .message("Therapie gelöscht!")
                            .showInformation();
                } catch (ServiceException e) {
                    Dialogs.create().title("Fehler beim Löschen einer Therapie")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            Dialogs.create()
                    .title("Ungültige Auswahl")
                    .masthead("Keine Therapie ausgewählt")
                    .message("Bitte wählen Sie eine Therapie aus der Tabelle aus.")
                    .showWarning();
        }
    }

    @FXML
    public void handlePrintBill() {
        TherapyAdapter ta = therapyTable.getSelectionModel().getSelectedItem();
        mainApp.showPrintBill(ta);
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setPatientManager(PatientManager patientManager) {
        this.patientManager = patientManager;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    private boolean hasTimeOverlap(TherapyAdapter therapyAdapter, List<TherapyAdapter> therapyAdapters) {
        for(TherapyAdapter ta : therapyAdapters) {
            if(therapyAdapter.getEntity().getId() != null && therapyAdapter.getEntity().getId().equals(ta.getEntity().getId())) {
                continue;
            }

            if(therapyAdapter.getEntity().getDateFrom().equals(ta.getEntity().getDateFrom())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateFrom().equals(ta.getEntity().getDateUntil())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateUntil().equals(ta.getEntity().getDateFrom())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateUntil().equals(ta.getEntity().getDateUntil())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateFrom().isAfter(ta.getEntity().getDateFrom()) && therapyAdapter.getEntity().getDateFrom().isBefore(ta.getEntity().getDateUntil())) {
                return true;
            }

            if(therapyAdapter.getEntity().getDateUntil().isAfter(ta.getEntity().getDateFrom()) && therapyAdapter.getEntity().getDateUntil().isBefore(ta.getEntity().getDateUntil())) {
                return true;
            }
        }

        return false;
    }
}
