package ch.camorcare.backoffice.presentation.view.patient.therapyTypeOccurrenceConstraint;

import ch.camorcare.backoffice.adapter.PatientAdapter;
import ch.camorcare.backoffice.adapter.PatientBasedTherapyTypeOccurrenceConstraintAdapter;
import ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.PatientConstraintManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.stage.Stage;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the TherapistBasedTherapyTypeOccurrenceConstraint update dialog window
 */
public class PatientBasedTherapyTypeOccurrenceConstraintUpdateDialogController implements Initializable {
    private static final int MAX_TIME_BLOCKS = 30;

    @FXML
    private TableView<PatientBasedTherapyTypeOccurrenceConstraintAdapter> therapyTypeTable;
    private ObservableList<PatientBasedTherapyTypeOccurrenceConstraintAdapter> therapyTypeTableData = FXCollections.observableArrayList();
    private ObservableList<TherapyType> therapyTypes = FXCollections.observableArrayList();

    @FXML
    private TableColumn<PatientBasedTherapyTypeOccurrenceConstraintAdapter, String> therapyTypeTableTypeColumn;

    @FXML
    private TableColumn<PatientBasedTherapyTypeOccurrenceConstraintAdapter, TherapyMode> therapyTypeTableModeColumn;

    @FXML
    private TableColumn<PatientBasedTherapyTypeOccurrenceConstraintAdapter, Number> therapyTypeTableOccurrenceColumn;

    @FXML
    private ComboBox<TherapyType> therapyTypeCombo;

    @FXML
    private ComboBox<TherapyMode> therapyModeCombo;

    @FXML
    private ComboBox<Number> occurrenceCombo;

    @FXML
    private Label occurrenceSumLabel;

    private Stage dialogStage;

    private TherapyManager therapyManager;

    private PatientAdapter patientAdapter;

    private PatientConstraintManager patientConstraintManager;

    private Integer timeBlockCounter;

    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        therapyTypeTableTypeColumn.setCellValueFactory(cellData -> cellData.getValue().getTherapyTypeAdapter().getNameProperty());
        therapyTypeTableModeColumn.setCellValueFactory(cellData -> cellData.getValue().getTherapyModeProperty());
        therapyTypeTableOccurrenceColumn.setCellValueFactory(cellData -> cellData.getValue().getMaxOccurrencePerWeekProperty());

        ObservableList<Number> timeSlots = FXCollections.observableArrayList();
        timeSlots.add(1);
        timeSlots.add(2);
        timeSlots.add(3);
        timeSlots.add(4);
        timeSlots.add(5);

        therapyTypeTableOccurrenceColumn.setCellFactory(ComboBoxTableCell.forTableColumn(timeSlots));
        occurrenceCombo.setItems(timeSlots);
        occurrenceCombo.getSelectionModel().select(4);

        therapyTypeTableOccurrenceColumn.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<PatientBasedTherapyTypeOccurrenceConstraintAdapter, Number>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<PatientBasedTherapyTypeOccurrenceConstraintAdapter, Number> t) {
                        ((PatientBasedTherapyTypeOccurrenceConstraintAdapter) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setMaxOccurrencePerWeekProperty((Integer) t.getNewValue());
                    }
                }
        );

        ObservableList<TherapyMode> therapyModes = FXCollections.observableArrayList();
        therapyModes.add(TherapyMode.SINGLE);
        therapyModes.add(TherapyMode.GROUP);
        therapyTypeTableModeColumn.setCellFactory(ComboBoxTableCell.forTableColumn(therapyModes));
        therapyModeCombo.setItems(therapyModes);
        therapyModeCombo.getSelectionModel().select(0);

        therapyTypeTableModeColumn.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<PatientBasedTherapyTypeOccurrenceConstraintAdapter, TherapyMode>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<PatientBasedTherapyTypeOccurrenceConstraintAdapter, TherapyMode> t) {
                        ((PatientBasedTherapyTypeOccurrenceConstraintAdapter) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setTherapyModeProperty(t.getNewValue());
                    }
                }
        );

        therapyTypeCombo.setButtonCell(new ListCell<TherapyType>() {
            @Override
            protected void updateItem(TherapyType item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText("");
                }
                else {
                    setText(item.getName());
                }
            }
        });

        therapyTypeCombo.setCellFactory(cell -> {
            return new ComboBoxListCell<TherapyType>() {
                @Override
                public void updateItem(TherapyType item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    }
                    else {
                        setText(item.getName());
                    }
                }
            };
        });

        therapyTypeCombo.valueProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
                if (newValue != null) {
                    therapyModeCombo.disableProperty().setValue(false);
                    ObservableList<TherapyMode> modes = FXCollections.observableArrayList();

                    if (therapyTypeCombo.getValue().isSuitableForIndividuals()) {
                        modes.add(TherapyMode.SINGLE);
                    }

                    if (therapyTypeCombo.getValue().isSuitableForGroups()) {
                        modes.add(TherapyMode.GROUP);
                    }

                    therapyModeCombo.getItems().clear();
                    therapyModeCombo.setItems(modes);

                    if (therapyModeCombo.getItems().size() > 0) {
                        therapyModeCombo.getSelectionModel().select(0);
                        therapyModeCombo.getSelectionModel().select(0);
                        occurrenceCombo.getSelectionModel().select(4);
                    }

                }
                else {
                    therapyModeCombo.getSelectionModel().clearSelection();
                    therapyModeCombo.disableProperty().setValue(true);
                    occurrenceCombo.getSelectionModel().clearSelection();
                    occurrenceCombo.disableProperty().setValue(true);
                }
            }
        });

        try {
            List<TherapyType> allTherapyTypes = therapyManager.findAllTherapyTypes();

            for (TherapyType t : allTherapyTypes) {
                therapyTypes.add(t);
            }
            therapyTypeCombo.setItems(therapyTypes);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        therapyTypeCombo.getSelectionModel().select(0);
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(375);
    }

    /**
     * @param patientAdapter
     */
    public void setPatientAdapter(PatientAdapter patientAdapter) {
        this.patientAdapter = patientAdapter;
        this.timeBlockCounter = 0;

        try {
            therapyTypeTableData.clear();

            for (PatientBasedTherapyTypeOccurrenceConstraint constraint : patientConstraintManager.findAllPatientBasedTherapyTypeOccurrenceConstraintsByPatient(patientAdapter.getEntity())) {
                timeBlockCounter += constraint.getMaxOccurencePerWeek();
                therapyTypeTableData.add(new PatientBasedTherapyTypeOccurrenceConstraintAdapter(constraint));
            }

            therapyTypeTable.setItems(therapyTypeTableData);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        updateTimeBlockCounter();
    }

    private void updateTimeBlockCounter() {
        String text = timeBlockCounter.toString() + " / " + MAX_TIME_BLOCKS;
        occurrenceSumLabel.setText(text);
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleAdd() {
        boolean exists = false;

        PatientBasedTherapyTypeOccurrenceConstraint constraint = new PatientBasedTherapyTypeOccurrenceConstraint();
        constraint.setPatient(patientAdapter.getEntity());
        constraint.setTherapyType(therapyTypeCombo.getSelectionModel().getSelectedItem());
        constraint.setTherapyMode(therapyModeCombo.getSelectionModel().getSelectedItem());
        constraint.setMaxOccurencePerWeek((Integer) occurrenceCombo.getSelectionModel().getSelectedItem());

        for(PatientBasedTherapyTypeOccurrenceConstraintAdapter adapter : therapyTypeTableData) {
            if(constraint.getTherapyType().getId().equals(adapter.getEntity().getTherapyType().getId())) {
                if(constraint.getTherapyMode().equals(adapter.getEntity().getTherapyMode())) {
                    exists = true;
                    break;
                }
            }
        }

        if(exists) {
            Dialogs.create()
                .title("Ungültige Eingabe")
                .masthead("Diese Therapieform wurde bereits zugeteilt")
                .message("Bitte überprüfen Sie Ihre Auswahl.")
                .showWarning();
        } else {
            if((this.timeBlockCounter + constraint.getMaxOccurencePerWeek()) > MAX_TIME_BLOCKS) {
                Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Wochenpensum zu hoch")
                    .message("Das gewählte Wochenpensum würde das Gesamtpensum übersteigen.")
                    .showWarning();
            } else {
                this.timeBlockCounter += constraint.getMaxOccurencePerWeek();
                updateTimeBlockCounter();

                therapyTypeTableData.add(new PatientBasedTherapyTypeOccurrenceConstraintAdapter(constraint));
            }
        }
    }

    @FXML
    private void handleDelete() {
        int selectedIndex = therapyTypeTable.getSelectionModel().getSelectedIndex();

        if (selectedIndex >= 0) {
            Action response = Dialogs.create()
                    .title("Bestätigungsdialog")
                    .masthead("Eintrag löschen")
                    .message("Möchten Sie diesen Eintrag wirklich löschen?")
                    .actions(org.controlsfx.dialog.Dialog.ACTION_YES, org.controlsfx.dialog.Dialog.ACTION_NO)
                    .showConfirm();
            if (response == org.controlsfx.dialog.Dialog.ACTION_YES) {
                this.timeBlockCounter -= therapyTypeTable.getSelectionModel().getSelectedItem().getMaxOccurrencePerWeekProperty().get();
                updateTimeBlockCounter();

                therapyTypeTableData.remove(selectedIndex);

                Dialogs.create()
                        .title("Informationsdialog")
                        .masthead(null)
                        .message("Eintrag gelöscht!")
                        .showInformation();
            }
        }
        else {
            Dialogs.create()
                    .title("Ungültige Auswahl")
                    .masthead("Kein Eintrag ausgewählt")
                    .message("Bitte wählen Sie einen Eintrag aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        try {
            List<PatientBasedTherapyTypeOccurrenceConstraint> constraints = new ArrayList<>();
            for (PatientBasedTherapyTypeOccurrenceConstraintAdapter constraint : therapyTypeTableData) {
                constraints.add(constraint.getEntity());
            }
            patientConstraintManager.createTherapyTypeOccurrenceConstraints(constraints, patientAdapter.getEntity());
        } catch (ServiceException | ValidationException e) {
            e.printStackTrace();
        }

        okClicked = true;
        dialogStage.close();
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setPatientConstraintManager(PatientConstraintManager patientConstraintManager) {
        this.patientConstraintManager = patientConstraintManager;
    }
}


