package ch.camorcare.backoffice.presentation.view.dutyCategory;

import ch.camorcare.backoffice.adapter.DutyCategoryAdapter;
import ch.camorcare.backoffice.entities.DutyCategory;
import ch.camorcare.backoffice.presentation.MainApp;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.DutyManager;
import ch.camorcare.backoffice.util.pdftools.PdfDutyCategoryConverter;
import ch.camorcare.backoffice.util.pdftools.PdfHouseServiceGenerator;
import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.*;
import org.joda.time.DateTime;

import java.awt.*;
import java.awt.Dialog;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the DutyCategory overview
 */
@SuppressWarnings("deprecation")
public class DutyCategoryOverviewController implements Initializable {

    @FXML
    private TableView<DutyCategoryAdapter> dutyCategoryTable;
    @FXML
    private TableColumn<DutyCategoryAdapter, String> nameColumn;
    @FXML
    private TableColumn<DutyCategoryAdapter, Number> durationColumn;

    @FXML
    private Label nameLabel;
    @FXML
    private Label durationLabel;

    @FXML
    private TextField filterField;

    /**
     * Therapy Manager.
     */
    private DutyManager dutyManager;

    private MainApp mainApp;
    private ObservableList<DutyCategoryAdapter> dutyCategoryData = FXCollections.observableArrayList();

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public DutyManager getDutyManager() {
        return dutyManager;
    }

    public void setDutyManager(DutyManager dutyManager) {
        this.dutyManager = dutyManager;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize the duty category table with the two columns.
        nameColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getNameProperty());
        durationColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getDurationProperty());

        // Clear duty category details.
        showDutyCategoryDetails(null);

        // Listen for selection changes and show the duty category details when changed.
        dutyCategoryTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showDutyCategoryDetails(newValue));

        // Fills duty category table with data
        InitDutyCategoryTable();
    }

    private void InitDutyCategoryTable() {
        try {
            dutyCategoryData.clear();
            List<DutyCategory> dutyCategories = dutyManager.findAllDutyCategories();

            for (DutyCategory dc : dutyCategories) {
                dutyCategoryData.add(new DutyCategoryAdapter(dc));
            }

            dutyCategoryTable.setItems(dutyCategoryData);

            // Wrap the ObservableList in a FilteredList (initially display all
            // data).
            FilteredList<DutyCategoryAdapter> filteredData = new FilteredList<>(dutyCategoryData, p -> true);

            // Set the filter Predicate whenever the filter changes.
            filterField
                    .textProperty()
                    .addListener(
                            (observable, oldValue, newValue) -> {
                                filteredData.setPredicate(dutyCategory -> {
                                    // If filter text is empty, display all therapy types.
                                    if (newValue == null || newValue.isEmpty()) {
                                        return true;
                                    }

                                    // Compare name and duration of every duty category with filter text.
                                    String lowerCaseFilter = newValue.toLowerCase();

                                    if (dutyCategory.getNameProperty().get().toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;
                                    }
                                    else if (dutyCategory.getDurationProperty().getValue().toString().toLowerCase()
                                            .contains(lowerCaseFilter)) {
                                        return true;
                                    }
                                    return false; // Does not match.
                                });
                            }
                    );

            // Wrap the FilteredList in a SortedList.
            SortedList<DutyCategoryAdapter> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(dutyCategoryTable.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            dutyCategoryTable.setItems(sortedData);
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Hausdienstkategorien")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    /**
     * Fills all text fields to show details about the duty category. If the specified
     * duty category is null, all text fields are cleared.
     *
     * @param dutyCategory the duty category or null
     */
    private void showDutyCategoryDetails(DutyCategoryAdapter dutyCategory) {
        if (dutyCategory != null) {
            nameLabel.setText(dutyCategory.getNameProperty().get());
            durationLabel.setText(dutyCategory.getDurationProperty().getValue().toString());
        }
        else {
            // duty category is null, remove all the text.
            nameLabel.setText("");
            durationLabel.setText("");
        }
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new duty category.
     */
    @FXML
    private void handleCreate() {
        DutyCategory tempDutyCategory = new DutyCategory();
        boolean okClicked = mainApp.showDutyCategoryEditDialog(new DutyCategoryAdapter(tempDutyCategory));
        if (okClicked) {
            try {
                tempDutyCategory = dutyManager.create(tempDutyCategory);
                DutyCategoryAdapter adapter = new DutyCategoryAdapter(tempDutyCategory);
                dutyCategoryData.add(adapter);
                dutyCategoryTable.getSelectionModel().select(adapter);
            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Anlegen einer Hausdienstkategorie")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected duty category.
     */
    @FXML
    private void handleUpdate() {
        DutyCategoryAdapter dca = dutyCategoryTable.getSelectionModel().getSelectedItem();
        if (dca != null) {
            boolean okClicked = mainApp.showDutyCategoryEditDialog(dca);
            if (okClicked) {
                try {
                    showDutyCategoryDetails(dca);
                    dutyManager.update(dca.getEntity());
                } catch (ServiceException | ValidationException e) {
                    Dialogs.create().title("Fehler beim Aktualisieren einer Hausdienstkategorie")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        }
        else {
            // Nothing selected.
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Keinen Hausdienst ausgewählt")
                    .message("Bitte wählen Sie einen Hausdienst aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDelete() {
        int selectedIndex = dutyCategoryTable.getSelectionModel()
                .getSelectedIndex();
        if (selectedIndex >= 0) {
            Action response = Dialogs.create().title("Bestätigungsdialog")
                    .masthead("Hausdienst löschen")
                    .message("Möchten Sie diesen Hausdienst wirklich löschen?")
                    .actions(org.controlsfx.dialog.Dialog.ACTION_YES, org.controlsfx.dialog.Dialog.ACTION_NO)
                    .showConfirm();
            if (response == org.controlsfx.dialog.Dialog.ACTION_YES) {
                try {
                    dutyManager.delete(dutyCategoryData
                            .get(selectedIndex).getEntity());
                    dutyCategoryData.remove(selectedIndex);
                    Dialogs.create().title("Informationsdialog")
                            .masthead(null).message("Hausdienst gelöscht!")
                            .showInformation();
                } catch (ServiceException e) {
                    Dialogs.create().title("Fehler beim Löschen einer Hausdienstkategorie")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        }
        else {
            Dialogs.create().title("Ungültige Auswahl")
                    .masthead("Keinen Hausdienst ausgewählt")
                    .message("Bitte wählen Sie einen Hausdienst aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when a user presses the button "Hausdienstplan generieren". This saves the generated PDF to the hard drive
     * in the user's home directory, opens the folder and opens the file for further use.
     */
    @FXML
    private void handlePrintDutyServices() {
        try {
            List<DutyCategory> findAll = dutyManager.findAllDutyCategories();

            DateTime today = new DateTime();
            String outputPath = System.getProperty("user.home") + "/Hausdienstplan.PDF";

            PdfHouseServiceGenerator.generatePDF(today, PdfDutyCategoryConverter.convert(findAll, today, true), outputPath);
            Desktop desktop = Desktop.getDesktop();
            // Open the user's home directory to let user delete the new file easier
            desktop.open(new File(System.getProperty("user.home")));
            // Open the file so the user can work with the file further
            desktop.open(new File(outputPath));
        } catch (ServiceException | IOException | PdfException e) {
            Dialogs.create().title("Fehler").masthead("Ein Fehler ist beim Drucken entstanden").message(e.getMessage()).showWarning();
        }
    }
}
