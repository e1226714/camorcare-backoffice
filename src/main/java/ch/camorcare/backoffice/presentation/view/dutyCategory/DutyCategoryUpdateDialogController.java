package ch.camorcare.backoffice.presentation.view.dutyCategory;

import ch.camorcare.backoffice.adapter.DutyCategoryAdapter;
import ch.camorcare.backoffice.util.validator.entities.DutyCategoryValidator;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the DutyCategory update dialog window
 */
@SuppressWarnings("deprecation")
public class DutyCategoryUpdateDialogController implements Initializable {

    @FXML
    private TextField nameField;
    @FXML
    private TextField durationField;

    private Stage dialogStage;
    private DutyCategoryAdapter dutyCategory;
    private DutyCategoryValidator dutyCategoryValidator;
    private boolean okClicked = false;

    public void setDutyCategoryValidator(DutyCategoryValidator dutyCategoryValidator) {
        this.dutyCategoryValidator = dutyCategoryValidator;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(125);
    }

    /**
     * Sets the duty category to be edited in the dialog.
     *
     * @param dutyCategory
     */
    public void setDutyCategory(DutyCategoryAdapter dutyCategory) {
        this.dutyCategory = dutyCategory;

        nameField.setText(dutyCategory.getNameProperty().getValue());
        durationField.setText(Integer.toString(dutyCategory.getDurationProperty().getValue()));
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            dutyCategory.setName(nameField.getText());
            dutyCategory.setDuration(Integer.parseInt(durationField.getText()));

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (nameField.getText() == null || nameField.getText().length() == 0) {
            errorMessage += "Ungültiger Name!\n";
        }

        if (durationField.getText() == null || durationField.getText().length() == 0) {
            errorMessage += "Ungültige Dauer!\n";
        } else {
            try {
                Integer.parseInt(durationField.getText());
                if (Integer.parseInt(durationField.getText()) <= 0) {
                    errorMessage += "Ungültige Dauer (muss größer als 0 sein)!\n";
                }
            } catch (NumberFormatException e) {
                errorMessage += "Ungültige Dauer (nur ganze Zahlen sind erlaubt)!\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Dialogs.create()
                    .title("Ungültige Eingabe")
                    .masthead("Bitte korrigieren Sie ihre Eingabe")
                    .message(errorMessage)
                    .showError();
            return false;
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
}
