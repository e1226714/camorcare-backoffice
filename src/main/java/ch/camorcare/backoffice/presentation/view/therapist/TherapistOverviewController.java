package ch.camorcare.backoffice.presentation.view.therapist;

import ch.camorcare.backoffice.adapter.TherapistAdapter;
import ch.camorcare.backoffice.adapter.TherapistBasedTherapyTypeOccurrenceConstraintAdapter;
import ch.camorcare.backoffice.adapter.TherapistBasedTimeBlockConstraintAdapter;
import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.presentation.MainApp;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.TherapistConstraintManager;
import ch.camorcare.backoffice.service.impl.TherapistManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.*;
import org.joda.time.DateTime;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for the Therapist overview
 */
@SuppressWarnings("deprecation")
public class TherapistOverviewController implements Initializable {

    @FXML
    private TableView<TherapistAdapter> therapistTable;
    private ObservableList<TherapistAdapter> therapistData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TherapistAdapter, String> firstNameColumn;

    @FXML
    private TableColumn<TherapistAdapter, String> lastNameColumn;

    @FXML
    private TableView<TherapistBasedTimeBlockConstraintAdapter> therapistTimeBlockTable;
    private ObservableList<TherapistBasedTimeBlockConstraintAdapter> therapistTimeBlockTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TherapistBasedTimeBlockConstraintAdapter, String> therapistTimeBlockTableColumnDay;

    @FXML
    private TableColumn<TherapistBasedTimeBlockConstraintAdapter, DateTime> therapistTimeBlockTableColumnTimes;

    @FXML
    private TableView<TherapistBasedTherapyTypeOccurrenceConstraintAdapter> therapistTherapyTypeTable;
    private ObservableList<TherapistBasedTherapyTypeOccurrenceConstraintAdapter> therapistTherapyTypeTableData = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, String> therapistTherapyTypeTableTypeColumn;

    @FXML
    private TableColumn<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, TherapyMode> therapistTherapyTypeTableModeColumn;

    @FXML
    private TableColumn<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, Number> therapistTherapyTypeTableMaxPerDayColumn;

    @FXML
    private Label firstNameLabel;

    @FXML
    private Label lastNameLabel;

    @FXML
    private Label genderLabel;

    @FXML
    private Label emailLabel;

    @FXML
    private TextField filterField;

    private TherapyManager therapyManager;

    private TherapistManager therapistManager;

    private TherapistConstraintManager therapistConstraintManager;

    private MainApp mainApp;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().getFirstNameProperty());
        lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().getLastNameProperty());

        therapistTimeBlockTableColumnDay.setCellValueFactory(cellData -> cellData.getValue().getTimeBlockAdapter().getDayProperty());
        therapistTimeBlockTableColumnTimes.setCellValueFactory(cellData -> cellData.getValue().getTimeBlockAdapter().getTimeStartProperty());

        therapistTimeBlockTableColumnTimes.setCellFactory(column -> {
            return new TableCell<TherapistBasedTimeBlockConstraintAdapter, DateTime>() {
                @Override
                protected void updateItem(DateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(DateTimeConverter.timeToString(item));
                    }
                }
            };
        });

        therapistTherapyTypeTableTypeColumn.setCellValueFactory(cellData -> cellData.getValue().getTherapyTypeAdapter().getNameProperty());
        therapistTherapyTypeTableModeColumn.setCellValueFactory(cellData -> cellData.getValue().getTherapyModeProperty());
        therapistTherapyTypeTableMaxPerDayColumn.setCellValueFactory(cellData -> cellData.getValue().getMaxOccurrencePerDayProperty());

        therapistTherapyTypeTableModeColumn.setCellFactory(column -> {
            return new TableCell<TherapistBasedTherapyTypeOccurrenceConstraintAdapter, TherapyMode>() {
                @Override
                protected void updateItem(TherapyMode item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        if (item == TherapyMode.GROUP) {
                            setText("Gruppentherapie");
                        } else {
                            setText("Einzeltherapie");
                        }
                    }
                }
            };
        });

        showDetails(null);

        // Listen for selection changes and show the therapist details when changed.
        therapistTable
            .getSelectionModel()
            .selectedItemProperty()
            .addListener((observable, oldValue, newValue) -> showDetails(newValue));

        initTable();
    }

    private void initTable() {
        try {
            therapistData.clear();
            List<Therapist> therapists = therapistManager.findAllTherapists();

            for (Therapist therapist : therapists) {
                therapistData.add(new TherapistAdapter(therapist));
            }

            therapistTable.setItems(therapistData);

            // Wrap the ObservableList in a FilteredList (initially display all data).
            FilteredList<TherapistAdapter> filteredData = new FilteredList<>(therapistData, p -> true);

            // Set the filter Predicate whenever the filter changes.
            filterField
                    .textProperty()
                    .addListener(
                            (observable, oldValue, newValue) -> {
                                filteredData.setPredicate(therapist -> {
                                    // If filter text is empty, display all therapists.
                                    if (newValue == null || newValue.isEmpty()) {
                                        return true;
                                    }

                                    // Compare first and last name of every therapist with filter text.
                                    String lowerCaseFilter = newValue.toLowerCase();

                                    if (therapist.getFirstNameProperty().get().toLowerCase().contains(lowerCaseFilter)) {
                                        return true;
                                    } else if (therapist.getLastNameProperty().get().toLowerCase().contains(lowerCaseFilter)) {
                                        return true;
                                    }

                                    return false;
                                });
                            }
                    );

            // Wrap the FilteredList in a SortedList.
            SortedList<TherapistAdapter> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(therapistTable.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            therapistTable.setItems(sortedData);
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapeutenübersicht")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    /**
     * Fills all text fields to show details about the therapist. If the specified
     * therapist is null, all text fields are cleared.
     *
     * @param therapist the therapist or null
     */
    private void showDetails(TherapistAdapter therapist) {
        if (therapist != null) {
            firstNameLabel.setText(therapist.getFirstNameProperty().get());
            lastNameLabel.setText(therapist.getLastNameProperty().get());
            emailLabel.setText(therapist.getEmailProperty().get());

            if (therapist.getEntity().getGender() == null || therapist.getEntity().getGender() == Gender.MALE) {
                genderLabel.setText("Männlich");
            } else {
                genderLabel.setText("Weiblich");
            }

            /**
             * Write time block information.
             */
            try {
                therapistTimeBlockTableData.clear();

                for (TherapistBasedTimeBlockConstraint constraint : therapistConstraintManager.findAllTherapistBasedTimeBlockConstraintsByTherapist(therapist.getEntity())) {
                    therapistTimeBlockTableData.add(new TherapistBasedTimeBlockConstraintAdapter(constraint));
                }

                therapistTimeBlockTable.setItems(therapistTimeBlockTableData);
            } catch (ServiceException e) {
                Dialogs.create().title("Fehler beim Initialisieren der Arbeitszeiten")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }

            /**
             * Write therapy type information.
             */
            try {
                therapistTherapyTypeTableData.clear();

                for(TherapistBasedTherapyTypeOccurrenceConstraint constraint : therapistConstraintManager.findAllTherapistBasedTherapyTypeOccurrenceConstraintsByTherapist(therapist.getEntity())) {
                    constraint.setTherapyType(therapyManager.findTherapyTypeById(constraint.getTherapyType()));
                    therapistTherapyTypeTableData.add(new TherapistBasedTherapyTypeOccurrenceConstraintAdapter(constraint));
                }

                therapistTherapyTypeTable.setItems(therapistTherapyTypeTableData);
            } catch (ServiceException e) {
                Dialogs.create().title("Fehler beim Initialisieren der Therapietypen")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }

        } else {
            firstNameLabel.setText("");
            lastNameLabel.setText("");
            genderLabel.setText("");
            emailLabel.setText("");
        }
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new therapist.
     */
    @FXML
    private void handleCreate() {
        Therapist tempTherapist = new Therapist();
        boolean okClicked = mainApp.showTherapistEditDialog(new TherapistAdapter(tempTherapist));

        if (okClicked) {
            try {
                tempTherapist = therapistManager.create(tempTherapist);
                TherapistAdapter adapter = new TherapistAdapter(tempTherapist);
                therapistData.add(adapter);
                therapistTable.getSelectionModel().select(adapter);
            } catch (ServiceException | ValidationException e) {
                Dialogs.create().title("Fehler beim Hinzufügen eines Therapeuten")
                        .masthead(null)
                        .message(e.getMessage())
                        .showWarning();
            }
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected therapist.
     */
    @FXML
    private void handleUpdate() {
        TherapistAdapter ta = therapistTable.getSelectionModel().getSelectedItem();

        if (ta != null) {
            boolean okClicked = mainApp.showTherapistEditDialog(ta);

            if (okClicked) {
                try {
                    showDetails(ta);
                    therapistManager.update(ta.getEntity());
                } catch (ServiceException | ValidationException e) {
                    Dialogs.create().title("Fehler beim Aktualisieren eines Therapeuten")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            Dialogs.create()
                .title("Ungültige Auswahl")
                .masthead("Kein Therapeut ausgewählt")
                .message("Bitte wählen Sie einen Therapeut aus der Tabelle aus.")
                .showWarning();
        }
    }

    /**
     *
     */
    @FXML
    private void handleUpdateTherapyTypeOccurrenceConstraint() {
        TherapistAdapter ta = therapistTable.getSelectionModel().getSelectedItem();

        if (ta != null) {
            mainApp.showTherapistBasedTherapyTypeOccurrenceConstraintUpdateDialog(ta);
            showDetails(ta);
        } else {
            Dialogs.create()
                    .title("Ungültige Auswahl")
                    .masthead("Kein Therapeut ausgewählt")
                    .message("Bitte wählen Sie einen Therapeut aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     *
     */
    @FXML
    private void handleUpdateTimeBlockConstraint() {
        TherapistAdapter ta = therapistTable.getSelectionModel().getSelectedItem();

        if (ta != null) {
            mainApp.showTherapistBasedTimeBlockConstraintEditDialog(ta);
            showDetails(ta);
        } else {
            Dialogs.create()
                    .title("Ungültige Auswahl")
                    .masthead("Kein Therapeut ausgewählt")
                    .message("Bitte wählen Sie einen Therapeut aus der Tabelle aus.")
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDelete() {
        int selectedIndex = therapistTable.getSelectionModel()
                .getSelectedIndex();
        if (selectedIndex >= 0) {
            Action response = Dialogs.create()
                    .title("Bestätigungsdialog")
                    .masthead("Therapeut löschen")
                    .message("Möchten Sie diesen Therapeut wirklich löschen?")
                    .actions(org.controlsfx.dialog.Dialog.ACTION_YES, org.controlsfx.dialog.Dialog.ACTION_NO)
                    .showConfirm();
            if (response == org.controlsfx.dialog.Dialog.ACTION_YES) {
                try {
                    therapistManager.delete(therapistData.get(selectedIndex).getEntity());
                    therapistData.remove(selectedIndex);

                    Dialogs.create()
                        .title("Informationsdialog")
                        .masthead(null)
                        .message("Therapeut gelöscht!")
                        .showInformation();
                } catch (ServiceException e) {
                    Dialogs.create().title("Fehler beim Löschen eines Therapeuten")
                            .masthead(null)
                            .message(e.getMessage())
                            .showWarning();
                }
            }
        } else {
            Dialogs.create()
                .title("Ungültige Auswahl")
                .masthead("Kein Therapeut ausgewählt")
                .message("Bitte wählen Sie einen Therapeut aus der Tabelle aus.")
                .showWarning();
        }
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setTherapistManager(TherapistManager therapistManager) {
        this.therapistManager = therapistManager;
    }

    public void setTherapistConstraintManager(TherapistConstraintManager therapistConstraintManager) {
        this.therapistConstraintManager = therapistConstraintManager;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
}
