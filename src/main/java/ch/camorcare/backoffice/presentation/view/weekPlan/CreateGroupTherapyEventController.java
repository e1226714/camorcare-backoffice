package ch.camorcare.backoffice.presentation.view.weekPlan;

import ch.camorcare.backoffice.adapter.TherapyEventAdapter;
import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.service.TherapyScheduleService;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.TherapistConstraintManager;
import ch.camorcare.backoffice.service.impl.TherapistManager;
import ch.camorcare.backoffice.service.impl.TherapyManager;
import ch.camorcare.backoffice.util.validator.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for creating a group therapy
 */
public class CreateGroupTherapyEventController implements Initializable {

    @FXML
    private ChoiceBox<TherapyType> therapyTypeChoiceBox;
    private ObservableList<TherapyType> therapyTypeData = FXCollections.observableArrayList();
    @FXML
    private ChoiceBox<TherapyRoom> therapyRoomChoiceBox;
    private ObservableList<TherapyRoom> therapyRoomData = FXCollections.observableArrayList();
    @FXML
    private ChoiceBox<Therapist> therapistChoiceBox;
    private ObservableList<Therapist> therapistData = FXCollections.observableArrayList();

    private Stage dialogStage;
    private TherapyPlan therapyPlan;
    private boolean okClicked = false;

    /**
     *
     */
    private TherapyManager therapyManager;

    /**
     *
     */
    private TherapistManager therapistManager;

    /**
     *
     */
    private TherapistConstraintManager therapistConstraintManager;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setMinWidth(600);
        this.dialogStage.setMinHeight(170);
    }

    public void setTherapyPlan(TherapyPlan therapyPlan) {

        this.therapyPlan = therapyPlan;

        initTherapyTypeChoiceBox(null);
        initTherapyRoomChoiceBox();

    }

    private void initTherapyTypeChoiceBox(Therapist therapist) {
        try {
            // clears the choice box
            therapyTypeChoiceBox.getItems().clear();

            // fill the choice box with data
            List<TherapyType> ltt = new ArrayList<>();

            if (therapist == null) {
                List<TherapyType> temp = therapyManager.findAllTherapyTypes();
                for(TherapyType therapyType : temp) {
                    if(therapyType.isSuitableForGroups()) {
                        ltt.add(therapyType);
                    }
                }
            } else {
                List<TherapyType> temp = therapyManager.findAllTherapyTypesByTherapist(therapist);
                for(TherapyType therapyType : temp) {
                    if(therapyType.isSuitableForGroups()) {
                        ltt.add(therapyType);
                    }
                }
            }

            if (ltt.size() > 0) {

                therapyTypeChoiceBox.
                        getSelectionModel().
                        selectedItemProperty().
                        addListener(
                                (observable, oldValue, newValue) -> initTherapistChoiceBox(newValue));

                for (TherapyType tt : ltt) {
                    therapyTypeData.add(tt);
                }

                therapyTypeChoiceBox.setItems(therapyTypeData);
                therapyTypeChoiceBox.setValue(therapyTypeData.get(0));

                // inits therapist choice box for given therapist
                initTherapistChoiceBox(therapyTypeData.get(0));

            }

        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapietypauswahl")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

    }

    private void initTherapistChoiceBox(TherapyType therapyType) {

        try {
            // clears the choice box
            therapistChoiceBox.getItems().clear();

            // fill the choice box with data
            List<Therapist> lt;
            if (therapyType == null) {
                // does not fill this choice box, if there are no therapists available for this therapy type
                return;
            }
            else {
                lt = therapistConstraintManager.findAllTherapistsByTherapyType(therapyType);
            }

            if (lt.size() > 0) {

                therapistData.clear();

                for (Therapist t : lt) {
                    therapistData.add(t);
                }

                therapistChoiceBox.setItems(therapistData);
                therapistChoiceBox.setValue(therapistData.get(0));

            }
        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapeutenauswahl")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

    }

    private void initTherapyRoomChoiceBox() {

        try {
            List<TherapyRoom> ltr = therapyManager.findAllTherapyRooms();

            if (ltr.size() > 0) {

                for (TherapyRoom tr : ltr) {
                    therapyRoomData.add(tr);
                }

                therapyRoomChoiceBox.setItems(therapyRoomData);
                therapyRoomChoiceBox.setValue(therapyRoomData.get(0));

            }

        } catch (ServiceException e) {
            Dialogs.create().title("Fehler beim Initialisieren der Therapieraumauswahl")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }

    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setTherapistManager(TherapistManager therapistManager) {
        this.therapistManager = therapistManager;
    }

    public void setTherapistConstraintManager(TherapistConstraintManager therapistConstraintManager) {
        this.therapistConstraintManager = therapistConstraintManager;
    }
    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {

        try {

            TherapyEvent te = new TherapyEvent();

            if (therapyTypeChoiceBox.getSelectionModel().getSelectedItem() == null) {
                Dialogs.create().title("Fehler beim Erzeugen eines Therapieevents")
                        .masthead("Keine Therapieformen im System vorhanden")
                        .message("Bitte legen Sie Therapieformen an")
                        .showWarning();
                return;
            }
            if (therapistChoiceBox.getSelectionModel().getSelectedItem() == null) {
                Dialogs.create().title("Fehler beim Erzeugen eines Therapieevents")
                        .masthead("Keine Therapeuten mit passender Ausbildung vorhanden")
                        .message("Bitte fügen sie den Therapeuten die fehlende Therapieform hinzu")
                        .showWarning();
                return;
            }
            if (therapyRoomChoiceBox.getSelectionModel().getSelectedItem() == null) {
                Dialogs.create().title("Fehler beim Erzeugen eines Therapieevents")
                        .masthead("Keine Therapieräume im System vorhanden")
                        .message("Bitte legen Sie Therapieräume an")
                        .showWarning();
                return;
            }

            TherapyType therapyType = therapyTypeChoiceBox.getSelectionModel().getSelectedItem();
            te.setTherapyType(therapyType);

            te.setTherapyMode(TherapyMode.GROUP);
            te.setTherapyPlan(therapyPlan);

            TherapyEvent persistedTherapyEvent = therapyManager.create(te);


            Therapist therapist = therapistChoiceBox.getSelectionModel().getSelectedItem();
            persistedTherapyEvent.setTherapist(therapist);


            TherapyRoom therapyRoom = therapyRoomChoiceBox.getSelectionModel().getSelectedItem();
            persistedTherapyEvent.setTherapyRoom(therapyRoom);

            therapyManager.update(persistedTherapyEvent);
            okClicked = true;
            dialogStage.close();
        } catch (ServiceException | ValidationException e) {
            Dialogs.create().title("Fehler beim Erzeugen eines Therapieevents")
                    .masthead(null)
                    .message(e.getMessage())
                    .showWarning();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {

        Action response = Dialogs.create()
                .title("Bestätigungsdialog")
                .masthead("Therapie erstellen verlassen")
                .message("Wollen Sie diese Seite verlassen ohne die Therapie anzulegen?")
                .actions(Dialog.ACTION_YES, Dialog.ACTION_NO)
                .showConfirm();

        if (response == Dialog.ACTION_YES) {

            dialogStage.close();

        } else {
            // ... user chose CANCEL, or closed the dialog
        }
    }
}
