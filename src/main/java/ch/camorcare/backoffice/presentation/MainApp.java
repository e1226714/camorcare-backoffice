package ch.camorcare.backoffice.presentation;

import ch.camorcare.backoffice.adapter.*;
import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyRoom;
import ch.camorcare.backoffice.presentation.view.LayoutController;
import ch.camorcare.backoffice.presentation.view.dutyCategory.DutyCategoryUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.patient.PatientUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.patient.therapistConstraint.PatientBasedTherapistConstraintUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.patient.therapistGenderConstraint.PatientBasedTherapistGenderConstraintUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.patient.therapyTypeOccurrenceConstraint.PatientBasedTherapyTypeOccurrenceConstraintUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.therapist.TherapistUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.therapist.therapyTypeOccurrenceConstraint.TherapistBasedTherapyTypeOccurrenceConstraintUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.therapist.timeBlockConstraint.TherapistBasedTimeBlockConstraintUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.therapy.PrintBillController;
import ch.camorcare.backoffice.presentation.view.therapy.TherapyUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.therapyPlan.TherapyPlanUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.therapyRoom.TherapyRoomUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.therapyType.TherapyTypeUpdateDialogController;
import ch.camorcare.backoffice.presentation.view.weekPlan.CreateGroupTherapyEventController;
import ch.camorcare.backoffice.presentation.view.weekPlan.EventCreatorController;
import ch.camorcare.backoffice.presentation.view.weekPlan.PlanOverviewController;
import impl.org.controlsfx.i18n.Localization;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.Locale;

/**
 * Main application class from which the application is controlled
 */
public class MainApp extends Application {

    /**
     *
     */
    private static final SpringFxmlLoader loader = new SpringFxmlLoader();

    /**
     *
     */
    private Stage primaryStage;

    /**
     *
     */
    private BorderPane layout;

    public static SpringFxmlLoader getLoader() {
        return loader;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Camor Care - Verwaltungssoftware");
        this.primaryStage.setMinWidth(1024);
        this.primaryStage.setMinHeight(768);
        this.primaryStage.setMaximized(true);
        Localization.setLocale(new Locale("de", "DE"));

        initLayout();
        showPatientOverview();
    }

    /**
     * Initializes the root layout.
     */
    public void initLayout() {
        layout = (BorderPane) loader.load("/fxml/layout.fxml");
        Scene scene = new Scene(layout);
        primaryStage.setScene(scene);

        // Set the patient into the controller.
        LayoutController controller = (LayoutController) loader.getController();
        controller.setMainApp(this);
        controller.setLayout(layout);

        primaryStage.show();
    }

    public void showTherapyOverview() {
        layout.setCenter((SplitPane) loader.load("/fxml/therapy/overview.fxml"));
    }

    public boolean showTherapyUpdateDialog(TherapyAdapter therapy) {
        AnchorPane therapyUpdateDialog = (AnchorPane) loader.load("/fxml/therapy/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Therapie bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(therapyUpdateDialog);
        dialogStage.setScene(scene);

        // Set the duty categorie into the controller.
        TherapyUpdateDialogController controller = (TherapyUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTherapy(therapy);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    /**
     * Shows the duty category overview inside the root layout.
     */
    public void showDutyCategoryOverview() {
        layout.setCenter((SplitPane) loader.load("/fxml/dutyCategory/overview.fxml"));
    }

    /**
     * Opens a dialog to edit details for the specified duty category. If the user
     * clicks OK, the changes are saved into the provided duty category object and true
     * is returned.
     *
     * @param dutyCategory the dutyCategory object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showDutyCategoryEditDialog(DutyCategoryAdapter dutyCategory) {
        AnchorPane dutyCategoryEditDialog = (AnchorPane) loader.load("/fxml/dutyCategory/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Hausdienst bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(dutyCategoryEditDialog);
        dialogStage.setScene(scene);

        // Set the duty categorie into the controller.
        DutyCategoryUpdateDialogController controller = (DutyCategoryUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setDutyCategory(dutyCategory);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    /**
     * Shows the patient overview inside the root layout.
     */
    public void showPatientOverview() {
        layout.setCenter((SplitPane) loader.load("/fxml/patient/overview.fxml"));
    }

    /**
     * Opens a dialog to edit details for the specified patient. If the user
     * clicks OK, the changes are saved into the provided patient object and true
     * is returned.
     *
     * @param patient the patient object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showPatientEditDialog(PatientAdapter patient) {
        AnchorPane patientEditDialog = (AnchorPane) loader.load("/fxml/patient/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Patient bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(patientEditDialog);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        PatientUpdateDialogController controller = (PatientUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setPatient(patient);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    public void showPatientBasedTherapyTypeConstraintCreateDialog(PatientAdapter patientAdapter) {
        AnchorPane pane = (AnchorPane) loader.load("/fxml/patient/therapyTypeConstraint/create.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Therapieformen zuteilen");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(pane);
        dialogStage.setScene(scene);

        /*
        // Set the patient into the controller.
        PatientBasedTherapyTypeConstraintUpdateDialogController controller = (PatientBasedTherapyTypeConstraintUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setPatientAdapter(patientAdapter);
        */

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();
    }

    public boolean showPatientBasedTherapyTypeOccurrenceConstraintUpdateDialog(PatientAdapter patientAdapter) {
        AnchorPane pane = (AnchorPane) loader.load("/fxml/patient/therapyTypeOccurrenceConstraint/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Therapieformen bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(pane);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        PatientBasedTherapyTypeOccurrenceConstraintUpdateDialogController controller = (PatientBasedTherapyTypeOccurrenceConstraintUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setPatientAdapter(patientAdapter);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    public boolean showPatientBasedTherapistGenderConstraintUpdateDialog(PatientAdapter patientAdapter) {
        AnchorPane pane = (AnchorPane) loader.load("/fxml/patient/therapistGenderConstraint/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Präferenzen (Geschlecht) bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(pane);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        PatientBasedTherapistGenderConstraintUpdateDialogController controller = (PatientBasedTherapistGenderConstraintUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setPatientAdapter(patientAdapter);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    public boolean showPatientBasedTherapistConstraintUpdateDialog(PatientAdapter patientAdapter) {
        AnchorPane pane = (AnchorPane) loader.load("/fxml/patient/therapistConstraint/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Präferenzen (Therapeut) bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(pane);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        PatientBasedTherapistConstraintUpdateDialogController controller = (PatientBasedTherapistConstraintUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setPatientAdapter(patientAdapter);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    /**
     * Shows the therapist overview.
     */
    public void showTherapistOverview() {
        layout.setCenter((SplitPane) loader.load("/fxml/therapist/overview.fxml"));
    }

    /**
     * @param therapist
     * @return
     */
    public boolean showTherapistEditDialog(TherapistAdapter therapist) {
        AnchorPane pane = (AnchorPane) loader.load("/fxml/therapist/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Therapeut bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(pane);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        TherapistUpdateDialogController controller = (TherapistUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTherapist(therapist);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    public boolean showTherapistBasedTimeBlockConstraintEditDialog(TherapistAdapter therapist) {
        AnchorPane pane = (AnchorPane) loader.load("/fxml/therapist/timeBlockConstraint/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Verfügbarkeit bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(pane);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        TherapistBasedTimeBlockConstraintUpdateDialogController controller = (TherapistBasedTimeBlockConstraintUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTherapist(therapist);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    public boolean showTherapistBasedTherapyTypeOccurrenceConstraintUpdateDialog(TherapistAdapter therapist) {
        AnchorPane pane = (AnchorPane) loader.load("/fxml/therapist/therapyTypeOccurrenceConstraint/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Therapieformen bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(pane);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        TherapistBasedTherapyTypeOccurrenceConstraintUpdateDialogController controller = (TherapistBasedTherapyTypeOccurrenceConstraintUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTherapist(therapist);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    /**
     * Shows the therapy room overview inside the root layout.
     */
    public void showTherapyRoomOverview() {
        layout.setCenter((SplitPane) loader.load("/fxml/therapyRoom/overview.fxml"));
    }

    /**
     * Opens a dialog to edit details for the specified therapy room. If the user
     * clicks OK, the changes are saved into the provided therapy room object and true
     * is returned.
     *
     * @param therapyRoom the therapyRoom object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showTherapyRoomEditDialog(TherapyRoomAdapter therapyRoom) {
        AnchorPane therapyRoomEditDialog = (AnchorPane) loader.load("/fxml/therapyRoom/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Therapieraum bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(therapyRoomEditDialog);
        dialogStage.setScene(scene);

        // Set the therapy room into the controller.
        TherapyRoomUpdateDialogController controller = (TherapyRoomUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTherapyRoom(therapyRoom);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    /**
     * Shows the therapy type overview inside the root layout.
     */
    public void showTherapyTypeOverview() {
        layout.setCenter((SplitPane) loader.load("/fxml/therapyType/overview.fxml"));
    }

    /**
     * Shows existing therapy plans.
     */
    public void showTherapyPlanOverview() {
        layout.setCenter((SplitPane) loader.load("/fxml/therapyPlan/overview.fxml"));
    }

    /**
     * Opens a dialog to edit details for the specified therapy type. If the user
     * clicks OK, the changes are saved into the provided therapy type object and true
     * is returned.
     *
     * @param therapyType the therapyType object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showTherapyTypeEditDialog(TherapyTypeAdapter therapyType) {
        AnchorPane therapyTypeEditDialog = (AnchorPane) loader.load("/fxml/therapyType/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Therapieform bearbeiten");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(therapyTypeEditDialog);
        dialogStage.setScene(scene);

        // Set the therapy type into the controller.
        TherapyTypeUpdateDialogController controller = (TherapyTypeUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTherapyType(therapyType);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    public boolean showTherapyPlanEditDialog(TherapyPlanAdapter therapyPlanAdapter) {
        AnchorPane therapyTypeEditDialog = (AnchorPane) loader.load("/fxml/therapyPlan/update.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Bearbeite Therapieplan");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(therapyTypeEditDialog);
        dialogStage.setScene(scene);

        // Set the therapy type into the controller.
        TherapyPlanUpdateDialogController controller = (TherapyPlanUpdateDialogController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTherapyPlan(therapyPlanAdapter);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    /**
     * Shows the event creator overview inside the root layout.
     */
    public void showEventCreator(TherapyPlanAdapter therapyPlanAdapter) {
        //layout.setCenter((AnchorPane) loader.load("/fxml/weekPlan/eventCreator.fxml"));

        SplitPane eventCreator = (SplitPane) loader.load("/fxml/weekPlan/eventCreator.fxml");

        Stage dialogStage = new Stage();
        dialogStage.setTitle(therapyPlanAdapter.toString());
        dialogStage.initOwner(primaryStage);

        //layout = (BorderPane) loader.load("/fxml/weekPlan/eventCreator.fxml");
        Scene scene = new Scene(eventCreator);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        EventCreatorController controller = (EventCreatorController) loader.getController();
        controller.setStage(dialogStage);
        controller.setMainApp(this);
        controller.setTherapyPlanAdapter(therapyPlanAdapter);

        dialogStage.show();
    }

    public boolean showCreateGroupTherapyEvent(TherapyPlanAdapter therapyPlanAdapter) {
        AnchorPane createGroupTherapyEvent = (AnchorPane) loader.load("/fxml/weekPlan/createGroupTherapyEvent.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Erstelle Gruppentherapie");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(createGroupTherapyEvent);
        dialogStage.setScene(scene);

        // Set the therapy plan into the controller.
        CreateGroupTherapyEventController controller = (CreateGroupTherapyEventController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTherapyPlan(therapyPlanAdapter.getEntity());

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return controller.isOkClicked();
    }



    /**
     * Shows the plan overview inside the root layout.
     */
    public void showPlanOverview(TherapyPlanAdapter therapyPlanAdapter) {

        //layout.setCenter((AnchorPane) loader.load("/fxml/weekPlan/planOverview.fxml"));

        AnchorPane eventCreator = (AnchorPane) loader.load("/fxml/weekPlan/planOverview.fxml");

        Stage dialogStage = new Stage();
        dialogStage.initOwner(primaryStage);

        //layout = (BorderPane) loader.load("/fxml/weekPlan/eventCreator.fxml");
        Scene scene = new Scene(eventCreator);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        PlanOverviewController controller = (PlanOverviewController) loader.getController();
        controller.setMainApp(this);
        controller.setStage(dialogStage);
        controller.setTherapyPlan(therapyPlanAdapter.getEntity());

        dialogStage.show();
    }

    /**
     * Shows the plan overview inside the root layout.
     */
    public void showPlanOverview(TherapyPlan therapyPlan, Therapy therapy, Therapist therapist, TherapyRoom therapyRoom) {

        //layout.setCenter((AnchorPane) loader.load("/fxml/weekPlan/planOverview.fxml"));

        AnchorPane eventCreator = (AnchorPane) loader.load("/fxml/weekPlan/planOverview.fxml");

        Stage dialogStage = new Stage();
        dialogStage.initOwner(primaryStage);

        //layout = (BorderPane) loader.load("/fxml/weekPlan/eventCreator.fxml");
        Scene scene = new Scene(eventCreator);
        dialogStage.setScene(scene);

        // Set the patient into the controller.
        PlanOverviewController controller = (PlanOverviewController) loader.getController();
        controller.setMainApp(this);
        controller.setStage(dialogStage);
        controller.setTherapy(therapy);
        controller.setTherapist(therapist);
        controller.setTherapyRoom(therapyRoom);
        controller.setTherapyPlan(therapyPlan);

        dialogStage.show();
    }

    public void showPrintBill(TherapyAdapter therapyAdapter) {
        AnchorPane printBill = (AnchorPane) loader.load("/fxml/therapy/printBill.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Rechnung Generieren");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(printBill);
        dialogStage.setScene(scene);

        // Set the therapy plan into the controller.
        PrintBillController controller = (PrintBillController) loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setTherapy(therapyAdapter);

        // Show the dialog and wait until the user closes it
        dialogStage.show();
    }
}
