package ch.camorcare.backoffice.presentation;

/**
 * The presentation package contains all of the files necessary for the representation of the user interface (UI). This
 * particular implementation uses JavaFX.
 * <p/>
 * The {@link ch.camorcare.backoffice.presentation.MainApp} file is responsible for loading all UI files with which
 * the user interacts.
 * <p/>
 * There is also a file called {@link ch.camorcare.backoffice.presentation.SpringFxmlLoader} that loads the Spring
 * configuration information for each one of the FXML files.
 */