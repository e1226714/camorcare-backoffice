package ch.camorcare.backoffice.presentation;

import ch.camorcare.backoffice.presentation.view.LayoutController;

/**
 * Controls the screen
 */
public abstract class ScreenController {

    /**
     *
     */
    private MainApp mainApp;

    /**
     *
     */
    protected LayoutController layoutController;

    public MainApp getMainApp() {
        return mainApp;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public LayoutController getLayoutController() {
        return layoutController;
    }

    public void setLayoutController(LayoutController layoutController) {
        this.layoutController = layoutController;
    }
}
