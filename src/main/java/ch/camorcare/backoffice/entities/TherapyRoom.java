package ch.camorcare.backoffice.entities;

/**
 * This class represents a therapy room.
 */
public class TherapyRoom extends BasicDTO {

    /**
     * The name of the therapy room (e.g., "Entspannungsraum")
     * <p/>
     * max length = 100.
     */
    private String name;

    /**
     * The capacity / amount of patients it holds at a time.
     */
    private int capacity;

    /**
     * Default constructor.
     */
    public TherapyRoom() {

    }

    /**
     * Copy constructor.
     *
     * @param therapyRoom
     */
    public TherapyRoom(TherapyRoom therapyRoom) {
        super(therapyRoom);
        this.setName(therapyRoom.getName());
        this.setCapacity(therapyRoom.getCapacity());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return name;
    }

    public Boolean equals(TherapyRoom therapyRoom) {
        if(therapyRoom == null)
            return false;
        if(name.equals(therapyRoom.getName())
                && capacity == therapyRoom.getCapacity())
            return true;
        return false;
    }
}

