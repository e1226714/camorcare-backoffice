package ch.camorcare.backoffice.entities;

import org.joda.time.DateTime;

/**
 *
 */
public abstract class BasicDTO {

    /**
     * Primary identifier.
     */
    private Integer id;

    /**
     * Time of creation.
     */
    private DateTime timeCreated;

    /**
     * Time of modification.
     */
    private DateTime timeModified;

    /**
     * Time of deletion.
     * Nullable.
     */
    private DateTime timeDeleted;

    /**
     * Default constructor.
     */
    public BasicDTO() {

    }

    /**
     * Copy constructor.
     *
     * @param basicDTO instance to copy
     */
    public BasicDTO(BasicDTO basicDTO) {
        this.setId(basicDTO.getId());
        this.setTimeCreated(basicDTO.getTimeCreated());
        this.setTimeModified(basicDTO.getTimeModified());
        this.setTimeDeleted(basicDTO.getTimeDeleted());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DateTime getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(DateTime timeCreated) {
        this.timeCreated = timeCreated;
    }

    public DateTime getTimeModified() {
        return timeModified;
    }

    public void setTimeModified(DateTime timeModified) {
        this.timeModified = timeModified;
    }

    public DateTime getTimeDeleted() {
        return timeDeleted;
    }

    public void setTimeDeleted(DateTime timeDeleted) {
        this.timeDeleted = timeDeleted;
    }

}
