package ch.camorcare.backoffice.entities;

/**
 *
 */
public class Invoice extends BasicDTO {

    /**
     *
     */
    private Therapy therapy;

    /**
     * Nullable.
     */
    private Address address;

    /**
     *
     */
    private boolean vatIncluded;

    public Invoice() {

    }

    /**
     * Copy constructor
     *
     * @param invoice instance to be copied
     */
    public Invoice(Invoice invoice) {
        super(invoice);
        this.setTherapy(invoice.getTherapy());
        this.setAddress(invoice.getAddress());
        this.setVatIncluded(invoice.isVatIncluded());
    }

    public Therapy getTherapy() {
        return therapy;
    }

    public void setTherapy(Therapy therapy) {
        this.therapy = therapy;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isVatIncluded() {
        return vatIncluded;
    }

    public void setVatIncluded(boolean vatIncluded) {
        this.vatIncluded = vatIncluded;
    }
}
