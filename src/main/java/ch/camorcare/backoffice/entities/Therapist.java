package ch.camorcare.backoffice.entities;

import ch.camorcare.backoffice.entities.type.Gender;

import java.util.List;

/**
 * This class represents a therapist.
 */
public class Therapist extends BasicDTO {

    /**
     * The first name of the therapist.
     * <p/>
     * max length = 100.
     */
    private String firstName;

    /**
     * The last name of the therapist.
     * <p/>
     * max length = 100.
     */
    private String lastName;

    /**
     * The gender of the therapist.
     */
    private Gender gender;

    /**
     * The email of the therapist.
     * <p/>
     * max length = 200.
     */
    private String email;

    /**
     * A list of therapy types in which the therapist is educated in.
     */
    private List<TherapyType> therapyTypes;

    /**
     * Default constructor.
     */
    public Therapist() {
    }

    /**
     * Copy constructor.
     *
     * @param therapist
     */
    public Therapist(Therapist therapist) {
        super(therapist);
        this.setFirstName(therapist.getFirstName());
        this.setLastName(therapist.getLastName());
        this.setGender(therapist.getGender());
        this.setEmail(therapist.getEmail());
        this.setTherapyTypes(therapist.getTherapyTypes());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<TherapyType> getTherapyTypes() {
        return therapyTypes;
    }

    public void setTherapyTypes(List<TherapyType> therapyTypes) {
        this.therapyTypes = therapyTypes;
    }

    @Override
    public String toString() {
        return lastName + " " + firstName;
    }

    public boolean equals(Therapist therapist) {
        if(therapist == null)
            return false;
        if(firstName.equals(therapist.getFirstName())
                && lastName.equals(therapist.getLastName())
                && gender.equals(therapist.getGender())
                && email.equals(therapist.getEmail()))
            return true;
        return false;
    }
}