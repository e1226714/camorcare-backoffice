package ch.camorcare.backoffice.entities;

import ch.camorcare.backoffice.entities.type.TherapyMode;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a therapy event. A therapy event is held by one {@link ch.camorcare.backoffice.entities.Therapist}
 * and multiple {@link ch.camorcare.backoffice.entities.Patient} can participate. An event lasts takes 45 minutes.
 */
public class TherapyEvent extends BasicDTO {

    /**
     * Related therapy plan.
     */
    private TherapyPlan therapyPlan;

    /**
     * The responsible therapist.
     */
    private Therapist therapist;

    /**
     * What kind of therapy it is.
     */
    private TherapyType therapyType;

    /**
     * Single or group.
     */
    private TherapyMode therapyMode;

    /**
     * Where the therapy takes place.
     */
    private TherapyRoom therapyRoom;

    /**
     * Start time of this event.
     */
    private DateTime therapyTime;

    /**
     * Start TimeBlock of the event.
     */
    private TimeBlock therapyTimeBlock;

    /**
     *
     */
    private List<Therapy> therapies;

    /**
     * Default constructor.
     */
    public TherapyEvent() {
        this.therapies = new ArrayList<>();
    }

    /**
     * Copy constructor.
     *
     * @param therapyEvent
     */
    public TherapyEvent(TherapyEvent therapyEvent) {
        super(therapyEvent);
        this.setTherapyPlan(therapyEvent.getTherapyPlan());
        this.setTherapist(therapyEvent.getTherapist());
        this.setTherapyType(therapyEvent.getTherapyType());
        this.setTherapyMode(therapyEvent.getTherapyMode());
        this.setTherapyRoom(therapyEvent.getTherapyRoom());
        this.setTherapyTime(therapyEvent.getTherapyTime());
        this.setTherapyTimeBlock(therapyEvent.getTherapyTimeBlock());
        this.setTherapies(therapyEvent.getTherapies());
    }

    public Therapist getTherapist() {
        return therapist;
    }

    public void setTherapist(Therapist therapist) {
        this.therapist = therapist;
    }

    public TherapyType getTherapyType() {
        return therapyType;
    }

    public void setTherapyType(TherapyType therapyType) {
        this.therapyType = therapyType;
    }

    public TherapyRoom getTherapyRoom() {
        return therapyRoom;
    }

    public void setTherapyRoom(TherapyRoom therapyRoom) {
        this.therapyRoom = therapyRoom;
    }

    public TimeBlock getTherapyTimeBlock() {
        return therapyTimeBlock;
    }

    public void setTherapyTimeBlock(TimeBlock therapyTime) {
        this.therapyTimeBlock = therapyTime;
    }

    public TherapyMode getTherapyMode() {
        return therapyMode;
    }

    public void setTherapyMode(TherapyMode therapyMode) {
        this.therapyMode = therapyMode;
    }

    public DateTime getTherapyTime() {
        return therapyTime;
    }

    public void setTherapyTime(DateTime therapyTime) {
        this.therapyTime = therapyTime;
    }

    public TherapyPlan getTherapyPlan() {
        return therapyPlan;
    }

    public void setTherapyPlan(TherapyPlan therapyPlan) {
        this.therapyPlan = therapyPlan;
    }

    public List<Therapy> getTherapies() {
        return therapies;
    }

    public void setTherapies(List<Therapy> therapies) {
        this.therapies = therapies;
    }

    public void addTherapy(Therapy therapy) {
        therapies.add(therapy);
    }

    public String getName() {
        return therapyType.getName();
    }
}