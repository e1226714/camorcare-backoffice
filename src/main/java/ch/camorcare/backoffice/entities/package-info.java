package ch.camorcare.backoffice.entities;

/**
 * This package contains all of the entities (DTOs) of the project. They essentially represent the individual SQL
 * tables as stored in the database. These DTOs are transferred between the three layers of the project (presentation,
 * service and persistence) to allow simple, object-based communication that all layers understand, regardless of what
 * the underlying implementation of the various interfaces actually are.
 */