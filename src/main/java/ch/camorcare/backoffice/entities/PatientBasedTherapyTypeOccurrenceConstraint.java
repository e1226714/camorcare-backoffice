package ch.camorcare.backoffice.entities;

import ch.camorcare.backoffice.entities.type.TherapyMode;

/**
 * This constraint defines how often a week a {@link ch.camorcare.backoffice.entities.Patient} is allowed to receive a
 * specific {@link ch.camorcare.backoffice.entities.TherapyType}.
 * <p/>
 * Example constraint: Patient P is only allowed to consume a maximum of 2 therapy events of the type "Massage" each
 * week.
 */
public class PatientBasedTherapyTypeOccurrenceConstraint extends PatientBasedConstraint {

    /**
     * Therapy type which is regulated by this constraint.
     */
    private TherapyType therapyType;

    /**
     *
     */
    private TherapyMode therapyMode;

    /**
     * Maximum occurrence of the related therapy type for this patient per week.
     */
    private Integer maxOccurencePerWeek;

    /**
     *
     */
    public PatientBasedTherapyTypeOccurrenceConstraint() {

    }

    /**
     * Copy constructor
     *
     * @param constraint
     */
    public PatientBasedTherapyTypeOccurrenceConstraint(PatientBasedTherapyTypeOccurrenceConstraint constraint) {
        this.setId(constraint.getId());
        this.setPatient(constraint.getPatient());
        this.setTherapyType(constraint.getTherapyType());
        this.setTherapyMode(constraint.getTherapyMode());
        this.setMaxOccurencePerWeek(constraint.getMaxOccurencePerWeek());
        this.setTimeCreated(constraint.getTimeCreated());
        this.setTimeModified(constraint.getTimeModified());
        this.setTimeDeleted(constraint.getTimeDeleted());
    }

    public TherapyType getTherapyType() {
        return therapyType;
    }

    public void setTherapyType(TherapyType therapyType) {
        this.therapyType = therapyType;
    }

    public TherapyMode getTherapyMode() {
        return therapyMode;
    }

    public void setTherapyMode(TherapyMode therapyMode) {
        this.therapyMode = therapyMode;
    }

    public Integer getMaxOccurencePerWeek() {
        return maxOccurencePerWeek;
    }

    public void setMaxOccurencePerWeek(Integer maxOccurencePerWeek) {
        this.maxOccurencePerWeek = maxOccurencePerWeek;
    }
}
