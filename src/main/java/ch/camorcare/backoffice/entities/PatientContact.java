package ch.camorcare.backoffice.entities;

/**
 * A contact person can be related to one {@link ch.camorcare.backoffice.entities.Patient}, a patient can relate to
 * multiple contacts.
 */
public class PatientContact extends BasicDTO {

    /**
     * Patient to whom this contact relates.
     */
    private Patient patient;

    /**
     * The contacts first name.
     */
    private String firstName;

    /**
     * The contacts last name.
     */
    private String lastName;

    /**
     * The phone number.
     * <p/>
     * max length = 30.
     */
    private String phone;

    /**
     * The email address.
     */
    private String email;

    /**
     *
     */
    public PatientContact() {

    }

    /**
     * Copy constructor
     *
     * @param patientContact
     */
    public PatientContact(PatientContact patientContact) {
        super(patientContact);
        this.setPatient(patientContact.getPatient());
        this.setFirstName(patientContact.getFirstName());
        this.setLastName(patientContact.getLastName());
        this.setPhone(patientContact.getPhone());
        this.setEmail(patientContact.getEmail());
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
