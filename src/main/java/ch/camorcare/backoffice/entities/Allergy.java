package ch.camorcare.backoffice.entities;

/**
 * Represents a type of allergy.
 */
public class Allergy extends BasicDTO {

    /**
     * The name of the allergy.
     */
    private String name;

    public Allergy() {

    }

    public Allergy(Allergy allergy) {
        super(allergy);
        this.setName(allergy.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
