package ch.camorcare.backoffice.entities;

/**
 * Extra information for a {@link ch.camorcare.backoffice.entities.Patient} that cannot easily be expressed in a
 * variable.
 */
public class PatientComment extends BasicDTO {

    /**
     * Related patient.
     */
    private Patient patient;

    /**
     * Content of the comment. max length = 1000
     */
    private String content;

    public PatientComment() {

    }

    public PatientComment(PatientComment patientComment) {
        super(patientComment);
        this.setPatient(patientComment.getPatient());
        this.setContent(patientComment.getContent());
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
