package ch.camorcare.backoffice.entities;

import ch.camorcare.backoffice.entities.type.Day;
import org.joda.time.DateTime;

/**
 * This class represents a time block. A time block is defined for a specific weekday ({@link ch.camorcare.backoffice.entities.type.Day}) in combination with a specific
 * time ({@link org.joda.time.DateTime}). A time block repeats itself every week. A time block lasts exactly 45 minutes.
 */
public class TimeBlock extends BasicDTO {

    /**
     * Weekday.
     */
    private Day day;

    /**
     * Start time of this time block.
     */
    private DateTime timeStart;

    /**
     * Predecessor. Null if there is none.
     */
    private TimeBlock predecessor;

    /**
     * Default constructor.
     */
    public TimeBlock() {

    }

    /**
     * Copy constructor.
     *
     * @param timeBlock
     */
    public TimeBlock(TimeBlock timeBlock) {
        super(timeBlock);
        this.setDay(timeBlock.getDay());
        this.setTimeStart(timeBlock.getTimeStart());
        this.setPredecessor(timeBlock.getPredecessor());
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public DateTime getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(DateTime timeStart) {
        this.timeStart = timeStart;
    }

    public TimeBlock getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(TimeBlock predecessor) {
        this.predecessor = predecessor;
    }

    public boolean equals(TimeBlock timeBlock){
        if(timeBlock == null)
            return false;
        if(day.equals(timeBlock.getDay())
                && timeStart.equals(timeBlock.getTimeStart()))
            return true;
        return false;
    }
}
