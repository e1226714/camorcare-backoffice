package ch.camorcare.backoffice.entities;

/**
 * This constraint defines that the related {@link ch.camorcare.backoffice.entities.Therapist} is available for the
 * related period of time ({@link ch.camorcare.backoffice.entities.TimeBlock}).
 * <p/>
 * Example constraint: Therapist T is available for TimeBlock #14 (which could for example be defined as "Monday,
 * starting on 11.00am").
 */
public class TherapistBasedTimeBlockConstraint extends TherapistBasedConstraint {

    /**
     * The related TimeBlock.
     */
    private TimeBlock timeBlock;

    /**
     * Default constructor.
     */
    public TherapistBasedTimeBlockConstraint() {

    }

    /**
     * Copy constructor.
     *
     * @param constraint
     */
    public TherapistBasedTimeBlockConstraint(TherapistBasedTimeBlockConstraint constraint) {
        this.setId(constraint.getId());
        this.setTherapist(constraint.getTherapist());
        this.setTimeBlock(constraint.getTimeBlock());
        this.setTimeCreated(constraint.getTimeCreated());
        this.setTimeModified(constraint.getTimeModified());
        this.setTimeDeleted(constraint.getTimeDeleted());
    }

    public TimeBlock getTimeBlock() {
        return timeBlock;
    }

    public void setTimeBlock(TimeBlock timeBlock) {
        this.timeBlock = timeBlock;
    }
}
