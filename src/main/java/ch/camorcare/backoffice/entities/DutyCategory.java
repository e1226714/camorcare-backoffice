package ch.camorcare.backoffice.entities;


import ch.camorcare.backoffice.entities.type.Day;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a duty category.
 */
public class DutyCategory extends BasicDTO {

    /**
     * Descriptive name of this category.
     * max length = 50
     */
    private String name;

    /**
     * Duration in minutes.
     */
    private int duration;

    /**
     * List of weekdays on which this duty has to be performed.
     */
    private List<Day> weekdays;

    /**
     * Default constructor.
     */
    public DutyCategory() {
        this.weekdays = new ArrayList<>();
    }

    /**
     * Copy constructor.
     *
     * @param dutyCategory
     */
    public DutyCategory(DutyCategory dutyCategory) {
        super(dutyCategory);
        this.setName(dutyCategory.getName());
        this.setDuration(dutyCategory.getDuration());
        this.setWeekdays(dutyCategory.getWeekdays());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<Day> getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(List<Day> weekdays) {
        this.weekdays = weekdays;
    }
}
