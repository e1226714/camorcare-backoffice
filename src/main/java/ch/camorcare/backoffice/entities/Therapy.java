package ch.camorcare.backoffice.entities;

import org.joda.time.DateTime;

/**
 * This represents the stay/therapy of a {@link ch.camorcare.backoffice.entities.Patient}. One patient can stay multiple
 * times, therefore may have multiple therapies.
 */
public class Therapy extends BasicDTO {

    /**
     * The related patient.
     */
    private Patient patient;

    /**
     * The start date of the sick leave.
     */
    private DateTime dateSickLeave;

    /**
     * The start date of the stay.
     */
    private DateTime dateFrom;

    /**
     * The end date of the stay. May be null when it's not sure, when the patient will be able to be released.
     * <p/>
     * Nullable.
     */
    private DateTime dateUntil;

    /**
     * Default constructor.
     */
    public Therapy() {

    }

    /**
     * Copy constructor.
     *
     * @param therapy
     */
    public Therapy(Therapy therapy) {
        super(therapy);
        this.setPatient(therapy.getPatient());
        this.setDateSickLeave(therapy.getDateSickLeave());
        this.setDateFrom(therapy.getDateFrom());
        this.setDateUntil(therapy.getDateUntil());
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public DateTime getDateSickLeave() {
        return dateSickLeave;
    }

    public void setDateSickLeave(DateTime dateSickLeave) {
        this.dateSickLeave = dateSickLeave;
    }

    public DateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(DateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public DateTime getDateUntil() {
        return dateUntil;
    }

    public void setDateUntil(DateTime dateUntil) {
        this.dateUntil = dateUntil;
    }

}
