package ch.camorcare.backoffice.entities;

/**
 * This constraint represents a constraint which was defined by a {@link ch.camorcare.backoffice.entities.Therapist}.
 */
public abstract class TherapistBasedConstraint extends BasicDTO {

    /**
     * Related therapist of this constraint.
     */
    private Therapist therapist;

    public Therapist getTherapist() {
        return therapist;
    }

    public void setTherapist(Therapist therapist) {
        this.therapist = therapist;
    }
}
