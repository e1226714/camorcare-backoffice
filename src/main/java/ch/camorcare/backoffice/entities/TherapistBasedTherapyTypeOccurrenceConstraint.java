package ch.camorcare.backoffice.entities;

import ch.camorcare.backoffice.entities.type.TherapyMode;

/**
 * This constraint defines how often a day a {@link ch.camorcare.backoffice.entities.Therapist} is willing to perform a
 * specific {@link ch.camorcare.backoffice.entities.TherapyType}.
 * <p/>
 * Example constraint: Therapist T is only willing to perform a maximum of 4 therapy events of the type "Psychotherapy"
 * each day.
 */
public class TherapistBasedTherapyTypeOccurrenceConstraint extends TherapistBasedConstraint {

    /**
     * Therapy type which is regulated by this constraint.
     */
    private TherapyType therapyType;

    /**
     *
     */
    private TherapyMode therapyMode;

    /**
     * Maximum occurence of the related therapy type for this therapist per day.
     */
    private Integer maxOccurrencePerDay;

    /**
     * Default constructor.
     */
    public TherapistBasedTherapyTypeOccurrenceConstraint() {
    }

    /**
     * Copy constructor.
     *
     * @param original
     */
    public TherapistBasedTherapyTypeOccurrenceConstraint(TherapistBasedTherapyTypeOccurrenceConstraint original) {
        this.setId(original.getId());
        this.setTherapist(original.getTherapist());
        this.setTherapyType(original.getTherapyType());
        this.setTherapyMode(original.getTherapyMode());
        this.setMaxOccurrencePerDay(original.getMaxOccurrencePerDay());
        this.setTimeCreated(original.getTimeCreated());
        this.setTimeModified(original.getTimeModified());
        this.setTimeDeleted(original.getTimeDeleted());
    }

    public TherapyType getTherapyType() {
        return therapyType;
    }

    public void setTherapyType(TherapyType therapyType) {
        this.therapyType = therapyType;
    }

    public TherapyMode getTherapyMode() {
        return therapyMode;
    }

    public void setTherapyMode(TherapyMode therapyMode) {
        this.therapyMode = therapyMode;
    }

    public Integer getMaxOccurrencePerDay() {
        return maxOccurrencePerDay;
    }

    public void setMaxOccurrencePerDay(Integer maxOccurrencePerDay) {
        this.maxOccurrencePerDay = maxOccurrencePerDay;
    }
}
