package ch.camorcare.backoffice.entities;

/**
 * This constraint represents a constraint which was defined by a {@link ch.camorcare.backoffice.entities.Patient}.
 */
public abstract class PatientBasedConstraint extends BasicDTO {

    /**
     * Related patient of this constraint.
     */
    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
