package ch.camorcare.backoffice.entities;

import org.joda.time.DateTime;

import java.util.List;

/**
 * This class represents a therapy plan. It refers to a concrete stay (therefore a concrete {@link
 * ch.camorcare.backoffice.entities.Patient}) and is valid for a whole week (= max. 7 days).
 */
public class TherapyPlan extends BasicDTO {

    /**
     * Start date for the therapy plan.
     */
    private DateTime dateFrom;

    /**
     * End date of the therapy plan.
     */
    private DateTime dateUntil;

    /**
     * List of related therapy events.
     */
    private List<TherapyEvent> therapyEventList;

    /**
     * Default constructor.
     */
    public TherapyPlan() {

    }

    /**
     * Copy constructor.
     *
     * @param therapyPlan
     */
    public TherapyPlan(TherapyPlan therapyPlan) {
        super(therapyPlan);
        this.setDateFrom(therapyPlan.getDateFrom());
        this.setDateUntil(therapyPlan.getDateUntil());
        this.setTherapyEventList(therapyPlan.getTherapyEventList());
    }

    public DateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(DateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public DateTime getDateUntil() {
        return dateUntil;
    }

    public void setDateUntil(DateTime dateUntil) {
        this.dateUntil = dateUntil;
    }

    public List<TherapyEvent> getTherapyEventList() {
        return therapyEventList;
    }

    public void setTherapyEventList(List<TherapyEvent> therapyEventList) {
        this.therapyEventList = therapyEventList;
    }

    @Override
    public String toString() {
        if (dateFrom != null) {
            return dateFrom.toString();
        }
        else {
            return "dateFrom is null";
        }
    }
}