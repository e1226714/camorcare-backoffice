package ch.camorcare.backoffice.entities.type;

/**
 * Enumeration for days of the week
 */
public enum Day {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    @Override
    public String toString() {
        switch(this) {
            case MONDAY: return "Montag";
            case TUESDAY: return "Dienstag";
            case WEDNESDAY: return "Mittwoch";
            case THURSDAY: return "Donnerstag";
            case FRIDAY: return "Freitag";
            case SATURDAY: return "Samstag";
            case SUNDAY: return "Sonntag";
            default: throw new IllegalArgumentException();
        }
    }
}
