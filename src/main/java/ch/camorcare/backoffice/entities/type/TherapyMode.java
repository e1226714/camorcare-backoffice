package ch.camorcare.backoffice.entities.type;

/**
 * Enumeration for therapy modes.
 */
public enum TherapyMode {
    SINGLE,
    GROUP;

    @Override
    public String toString() {
        switch(this) {
            case SINGLE: return "Einzeltherapie";
            case GROUP: return "Gruppentherapie";
            default: throw new IllegalArgumentException();
        }
    }
}
