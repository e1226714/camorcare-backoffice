package ch.camorcare.backoffice.entities.type;

/**
 * Enumeration for gender values.
 */
public enum Gender {
    FEMALE,
    MALE;

    @Override
    public String toString() {
        switch(this) {
            case FEMALE: return "Weiblich";
            case MALE: return "Männlich";
            default: throw new IllegalArgumentException();
        }
    }
}
