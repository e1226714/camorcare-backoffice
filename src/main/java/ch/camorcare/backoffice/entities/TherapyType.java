package ch.camorcare.backoffice.entities;

import java.util.List;

/**
 * This represents a therapy type.
 */
public class TherapyType extends BasicDTO {

    /**
     * The name of the therapy type (e.g., "Yoga"). max length = 100.
     */
    private String name;

    /**
     * Determines whether this therapy type is suitable for individual therapies. default = false
     */
    private boolean suitableForIndividuals = false;

    /**
     * Determines whether this therapy type is suitable for individual therapies. default = false
     */
    private boolean suitableForGroups = false;

    /**
     * Therapy duration for individual settings;
     */
    private Integer individualTherapyDuration = 45;

    /**
     * Therapy duration for group settings;
     */
    private Integer groupTherapyDuration = 45;

    /**
     * Determines whether the therapist (for individual settings) may vary over time. default = false
     */
    private boolean individualTherapistFixed = false;

    /**
     * Determines whether the therapist (for group settings) may vary over time. default = false
     */
    private boolean groupTherapistFixed = false;

    /**
     * Minimum number of patients for a group therapy of this type. default = 0
     */
    private Integer minGroupSize = 0;

    /**
     * Maximum number of patients for a group therapy of this type. default = 0
     */
    private Integer maxGroupSize = 0;

    /**
     * A list of therapists which are specialized in this type of therapy. ManyToMany(fetch = FetchType.LAZY, mappedBy =
     * "therapyTypes")
     */
    private List<Therapist> therapists;

    /**
     * Default constructor.
     */
    public TherapyType() {

    }

    /**
     * Copy constructor.
     *
     * @param therapyType
     */
    public TherapyType(TherapyType therapyType) {
        super(therapyType);
        this.setName(therapyType.getName());
        this.setSuitableForIndividuals(therapyType.isSuitableForIndividuals());
        this.setIndividualTherapyDuration(therapyType.getIndividualTherapyDuration());
        this.setIndividualTherapistFixed(therapyType.isIndividualTherapistFixed());
        this.setSuitableForGroups(therapyType.isSuitableForGroups());
        this.setGroupTherapyDuration(therapyType.getGroupTherapyDuration());
        this.setGroupTherapistFixed(therapyType.isGroupTherapistFixed());
        this.setMinGroupSize(therapyType.getMinGroupSize());
        this.setMaxGroupSize(therapyType.getMaxGroupSize());
        this.setTherapists(therapyType.getTherapists());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Therapist> getTherapists() {
        return therapists;
    }

    public void setTherapists(List<Therapist> therapists) {
        this.therapists = therapists;
    }

    public boolean isSuitableForGroups() {
        return suitableForGroups;
    }

    public void setSuitableForGroups(boolean suitableForGroups) {
        this.suitableForGroups = suitableForGroups;
    }

    public boolean isSuitableForIndividuals() {
        return suitableForIndividuals;
    }

    public void setSuitableForIndividuals(boolean suitableForIndividuals) {
        this.suitableForIndividuals = suitableForIndividuals;
    }

    public Integer getIndividualTherapyDuration() {
        return individualTherapyDuration;
    }

    public void setIndividualTherapyDuration(Integer individualTherapyDuration) {
        this.individualTherapyDuration = individualTherapyDuration;
    }

    public Integer getGroupTherapyDuration() {
        return groupTherapyDuration;
    }

    public void setGroupTherapyDuration(Integer groupTherapyDuration) {
        this.groupTherapyDuration = groupTherapyDuration;
    }

    public boolean isIndividualTherapistFixed() {
        return individualTherapistFixed;
    }

    public void setIndividualTherapistFixed(boolean individualTherapistFixed) {
        this.individualTherapistFixed = individualTherapistFixed;
    }

    public boolean isGroupTherapistFixed() {
        return groupTherapistFixed;
    }

    public void setGroupTherapistFixed(boolean groupTherapistFixed) {
        this.groupTherapistFixed = groupTherapistFixed;
    }

    public Integer getMinGroupSize() {
        return minGroupSize;
    }

    public void setMinGroupSize(Integer minGroupSize) {
        this.minGroupSize = minGroupSize;
    }

    public Integer getMaxGroupSize() {
        return maxGroupSize;
    }

    public void setMaxGroupSize(Integer maxGroupSize) {
        this.maxGroupSize = maxGroupSize;
    }

    @Override
    public String toString() {
        return name;
    }
}
