package ch.camorcare.backoffice.entities;

/**
 * This constraint represents which {@link ch.camorcare.backoffice.entities.Patient} prefers which {@link
 * ch.camorcare.backoffice.entities.Therapist} in combination with a specific {@link
 * ch.camorcare.backoffice.entities.TherapyType}.
 * <p/>
 * Example constraint: Patient P prefers Therapist T when it comes to a therapy of the type "massage".
 */
public class PatientBasedTherapistConstraint extends PatientBasedConstraint {

    /**
     * The therapist who is preferred for this therapy type by the related patient.
     */
    private Therapist therapist;

    /**
     * The therapy type for which this therapist is preferred by the related patient.
     */
    private TherapyType therapyType;

    /**
     *
     */
    public PatientBasedTherapistConstraint() {

    }

    /**
     * Copy constructor
     *
     * @param constraint
     */
    public PatientBasedTherapistConstraint(PatientBasedTherapistConstraint constraint) {
        this.setId(constraint.getId());
        this.setPatient(constraint.getPatient());
        this.setTherapist(constraint.getTherapist());
        this.setTherapyType(constraint.getTherapyType());
        this.setTimeCreated(constraint.getTimeCreated());
        this.setTimeModified(constraint.getTimeModified());
        this.setTimeDeleted(constraint.getTimeDeleted());
    }

    public Therapist getTherapist() {
        return therapist;
    }

    public void setTherapist(Therapist therapist) {
        this.therapist = therapist;
    }

    public TherapyType getTherapyType() {
        return therapyType;
    }

    public void setTherapyType(TherapyType therapyType) {
        this.therapyType = therapyType;
    }
}
