package ch.camorcare.backoffice.entities;

import ch.camorcare.backoffice.entities.type.Gender;
import org.joda.time.DateTime;

import java.util.List;

/**
 * This class represents a patient.
 */
public class Patient extends BasicDTO {

    /**
     * The patients first name.
     * max length = 100.
     */
    private String firstName;

    /**
     * The patients last name.
     * max length = 100.
     */
    private String lastName;

    /**
     * The gender of the patient.
     */
    private Gender gender;

    /**
     * Birthday of the patient
     */
    private DateTime birthdate;

    /**
     * The address of the patient.
     */
    private Address address;

    /**
     * The social security number.
     * max length = 20.
     */
    private String socialSecurityNumber;

    /**
     * The phone number.
     * max length = 30.
     */
    private String phone;

    /**
     * Email address of the patient.
     * max length = 300.
     */
    private String email;

    /**
     * Religion. This is necessary for meditation therapy.
     * max length = 100.
     */
    private String religion = "unknown";

    /**
     * List of allergies.
     */
    private List<Allergy> allergyList;

    /**
     * List of comments on the patient.
     */
    private List<PatientComment> patientCommentList;

    /**
     * Default constructor.
     */
    public Patient() {

    }

    /**
     * Copy constructor.
     *
     * @param patient
     */
    public Patient(Patient patient) {
        super(patient);
        this.setFirstName(patient.getFirstName());
        this.setLastName(patient.getLastName());
        this.setGender(patient.getGender());
        this.setBirthdate(patient.getBirthdate());
        this.setAddress(patient.getAddress());
        this.setSocialSecurityNumber(patient.getSocialSecurityNumber());
        this.setPhone(patient.getPhone());
        this.setEmail(patient.getEmail());
        this.setAllergyList(patient.getAllergyList());
        this.setPatientCommentList(patient.getPatientCommentList());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public DateTime getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(DateTime birthdate) {
        this.birthdate = birthdate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public List<Allergy> getAllergyList() {
        return allergyList;
    }

    public void setAllergyList(List<Allergy> allergyList) {
        this.allergyList = allergyList;
    }

    public List<PatientComment> getPatientCommentList() {
        return patientCommentList;
    }

    public void setPatientCommentList(List<PatientComment> patientCommentList) {
        this.patientCommentList = patientCommentList;
    }
}
