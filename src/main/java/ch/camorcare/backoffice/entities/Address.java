package ch.camorcare.backoffice.entities;

/**
 * Represents an address.
 */
public class Address extends BasicDTO {

    /**
     * The street name of the address.
     * max length = 150.
     */
    private String street;

    /**
     * The postal code / zip code.
     * max length = 10.
     */
    private String zip;

    /**
     * The city of the address.
     * max length = 50.
     */
    private String city;

    /**
     * Country code (ISO 3166-1 alpha-2 format).
     * length = 2.
     */
    private String countryCode;

    /**
     * Default constructor.
     */
    public Address() {

    }

    /**
     * Copy constructor.
     *
     * @param address
     */
    public Address(Address address) {
        super(address);
        this.setStreet(address.getStreet());
        this.setZip(address.getZip());
        this.setCity(address.getCity());
        this.setCountryCode(address.getCountryCode());
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "Address[id="+this.getId()+"]";
    }
}
