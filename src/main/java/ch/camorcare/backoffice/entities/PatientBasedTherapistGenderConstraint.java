package ch.camorcare.backoffice.entities;

import ch.camorcare.backoffice.entities.type.Gender;

/**
 * This constraint represents which {@link ch.camorcare.backoffice.entities.Patient} prefers which {@link
 * ch.camorcare.backoffice.entities.type.Gender} of a {@link ch.camorcare.backoffice.entities.Therapist} in combination
 * with a specific {@link ch.camorcare.backoffice.entities.TherapyType}.
 * <p/>
 * Example constraint: Patient P prefers male therapists when it comes to a therapy of the type "massage".
 */
public class PatientBasedTherapistGenderConstraint extends PatientBasedConstraint {

    /**
     * Preferred therapist gender for this kind of therapy.
     */
    private Gender gender;

    /**
     * The therapy type for which this therapist is preferred by the related patient.
     */
    private TherapyType therapyType;

    /**
     *
     */
    public PatientBasedTherapistGenderConstraint() {

    }

    /**
     * Copy constructor
     *
     * @param constraint
     */
    public PatientBasedTherapistGenderConstraint(PatientBasedTherapistGenderConstraint constraint) {
        this.setId(constraint.getId());
        this.setPatient(constraint.getPatient());
        this.setGender(constraint.getGender());
        this.setTherapyType(constraint.getTherapyType());
        this.setTimeCreated(constraint.getTimeCreated());
        this.setTimeModified(constraint.getTimeModified());
        this.setTimeDeleted(constraint.getTimeDeleted());
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public TherapyType getTherapyType() {
        return therapyType;
    }

    public void setTherapyType(TherapyType therapyType) {
        this.therapyType = therapyType;
    }
}
