package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's TherapistBasedTimeBlockConstraint
 * table.
 */
public interface TherapistBasedTimeBlockConstraintService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} to the persistence layer to
     * be inserted into the database.
     *
     * @param constraint TherapistBasedTimeBlockConstraint to be created
     * @return Created TherapistBasedTimeBlockConstraint
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapistBasedTimeBlockConstraint create(TherapistBasedTimeBlockConstraint constraint) throws ServiceException, ValidationException;

    /**
     * Passes a list of {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} to the persistence
     * layer along with the {@link ch.camorcare.backoffice.entities.Therapist} for whom the constraints are relevant.
     *
     * @param constraints The TherapistBasedTimeBlockConstraints to be added to the database
     * @param therapist   The Therapist for whom the constraints are relevant
     * @return A list of complete TherapistBasedTimeBlockConstraint objects
     * @throws ServiceException
     * @throws ValidationException
     */
    public List<TherapistBasedTimeBlockConstraint> create(List<TherapistBasedTimeBlockConstraint> constraints, Therapist therapist) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} to the persistence layer
     * whose data is updated in the database.
     *
     * @param constraint TherapistBasedTimeBlockConstraint to be updated
     * @return Updated TherapistBasedTimeBlockConstraint
     * @throws ServiceException
     * @throws ValidationException
     */
    public void update(TherapistBasedTimeBlockConstraint constraint) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} to the persistence layer to
     * be deleted.
     *
     * @param constraint TherapistBasedTimeBlockConstraint to be deleted
     * @throws ServiceException
     */
    public void delete(TherapistBasedTimeBlockConstraint constraint) throws ServiceException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapist} to the persistence layer to be used to search for
     * {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} objects that need to be deleted from
     * the database.
     *
     * @param therapist TherapistBasedTimeBlockConstraint to be deleted
     * @throws ServiceException
     */
    public void deleteTherapistBasedTimeBlockConstraintsByTherapist(Therapist therapist) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} objects in the database.
     *
     * @return List of TherapistBasedTimeBlockConstraint objects
     * @throws ServiceException
     */
    public List<TherapistBasedTimeBlockConstraint> findAllTherapistBasedTimeBlockConstraints() throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} objects in the database
     * relevant to a specific {@link ch.camorcare.backoffice.entities.Therapist}.
     *
     * @return List of TherapistBasedTimeBlockConstraint objects
     * @throws ServiceException
     */
    public List<TherapistBasedTimeBlockConstraint> findAllTherapistBasedTimeBlockConstraintsByTherapist(Therapist therapist) throws ServiceException;
}
