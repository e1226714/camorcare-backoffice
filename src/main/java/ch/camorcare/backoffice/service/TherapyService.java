package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's Therapy table.
 */
public interface TherapyService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapy} to the persistence layer to be inserted into the
     * database.
     *
     * @param original Therapy to be created
     * @return Created Therapy
     * @throws ServiceException
     * @throws ValidationException
     */
    public Therapy create(Therapy original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapy} to the persistence layer whose data is updated in
     * the database.
     *
     * @param original Therapy to be updated
     * @return The updated Therapy
     * @throws ServiceException
     * @throws ValidationException
     */
    public Therapy update(Therapy original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapy} to the persistence layer to be deleted.
     *
     * @param original Therapy to be deleted
     * @throws ServiceException
     */
    public void delete(Therapy original) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.Therapy} objects in the database.
     *
     * @return List of Therapy objects
     * @throws ServiceException
     */
    public List<Therapy> findAllTherapies() throws ServiceException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyPlan} to the persistence layer and then returns all
     * {@link ch.camorcare.backoffice.entities.Therapy} objects for the given TherapyPlan.
     *
     * @param original The TherapyPlan to be passed
     * @return A list of Therapy objects
     * @throws ServiceException
     */
    public List<Therapy> findAllTherapiesByTherapyPlan(TherapyPlan original) throws ServiceException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Patient} to the persistence layer and then returns all
     * {@link ch.camorcare.backoffice.entities.Therapy} objects for the given Patient.
     *
     * @param entity The Patient to be passed
     * @return A list of Therapy objects
     * @throws ServiceException
     */
    public List<Therapy> findAllTherapiesByPatient(Patient entity) throws ServiceException;
}
