package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's Patient table.
 */
public interface PatientService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Patient} to the persistence layer to be inserted into the
     * database.
     *
     * @param original Patient to be created
     * @return Created Patient
     * @throws ServiceException
     * @throws ValidationException
     */
    public Patient create(Patient original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Patient} to the persistence layer whose data is updated in
     * the database.
     *
     * @param original Patient to be updated
     * @throws ServiceException
     * @throws ValidationException
     */
    public void update(Patient original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Patient} to the persistence layer to be deleted.
     *
     * @param original Patient to be deleted
     * @throws ServiceException
     */
    public void delete(Patient original) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.Patient} objects in the database.
     *
     * @return List of Patient objects
     * @throws ServiceException
     */
    public List<Patient> findAllPatients() throws ServiceException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Patient} to the persistence layer and returns a Patient object
     * if the Patient is in the database, else <code>null</code>.
     *
     * @param patient Patient to be passed
     * @return Patient if found in the database, else <code>null</code>
     * @throws ServiceException
     */
    public Patient findOnePatientById(Patient patient) throws ServiceException;
}