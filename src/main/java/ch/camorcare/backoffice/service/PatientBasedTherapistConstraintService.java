package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's PatientBasedTherapistConstraint
 * table.
 */
public interface PatientBasedTherapistConstraintService {

    /**
     * Passes a list of {@link ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint} to the persistence
     * layer along with the {@link ch.camorcare.backoffice.entities.Patient} for whom the constraints are relevant.
     *
     * @param constraints The PatientBasedTherapistConstraints to be added to the database
     * @param patient     The Patient for whom the constraints are relevant
     * @return A list of complete PatientBasedTherapistConstraint objects
     * @throws ServiceException
     * @throws ValidationException
     */
    public List<PatientBasedTherapistConstraint> createPatientBasedTherapistGenderConstraints(List<PatientBasedTherapistConstraint> constraints, Patient patient) throws ServiceException, ValidationException;

    /**
     * Passes a specific {@link ch.camorcare.backoffice.entities.Patient} to the persistence layer so that way a list of
     * all {@link ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint} objects relevant for a specific
     * Patient can be found.
     *
     * @param patient The Patient used for the search function
     * @return A list of PatientBasedTherapistConstraint objects
     * @throws ServiceException
     * @throws ValidationException
     */
    public List<PatientBasedTherapistConstraint> findAllTherapistConstraintsByPatient(Patient patient) throws ServiceException, ValidationException;

}
