package ch.camorcare.backoffice.service;

/**
 * The service package represents the middle layer between the persistence layer and the presentation UI layer. To put
 * it one way, this is where the magic happens. This layer provides for any and all business logic that the program
 * requires to complete its tasks. This involves routing requested data from the DAOs to the correct UI and back,
 * performing checks on the validity of the data (if necessary) and further serves as a layer of abstraction. The only
 * requirement of the implementation is that the interfaces are implemented and interact with the DTOs.
 * <p/>
 * There is furthermore exception(s) designed specifically for the service layer to pass to the user interface and
 * thus obfuscate the connection to the persistence layer even further. Exceptions from the persistence layer are
 * caught in the service layer, converted and, if necessary, handled further before notifying the presentation layer
 * of any errors.
 */