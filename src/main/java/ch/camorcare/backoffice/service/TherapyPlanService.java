package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's TherapyPlan table.
 */
public interface TherapyPlanService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyPlan} to the persistence layer to be inserted into the
     * database.
     *
     * @param original TherapyPlan to be created
     * @return Created TherapyPlan
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapyPlan create(TherapyPlan original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyPlan} to the persistence layer whose data is updated in
     * the database.
     *
     * @param original TherapyPlan to be updated
     * @return Updated TherapyPlan
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapyPlan update(TherapyPlan original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyPlan} to the persistence layer to be deleted.
     *
     * @param original TherapyPlan to be deleted
     * @throws ServiceException
     */
    public void delete(TherapyPlan original) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.TherapyPlan} objects in the database.
     *
     * @return List of TherapyPlan objects
     * @throws ServiceException
     */
    public List<TherapyPlan> findAllTherapyPlans() throws ServiceException;

}
