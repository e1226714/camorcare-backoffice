package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.TherapyEvent;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyRoom;
import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's TherapyRoom table.
 */
public interface TherapyRoomService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyRoom} to the persistence layer to be inserted into the
     * database.
     *
     * @param original TherapyRoom to be created
     * @return Created TherapyRoom
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapyRoom create(TherapyRoom original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyRoom} to the persistence layer whose data is updated in
     * the database.
     *
     * @param original TherapyRoom to be updated
     * @return Updated TherapyRoom
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapyRoom update(TherapyRoom original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyRoom} to the persistence layer to be deleted.
     *
     * @param original TherapyRoom to be deleted
     * @throws ServiceException
     */
    public void delete(TherapyRoom original) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.TherapyRoom} objects in the database.
     *
     * @return List of TherapyRoom objects
     * @throws ServiceException
     */
    public List<TherapyRoom> findAllTherapyRooms() throws ServiceException;

    /**
     * Returns all therapy rooms available for a specific time block for a therapy event.
     * considers necessary room size and already taken rooms.
     *
     * @param therapyEvent
     * @param timeBlock
     * @return
     * @throws ServiceException
     * @throws ValidationException
     */
    public List<TherapyRoom> findAllTherapyRoomsByTherapyEventAndTimeBlock(TherapyEvent therapyEvent, TimeBlock timeBlock) throws ServiceException, ValidationException;

    /**
     * Returns all therapy rooms available for a therapy event.
     * considers necessary room size
     *
     * @param therapyEvent
     * @return
     * @throws ServiceException
     * @throws ValidationException
     */
    public List<TherapyRoom> findAllTherapyRoomsByTherapyEvent(TherapyEvent therapyEvent) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyPlan} to the persistence layer and expects a list of
     * {@link ch.camorcare.backoffice.entities.TherapyRoom} objects that are reserved during this TherapyPlan.
     *
     * @param therapyPlan The TherapyPlan to be passed
     * @return A list of TherapyRoom objects
     * @throws ServiceException
     */
    public List<TherapyRoom> findAllTherapyRoomsByTherapyPlan(TherapyPlan therapyPlan) throws ServiceException;
}
