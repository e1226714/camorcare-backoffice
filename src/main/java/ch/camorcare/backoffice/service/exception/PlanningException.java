package ch.camorcare.backoffice.service.exception;

/**
 * Defines an exception occurring at service level.
 */
public class PlanningException extends Exception {

    private static final long serialVersionUID = 8266436627403462987L;

    public PlanningException() {
        super();
    }

    public PlanningException(String string) {
        super(string);
    }
}