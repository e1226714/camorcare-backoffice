package ch.camorcare.backoffice.service.exception;

/**
 * Defines an exception occurring at service level.
 */
public class ServiceException extends Exception {

    private static final long serialVersionUID = -3233908355822751778L;

    public ServiceException() {
        super();
    }

    public ServiceException(String string) {
        super(string);
    }
}