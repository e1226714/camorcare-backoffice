package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's
 * PatientBasedTherapyTypeOccurrenceConstraint table.
 */
public interface PatientBasedTherapyTypeOccurrenceConstraintService {

    /**
     * Passes a list of {@link ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint} to the
     * persistence layer along with the {@link ch.camorcare.backoffice.entities.Therapist} for whom the constraints are
     * relevant.
     *
     * @param constraints The PatientBasedTherapyTypeOccurrenceConstraints to be added to the database
     * @param patient     The Patient for whom the constraints are relevant
     * @return A list of complete PatientBasedTherapyTypeOccurrenceConstraint objects
     * @throws ch.camorcare.backoffice.service.exception.ServiceException
     * @throws ch.camorcare.backoffice.util.validator.ValidationException
     */
    public List<PatientBasedTherapyTypeOccurrenceConstraint> createTherapyTypeOccurrenceConstraints(List<PatientBasedTherapyTypeOccurrenceConstraint> constraints, Patient patient) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Patient} object to the persistence layer to be used in a search
     * function that returns all {@link ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint}
     * objects stored in the database.
     *
     * @return List of all PatientBasedTherapyTypeOccurrenceConstraints
     * @throws ch.camorcare.backoffice.service.exception.ServiceException
     */
    public List<PatientBasedTherapyTypeOccurrenceConstraint> findAllPatientBasedTherapyTypeOccurrenceConstraintsByPatient(Patient patient) throws ServiceException;

}
