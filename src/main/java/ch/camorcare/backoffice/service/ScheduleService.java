package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.TherapyEvent;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;
import java.util.Map;

/**
 * This interface provides various methods for interfacing with database tables relevant to scheduling.
 */
public interface ScheduleService {

    /**
     * This method creates all therapy events of mode "SINGLE" for a given period. The relevant period is determined by
     * the {@link ch.camorcare.backoffice.entities.TherapyPlan}, which basically represents a week of a year. This
     * object is then passed to the persistence layer to be saved to the database.
     * <p/>
     * When creating the event, all related participations (link to therapies ({@link
     * ch.camorcare.backoffice.entities.TherapyEvent}) and therefore also each {@link
     * ch.camorcare.backoffice.entities.Patient}) are created as well.
     * <p/>
     * After the creation of events is completed, those events will still lack mandatory information, which has to be
     * supplemented by the automatic plan creation:
     * <ul>
     *     <li>{@link ch.camorcare.backoffice.entities.Therapist}</li>
     *     <li>{@link ch.camorcare.backoffice.entities.TherapyRoom}</li>
     *     <li>TherapyTime ({@link org.joda.time.DateTime})</li>
     *     <li>TherapyTimeBlock ({@link ch.camorcare.backoffice.entities.TimeBlock})</li>
     * </ul>
     *
     * @param therapyPlan The TherapyPlan being created
     * @return A map of Patient and the TherapyEvents that they attend
     * @throws ServiceException
     * @throws ValidationException
     * @see ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint
     * @see ch.camorcare.backoffice.entities.type.TherapyMode
     * @see ch.camorcare.backoffice.entities.TherapyPlan
     * @see ch.camorcare.backoffice.entities.TherapyEvent
     */
    public Map<Patient, List<TherapyEvent>> createSingleTherapyEventsForPlan(TherapyPlan therapyPlan) throws ServiceException, ValidationException;

    /**
     * This method creates all therapy events of mode "GROUP" for a given period. The relevant period is determined by
     * the {@link ch.camorcare.backoffice.entities.TherapyPlan}, which basically represents a week of a year. This
     * object is then passed to the persistence layer to be saved to the database.
     * <p/>
     * When creating the event, all related participations (link to therapies ({@link
     * ch.camorcare.backoffice.entities.TherapyEvent}) and therefore also each {@link
     * ch.camorcare.backoffice.entities.Patient}) are created as well.
     * <p/>
     * After the creation of events is completed, those events will still lack mandatory information, which has to be
     * supplemented by the automatic plan creation, namely {@link ch.camorcare.backoffice.entities.Therapist}, {@link
     * ch.camorcare.backoffice.entities.TherapyRoom}, TherapyTime ({@link org.joda.time.DateTime}), TherapyTimeBlock
     * ({@link ch.camorcare.backoffice.entities.TimeBlock}).
     *
     * @param therapyPlan The TherapyPlan being created
     * @return A map of Patient and the TherapyEvents that they attend
     * @throws ServiceException
     * @throws ValidationException
     * @see ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint
     * @see ch.camorcare.backoffice.entities.type.TherapyMode
     * @see ch.camorcare.backoffice.entities.TherapyPlan
     * @see ch.camorcare.backoffice.entities.TherapyEvent
     */
    public Map<TherapyType, List<TherapyEvent>> createGroupTherapyEventsForPlan(TherapyPlan therapyPlan) throws ServiceException, ValidationException;

}
