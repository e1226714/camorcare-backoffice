package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's TherapyType table.
 */
public interface TherapyTypeService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyType} to the persistence layer to be inserted into the
     * database.
     *
     * @param original TherapyType to be created
     * @return Created TherapyType
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapyType create(TherapyType original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyType} to the persistence layer whose data is updated in
     * the database.
     *
     * @param original TherapyType to be updated
     * @return The updated TherapyType
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapyType update(TherapyType original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyType} to the persistence layer to be deleted.
     *
     * @param original TherapyType to be deleted
     * @throws ServiceException
     */
    public void delete(TherapyType original) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.TherapyType} objects in the database.
     *
     * @return List of TherapyType objects
     * @throws ServiceException
     */
    public List<TherapyType> findAllTherapyTypes() throws ServiceException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapist} to the persistence layer and then returns all
     * {@link ch.camorcare.backoffice.entities.TherapyType} objects for the given Therapist.
     *
     * @param therapist The Therapist to be passed
     * @return A list of Therapy objects
     * @throws ServiceException
     */
    public List<TherapyType> findAllTherapyTypesByTherapist(Therapist therapist) throws ServiceException;
}
