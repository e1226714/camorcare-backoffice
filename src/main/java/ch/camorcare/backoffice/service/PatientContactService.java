package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.PatientContact;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's PatientContact table.
 */
public interface PatientContactService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.PatientContact} to the persistence layer to be inserted into the
     * database.
     *
     * @param original PatientContact to be created
     * @return Created PatientContact
     * @throws ServiceException
     * @throws ValidationException
     */
    public PatientContact create(PatientContact original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.PatientContact} to the persistence layer whose data is updated in
     * the database.
     *
     * @param original PatientContact to be updated
     * @return Updated PatientContact
     * @throws ServiceException
     * @throws ValidationException
     */
    public PatientContact update(PatientContact original) throws ServiceException, ValidationException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.PatientContact} objects in the database.
     *
     * @return List of PatientContact objects
     * @throws ServiceException
     */
    public void delete(PatientContact original) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.PatientContact} objects in the database.
     *
     * @return List of PatientContact objects
     * @throws ServiceException
     */
    public List<PatientContact> findAllPatientContacts() throws ServiceException;
}
