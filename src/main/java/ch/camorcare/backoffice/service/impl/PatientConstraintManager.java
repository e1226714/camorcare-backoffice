package ch.camorcare.backoffice.service.impl;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint;
import ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint;
import ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.persistence.PatientBasedTherapistConstraintDAO;
import ch.camorcare.backoffice.persistence.PatientBasedTherapistGenderConstraintDAO;
import ch.camorcare.backoffice.persistence.PatientBasedTherapyTypeOccurrenceConstraintDAO;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.PatientBasedTherapistConstraintService;
import ch.camorcare.backoffice.service.PatientBasedTherapistGenderConstraintService;
import ch.camorcare.backoffice.service.PatientBasedTherapyTypeOccurrenceConstraintService;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of PatientBasedConstraint services.
 */
public class PatientConstraintManager implements PatientBasedTherapistGenderConstraintService, PatientBasedTherapistConstraintService, PatientBasedTherapyTypeOccurrenceConstraintService {

    /**
     * The data access object for a PatientBasedTherapistGenderConstraint
     */
    private PatientBasedTherapistGenderConstraintDAO patientBasedTherapistGenderConstraintDAO;

    /**
     * The data access object for a PatientBasedTherapistConstraint
     */
    private PatientBasedTherapistConstraintDAO patientBasedTherapistConstraintDAO;

    /**
     * The data access object for a PatientBasedTherapyTypeOccurrenceConstraint
     */
    private PatientBasedTherapyTypeOccurrenceConstraintDAO patientBasedTherapyTypeOccurrenceConstraintDAO;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(PatientConstraintManager.class.getName());

    public void setPatientBasedTherapistGenderConstraintDAO(PatientBasedTherapistGenderConstraintDAO patientBasedTherapistGenderConstraintDAO) {
        this.patientBasedTherapistGenderConstraintDAO = patientBasedTherapistGenderConstraintDAO;
    }

    public void setPatientBasedTherapistConstraintDAO(PatientBasedTherapistConstraintDAO patientBasedTherapistConstraintDAO) {
        this.patientBasedTherapistConstraintDAO = patientBasedTherapistConstraintDAO;
    }

    public void setPatientBasedTherapyTypeOccurrenceConstraintDAO(PatientBasedTherapyTypeOccurrenceConstraintDAO patientBasedTherapyTypeOccurrenceConstraintDAO) {
        this.patientBasedTherapyTypeOccurrenceConstraintDAO = patientBasedTherapyTypeOccurrenceConstraintDAO;
    }

    @Override
    public List<PatientBasedTherapistGenderConstraint> create(List<PatientBasedTherapistGenderConstraint> constraints, Patient patient) throws ServiceException, ValidationException {
        List<PatientBasedTherapistGenderConstraint> result = new ArrayList<>();

        try {
            patientBasedTherapistGenderConstraintDAO.deleteAllByPatient(patient);

            for (PatientBasedTherapistGenderConstraint constraint : constraints) {
                constraint.setId(null);
                result.add(patientBasedTherapistGenderConstraintDAO.create(constraint));
            }

        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }

    @Override
    public List<PatientBasedTherapistGenderConstraint> findAllTherapistGenderConstraintsByPatient(Patient patient) throws ServiceException, ValidationException {
        try {
            return patientBasedTherapistGenderConstraintDAO.findAllByPatient(patient, true);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<PatientBasedTherapistConstraint> createPatientBasedTherapistGenderConstraints(List<PatientBasedTherapistConstraint> constraints, Patient patient) throws ServiceException, ValidationException {
        List<PatientBasedTherapistConstraint> result = new ArrayList<>();

        try {
            patientBasedTherapistConstraintDAO.deleteAllByPatient(patient);

            for (PatientBasedTherapistConstraint constraint : constraints) {
                constraint.setId(null);
                result.add(patientBasedTherapistConstraintDAO.create(constraint));
            }

        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }

    @Override
    public List<PatientBasedTherapistConstraint> findAllTherapistConstraintsByPatient(Patient patient) throws ServiceException, ValidationException {
        try {
            return patientBasedTherapistConstraintDAO.findAllByPatient(patient, true);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<PatientBasedTherapyTypeOccurrenceConstraint> createTherapyTypeOccurrenceConstraints(List<PatientBasedTherapyTypeOccurrenceConstraint> constraints, Patient patient) throws ServiceException, ValidationException {
        List<PatientBasedTherapyTypeOccurrenceConstraint> result = new ArrayList<>();

        try {
            if (findAllPatientBasedTherapyTypeOccurrenceConstraintsByPatient(patient).size()>0) {
                patientBasedTherapyTypeOccurrenceConstraintDAO.deleteAllByPatient(patient);
            }
            for (PatientBasedTherapyTypeOccurrenceConstraint constraint : constraints) {
                constraint.setId(null);
                result.add(patientBasedTherapyTypeOccurrenceConstraintDAO.create(constraint));
            }

        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }

    @Override
    public List<PatientBasedTherapyTypeOccurrenceConstraint> findAllPatientBasedTherapyTypeOccurrenceConstraintsByPatient(Patient patient) throws ServiceException {
        try {
            return patientBasedTherapyTypeOccurrenceConstraintDAO.findAllByPatient(patient, true);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }
}
