package ch.camorcare.backoffice.service.impl;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.persistence.PatientBasedTherapyTypeOccurrenceConstraintDAO;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.ScheduleService;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the ScheduleService.
 */
public class ScheduleManager implements ScheduleService {

    /**
     * Manager for therapy related operations.
     */
    private TherapyManager therapyManager;

    /**
     * Manager for patient related operations.
     */
    private PatientManager patientManager;

    /**
     * DAO for patient based therapy type occurrence constraints.
     */
    private PatientBasedTherapyTypeOccurrenceConstraintDAO patientBasedTherapyTypeOccurrenceConstraintDAO;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(ScheduleManager.class.getName());


    @Override
    public Map<Patient, List<TherapyEvent>> createSingleTherapyEventsForPlan(TherapyPlan therapyPlan) throws ServiceException, ValidationException {
        Map<Patient, List<TherapyEvent>> result = new HashMap<>();

        try {
            log.debug("createSingleTherapyEventsForPlan");
            List<Therapy> therapies = therapyManager.getTherapyDAO().findAllByTherapyPlan(therapyPlan);

            /**
             * Add patients to list.
             */
            for(Therapy therapy : therapies) {
                Patient patient = patientManager.findOnePatientById(therapy.getPatient());
                List<PatientBasedTherapyTypeOccurrenceConstraint> therapyTypeConstraints = patientBasedTherapyTypeOccurrenceConstraintDAO.findAllByPatient(patient, false);
                List<TherapyEvent> events = new ArrayList<>();

                for (PatientBasedTherapyTypeOccurrenceConstraint therapyTypeConstraint : therapyTypeConstraints) {

                    // single mode therapy
                    if(therapyTypeConstraint.getTherapyMode().equals(TherapyMode.SINGLE)) {

                        for(int i = 0; i < therapyTypeConstraint.getMaxOccurencePerWeek(); i++) {
                            TherapyEvent therapyEvent = new TherapyEvent();
                            therapyEvent.setTherapyPlan(therapyPlan);
                            therapyEvent.setTherapyType(therapyTypeConstraint.getTherapyType());
                            therapyEvent.setTherapyMode(TherapyMode.SINGLE);
                            therapyEvent.getTherapies().add(therapy);

                            therapyEvent = therapyManager.create(therapyEvent);
                            events.add(therapyEvent);
                        }
                    }
                }

                result.put(patient, events);
            }

        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }

    @Override
    public Map<TherapyType, List<TherapyEvent>> createGroupTherapyEventsForPlan(TherapyPlan therapyPlan) throws ServiceException, ValidationException {
        log.debug("createGroupTherapyEventsForPlan");
        Map<TherapyType, List<TherapyEvent>> result = new HashMap<>();

        try {
            Map<TherapyType, Map<String, Integer>> stats = patientBasedTherapyTypeOccurrenceConstraintDAO.findAllStatsByTherapyPlanGroupedByTherapyType(therapyPlan);

            for (Map.Entry<TherapyType, Map<String, Integer>> entry : stats.entrySet()) {
                TherapyType therapyType = entry.getKey();
                Map<String, Integer> therapyTypeStats = entry.getValue();

                List<TherapyEvent> events = new ArrayList<>();
                therapyType = therapyManager.getTherapyTypeDAO().findOneById(therapyType);

                int sum = (int) Math.ceil((therapyTypeStats.get("SUM") / therapyType.getMaxGroupSize()));
                int eventsNeeded = Math.max(sum, therapyTypeStats.get("MAX"));

                for(int i = 0; i < eventsNeeded; i++) {
                    TherapyEvent event = new TherapyEvent();
                    event.setTherapyPlan(therapyPlan);
                    event.setTherapyType(therapyType);
                    event.setTherapyMode(TherapyMode.GROUP);

                    event = this.therapyManager.create(event);
                    events.add(event);
                }

                result.put(therapyType, events);
            }
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }

    public void setTherapyManager(TherapyManager therapyManager) {
        this.therapyManager = therapyManager;
    }

    public void setPatientManager(PatientManager patientManager) {
        this.patientManager = patientManager;
    }

    public void setPatientBasedTherapyTypeOccurrenceConstraintDAO(PatientBasedTherapyTypeOccurrenceConstraintDAO patientBasedTherapyTypeOccurrenceConstraintDAO) {
        this.patientBasedTherapyTypeOccurrenceConstraintDAO = patientBasedTherapyTypeOccurrenceConstraintDAO;
    }
}
