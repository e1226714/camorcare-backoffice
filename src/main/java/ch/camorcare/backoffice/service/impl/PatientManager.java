package ch.camorcare.backoffice.service.impl;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.persistence.*;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.PatientService;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Implementation of the PatientService.
 */
public class PatientManager implements PatientService {

    /**
     * The data access object for patient.
     */
    private PatientDAO patientDAO;

    /**
     * Patient validator.
     */
    private PatientValidator patientValidator;

    /**
     * The data access object for addresses.
     */
    private AddressDAO addressDAO;

    /**
     * Address validator.
     */
    private AddressValidator addressValidator;

    /**
     * The data access object for patient allergy.
     */
    private AllergyDAO allergyDAO;

    /**
     * Patient allergy validator.
     */
    private AllergyValidator allergyValidator;

    /**
     * The data access object for comments on patients.
     */
    private PatientCommentDAO patientCommentDAO;

    /**
     * Patient comment validator.
     */
    private PatientCommentValidator patientCommentValidator;

    /**
     * The data access object for patient contact.
     */
    private PatientContactDAO patientContactDAO;

    /**
     * Patient contact validator.
     */
    private PatientContactValidator patientContactValidator;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(PatientManager.class.getName());

    @Override
    public Patient create(Patient original) throws ServiceException, ValidationException {
        log.debug("Create: " + original);

        try {
            Patient existing = patientDAO.findOneBySSNR(original);

            if(existing != null) {
                throw new ServiceException("Patient mit dieser Versicherungsnummer existiert bereits.");
            }

            original.setAddress(addressDAO.create(original.getAddress()));
            return patientDAO.create(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void update(Patient p) throws ServiceException, ValidationException {
        log.debug("Update: " + p);
        try {
            addressDAO.update(p.getAddress());
            patientDAO.update(p);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void delete(Patient p) throws ServiceException {
        log.debug("Delete: " + p);
        try {
            patientDAO.delete(p);
            addressDAO.delete(p.getAddress());
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Patient> findAllPatients() throws ServiceException {
        log.debug("findAllPatients");

        try {
            List<Patient> patients = patientDAO.findAll();

            for (Patient p : patients) {
                p.setAddress(addressDAO.findOneById(p.getAddress()));
            }

            return patients;
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Patient findOnePatientById(Patient p) throws ServiceException {
        log.debug("findOnePatientById");

        try {
            Patient patient = patientDAO.findOneById(p);
            patient.setAddress(addressDAO.findOneById(patient.getAddress()));
            return patient;
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    public PatientDAO getPatientDAO() {
        return patientDAO;
    }

    public void setPatientDAO(PatientDAO patientDAO) {
        this.patientDAO = patientDAO;
    }

    public AddressDAO getAddressDAO() {
        return addressDAO;
    }

    public void setAddressDAO(AddressDAO addressDAO) {
        this.addressDAO = addressDAO;
    }

    public AllergyDAO getAllergyDAO() {
        return allergyDAO;
    }

    public void setAllergyDAO(AllergyDAO allergyDAO) {
        this.allergyDAO = allergyDAO;
    }

    public PatientCommentDAO getPatientCommentDAO() {
        return patientCommentDAO;
    }

    public void setPatientCommentDAO(PatientCommentDAO patientCommentDAO) {
        this.patientCommentDAO = patientCommentDAO;
    }

    public PatientContactDAO getPatientContactDAO() {
        return patientContactDAO;
    }

    public void setPatientContactDAO(PatientContactDAO patientContactDAO) {
        this.patientContactDAO = patientContactDAO;
    }

    public PatientValidator getPatientValidator() {
        return patientValidator;
    }

    public void setPatientValidator(PatientValidator patientValidator) {
        this.patientValidator = patientValidator;
    }

    public AddressValidator getAddressValidator() {
        return addressValidator;
    }

    public void setAddressValidator(AddressValidator addressValidator) {
        this.addressValidator = addressValidator;
    }

    public AllergyValidator getAllergyValidator() {
        return allergyValidator;
    }

    public void setAllergyValidator(AllergyValidator allergyValidator) {
        this.allergyValidator = allergyValidator;
    }

    public PatientCommentValidator getPatientCommentValidator() {
        return patientCommentValidator;
    }

    public void setPatientCommentValidator(PatientCommentValidator patientCommentValidator) {
        this.patientCommentValidator = patientCommentValidator;
    }

    public PatientContactValidator getPatientContactValidator() {
        return patientContactValidator;
    }

    public void setPatientContactValidator(PatientContactValidator patientContactValidator) {
        this.patientContactValidator = patientContactValidator;
    }
}
