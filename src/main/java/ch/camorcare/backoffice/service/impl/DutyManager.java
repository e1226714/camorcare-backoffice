package ch.camorcare.backoffice.service.impl;

import ch.camorcare.backoffice.entities.DutyCategory;
import ch.camorcare.backoffice.persistence.DutyCategoryDAO;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.DutyCategoryService;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.DutyCategoryValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Implementation of DutyCategoryService.
 */
public class DutyManager implements DutyCategoryService {

    /**
     * The data access object for DutyCategory.
     */
    private DutyCategoryDAO dutyCategoryDAO;

    /**
     * The duty category validator.
     */
    private DutyCategoryValidator dutyCategoryValidator;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(DutyManager.class.getName());

    @Override
    public DutyCategory create(DutyCategory original) throws ServiceException, ValidationException {
        dutyCategoryValidator.validate(original);
        DutyCategory dutyCategory = null;

        try {
            log.debug("Create: " + original);
            dutyCategory = dutyCategoryDAO.create(original);
        } catch (ValidationException e) {
            log.error("ValidationException " + e.getMessage());
            throw new ServiceException("Invalid entity");
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return dutyCategory;
    }

    @Override
    public DutyCategory update(DutyCategory original) throws ServiceException, ValidationException {
        dutyCategoryValidator.validate(original);
        DutyCategory dutyCategory = null;

        try {
            log.debug("Update: " + original);
            dutyCategory = dutyCategoryDAO.update(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return dutyCategory;
    }

    @Override
    public void delete(DutyCategory original) throws ServiceException {
        try {
            log.debug("Delete: " + original);
            dutyCategoryDAO.delete(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<DutyCategory> findAllDutyCategories() throws ServiceException {
        try {
            log.debug("findAllDutyCategories");
            return dutyCategoryDAO.findAll();
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    public void setDutyCategoryDAO(DutyCategoryDAO dutyCategoryDAO) {
        this.dutyCategoryDAO = dutyCategoryDAO;
    }

    public void setDutyCategoryValidator(DutyCategoryValidator dutyCategoryValidator) {
        this.dutyCategoryValidator = dutyCategoryValidator;
    }
}
