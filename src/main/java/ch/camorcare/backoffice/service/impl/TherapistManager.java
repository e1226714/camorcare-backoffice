package ch.camorcare.backoffice.service.impl;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.TherapistDAO;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.TherapistService;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Implementation of TherapistService.
 */
public class TherapistManager implements TherapistService {

    /**
     * The data access object for Therapist.
     */
    private TherapistDAO therapistDAO;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapistManager.class.getName());

    @Override
    public Therapist create(Therapist original) throws ServiceException, ValidationException {
        Therapist therapist;

        try {
            log.debug("create: " + original);

            if (original.getId() != null) {
                log.error("ID is already assigned");
                throw new ValidationException("ID is not null");
            }

            therapist = therapistDAO.create(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapist;
    }

    @Override
    public Therapist update(Therapist original) throws ServiceException, ValidationException {
        Therapist therapist;

        try {
            log.debug("update: " + original);

            if (original.getId() == null) {
                log.error("ID is not assigned");
                throw new ValidationException("ID is null");
            }

            therapist = therapistDAO.update(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapist;
    }

    @Override
    public void delete(Therapist original) throws ServiceException {
        try {
            log.debug("delete: " + original);
            therapistDAO.delete(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Therapist> findAllTherapists() throws ServiceException {
        try {
            log.debug("findAllTherapists");
            return therapistDAO.findAll();
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    @Deprecated
    public List<Therapist> findAllTherapistsByTherapyType(TherapyType therapyType) throws ServiceException {
        try {
            log.debug("findAllTherapistsByTherapyType");
            return therapistDAO.findAllByTherapyType(therapyType);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Therapist> findAllTherapistsByTherapyPlan(TherapyPlan therapyPlan) throws ServiceException {
        try {
            log.debug("findAllTherapistsByTherapyPlan");
            return therapistDAO.findAllByTherapyPlan(therapyPlan);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Therapist findOneById(Therapist therapist) throws ServiceException {
        try {
            log.debug("findOneById");
            return therapistDAO.findOneById(therapist);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    public TherapistDAO getTherapistDAO() {
        return therapistDAO;
    }

    public void setTherapistDAO(TherapistDAO therapistDAO) {
        this.therapistDAO = therapistDAO;
    }
}
