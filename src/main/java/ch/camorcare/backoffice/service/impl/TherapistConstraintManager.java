package ch.camorcare.backoffice.service.impl;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.persistence.TherapistBasedTherapyTypeOccurrenceConstraintDAO;
import ch.camorcare.backoffice.persistence.TherapistBasedTimeBlockConstraintDAO;
import ch.camorcare.backoffice.persistence.TherapistDAO;
import ch.camorcare.backoffice.persistence.TimeBlockDAO;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.TherapistBasedTherapyTypeOccurrenceConstraintService;
import ch.camorcare.backoffice.service.TherapistBasedTimeBlockConstraintService;
import ch.camorcare.backoffice.service.TimeBlockService;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the TimeBlockService, TherapistBasedTimeBlockConstraintService and
 * TherapistBasedTherapyTypeOccurrenceConstraintService.
 */
public class TherapistConstraintManager implements TimeBlockService, TherapistBasedTimeBlockConstraintService, TherapistBasedTherapyTypeOccurrenceConstraintService {

    /**
     * The data access object for TimeBlock
     */
    private TherapistDAO therapistDAO;

    /**
     *
     */
    private TimeBlockDAO timeBlockDAO;

    /**
     * The data access object for TherapistBasedTimeBlockConstraint
     */
    private TherapistBasedTimeBlockConstraintDAO therapistBasedTimeBlockConstraintDAO;

    /**
     * The data access object for TherapistBasedTherapyTypeOccurrence
     */
    private TherapistBasedTherapyTypeOccurrenceConstraintDAO therapistBasedTherapyTypeOccurrenceConstraintDAO;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapistConstraintManager.class.getName());

    public void setTherapistDAO(TherapistDAO therapistDAO) {
        this.therapistDAO = therapistDAO;
    }

    public void setTimeBlockDAO(TimeBlockDAO timeBlockDAO) {
        this.timeBlockDAO = timeBlockDAO;
    }

    public void setTherapistBasedTimeBlockConstraintDAO(TherapistBasedTimeBlockConstraintDAO constraintDAO) {
        this.therapistBasedTimeBlockConstraintDAO = constraintDAO;
    }

    public void setTherapistBasedTherapyTypeOccurrenceConstraintDAO(TherapistBasedTherapyTypeOccurrenceConstraintDAO constraintDAO) {
        this.therapistBasedTherapyTypeOccurrenceConstraintDAO = constraintDAO;
    }

    @Override
    public TherapistBasedTimeBlockConstraint create(TherapistBasedTimeBlockConstraint constraint) throws ServiceException, ValidationException {
        TherapistBasedTimeBlockConstraint result;

        try {
            result = therapistBasedTimeBlockConstraintDAO.create(constraint);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }

    @Override
    public List<TherapistBasedTimeBlockConstraint> create(List<TherapistBasedTimeBlockConstraint> constraints, Therapist therapist) throws ServiceException, ValidationException {
        List<TherapistBasedTimeBlockConstraint> result = new ArrayList<>();

        try {
            if (therapistBasedTimeBlockConstraintDAO.findAllByTherapist(therapist,false).size()>0) {
                therapistBasedTimeBlockConstraintDAO.deleteByTherapist(therapist);
            }

            for (TherapistBasedTimeBlockConstraint constraint : constraints) {
                result.add(this.create(constraint));
            }

        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }

    @Override
    public void update(TherapistBasedTimeBlockConstraint constraint) throws ServiceException, ValidationException {
        // TODO Please implement
    }

    @Override
    public void delete(TherapistBasedTimeBlockConstraint constraint) throws ServiceException {
        // TODO Please implement
    }

    @Override
    public void deleteTherapistBasedTimeBlockConstraintsByTherapist(Therapist therapist) throws ServiceException {
        // TODO Please implement
    }

    @Override
    public List<TherapistBasedTimeBlockConstraint> findAllTherapistBasedTimeBlockConstraints() throws ServiceException {
        // TODO Please implement
        return null;
    }

    @Override
    public List<TherapistBasedTimeBlockConstraint> findAllTherapistBasedTimeBlockConstraintsByTherapist(Therapist therapist) throws ServiceException {
        try {
            return therapistBasedTimeBlockConstraintDAO.findAllByTherapist(therapist, true);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TimeBlock> findAllTimeBlocks() throws ServiceException {
        try {
            return timeBlockDAO.findAll();
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Map<Day, List<TimeBlock>> findAllTimeBlocksGroupedByDay() throws ServiceException {
        try {
            return timeBlockDAO.findAllGroupedByDay();
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapistBasedTherapyTypeOccurrenceConstraint> findAllTherapistBasedTherapyTypeOccurrenceConstraintsByTherapist(Therapist therapist) throws ServiceException {
        try {
            return therapistBasedTherapyTypeOccurrenceConstraintDAO.findAllByTherapist(therapist);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public TherapistBasedTherapyTypeOccurrenceConstraint create(TherapistBasedTherapyTypeOccurrenceConstraint constraint) throws ServiceException, ValidationException {
        TherapistBasedTherapyTypeOccurrenceConstraint result;

        try {
            result = therapistBasedTherapyTypeOccurrenceConstraintDAO.create(constraint);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }

    @Override
    public TherapistBasedTherapyTypeOccurrenceConstraint update(TherapistBasedTherapyTypeOccurrenceConstraint constraint) throws ServiceException, ValidationException {
        TherapistBasedTherapyTypeOccurrenceConstraint result;
        try {
            result = therapistBasedTherapyTypeOccurrenceConstraintDAO.update(constraint);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }

    @Override
    public void delete(TherapistBasedTherapyTypeOccurrenceConstraint constraint) throws ServiceException {
        try {
            therapistBasedTherapyTypeOccurrenceConstraintDAO.delete(constraint);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapistBasedTherapyTypeOccurrenceConstraint> createTherapyTypeOccurrenceConstraints(List<TherapistBasedTherapyTypeOccurrenceConstraint> constraints, Therapist therapist) throws ServiceException, ValidationException {
        List<TherapistBasedTherapyTypeOccurrenceConstraint> result = new ArrayList<>();

        for (TherapistBasedTherapyTypeOccurrenceConstraint constraint : constraints) {
            if (constraint.getId() == null) {
                result.add(this.create(constraint));
            }
            else {
                result.add(this.update(constraint));
            }
        }

        return result;
    }

    public List<Therapist> findAllTherapistsByTherapyType(TherapyType therapyType) throws ServiceException {
        List<Therapist> result = new ArrayList<>();

        try {
            List<TherapistBasedTherapyTypeOccurrenceConstraint> constraints = therapistBasedTherapyTypeOccurrenceConstraintDAO.findAllByTherapyType(therapyType);
            List<Integer> therapistIDs = new ArrayList<>();

            for (TherapistBasedTherapyTypeOccurrenceConstraint constraint : constraints) {
                if (!therapistIDs.contains(constraint.getTherapist().getId())) {
                    result.add(therapistDAO.findOneById(constraint.getTherapist()));
                    therapistIDs.add(constraint.getTherapist().getId());
                }
            }
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return result;
    }
}
