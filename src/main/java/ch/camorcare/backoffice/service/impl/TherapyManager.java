package ch.camorcare.backoffice.service.impl;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.persistence.*;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.*;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.List;

/**
 * Implementation of TherapyService, TherapyTypeService, TherapyEventService, TherapyRoomService and
 * TherapyPlanService.
 */
public class TherapyManager implements TherapyService, TherapyTypeService, TherapyEventService, TherapyRoomService, TherapyPlanService {

    /**
     * The data access object for therapy.
     */
    private TherapyDAO therapyDAO;

    /**
     * The data access object for therapy type.
     */
    private TherapyTypeDAO therapyTypeDAO;

    /**
     * The data access object for therapy event.
     */
    private TherapyEventDAO therapyEventDAO;

    /**
     * The data access object for therapy type.
     */
    private TherapyRoomDAO therapyRoomDAO;

    /**
     * The data access object for therapy plan.
     */
    private TherapyPlanDAO therapyPlanDAO;

    /**
     * The data access object for therapist
     */
    private TherapistDAO therapistDAO;

    /**
     * The data access object for patients
     */
    private PatientDAO patientDAO;

    /**
     * The data access object for timeBlock
     */
    private TimeBlockDAO timeBlockDAO;

    /**
     * The data access object for therapistBasedTimeBlockConstraint
     */
    private TherapistBasedTimeBlockConstraintDAO therapistBasedTimeBlockConstraintDAO;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapyManager.class.getName());

    @Override
    public TherapyEvent create(TherapyEvent original) throws ServiceException, ValidationException {
        TherapyEvent therapyEvent = null;

        try {
            log.debug("Create: " + original);
            therapyEvent = therapyEventDAO.create(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapyEvent;
    }

    @Override
    public TherapyEvent update(TherapyEvent original) throws ServiceException, ValidationException {
        TherapyEvent therapyEvent = null;

        try {
            log.debug("Update: " + original);
            therapyEvent = therapyEventDAO.update(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapyEvent;
    }

    @Override
    public void delete(TherapyEvent original) throws ServiceException {
        try {
            log.debug("Delete: " + original);
            therapyEventDAO.delete(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyEvent> findAllTherapyEvents() throws ServiceException {
        try {
            log.debug("findAllTherapyEvents");
            List<TherapyEvent> lte = therapyEventDAO.findAll();
            for (TherapyEvent te : lte) {
                if (te.getTherapist() != null) {
                    te.setTherapist(therapistDAO.findOneById(te.getTherapist()));
                }
                if (te.getTherapyType() != null) {
                    te.setTherapyType(therapyTypeDAO.findOneById(te.getTherapyType()));
                }
                if (te.getTherapyPlan() != null) {
                    te.setTherapyPlan(therapyPlanDAO.findOneById(te.getTherapyPlan()));
                }
                if (te.getTherapyTimeBlock() != null) {
                    te.setTherapyTimeBlock(timeBlockDAO.findOneById(te.getTherapyTimeBlock()));
                }
                if (te.getTherapies() != null) {
                    te.setTherapies(therapyDAO.findAllByTherapyPlan(te.getTherapyPlan()));

                    List<Therapy> lt = te.getTherapies();
                    for (Therapy t : lt) {
                        t.setPatient(patientDAO.findOneById(t.getPatient()));
                    }
                }
            }
            return lte;
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyEvent> findAllTherapyEventsByTherapyPlan(TherapyPlan therapyPlan) throws ServiceException {
        try {
            log.debug("findAllTherapyEvents");
            List<TherapyEvent> lte = therapyEventDAO.findAllByTherapyPlan(therapyPlan, false);
            for (TherapyEvent te : lte) {
                if (te.getTherapist() != null) {
                    te.setTherapist(therapistDAO.findOneById(te.getTherapist()));
                }
                if (te.getTherapyType() != null) {
                    te.setTherapyType(therapyTypeDAO.findOneById(te.getTherapyType()));
                }
                if (te.getTherapyPlan() != null) {
                    te.setTherapyPlan(therapyPlanDAO.findOneById(te.getTherapyPlan()));
                }
                if (te.getTherapies() != null) {
                    te.setTherapies(therapyDAO.findAllByTherapyPlan(te.getTherapyPlan()));

                    List<Therapy> lt = te.getTherapies();
                    for (Therapy t : lt) {
                        t.setPatient(patientDAO.findOneById(t.getPatient()));
                    }
                }
            }
            return lte;
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        } catch (ValidationException e) {
            log.error("ValidationException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyEvent> getTherapyEventsByTherapyPlanAndTherapyRoom(TherapyPlan therapyPlan, TherapyRoom therapyRoom) throws ServiceException, ValidationException {
        try {
            log.debug("findAllTherapyEvents for Room: " + therapyRoom.getName());
            List<TherapyEvent> lte = therapyEventDAO.findAllByTherapyRoomAndTherapyPlan(therapyRoom, therapyPlan, false);
            for (TherapyEvent te : lte) {
                if (te.getTherapist() != null) {
                    te.setTherapist(therapistDAO.findOneById(te.getTherapist()));
                }
                if (te.getTherapyType() != null) {
                    te.setTherapyType(therapyTypeDAO.findOneById(te.getTherapyType()));
                }
                if (te.getTherapyPlan() != null) {
                    te.setTherapyPlan(therapyPlanDAO.findOneById(te.getTherapyPlan()));
                }
                if (te.getTherapyTimeBlock() != null) {
                    te.setTherapyTimeBlock(timeBlockDAO.findOneById(te.getTherapyTimeBlock()));
                }
                if (te.getTherapyRoom() != null) {
                    te.setTherapyRoom(therapyRoomDAO.findOneById(te.getTherapyRoom()));
                }
                if (te.getTherapies() != null) {
                    te.setTherapies(therapyDAO.findAllByTherapyPlan(te.getTherapyPlan()));

                    List<Therapy> lt = te.getTherapies();
                    for (Therapy t : lt) {
                        t.setPatient(patientDAO.findOneById(t.getPatient()));
                    }
                }
            }
            return lte;
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyEvent> getTherapyEventsByTherapyPlanAndTherapist(TherapyPlan therapyPlan, Therapist therapist) throws ServiceException, ValidationException {
        try {
            log.debug("findAllTherapyEvents for Therapist: " + therapist.getFirstName() + " " + therapist.getLastName());
            List<TherapyEvent> lte = therapyEventDAO.findAllByTherapistAndTherapyPlan(therapist, therapyPlan, false);
            for (TherapyEvent te : lte) {
                if (te.getTherapist() != null) {
                    te.setTherapist(therapistDAO.findOneById(te.getTherapist()));
                }
                if (te.getTherapyType() != null) {
                    te.setTherapyType(therapyTypeDAO.findOneById(te.getTherapyType()));
                }
                if (te.getTherapyPlan() != null) {
                    te.setTherapyPlan(therapyPlanDAO.findOneById(te.getTherapyPlan()));
                }
                if (te.getTherapyTimeBlock() != null) {
                    te.setTherapyTimeBlock(timeBlockDAO.findOneById(te.getTherapyTimeBlock()));
                }
                if (te.getTherapyRoom() != null) {
                    te.setTherapyRoom(therapyRoomDAO.findOneById(te.getTherapyRoom()));
                }
                if (te.getTherapies() != null) {
                    te.setTherapies(therapyDAO.findAllByTherapyPlan(te.getTherapyPlan()));

                    List<Therapy> lt = te.getTherapies();
                    for (Therapy t : lt) {
                        t.setPatient(patientDAO.findOneById(t.getPatient()));
                    }
                }
            }
            return lte;
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyEvent> getTherapyEventsByTherapyPlanAndPatient(TherapyPlan therapyPlan, Patient patient) throws ServiceException, ValidationException {
        try {
            log.debug("findAllTherapyEvents for Patient: " + patient.getFirstName() + " " + patient.getLastName());
            List<Therapy> therapies = findAllTherapiesByTherapyPlan(therapyPlan);
            for (Therapy therapy : therapies) {
                if (therapy.getPatient().getId().equals(patient.getId())) {
                    List<TherapyEvent> lte = therapyEventDAO.findAllByTherapyAndTherapyPlan(therapy, therapyPlan, false);

                    // fills entities with all attributes (eager loading)
                    for (TherapyEvent te : lte) {
                        if (te.getTherapist() != null) {
                            te.setTherapist(therapistDAO.findOneById(te.getTherapist()));
                        }
                        if (te.getTherapyType() != null) {
                            te.setTherapyType(therapyTypeDAO.findOneById(te.getTherapyType()));
                        }
                        if (te.getTherapyPlan() != null) {
                            te.setTherapyPlan(therapyPlanDAO.findOneById(te.getTherapyPlan()));
                        }
                        if (te.getTherapyTimeBlock() != null) {
                            te.setTherapyTimeBlock(timeBlockDAO.findOneById(te.getTherapyTimeBlock()));
                        }
                        if (te.getTherapyRoom() != null) {
                            te.setTherapyRoom(therapyRoomDAO.findOneById(te.getTherapyRoom()));
                        }
                        if (te.getTherapies() != null) {
                            te.setTherapies(therapyDAO.findAllByTherapyPlan(te.getTherapyPlan()));

                            List<Therapy> lt = te.getTherapies();
                            for (Therapy t : lt) {
                                t.setPatient(patientDAO.findOneById(t.getPatient()));
                            }
                        }

                    }

                    return lte;
                }
            }
            throw new ServiceException("no Therapies found for Patient");
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public TherapyRoom create(TherapyRoom original) throws ServiceException, ValidationException {
        TherapyRoom therapyRoom = null;

        try {
            log.debug("Create: " + original);
            therapyRoom = therapyRoomDAO.create(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException" + e);
            throw new ServiceException(e.toString());
        }

        return therapyRoom;
    }

    @Override
    public TherapyRoom update(TherapyRoom original) throws ServiceException, ValidationException {
        TherapyRoom therapyRoom = null;

        try {
            log.debug("Update: " + original);
            therapyRoom = therapyRoomDAO.update(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapyRoom;
    }

    @Override
    public void delete(TherapyRoom original) throws ServiceException {
        try {
            log.debug("Delete: " + original);
            therapyRoomDAO.delete(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyRoom> findAllTherapyRooms() throws ServiceException {
        try {
            log.debug("findAllTherapyRooms");
            return therapyRoomDAO.findAll();
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyRoom> findAllTherapyRoomsByTherapyPlan(TherapyPlan therapyPlan) throws ServiceException {
        try {
            log.debug("findAllTherapyRoomsByTherapyPlan");
            return therapyRoomDAO.findAllByTherapyPlan(therapyPlan);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Therapy create(Therapy original) throws ServiceException, ValidationException {
        Therapy therapy = null;

        try {
            log.debug("Create: " + original);
            therapy = therapyDAO.create(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapy;
    }

    @Override
    public Therapy update(Therapy original) throws ServiceException, ValidationException {
        Therapy therapy = null;

        try {
            log.debug("Update: " + original);
            therapy = therapyDAO.update(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapy;
    }

    @Override
    public void delete(Therapy original) throws ServiceException {
        try {
            log.debug("Delete: " + original);
            therapyDAO.delete(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Therapy> findAllTherapies() throws ServiceException {
        try {
            log.debug("findAllTherapies");
            List<Therapy> lt = therapyDAO.findAll();
            for (Therapy t : lt) {
                t.setPatient(patientDAO.findOneById(t.getPatient()));
            }
            return lt;
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Therapy> findAllTherapiesByTherapyPlan(TherapyPlan original) throws ServiceException {
        List<Therapy> therapies;

        try {
            log.debug("Create: " + original);
            therapies = therapyDAO.findAllByTherapyPlan(original);

            for (Therapy t : therapies) {
                if (t.getPatient() != null) {
                    t.setPatient(patientDAO.findOneById(t.getPatient()));
                }
            }
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapies;
    }

    @Override
    public List<Therapy> findAllTherapiesByPatient(Patient entity) throws ServiceException {
        List<Therapy> therapies;

        try {
            log.debug("findAllTherapiesByPatient: " + entity);
            therapies = therapyDAO.findAllByPatient(entity);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapies;
    }

    @Override
    public TherapyType create(TherapyType original) throws ServiceException, ValidationException {
        TherapyType therapyType = null;

        try {
            log.debug("Create: " + original);
            therapyType = therapyTypeDAO.create(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapyType;
    }

    @Override
    public TherapyType update(TherapyType original) throws ServiceException, ValidationException {
        TherapyType therapyType = null;

        try {
            log.debug("Update: " + original);
            therapyType = therapyTypeDAO.update(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapyType;
    }

    @Override
    public void delete(TherapyType original) throws ServiceException {
        try {
            log.debug("Delete: " + original);
            therapyTypeDAO.delete(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyType> findAllTherapyTypes() throws ServiceException {
        try {
            log.debug("findAllTherapyTypes");
            return therapyTypeDAO.findAll();
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    public TherapyType findTherapyTypeById(TherapyType therapyType) throws ServiceException {
        try {
            log.debug("findTherapyTypeById");
            return therapyTypeDAO.findOneById(therapyType);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyType> findAllTherapyTypesByTherapist(Therapist therapist) throws ServiceException {
        try {
            log.debug("findAllTherapyTypesByTherapist");
            return therapyTypeDAO.findAllByTherapist(therapist);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public TherapyPlan create(TherapyPlan original) throws ServiceException, ValidationException {
        TherapyPlan therapyPlan;

        try {
            log.debug("Create: " + original);
            therapyPlan = therapyPlanDAO.create(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapyPlan;
    }

    @Override
    public TherapyPlan update(TherapyPlan original) throws ServiceException, ValidationException {
        TherapyPlan therapyPlan;

        try {
            log.debug("Update: " + original);
            therapyPlan = therapyPlanDAO.update(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return therapyPlan;
    }

    @Override
    public void delete(TherapyPlan original) throws ServiceException {
        try {
            log.debug("Create: " + original);
            therapyPlanDAO.delete(original);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyPlan> findAllTherapyPlans() throws ServiceException {
        try {
            log.debug("findAllTherapyPlans");
            return therapyPlanDAO.findAll();
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public boolean createParticipation(TherapyEvent therapyEvent, Therapy therapy) throws ServiceException, ValidationException {
        try {
            log.debug("assign Therapy: " + therapy.getId() + " to TherapyEvent: " + therapyEvent.getId());
            int groupSize = therapyEventDAO.getParticipants(therapyEvent, false).size();
            if(groupSize >= therapyEvent.getTherapyType().getMaxGroupSize()){
                throw new ServiceException("max group size reached");
            }
            return therapyEventDAO.createParticipation(therapyEvent, therapy);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public boolean deleteParticipation(TherapyEvent therapyEvent, Therapy therapy) throws ServiceException, ValidationException {
        try {
            log.debug("delete assignment of Therapy: " + therapy.getId() + " to TherapyEvent: " + therapyEvent.getId());
            return therapyEventDAO.deleteParticipation(therapyEvent, therapy);
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Therapy> getParticipants(TherapyEvent therapyEvent) throws ServiceException, ValidationException {
        try {
            log.debug("get participants of TherapyEvent: " + therapyEvent.getId());

            List<Therapy> lt = therapyEventDAO.getParticipants(therapyEvent, true);
            for (Therapy t : lt) {
                t.setPatient(patientDAO.findOneById(t.getPatient()));
            }

            return lt;
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<TherapyRoom> findAllTherapyRoomsByTherapyEventAndTimeBlock(TherapyEvent therapyEvent, TimeBlock timeBlock) throws ServiceException, ValidationException {
        List<TherapyRoom> therapyRooms = null;
        log.debug("get all available rooms for event: " + therapyEvent.getId() + " and time block: " + timeBlock.getId());
        try {
            therapyRooms = this.findAllTherapyRoomsByTherapyEvent(therapyEvent);
            log.debug("remove all therapy rooms that are already assigned in other events for this time Block");
            Iterator<TherapyRoom> iterator = therapyRooms.iterator();
            while (iterator.hasNext()) {
                TherapyRoom therapyRoom = iterator.next();
                List<TherapyEvent> otherEvents = therapyEventDAO.findAllByTherapyRoomAndTherapyPlan(therapyRoom, therapyEvent.getTherapyPlan(), false);
                for (TherapyEvent te : otherEvents) {
                    if (te.getTherapyTimeBlock() != null && te.getTherapyTimeBlock().getId().equals(timeBlock.getId())
                            && !te.getId().equals(therapyEvent.getId())) {
                        iterator.remove();
                    }
                }
            }

        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        log.debug("count of rooms for event: " + therapyEvent.getId() + " for TimeBlock: " + timeBlock.getId() + " is: " +therapyRooms.size());
        return therapyRooms;
    }

    @Override
    public List<TherapyRoom> findAllTherapyRoomsByTherapyEvent(TherapyEvent therapyEvent) throws ServiceException, ValidationException {
        List<TherapyRoom> therapyRooms = null;
        log.debug("get all available rooms for event: " + therapyEvent.getId());
        try {
            therapyRooms = therapyRoomDAO.findAll();
            int participantCount = therapyEventDAO.getParticipants(therapyEvent, false).size();
            if (participantCount <= 0) {
                throw new ServiceException("therapy room for event without participants is not possible");
            }
            log.debug("remove all therapy rooms where room size doesn't fit");
            Iterator<TherapyRoom> iterator = therapyRooms.iterator();
            while (iterator.hasNext()) {
                TherapyRoom therapyRoom = iterator.next();
                if ((participantCount == 1 && therapyRoom.getCapacity() != 1)
                        || (therapyRoom.getCapacity() < participantCount)) {
                    iterator.remove();
                }
            }
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        log.debug("count of rooms for event: " + therapyEvent.getId() + " is: " +therapyRooms.size());
        return therapyRooms;
    }

    @Override
    public List<TimeBlock> findAllAvailableTimeBlocksByTherapyEvent(TherapyEvent therapyEvent) throws ServiceException, ValidationException {

        log.debug("get all available TimeBlocks for Event: " + therapyEvent.getId());
        List<TimeBlock> available = null;
        try {
            log.debug("get all possible TimeBlocks");
            available = timeBlockDAO.findAll();
            int count = available.size();
            log.debug("total available TimeBlocks: " + count);

            log.debug("remove all TimeBlocks where the Therapist is unavailable");
            List<TimeBlock> therapistFreeTimeBlocks = timeBlockDAO.findAllNegativeByTherapist(therapyEvent.getTherapist());
            for (TimeBlock forbiddenTimeBlock : therapistFreeTimeBlocks) {
                Iterator<TimeBlock> iterator = available.iterator();
                while (iterator.hasNext()) {
                    TimeBlock timeBlock = iterator.next();
                    if (timeBlock.getId().equals(forbiddenTimeBlock.getId())) {
                        iterator.remove();
                    }
                }
            }
            log.debug("removed TimeBlocks where Therapist is unavailable: " + (count - available.size()));
            count = available.size();



            log.debug("remove all TimeBlocks where the Therapist is already working");
            List<TherapyEvent> therapyEventsForTherapist = therapyEventDAO.findAllByTherapistAndTherapyPlan(therapyEvent.getTherapist(), therapyEvent.getTherapyPlan(), false);
            for (TherapyEvent te : therapyEventsForTherapist) {
                Iterator<TimeBlock> iterator = available.iterator();
                while (iterator.hasNext()) {
                    TimeBlock timeBlock = iterator.next();
                    if (te.getTherapyTimeBlock() != null && timeBlock.getId().equals(te.getTherapyTimeBlock().getId())) {
                        iterator.remove();
                    }
                }
            }
            log.debug("removed TimeBlocks where Therapist is already working: " + (count - available.size()));
            count = available.size();

            log.debug("remove all TimeBlocks where participating Patients are participating in another event");
            List<Therapy> therapies = getParticipants(therapyEvent);
            for (Therapy therapy : therapies) {
                List<TherapyEvent> therapyEventsForTherapy = therapyEventDAO.findAllByTherapyAndTherapyPlan(therapy, therapyEvent.getTherapyPlan(), false);
                for (TherapyEvent te : therapyEventsForTherapy) {
                    Iterator<TimeBlock> iterator = available.iterator();
                    while (iterator.hasNext()) {
                        TimeBlock timeBlock = iterator.next();
                        if (te.getTherapyTimeBlock() != null && timeBlock.getId().equals(te.getTherapyTimeBlock().getId())) {
                            iterator.remove();
                        }
                    }
                }
            }
            log.debug("removed TimeBlocks where participating Patients are already assigned: " + (count - available.size()));
            count = available.size();

            log.debug("remove all TimeBlocks where no more rooms are available, or selected room is already assigned at that time");
            Iterator<TimeBlock> iterator = available.iterator();
            while (iterator.hasNext()) {
                TimeBlock timeBlock = iterator.next();
                List<TherapyRoom> availableRooms = findAllTherapyRoomsByTherapyEventAndTimeBlock(therapyEvent, timeBlock);
                if (availableRooms.size() <= 0) {
                    iterator.remove();
                } else if(therapyEvent.getTherapyRoom() !=null) {//remove also if preselected room is not available in the other TimeBlock
                    boolean found = false;
                    for (TherapyRoom therapyRoom: availableRooms){
                        if (therapyRoom.getId().equals(therapyEvent.getTherapyRoom().getId())){
                            found = true;
                        }
                    }
                    if (!found){
                        iterator.remove();
                    }
                }
            }
            log.debug("removed TimeBlocks where no Rooms(or already assigned Room is not available): " + (count - available.size()));
            count = available.size();

        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

        return available;
    }

    public TherapyDAO getTherapyDAO() {
        return therapyDAO;
    }

    public void setTherapyDAO(TherapyDAO therapyDAO) {
        this.therapyDAO = therapyDAO;
    }

    public TherapyTypeDAO getTherapyTypeDAO() {
        return therapyTypeDAO;
    }

    public void setTherapyTypeDAO(TherapyTypeDAO therapyTypeDAO) {
        this.therapyTypeDAO = therapyTypeDAO;
    }

    public TherapyEventDAO getTherapyEventDAO() {
        return therapyEventDAO;
    }

    public void setTherapyEventDAO(TherapyEventDAO therapyEventDAO) {
        this.therapyEventDAO = therapyEventDAO;
    }

    public TherapyRoomDAO getTherapyRoomDAO() {
        return therapyRoomDAO;
    }

    public void setTherapyRoomDAO(TherapyRoomDAO therapyRoomDAO) {
        this.therapyRoomDAO = therapyRoomDAO;
    }

    public TherapyPlanDAO getTherapyPlanDAO() {
        return therapyPlanDAO;
    }

    public void setTherapyPlanDAO(TherapyPlanDAO therapyPlanDAO) {
        this.therapyPlanDAO = therapyPlanDAO;
    }

    public void setTherapistDAO(TherapistDAO therapistDAO) {
        this.therapistDAO = therapistDAO;
    }

    public void setPatientDAO(PatientDAO patientDAO) {
        this.patientDAO = patientDAO;
    }

    public void setTimeBlockDAO(TimeBlockDAO timeBlockDAO) {
        this.timeBlockDAO = timeBlockDAO;
    }

    public void setTherapistBasedTimeBlockConstraintDAO(TherapistBasedTimeBlockConstraintDAO therapistBasedTimeBlockConstraintDAO) {
        this.therapistBasedTimeBlockConstraintDAO = therapistBasedTimeBlockConstraintDAO;

    }
}
