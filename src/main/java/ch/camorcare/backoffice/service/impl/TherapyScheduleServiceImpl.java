package ch.camorcare.backoffice.service.impl;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.persistence.*;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.TherapyScheduleService;
import ch.camorcare.backoffice.service.exception.PlanningException;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.*;

import java.security.InvalidParameterException;
import java.util.*;

/**
 * Implementation of TherapyScheduleService
 */
public class TherapyScheduleServiceImpl implements TherapyScheduleService {

    private static final int PLACEHOLDER_ID = -1;

    private final int NBCLAUSES;
    private int MAXVAR;

    private TherapyRoomDAO therapyRoomDAO;
    private TherapistBasedTimeBlockConstraintDAO therapistBasedTimeBlockConstraintDAO;
    private TimeBlockDAO timeBlockDAO;
    private TherapyEventDAO therapyEventDAO;
    private TherapistDAO therapistDAO;
    private TherapistBasedTherapyTypeOccurrenceConstraintDAO therapistBasedTherapyTypeOccurrenceConstraintDAO;
    private TherapyDAO therapyDAO;
    private PatientBasedTherapyTypeOccurrenceConstraintDAO patientBasedTherapyTypeOccurrenceConstraintDAO;
    private PatientBasedTherapistConstraintDAO patientBasedTherapistConstraintDAO;
    private PatientBasedTherapistGenderConstraintDAO patientBasedTherapistGenderConstraintDAO;
    private TherapyTypeDAO therapyTypeDAO;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapyScheduleServiceImpl.class.getName());

    //######################################SCHEDULING################################
    /**
     * TimeBlocks for Therapies for each Day
     */
    private int[] timeBlocks = new int[7];

    /**
     * TimeBlocks ordered from Monday to Sunday (not yet ordered by time)
     */
    private List<TimeBlock> orderedTimeBlocks;

    /**
     * Ordered TherapyRooms
     */
    private List<TherapyRoom> orderedRooms;

    /**
     * Ordered TherapyEvents
     */
    private List<TherapyEvent> orderedEvents;

    /**
     * Ordered Therapists free TimeBlocks (who are assigned to the event)
     * <p/>
     * Key: Index of event; Value: List of forbidden TimeBlocks (indices)
     */
    private Map<Integer, List<Integer>> orderedFreeTime;

    /**
     * TherapyRooms that are not suitable for TherapyEvents
     * <p/>
     * Key: Index of event; Value: false/true: index of TherapyRooms that are not suitable
     */
    private Map<Integer, List<Integer>> forbiddenRoomsForEvents;

    /**
     * Event list (indices) for each Therapist
     * <p/>
     * Key: Therapist index; Value: List of TherapyEvents (indices)
     */
    private Map<Integer, List<Integer>> eventsForTherapists;

    /**
     * Event list(indexes) for each participant
     * <p/>
     * Key: Therapy index; Value: list of TherapyEvents (indices)
     */
    private Map<Integer, List<Integer>> eventsForParticipants;

    //#################################EVENT CREATION###########################################

    //####################################CONSTRUCTOR AND SETTER#######################################
    public TherapyScheduleServiceImpl() {
        NBCLAUSES = 5000000;
    }

    public void setTherapyRoomDAO(TherapyRoomDAO therapyRoomDAO) {
        this.therapyRoomDAO = therapyRoomDAO;
    }

    public void setTherapistBasedTimeBlockConstraintDAO(TherapistBasedTimeBlockConstraintDAO therapistBasedTimeBlockConstraintDAO) {
        this.therapistBasedTimeBlockConstraintDAO = therapistBasedTimeBlockConstraintDAO;
    }

    public void setTimeBlockDAO(TimeBlockDAO timeBlockDAO) {
        this.timeBlockDAO = timeBlockDAO;
    }

    public void setTherapyEventDAO(TherapyEventDAO therapyEventDAO) {
        this.therapyEventDAO = therapyEventDAO;
    }

    public void setTherapistDAO(TherapistDAO therapistDAO) {
        this.therapistDAO = therapistDAO;
    }

    public void setTherapistBasedTherapyTypeOccurrenceConstraintDAO(TherapistBasedTherapyTypeOccurrenceConstraintDAO therapistBasedTherapyTypeOccurrenceConstraintDAO) {
        this.therapistBasedTherapyTypeOccurrenceConstraintDAO = therapistBasedTherapyTypeOccurrenceConstraintDAO;
    }

    public void setTherapyDAO(TherapyDAO therapyDAO) {
        this.therapyDAO = therapyDAO;
    }

    public void setPatientBasedTherapyTypeOccurrenceConstraintDAO(PatientBasedTherapyTypeOccurrenceConstraintDAO patientBasedTherapyTypeOccurrenceConstraintDAO) {
        this.patientBasedTherapyTypeOccurrenceConstraintDAO = patientBasedTherapyTypeOccurrenceConstraintDAO;
    }

    public void setPatientBasedTherapistConstraintDAO(PatientBasedTherapistConstraintDAO patientBasedTherapistConstraintDAO) {
        this.patientBasedTherapistConstraintDAO = patientBasedTherapistConstraintDAO;
    }

    public void setPatientBasedTherapistGenderConstraintDAO(PatientBasedTherapistGenderConstraintDAO patientBasedTherapistGenderConstraintDAO) {
        this.patientBasedTherapistGenderConstraintDAO = patientBasedTherapistGenderConstraintDAO;
    }

    public void setTherapyTypeDAO(TherapyTypeDAO therapyTypeDAO) {
        this.therapyTypeDAO = therapyTypeDAO;
    }

    //#####################################SET UP FOR SCHEDULING AND EVENT CREATION#########################

    /**
     * Order and count TimeBlocks
     *
     * @param availableTimeBlocks
     */
    private void setUpTimeBlocks(List<TimeBlock> availableTimeBlocks) {
        //order TimeBlocks by day
        List<List<TimeBlock>> orderedTimeBlocksByDay = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            List<TimeBlock> day = new ArrayList<>();
            orderedTimeBlocksByDay.add(day);
        }
        for (TimeBlock t : availableTimeBlocks) {
            switch (t.getDay()) {
                case MONDAY:
                    orderedTimeBlocksByDay.get(0).add(t);
                    break;
                case TUESDAY:
                    orderedTimeBlocksByDay.get(1).add(t);
                    break;
                case WEDNESDAY:
                    orderedTimeBlocksByDay.get(2).add(t);
                    break;
                case THURSDAY:
                    orderedTimeBlocksByDay.get(3).add(t);
                    break;
                case FRIDAY:
                    orderedTimeBlocksByDay.get(4).add(t);
                    break;
                case SATURDAY:
                    orderedTimeBlocksByDay.get(5).add(t);
                    break;
                case SUNDAY:
                    orderedTimeBlocksByDay.get(6).add(t);
                    break;
            }
        }
        orderedTimeBlocks = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            List<TimeBlock> toAdd = orderedTimeBlocksByDay.get(i);
            orderedTimeBlocks.addAll(toAdd);
        }

        for (int i = 0; i < timeBlocks.length; i++) {
            timeBlocks[i] = orderedTimeBlocksByDay.get(i).size();
        }

    }

    /**
     * Order and count TherapyRooms
     *
     * @param rooms
     */
    private void setUpRooms(List<TherapyRoom> rooms) {
        //no special order needed so far
        orderedRooms = new ArrayList<>();
        for (TherapyRoom tr : rooms) {
            orderedRooms.add(tr);
        }

    }

    /**
     * Order and count TherapyEvents
     *
     * @param events
     */
    private void setUpEvents(List<TherapyEvent> events) {
        //no special order needed so far
        orderedEvents = new ArrayList<>();
        for (TherapyEvent te : events) {
            orderedEvents.add(te);
        }
    }

    /**
     * Set up Therapists' free time and sort by TherapyEvent index
     */
    private void setUpFreeTime() throws PersistenceException {
        orderedFreeTime = new HashMap<>();
        for (int eventIndex = 0; eventIndex < orderedEvents.size(); eventIndex++) {
            //look up the forbidden timeslots for the Therapist managing the event
            TherapyEvent event = orderedEvents.get(eventIndex);
            List<TimeBlock> freeTimeBlocks = timeBlockDAO.findAllNegativeByTherapist(event.getTherapist());


            List<Integer> forbiddenTimeBlocks = new ArrayList<>();
            //for each forbidden TimeBlock look up the associated index in the ordered Time Block List and add the index to the list

            for (TimeBlock timeBlock : freeTimeBlocks) {
                for (int timeBlockIndex = 0; timeBlockIndex < orderedTimeBlocks.size(); timeBlockIndex++) {
                    if (timeBlock.getId() == orderedTimeBlocks.get(timeBlockIndex).getId()) {
                        forbiddenTimeBlocks.add(timeBlockIndex);
                        break;//found, no other timeblock with same ID exists
                    }
                }
            }
            orderedFreeTime.put(eventIndex, forbiddenTimeBlocks);
        }
    }

    /**
     * Set up TherapyEvents for all participants and determine if the TherapyEvent is a individual or group event
     */
    private void setUpEventsForParticipants() throws PersistenceException, ValidationException {
        eventsForParticipants = new HashMap<>();
        forbiddenRoomsForEvents = new HashMap<>();
        for (int eventIndex = 0; eventIndex < orderedEvents.size(); eventIndex++) {
            TherapyEvent event = orderedEvents.get(eventIndex);
            List<Therapy> therapiesForEvent = therapyEventDAO.getParticipants(event, true);

            //add event to all participants
            for (Therapy therapy : therapiesForEvent) {
                if (!eventsForParticipants.containsKey(therapy.getId())) {
                    eventsForParticipants.put(therapy.getId(), new ArrayList<>());
                }
                eventsForParticipants.get(therapy.getId()).add(eventIndex);
            }
            forbiddenRoomsForEvents.put(eventIndex, new ArrayList<>());
            //look up rooms that are not suitable for the event
            for (int roomIndex = 0; roomIndex < orderedRooms.size(); roomIndex++) {
                //not suitable if event is a individual event and room is group room
                //or number of participants is too high for room capacity
                if (1 == therapiesForEvent.size() && 1 != orderedRooms.get(roomIndex).getCapacity()
                        || therapiesForEvent.size() > orderedRooms.get(roomIndex).getCapacity()) {
                    forbiddenRoomsForEvents.get(eventIndex).add(roomIndex);
                }
            }
        }
    }

    /**
     * Set up TherapyEvents for all Therapists
     */
    private void setUpEventsForTherapists() throws PersistenceException {
        //no special order needed so far
        eventsForTherapists = new HashMap<>();
        for (int eventIndex = 0; eventIndex < orderedEvents.size(); eventIndex++) {
            TherapyEvent event = orderedEvents.get(eventIndex);
            Therapist therapist = event.getTherapist();
            if (!eventsForTherapists.containsKey(therapist.getId())) {
                eventsForTherapists.put(therapist.getId(), new ArrayList<>());
            }
            eventsForTherapists.get(therapist.getId()).add(eventIndex);
        }
    }

    //####################################SCHEDULING AND CREATING#######################
    @Override
    public List<TherapyEvent> schedule(TherapyPlan plan) throws ServiceException, PlanningException, ValidationException {

        try {
            setUpEvents(therapyEventDAO.findAllByTherapyPlan(plan,true));
            setUpTimeBlocks(timeBlockDAO.findAll());
            setUpRooms(therapyRoomDAO.findAll());
            setUpFreeTime();
            setUpEventsForParticipants();
            setUpEventsForTherapists();
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage());
        }

        MAXVAR = orderedTimeBlocks.size() * orderedRooms.size() * orderedEvents.size();
        ISolver solver = SolverFactory.newDefault();
        // prepare the solver to accept MAXVAR variables. MANDATORY for MAXSAT solving
        solver.newVar(MAXVAR);
        solver.setExpectedNumberOfClauses(NBCLAUSES);

        try {
            solver.addAllClauses(exactlyOnePerEvent());
            solver.addAllClauses(maximumOneEventPerRoomPerTimeBlock());
            solver.addAllClauses(maximumOneEventPerRoomPerTimeBlockPerTherapist());
            solver.addAllClauses(maximumOneEventPerRoomPerTimeBlockPerPatient());
            solver.addAllClauses(onlySuitingRooms());
            solver.addAllClauses(respectTherapistFreeTime());
        } catch (ContradictionException e) {
            log.error("Contradiction" + e);
            throw new PlanningException("Contradiction encountered: " + e.getMessage());
        }

        List<TherapyEvent> solution = new ArrayList<>();
        IProblem problem = solver;
        solver.setTimeout(120); // 2 minutes

        try {
            if (problem.isSatisfiable()) {
                //log.debug("is Satisfiable" + problem.model());
                solution = getSolution(problem.model());
            }
            else {
                log.debug("not satisfiable");
                throw new PlanningException("not satisfiable");
            }
        } catch (TimeoutException e) {
            log.error("TimeoutException " + e.getMessage());
            throw new PlanningException("Timeout for planning");
        }

        return solution;
    }

    @Override
    public Map<TherapyEvent, List<Therapy>> createEventsForWeek(TherapyPlan plan) throws ServiceException, PlanningException, ValidationException {
        try {
            log.debug("create events for week from: " + plan.getDateFrom() + " to: " + plan.getDateUntil());
            EventCreationHelper helper = new EventCreationHelper(plan);
            Map<TherapyEvent, List<Therapy>> individuals = createIndividualEvents(helper, plan);
            Map<TherapyEvent, List<Therapy>> groupEvents = createGroupEvents(helper, plan);

            individuals = assignRemainingTherapistsWithGenderForEvents(helper, individuals);
            groupEvents = assignRemainingTherapistsWithGenderForEvents(helper, groupEvents);
            individuals = assignRemainingTherapistsForEvents(helper, individuals);
            groupEvents = assignRemainingTherapistsForEvents(helper, groupEvents);

            Map<TherapyEvent, List<Therapy>> allEvents = new HashMap<>();
            allEvents.putAll(individuals);
            allEvents.putAll(groupEvents);
            return allEvents;
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void createAllEventsWithParticipants(Map<TherapyEvent, List<Therapy>> events) throws ValidationException, ServiceException {
        try {
            for (TherapyEvent therapyEvent : events.keySet()) {
                TherapyEvent created = therapyEventDAO.create(therapyEvent);
                therapyEvent.setId(created.getId());
                therapyEventDAO.update(therapyEvent);
                for (Therapy relatedTherapy : events.get(therapyEvent)) {
                    therapyEventDAO.createParticipation(therapyEvent, relatedTherapy);
                }
            }
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteAllEventsWithParticipants(TherapyPlan plan) throws ValidationException, ServiceException {
        try {
            for (TherapyEvent therapyEvent : therapyEventDAO.findAllByTherapyPlan(plan, false)) {
                for (Therapy therapy : therapyEventDAO.getParticipants(therapyEvent, true)) {
                    therapyEventDAO.deleteParticipation(therapyEvent, therapy);
                }

                therapyEventDAO.delete(therapyEvent);

            }
        } catch (PersistenceException e) {
            log.error("PersistenceException " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    //###################################AUX EVENT CREATION########################################

    /**
     * Creates all individual events
     * Assigns TherapyType and Participant
     * Assigns Therapist only if preferred Therapist is set (unassigned Therapist has to be filled out after all
     * preferences have been assigned)
     *
     * @return
     */
    private Map<TherapyEvent, List<Therapy>> createIndividualEvents(EventCreationHelper helper, TherapyPlan plan) throws PersistenceException, PlanningException {
        log.debug("create individual events");
        Map<TherapyEvent, List<Therapy>> events = new HashMap<>();
        for (Therapy therapy : helper.getAllActiveTherapies()) {
            log.debug("look up TherapyTypeOccurence constraints for Therapy: " + therapy.getId());
            for (PatientBasedTherapyTypeOccurrenceConstraint therapyOccurrence : helper.getIndividualTherapyEvents(therapy)) {
                for (int eventCount = 1; eventCount <= therapyOccurrence.getMaxOccurencePerWeek(); eventCount++) {
                    TherapyType therapyType = therapyOccurrence.getTherapyType();
                    TherapyEvent therapyEvent = new TherapyEvent();
                    therapyEvent.setTherapyPlan(plan);
                    therapyEvent.setTherapyMode(TherapyMode.SINGLE);
                    log.debug("adding a new individual event to the event list for TherapyType: " + therapyType.getId());
                    therapyEvent.setTherapyType(therapyType);
                    log.debug("look up Therapist preferences for TherapyType: " + therapyType.getId());
                    Therapist assignedTherapist = null;
                    Therapist preferredTherapist = helper.getPreferredTherapist(therapy, therapyType);
                    Gender preferredGender = helper.getPreferredTherapistGender(therapy, therapyType);
                    if (preferredGender != null && preferredTherapist != null) {
                        log.error("preferences for therapist and Therapist Gender for the same therapyEvent!!");
                        throw new PlanningException("preferences for therapist and Therapist Gender for the same therapyEvent!!" +
                                " for Therapy: " + therapy.getId() + " and TherapyType: " + therapyType.getId());
                    }
                    else if (preferredTherapist != null) {
                        log.debug("assigning preferred Therapist");
                        assignedTherapist = preferredTherapist;
                        helper.decrementAvailabilityForTherapist(assignedTherapist);
                    }
                    else if (preferredGender != null) {
                        log.debug("assigning preferred Therapist Gender");
                        assignedTherapist = new Therapist();
                        assignedTherapist.setId(PLACEHOLDER_ID);
                        assignedTherapist.setGender(preferredGender);
                    }
                    therapyEvent.setTherapist(assignedTherapist);
                    log.debug("adding therapy: " + therapy.getId() + " to event for TherapyType: " + therapyType.getId());
                    List<Therapy> therapyList = new ArrayList<>();
                    therapyList.add(therapy);
                    events.put(therapyEvent, therapyList);
                }
            }
        }
        return events;
    }

    /**
     * Creates all group events
     * Assigns TherapyType and Participant
     * Assigns Therapist only if preferred Therapist is set(unassigned Therapist has to be filled out after all
     * preferences have been assigned)
     *
     * @return
     */
    private Map<TherapyEvent, List<Therapy>> createGroupEvents(EventCreationHelper helper, TherapyPlan plan) throws PersistenceException, PlanningException {
        log.debug("create group events");
        Map<TherapyEvent, List<Therapy>> events = new HashMap<>();
        for (Therapy therapy : helper.getAllActiveTherapies()) {
            log.debug("look up TherapyTypeOccurence constraints for Therapy: " + therapy.getId());
            for (PatientBasedTherapyTypeOccurrenceConstraint therapyOccurrence : helper.getGroupTherapyEvents(therapy)) {
                for (int eventCount = 1; eventCount <= therapyOccurrence.getMaxOccurencePerWeek(); eventCount++) {
                    TherapyType therapyType = therapyOccurrence.getTherapyType();
                    TherapyEvent therapyEvent = null;
                    log.debug("look up Therapist preferences for TherapyType: " + therapyType.getId());
                    Therapist preferredTherapist = helper.getPreferredTherapist(therapy, therapyType);
                    Gender preferredGender = helper.getPreferredTherapistGender(therapy, therapyType);
                    List<TherapyEvent> list = null;
                    if (preferredGender != null && preferredTherapist != null) {
                        log.error("preferences for therapist and Therapist Gender for the same therapyEvent!!");
                        throw new PlanningException("preferences for therapist and Therapist Gender for the same therapyEvent!!" +
                                " for Therapy: " + therapy.getId() + " and TherapyType: " + therapyType.getId());
                    }
                    else if (preferredTherapist != null) {
                        list = containsSuitingTherapyEvent(events, therapyType, therapy, preferredTherapist);
                    }
                    else if (preferredGender != null) {
                        list = containsSuitingTherapyEvent(events, therapyType, therapy, preferredGender);
                    }
                    else {
                        list = containsSuitingTherapyEvent(events, therapyType, therapy);
                    }

                    if (list.isEmpty()) {
                        log.debug("adding a new group event to the event list for TherapyType: " + therapyType.getId());
                        therapyEvent = new TherapyEvent();
                        therapyEvent.setTherapyPlan(plan);
                        therapyEvent.setTherapyType(therapyType);
                        therapyEvent.setTherapyMode(TherapyMode.GROUP);
                        if (preferredTherapist != null) {
                            therapyEvent.setTherapist(preferredTherapist);
                            helper.decrementAvailabilityForTherapist(preferredTherapist);
                        }
                        else if (preferredGender != null) {
                            Therapist genderedTherapist = new Therapist();
                            genderedTherapist.setId(PLACEHOLDER_ID);
                            genderedTherapist.setGender(preferredGender);
                            therapyEvent.setTherapist(genderedTherapist);
                        }
                        events.put(therapyEvent, new ArrayList<>());
                    }
                    else {
                        log.debug("suiting therapy event found");
                        therapyEvent = list.get(0);
                        if (preferredTherapist != null &&
                                (therapyEvent.getTherapist() == null || therapyEvent.getTherapist().getId().equals(PLACEHOLDER_ID))) {
                            therapyEvent.setTherapist(preferredTherapist);
                            helper.decrementAvailabilityForTherapist(preferredTherapist);

                        }
                        else if (preferredGender != null && therapyEvent.getTherapist() == null) {
                            Therapist genderedTherapist = new Therapist();
                            genderedTherapist.setId(PLACEHOLDER_ID);
                            genderedTherapist.setGender(preferredGender);
                            therapyEvent.setTherapist(genderedTherapist);
                        }
                    }

                    log.debug("adding therapy: " + therapy.getId() + " to event for TherapyType: " + therapyType.getId());
                    events.get(therapyEvent).add(therapy);
                }
            }
        }
        return events;
    }

    /**
     * Assigns missing Therapists (only gender preference set) for already created TherapyEvents
     *
     * @return
     */
    private <A> Map<TherapyEvent, A> assignRemainingTherapistsWithGenderForEvents(EventCreationHelper helper, Map<TherapyEvent, A> events) throws PersistenceException, PlanningException {
        log.debug("assign Therapists to remaining events that have gender preference");
        for (TherapyEvent therapyEvent : events.keySet()) {
            if (therapyEvent.getTherapist() != null && therapyEvent.getTherapist().getId().equals(PLACEHOLDER_ID)) {
                log.debug("assigning therapist with gender: " + therapyEvent.getTherapist().getGender() + " for event for TherapyType: " + therapyEvent.getTherapyType().getId());
                Therapist assigned = helper.getTherapistForTherapyType(therapyEvent.getTherapyType(), therapyEvent.getTherapist().getGender());
                therapyEvent.setTherapist(assigned);
                helper.decrementAvailabilityForTherapist(assigned);
            }
        }
        return events;
    }

    /**
     * Assigns missing Therapists for already created TherapyEvents
     *
     * @return
     */
    private <A> Map<TherapyEvent, A> assignRemainingTherapistsForEvents(EventCreationHelper helper, Map<TherapyEvent, A> events) throws PersistenceException, PlanningException {
        log.debug("assign Therapists to remaining events");
        for (TherapyEvent therapyEvent : events.keySet()) {
            if (therapyEvent.getTherapist() == null) {
                log.debug("assigning therapist for event for TherapyType: " + therapyEvent.getTherapyType().getId());
                Therapist assigned = helper.getTherapistForTherapyType(therapyEvent.getTherapyType());
                therapyEvent.setTherapist(assigned);
                helper.decrementAvailabilityForTherapist(assigned);
            }
        }
        return events;
    }

    /**
     * Returns a list of TherapyEvents that suits the requirements for adding another Therapy
     *
     * @param existingEvents TherapyEvents that have been already created
     * @param therapyType    TherapyType that has to be held
     * @param therapy        Participant for the therapyEvent
     * @return
     */
    private List<TherapyEvent> containsSuitingTherapyEvent(Map<TherapyEvent, List<Therapy>> existingEvents, TherapyType therapyType, Therapy therapy) {
        log.debug("look up if TherapyEvents exists for TherapyType: " + therapyType.getId());
        List<TherapyEvent> list = new ArrayList<>();
        for (TherapyEvent event : existingEvents.keySet()) {
            log.trace("trying TherapyEvent: " + event);
            if (event.getTherapyType().getId().equals(therapyType.getId())) {
                if (existingEvents.get(event).size() < therapyType.getMaxGroupSize()) {
                    if (!containsTherapy(existingEvents.get(event), therapy)) {
                        list.add(event);
                    }
                    else {
                        log.trace("Therapy already assigned");
                    }
                }
                else {
                    log.trace("Group is full");
                }
            }
            else {
                log.trace("TherapyType doesn't match");
            }
        }
        log.debug("found TherapyEvents: " + list.size());
        return list;
    }

    /**
     * Returns a list of TherapyEvents that suits the requirements for adding another Therapy
     *
     * @param existingEvents TherapyEvents that have been already created
     * @param therapyType    TherapyType that has to be held
     * @param therapy        Participant for the therapyEvent
     * @param gender         Preferred gender of the Therapist
     * @return
     */
    private List<TherapyEvent> containsSuitingTherapyEvent(Map<TherapyEvent, List<Therapy>> existingEvents, TherapyType therapyType, Therapy therapy, Gender gender) {
        log.debug("look up if TherapyEvents exists for TherapyType: " + therapyType.getId() + " and Therapist with gender: " + gender);
        List<TherapyEvent> list = new ArrayList<>();
        for (TherapyEvent event : existingEvents.keySet()) {
            log.trace("trying TherapyEvent: " + event);
            if (event.getTherapyType().getId().equals(therapyType.getId())) {
                if (existingEvents.get(event).size() < therapyType.getMaxGroupSize()) {
                    if (!containsTherapy(existingEvents.get(event), therapy)) {
                        if (event.getTherapist() == null || event.getTherapist().getGender().equals(gender)) {
                            list.add(event);
                        }
                        else {
                            log.trace("no Gender Match");
                        }
                    }
                    else {
                        log.trace("Therapy already assigned");
                    }
                }
                else {
                    log.trace("Group is full");
                }
            }
            else {
                log.trace("TherapyType doesn't match");
            }
        }
        log.debug("found TherapyEvents: " + list.size());
        return list;
    }

    /**
     * Returns a list of TherapyEvents that suits the requirements for adding another Therapy
     *
     * @param existingEvents TherapyEvents that have been already created
     * @param therapyType    TherapyType that has to be held
     * @param therapy        Participant for the therapyEvent
     * @param therapist      Preferred Therapist
     * @return
     */
    private List<TherapyEvent> containsSuitingTherapyEvent(Map<TherapyEvent, List<Therapy>> existingEvents, TherapyType therapyType, Therapy therapy, Therapist therapist) {
        log.debug("look up if TherapyEvents exists for TherapyType: " + therapyType.getId() + " and Therapist: " + therapist.getId());
        List<TherapyEvent> list = new ArrayList<>();
        for (TherapyEvent event : existingEvents.keySet()) {
            log.trace("trying TherapyEvent: " + event);
            if (event.getTherapyType().getId().equals(therapyType.getId())) {
                if (existingEvents.get(event).size() < therapyType.getMaxGroupSize()) {
                    if (!containsTherapy(existingEvents.get(event), therapy)) {
                        if (event.getTherapist() == null || event.getTherapist().getId().equals(therapist.getId())) {
                            list.add(event);
                        }
                        else {
                            log.trace("no Therapist Match");
                        }
                    }
                    else {
                        log.trace("Therapy already assigned");
                    }
                }
                else {
                    log.trace("Group is full");
                }
            }
            else {
                log.trace("TherapyType doesn't match");
            }
        }
        log.debug("found TherapyEvents: " + list.size());
        return list;
    }

    /**
     * @param existing Existing Therapies
     * @param therapy  Therapy to be looked up
     * @return
     */
    private boolean containsTherapy(List<Therapy> existing, Therapy therapy) {
        for (Therapy t : existing) {
            if (t.getId().equals(therapy.getId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns clauses for the "exactly one constraint"
     * At least one: v1 or v2 or v3,... for all timeBlocks and rooms for one event
     * At most one: (-vi or -vj) for all i, j, where j>i for one event
     *
     * @return
     */
    private IVec exactlyOnePerEvent() {
        IVec vec = new Vec();

        for (int baseEvent = 1; baseEvent <= orderedEvents.size(); baseEvent++) {//for all events
            //at least one
            IVecInt alo = new VecInt();
            for (int baseRoom = 1; baseRoom <= orderedRooms.size(); baseRoom++) {//for all rooms
                for (int baseDay = 1; baseDay <= timeBlocks.length; baseDay++) {//for all days
                    for (int baseTimeBlock = 1; baseTimeBlock <= timeBlocks[baseDay - 1]; baseTimeBlock++) {//for all timeBlocks
                        alo.push(getVarNr(baseEvent, baseRoom, baseDay, baseTimeBlock));
                    }
                }
            }
            vec.push(alo);
            //at most one
            IVecInt amo;
            for (int baseRoom = 1; baseRoom <= orderedRooms.size(); baseRoom++) {//for all rooms
                for (int baseDay = 1; baseDay <= timeBlocks.length; baseDay++) {//for all days
                    for (int baseTimeBlock = 1; baseTimeBlock <= timeBlocks[baseDay - 1]; baseTimeBlock++) {//for all timeBlocks

                        for (int otherRoom = baseRoom; otherRoom <= orderedRooms.size(); otherRoom++) {//for all other rooms
                            for (int otherDay = 1; otherDay <= timeBlocks.length; otherDay++) {//for all other days
                                for (int otherTimeBlock = 1; otherTimeBlock <= timeBlocks[otherDay - 1]; otherTimeBlock++) {//for all other timeBlocks
                                    if (otherRoom != baseRoom || otherDay > baseDay || (otherDay == baseDay && otherTimeBlock > baseTimeBlock)) {
                                        amo = new VecInt();
                                        amo.push(-getVarNr(baseEvent, baseRoom, baseDay, baseTimeBlock));
                                        amo.push(-getVarNr(baseEvent, otherRoom, otherDay, otherTimeBlock));
                                        vec.push(amo);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //private methods can't be tested easily(reflection, nested test class, changing to public,...)
        int expectedCount = orderedEvents.size() + orderedEvents.size() *
                ((orderedTimeBlocks.size() * orderedRooms.size()) * (orderedTimeBlocks.size() * orderedRooms.size() - 1) / 2);
        assert (expectedCount == vec.size());
        return vec;
    }

    //################################CONSTRAINT CREATION AND VARIABLE HANDLING FRO SCHEDULING##########################

    /**
     * Returns clauses for the "rooms can't be occupied by more than one event at the same time" constraint
     * at most one: (-Eventi or -Eventj) for all i,j where j>i for all rooms and TimeBlocks
     *
     * @return
     */
    private IVec maximumOneEventPerRoomPerTimeBlock() {
        IVec vec = new Vec();
        IVecInt amo;
        for (int baseRoom = 1; baseRoom <= orderedRooms.size(); baseRoom++) {
            for (int baseDay = 1; baseDay <= timeBlocks.length; baseDay++) {
                for (int baseTimeBlock = 1; baseTimeBlock <= timeBlocks[baseDay - 1]; baseTimeBlock++) {
                    for (int baseEvent = 1; baseEvent <= orderedEvents.size(); baseEvent++) {
                        for (int otherEvent = baseEvent + 1; otherEvent <= orderedEvents.size(); otherEvent++) {
                            amo = new VecInt();
                            amo.push(-getVarNr(baseEvent, baseRoom, baseDay, baseTimeBlock));
                            amo.push(-getVarNr(otherEvent, baseRoom, baseDay, baseTimeBlock));
                            vec.push(amo);
                        }
                    }
                }
            }
        }
        //private methods can't be tested easily(reflection, nested test class, changing to public,...)
        int expectedCount = orderedRooms.size() * orderedTimeBlocks.size() * ((orderedEvents.size()) * (orderedEvents.size() - 1) / 2);
        assert (expectedCount == vec.size());
        return vec;
    }

    /**
     * For each TherapyEvent for each TimeBlock there can be either event1 (in one of the rooms) or event2-n (in one of
     * the rooms)
     *
     * @return
     */
    private IVec maximumOneEventPerRoomPerTimeBlockPerTherapist() {
        IVec vec = new Vec();
        int expectedCount = 0;
        for (Integer therapistId : eventsForTherapists.keySet()) {
            List<Integer> eventIndexList = eventsForTherapists.get(therapistId);
            int expectedSingleCount = 0;
            IVecInt amo;
            for (int baseEvent = 0; baseEvent < eventIndexList.size(); baseEvent++) {
                int expectedCountOneEvent = orderedTimeBlocks.size() * (eventIndexList.size() - 1 - baseEvent) * orderedRooms.size() * orderedRooms.size();
                expectedSingleCount += expectedCountOneEvent;
                for (int baseDay = 1; baseDay <= timeBlocks.length; baseDay++) {//for all days
                    for (int baseTimeBlock = 1; baseTimeBlock <= timeBlocks[baseDay - 1]; baseTimeBlock++) {//for all timeBlocks
                        //for 1 time slot there are x rooms for the base event
                        //and for y other events also n rooms that the other event could take place in the same timeslot
                        //so there are x*y*x "at most one" constraints
                        for (int baseRoom = 1; baseRoom <= orderedRooms.size(); baseRoom++) {//for all rooms
                            for (int otherEvent = baseEvent + 1; otherEvent < eventIndexList.size(); otherEvent++) {
                                for (int otherRoom = 1; otherRoom <= orderedRooms.size(); otherRoom++) {//for all other rooms
                                    amo = new VecInt();
                                    int event1 = eventIndexList.get(baseEvent) + 1;
                                    int event2 = eventIndexList.get(otherEvent) + 1;
                                    amo.push(-getVarNr(event1, baseRoom, baseDay, baseTimeBlock));
                                    amo.push(-getVarNr(event2, otherRoom, baseDay, baseTimeBlock));
                                    vec.push(amo);
                                }
                            }
                        }
                    }
                }

            }
            expectedCount += expectedSingleCount;

        }

        assert (expectedCount == vec.size());
        return vec;
    }

    /**
     * Returns clauses for the "patient can only participate at one event at a time" constraint for each TherapyEvent
     * for each TimeBlock there can be either event1 (in one of the rooms) or event2-n(in one of the rooms)
     *
     * @return
     */
    private IVec maximumOneEventPerRoomPerTimeBlockPerPatient() {
        IVec vec = new Vec();
        int expectedCount = 0;
        for (Integer therapyId : eventsForParticipants.keySet()) {
            List<Integer> eventIndexList = eventsForParticipants.get(therapyId);
            int expectedSingleCount = 0;
            IVecInt amo;
            for (int baseEvent = 0; baseEvent < eventIndexList.size(); baseEvent++) {
                int expectedCountOneEvent = orderedTimeBlocks.size() * (eventIndexList.size() - 1 - baseEvent) * orderedRooms.size() * orderedRooms.size();
                expectedSingleCount += expectedCountOneEvent;
                for (int baseDay = 1; baseDay <= timeBlocks.length; baseDay++) {//for all days
                    for (int baseTimeBlock = 1; baseTimeBlock <= timeBlocks[baseDay - 1]; baseTimeBlock++) {//for all timeBlocks
                        //for 1 time slot there are x rooms for the base event
                        //and for y other events also n rooms that the other event could take place in the same timeslot
                        //so there are x*y*x "at most one" constraints
                        for (int baseRoom = 1; baseRoom <= orderedRooms.size(); baseRoom++) {//for all rooms
                            for (int otherEvent = baseEvent + 1; otherEvent < eventIndexList.size(); otherEvent++) {
                                for (int otherRoom = 1; otherRoom <= orderedRooms.size(); otherRoom++) {//for all other rooms
                                    amo = new VecInt();
                                    int event1 = eventIndexList.get(baseEvent) + 1;
                                    int event2 = eventIndexList.get(otherEvent) + 1;
                                    amo.push(-getVarNr(event1, baseRoom, baseDay, baseTimeBlock));
                                    amo.push(-getVarNr(event2, otherRoom, baseDay, baseTimeBlock));
                                    vec.push(amo);
                                }
                            }
                        }
                    }
                }

            }
            expectedCount += expectedSingleCount;

        }

        assert (expectedCount == vec.size());
        return vec;
    }

    /**
     * Returns clauses for the "event can only happen in suiting rooms" constraint if for one TherapyEvent a
     * TherapyRoom
     * is not suitable, you have to lock all TimeBlocks for this TherapyEvent/TherapyRoom combination
     *
     * @return
     */
    private IVec onlySuitingRooms() {
        IVec vec = new Vec();
        int expectedCount = 0;
        IVecInt not;
        for (Integer eventIndex : forbiddenRoomsForEvents.keySet()) {
            List<Integer> forbiddenRoomsForEvent = forbiddenRoomsForEvents.get(eventIndex);
            int expectedCountOneEvent = orderedTimeBlocks.size() * forbiddenRoomsForEvent.size();
            expectedCount += expectedCountOneEvent;
            for (int i = 0; i < forbiddenRoomsForEvent.size(); i++) {
                for (int baseDay = 1; baseDay <= timeBlocks.length; baseDay++) {//for all days
                    for (int baseTimeBlock = 1; baseTimeBlock <= timeBlocks[baseDay - 1]; baseTimeBlock++) {//for all timeBlocks
                        not = new VecInt();
                        not.push(-getVarNr(eventIndex + 1, forbiddenRoomsForEvent.get(i) + 1, baseDay, baseTimeBlock));
                        vec.push(not);
                    }
                }
            }
        }
        assert (expectedCount == vec.size());
        return vec;
    }

    /**
     * Returns clauses for the "respect free time of Therapists" constraint
     * <p/>
     * Lock all TherapyEvents for all rooms for the locked TimeBlocks
     *
     * @return
     */
    private IVec respectTherapistFreeTime() {
        IVec vec = new Vec();
        int expectedCount = 0;
        IVecInt not;
        for (Integer eventIndex : orderedFreeTime.keySet()) {
            List<Integer> lockedTimeSlotsForEvent = orderedFreeTime.get(eventIndex);
            int expectedCountOneEvent = orderedRooms.size() * lockedTimeSlotsForEvent.size();
            expectedCount += expectedCountOneEvent;
            for (Integer timeBlockIndex : lockedTimeSlotsForEvent) {
                for (int baseRoom = 1; baseRoom <= orderedRooms.size(); baseRoom++) {//for all rooms
                    not = new VecInt();
                    not.push(-getVarNr(eventIndex + 1, baseRoom, timeBlockIndex + 1));
                    vec.push(not);
                }
            }
        }
        assert (expectedCount == vec.size());
        return vec;
    }

    private List<TherapyEvent> getSolution(int[] model) {
        List<TherapyEvent> events = new ArrayList<>();
        for (int i : model) {
            if (i > 0) {
                events.add(getEventForVariable(i));
            }
        }
        return events;
    }

    /**
     * Returns the variable number for the specified parameters
     *
     * @param eventNr
     * @param roomNr
     * @param day
     * @param timeslot
     * @return
     */
    private int getVarNr(int eventNr, int roomNr, int day, int timeslot) {
        if (eventNr <= 0 || roomNr <= 0 || day <= 0 || day > timeBlocks.length || timeslot <= 0 || timeslot > timeBlocks[day - 1]) {
            throw new InvalidParameterException();
        }

        int absoluteTimeslot = 0;

        //count timeBlocks of full days
        for (int i = 0; i < day - 1; i++) {
            absoluteTimeslot += timeBlocks[i];
        }
        absoluteTimeslot += timeslot;

        return (eventNr - 1) * orderedRooms.size() * orderedTimeBlocks.size() + (roomNr - 1) * orderedTimeBlocks.size() + absoluteTimeslot;

    }

    /**
     * Returns the variable number for the specified parameters
     *
     * @param eventNr
     * @param roomNr
     * @param absoluteTimeBlock
     * @return
     */
    private int getVarNr(int eventNr, int roomNr, int absoluteTimeBlock) {
        if (eventNr <= 0 || roomNr <= 0 || absoluteTimeBlock <= 0 || absoluteTimeBlock > orderedTimeBlocks.size()) {
            throw new InvalidParameterException();
        }

        return (eventNr - 1) * orderedRooms.size() * orderedTimeBlocks.size() + (roomNr - 1) * orderedTimeBlocks.size() + absoluteTimeBlock;

    }

    /**
     * Returns the TherapyEvent with assigned TherapyRoom and TimeBlock
     *
     * @param variableNr
     * @return Event with assigned TherapyRoom and TimeBlock
     */
    private TherapyEvent getEventForVariable(int variableNr) {
        int varNr = variableNr - 1; //as variables go from 1-n for 1st Event/Room/Day it has to be corrected to 0-(n-1), so that n is already the next event/Room/Day
        //get Event
        int eventNr = varNr / orderedRooms.size() / orderedTimeBlocks.size();
        TherapyEvent event = new TherapyEvent(orderedEvents.get(eventNr));
        //get Room
        int eventBase = eventNr * orderedRooms.size() * orderedTimeBlocks.size();
        int roomNr = (varNr - eventBase) / orderedTimeBlocks.size();
        event.setTherapyRoom(orderedRooms.get(roomNr));
        //get TimeBlock
        int roomBase = roomNr * orderedTimeBlocks.size() + eventBase;
        int timeBlockNr = varNr - roomBase;
        event.setTherapyTimeBlock(orderedTimeBlocks.get(timeBlockNr));
        event.setTherapyTime(DateTimeConverter.dateTimeFromTherapyPlanAndTimeBlock(event.getTherapyPlan(), event.getTherapyTimeBlock()));
        return event;
    }

    private class EventCreationHelper {

        private TherapyPlan activePlan = null;
        /**
         * All active Therapies
         */
        private List<Therapy> activeTherapies = null;

        /**
         * Therapists and their availability
         */
        private Map<Therapist, Integer> therapistsAvailability = null;

        /**
         * All TherapyTypes and occurrences sorted by Therapies
         */
        private Map<Therapy, List<PatientBasedTherapyTypeOccurrenceConstraint>> therapyTypesByTherapy = null;

        /**
         * Preferred Therapist for each TherapyType sorted by Therapy
         */
        private Map<Therapy, Map<TherapyType, Therapist>> preferredTherapistForEvents = null;

        /**
         * Preferred Therapist Gender for each TherapyType sorted by Therapy
         */
        private Map<Therapy, Map<TherapyType, Gender>> preferredTherapistGenderForEvents = null;

        private EventCreationHelper(TherapyPlan plan) throws PersistenceException, PlanningException {
            log.debug("construct eventCreationHelper for date " + plan.getDateFrom() + " to " + plan.getDateUntil());
            this.activePlan = plan;
            setUpActiveTherapies();
            setUpTherapists();
            setUpNominalTherapyTypes();
            setUpPreferredTherapists();
            setUpPreferredTherapistGender();
        }

        /**
         * Set availabilities for Therapists
         *
         * @throws PersistenceException
         */
        private void setUpTherapists() throws PersistenceException {
            log.debug("set up therapist availabilities");
            therapistsAvailability = new HashMap<>();
            List<Therapist> allTherapists = therapistDAO.findAll();
            for (Therapist therapist : allTherapists) {
                int availableTimeBlocks = therapistBasedTimeBlockConstraintDAO.findAllByTherapist(therapist, true).size();
                log.trace("therapist with id " + therapist.getId() + " has " + availableTimeBlocks + " available time blocks");
                if (availableTimeBlocks > 0) {
                    therapistsAvailability.put(therapist, availableTimeBlocks);
                }
            }
            log.debug("size of available Therapists: " + therapistsAvailability.entrySet().size());
        }

        /**
         * Set availabilities for therapists
         *
         * @throws PersistenceException
         */
        private void setUpNominalTherapyTypes() throws PersistenceException {
            log.debug("set up nominal TherapyTypes");
            therapyTypesByTherapy = new HashMap<>();
            for (Therapy therapy : activeTherapies) {
                List<PatientBasedTherapyTypeOccurrenceConstraint> therapyTypes = patientBasedTherapyTypeOccurrenceConstraintDAO.findAllByPatient(therapy.getPatient(), true);
                log.debug(therapyTypes.size() + " TherapyTypes found for Therapy: " + therapy.getId());
                therapyTypesByTherapy.put(therapy, therapyTypes);
            }
        }

        /**
         * Lists all Therapist preferences for all TherapyTypes
         * Sets up the Map for every Therapy
         * If Therapy has no preferences, the map is empty
         * If TherapyType for a Therapy has no preference, map has null-value as preference
         *
         * @throws PlanningException    if more than 1 Therapist is set as preference
         * @throws PersistenceException
         */
        private void setUpPreferredTherapists() throws PlanningException, PersistenceException {
            log.debug("set up Therapist preferences");
            preferredTherapistForEvents = new HashMap<>();
            for (Therapy therapy : activeTherapies) {
                Map<TherapyType, Therapist> preferredTherapist = new HashMap<>();
                for (PatientBasedTherapyTypeOccurrenceConstraint therapyTypeOccurenceConstraint : getAllTherapyTypes(therapy)) {
                    TherapyType therapyType = therapyTypeDAO.findOneById(therapyTypeOccurenceConstraint.getTherapyType());
                    List<PatientBasedTherapistConstraint> preferredTherapistsForPatient = patientBasedTherapistConstraintDAO.findAllByPatient(therapy.getPatient(), true);
                    List<Therapist> therapistsForTherapyType = new ArrayList<>();
                    for (PatientBasedTherapistConstraint constraint : preferredTherapistsForPatient) {
                        if (constraint.getTherapyType().getId().equals(therapyType.getId())) {
                            therapistsForTherapyType.add(constraint.getTherapist());
                        }
                    }
                    if (therapistsForTherapyType.size() > 1) {
                        log.error("maximum 1 preferred Therapist allowed. Therapy Id: " + therapy.getId() + " TherapyType Id: " + therapyType.getId());
                        throw new PlanningException("maximum 1 preferred Therapist allowed. Therapy Id: " + therapy.getId() + " TherapyType Id: " + therapyType.getId());
                    }
                    if (therapistsForTherapyType.size() > 0) {
                        Therapist assigned = therapistsForTherapyType.get(0);
                        log.trace("preferred Therapist assigned for Therapy Id: " + therapy.getId() +
                                " TherapyType Id: " + therapyType.getId() + "Therapist Id: " + assigned.getId());
                        preferredTherapist.put(therapyType, assigned);
                    }
                    else {
                        log.trace("preferred Therapist not assigned for Therapy Id: " + therapy.getId() +
                                " TherapyType Id: " + therapyType.getId());
                        preferredTherapist.put(therapyType, null);
                    }
                }

                preferredTherapistForEvents.put(therapy, preferredTherapist);

            }

        }

        /**
         * Lists all Therapist Gender preferences for all TherapyTypes
         * Sets up the Map for every Therapy
         * If therapy has no preferences, the map is empty
         * If therapyType for a Therapy has no preference, map has null-value as preference
         *
         * @throws PlanningException    if more than 1 Therapist Gender is set as preference
         * @throws PersistenceException
         */
        private void setUpPreferredTherapistGender() throws PersistenceException, PlanningException {
            log.debug("set up Therapist Gender preferences");
            //get all preferred Therapist Gender by Therapy
            preferredTherapistGenderForEvents = new HashMap<>();
            for (Therapy therapy : activeTherapies) {
                Map<TherapyType, Gender> preferredTherapistGender = new HashMap<>();
                for (PatientBasedTherapyTypeOccurrenceConstraint therapyTypeOccurenceConstraint : getAllTherapyTypes(therapy)) {
                    TherapyType therapyType = therapyTypeDAO.findOneById(therapyTypeOccurenceConstraint.getTherapyType());
                    List<PatientBasedTherapistGenderConstraint> preferredTherapistGendersForPatient = patientBasedTherapistGenderConstraintDAO.findAllByPatient(therapy.getPatient(), true);
                    List<Gender> therapistGendersForTherapyType = new ArrayList<>();
                    for (PatientBasedTherapistGenderConstraint constraint : preferredTherapistGendersForPatient) {
                        if (constraint.getTherapyType().getId().equals(therapyType.getId())) {
                            therapistGendersForTherapyType.add(constraint.getGender());
                        }
                    }
                    if (therapistGendersForTherapyType.size() > 1) {
                        log.error("maximum 1 preferred Therapist Gender allowed. Therapy Id: " + therapy.getId() + " TherapyType Id: " + therapyType.getId());
                        throw new PlanningException("maximum 1 preferred Therapist Gender allowed. Therapy Id: " + therapy.getId() + " TherapyType Id: " + therapyType.getId());
                    }
                    if (therapistGendersForTherapyType.size() > 0) {
                        Gender assigned = therapistGendersForTherapyType.get(0);
                        log.trace("preferred Therapist Gender assigned for Therapy Id: " + therapy.getId() +
                                " TherapyType Id: " + therapyType.getId() + "Therapist gender: " + assigned);
                        preferredTherapistGender.put(therapyType, therapistGendersForTherapyType.get(0));
                    }
                    else {
                        log.trace("preferred Therapist Gender not assigned for Therapy Id: " + therapy.getId() +
                                " TherapyType Id: " + therapyType.getId());
                        preferredTherapistGender.put(therapyType, null);
                    }
                }

                preferredTherapistGenderForEvents.put(therapy, preferredTherapistGender);

            }
        }

        /**
         * Set up all Therapies that are valid for this week
         *
         * @throws PersistenceException
         */
        private void setUpActiveTherapies() throws PersistenceException {
            if (activeTherapies == null) {
                activeTherapies = therapyDAO.findAllByTherapyPlan(activePlan);
            }
            log.debug("count of active Therapies: " + activeTherapies.size());
        }

        /**
         * Set up all Therapies that are valid for this week
         *
         * @throws PersistenceException
         */
        private List<Therapy> getAllActiveTherapies() throws PersistenceException {
            return activeTherapies;
        }

        /**
         * Looks up the preferred Therapist for the Therapy and TherapyType
         *
         * @param therapy
         * @param therapyType
         * @return Preferred Therapist. if no preference set, returns null
         * @throws PersistenceException
         * @throws PlanningException
         */
        private Therapist getPreferredTherapist(Therapy therapy, TherapyType therapyType) throws PersistenceException, PlanningException {
            log.debug("get preferred Therapist for for Therapy: " + therapy.getId() + " and TherapyType: " + therapyType.getId());
            Therapy nativeTherapy = getInstanceById(therapy, preferredTherapistForEvents.keySet());
            TherapyType nativeTherapyType = getInstanceById(therapyType, preferredTherapistForEvents.get(nativeTherapy).keySet());
            if (nativeTherapyType == null) {
                log.debug("no entry for TherapyType found");
                return null;
            }
            Therapist returned = preferredTherapistForEvents.get(nativeTherapy).get(nativeTherapyType);
            if (returned == null) {
                log.debug("no preferred therapist found");
                return null;
            }
            log.debug("preferred therapist: " + returned.getId());
            return returned;
        }

        /**
         * Looks up the preferred Therapist Gender for the Therapy and TherapyType
         *
         * @param therapy
         * @param therapyType
         * @return Preferred Therapist. if no preference set, returns null
         * @throws PersistenceException
         * @throws PlanningException
         */
        private Gender getPreferredTherapistGender(Therapy therapy, TherapyType therapyType) throws PersistenceException, PlanningException {
            log.debug("get preferred Therapist Gender for for Therapy: " + therapy.getId() + " and TherapyType: " + therapyType.getId());
            Therapy nativeTherapy = getInstanceById(therapy, preferredTherapistGenderForEvents.keySet());
            TherapyType nativeTherapyType = getInstanceById(therapyType, preferredTherapistGenderForEvents.get(nativeTherapy).keySet());
            if (nativeTherapyType == null) {
                log.debug("no entry for TherapyType found");
                return null;
            }
            Gender returned = preferredTherapistGenderForEvents.get(nativeTherapy).get(nativeTherapyType);
            if (returned == null) {
                log.debug("no preferred therapist gender found");
                return null;
            }
            log.debug("preferred therapist gender: " + returned);
            return returned;
        }

        /**
         * Returns the most suitable Therapist for a TherapyType
         *
         * @param therapyType The TherapyType the Therapist has to be qualified in
         * @return
         */
        private Therapist getTherapistForTherapyType(TherapyType therapyType) throws PersistenceException, PlanningException {
            log.debug("get Therapist for TherapyType: " + therapyType.getId());
            Therapist selectedTherapist = null;
            for (Therapist therapist : getTherapistsForTherapyType(therapyType)) {
                //choose the therapist with the most available timeBlocks
                if (selectedTherapist == null ||
                        getAvailableTimeBlocksForTherapist(therapist) > getAvailableTimeBlocksForTherapist(selectedTherapist)) {
                    selectedTherapist = therapist;
                    log.trace("currently selected Therapist: " + selectedTherapist.getId() + " with available TimeBlock count of: " + getAvailableTimeBlocksForTherapist(selectedTherapist));
                }
            }
            if (selectedTherapist == null) {
                throw new PlanningException("no Therapist available for Therapy Type: " + therapyType.getName());
            }
            log.debug("selected Therapist: " + selectedTherapist.getId());
            return selectedTherapist;
        }

        /**
         * Returns the most suitable Therapist for a TherapyType
         *
         * @param therapyType     The TherapyType the Therapist has to be qualified in
         * @param preferredGender The Therapist's Gender
         * @return
         */
        private Therapist getTherapistForTherapyType(TherapyType therapyType, Gender preferredGender) throws PersistenceException, PlanningException {
            log.debug("get Therapist for TherapyType: " + therapyType.getId() + " with gender: " + preferredGender);
            Therapist selectedTherapist = null;
            for (Therapist therapist : getTherapistsForTherapyType(therapyType, preferredGender)) {
                //choose the therapist with the most available timeBlocks
                if (selectedTherapist == null ||
                        getAvailableTimeBlocksForTherapist(therapist) > getAvailableTimeBlocksForTherapist(selectedTherapist)) {
                    selectedTherapist = therapist;
                    log.trace("currently selected Therapist: " + selectedTherapist.getId() + " with available TimeBlock count of: " + getAvailableTimeBlocksForTherapist(selectedTherapist));
                }
            }
            if (selectedTherapist == null){
                throw new PlanningException("no Therapist available for Therapy Type: " + therapyType.getName() + " and Gender: " + preferredGender);
            }
            log.debug("selected Therapist: " + selectedTherapist.getId());
            return selectedTherapist;
        }

        /**
         * @param therapyType TherapyType the Therapist has to be qualified for
         * @return Qualified therapists
         */
        private List<Therapist> getTherapistsForTherapyType(TherapyType therapyType) throws PersistenceException {
            log.debug("get all qualified Therapists for TherapyType: " + therapyType.getId());
            List<Therapist> qualified = new ArrayList<>();
            for (Therapist therapist : therapistsAvailability.keySet()) {
                List<TherapistBasedTherapyTypeOccurrenceConstraint> tbaTherapyTypeOccurrenceConstraints= therapistBasedTherapyTypeOccurrenceConstraintDAO.findAllByTherapist(therapist);
                List<TherapyType> therapyTypes = new ArrayList<>();
                for (TherapistBasedTherapyTypeOccurrenceConstraint therapistBasedTherapyTypeOccurrenceConstraint: tbaTherapyTypeOccurrenceConstraints){
                    therapyTypes.add(therapistBasedTherapyTypeOccurrenceConstraint.getTherapyType());
                }
                TherapyType found = getInstanceById(therapyType, therapyTypes);
                if (found != null) {
                    qualified.add(therapist);
                }
            }
            log.debug("count of qualified Therapists: " + qualified.size());
            return qualified;
        }

        /**
         * @param therapyType     TherapyType the Therapist has to be qualified for
         * @param preferredGender Preferred gender of the Therapist
         * @return
         */
        private List<Therapist> getTherapistsForTherapyType(TherapyType therapyType, Gender preferredGender) throws PersistenceException {
            log.debug("get all qualified Therapists for TherapyType: " + therapyType.getId() + " with gender: " + preferredGender);
            List<Therapist> qualified = new ArrayList<>();
            for (Therapist therapist : therapistsAvailability.keySet()) {
                if (therapist.getGender().equals(preferredGender)) {
                    List<TherapistBasedTherapyTypeOccurrenceConstraint> tbaTherapyTypeOccurrenceConstraints= therapistBasedTherapyTypeOccurrenceConstraintDAO.findAllByTherapist(therapist);
                    List<TherapyType> therapyTypes = new ArrayList<>();
                    for (TherapistBasedTherapyTypeOccurrenceConstraint therapistBasedTherapyTypeOccurrenceConstraint: tbaTherapyTypeOccurrenceConstraints){
                        therapyTypes.add(therapistBasedTherapyTypeOccurrenceConstraint.getTherapyType());
                    }
                    TherapyType found = getInstanceById(therapyType, therapyTypes);
                    if (found != null) {
                        qualified.add(therapist);
                    }
                }
            }
            log.debug("count of qualified Therapists: " + qualified.size());
            return qualified;
        }

        /**
         * Returns the available TimeBlocks for a Therapist
         *
         * @param therapist
         * @return
         */
        private int getAvailableTimeBlocksForTherapist(Therapist therapist) {
            log.debug("get available Time Blocks for Therapist: " + therapist.getId());
            Therapist nativeTherapist = getInstanceById(therapist, therapistsAvailability.keySet());
            if (nativeTherapist != null) {
                log.debug("count: " + therapistsAvailability.get(nativeTherapist));
                return therapistsAvailability.get(nativeTherapist);
            }
            return -1;
        }

        /**
         * @param therapist Therapist whose availability has to be decremented
         */
        private void decrementAvailabilityForTherapist(Therapist therapist) throws PlanningException {
            log.debug("decrement available Time Blocks for Therapist: " + therapist.getId());
            Therapist nativeTherapist = getInstanceById(therapist, therapistsAvailability.keySet());
            if (nativeTherapist != null) {
                int newCount = therapistsAvailability.get(nativeTherapist) - 1;
                if (newCount < 0) {
                    throw new PlanningException("Therapist: " + nativeTherapist.getId() + "has no more available hours");
                }
                log.trace("decrement done");
                therapistsAvailability.put(nativeTherapist, newCount);
            }
        }

        /**
         * Returns all TherapyTypes for one Therapy
         *
         * @param therapy
         * @return
         * @throws PersistenceException
         */
        private List<PatientBasedTherapyTypeOccurrenceConstraint> getAllTherapyTypes(Therapy therapy) throws PersistenceException {
            log.debug("get all TherapyTypes(to be assigned) for Therapy: " + therapy.getId());
            if (therapyTypesByTherapy == null) {
                //get all TherapyType Constraints for all Therapies
                therapyTypesByTherapy = new HashMap<>();
                for (Therapy t : activeTherapies) {
                    therapyTypesByTherapy.put(t, patientBasedTherapyTypeOccurrenceConstraintDAO.findAllByPatient(t.getPatient(), false));
                }
            }
            Therapy nativeTherapy = getInstanceById(therapy, therapyTypesByTherapy.keySet());
            List<PatientBasedTherapyTypeOccurrenceConstraint> returned = therapyTypesByTherapy.get(nativeTherapy);
            log.debug("count of therapy types to be assigned: " + returned.size());
            return returned;
        }

        /**
         * Returns all individual TherapyEvents for a Therapy
         *
         * @param therapy
         * @return null if no individual TherapyType exists
         * @throws PersistenceException
         */
        private List<PatientBasedTherapyTypeOccurrenceConstraint> getIndividualTherapyEvents(Therapy therapy) throws PersistenceException {
            log.debug("get all individual TherapyTypes(to be assigned) for Therapy: " + therapy.getId());
            Therapy nativeTherapy = getInstanceById(therapy, therapyTypesByTherapy.keySet());
            List<PatientBasedTherapyTypeOccurrenceConstraint> list = new ArrayList<>();
            for (PatientBasedTherapyTypeOccurrenceConstraint constraint : therapyTypesByTherapy.get(nativeTherapy)) {
                if (constraint.getTherapyMode().equals(TherapyMode.SINGLE)) {
                    list.add(constraint);
                }
            }
            log.debug("count of individual therapy types to be assigned: " + list.size());
            return list;
        }

        /**
         * Returns all group TherapyEvents for a Therapy
         *
         * @param therapy
         * @return null if no group therapyType exists
         * @throws PersistenceException
         */
        private List<PatientBasedTherapyTypeOccurrenceConstraint> getGroupTherapyEvents(Therapy therapy) throws PersistenceException {
            log.debug("get all group TherapyTypes(to be assigned) for Therapy: " + therapy.getId());
            Therapy nativeTherapy = getInstanceById(therapy, therapyTypesByTherapy.keySet());
            List<PatientBasedTherapyTypeOccurrenceConstraint> list = new ArrayList<>();
            for (PatientBasedTherapyTypeOccurrenceConstraint constraint : therapyTypesByTherapy.get(nativeTherapy)) {
                if (constraint.getTherapyMode().equals(TherapyMode.GROUP)) {
                    list.add(constraint);
                }
            }
            log.debug("count of group therapy types to be assigned: " + list.size());
            return list;
        }

        /**
         * Returns the instance inside the set with a certain ID
         *
         * @param searchFor   Instance with the ID to search for
         * @param setToSearch Set from which the instance should be extracted
         * @param <A>
         * @return The instance of the setToSearch that matches the ID in searchFor
         */
        private <A extends BasicDTO> A getInstanceById(A searchFor, Set<A> setToSearch) {
            A returned = null;
            for (A a : setToSearch) {
                if (searchFor.getId() == a.getId()) {
                    returned = a;
                    break;
                }
            }
            return returned;
        }

        /**
         * Returns the instance inside the list with a certain ID
         *
         * @param searchFor    Instance with the ID to search for
         * @param listToSearch List from which the instance should be extracted
         * @param <A>
         * @return The instance of the setToSearch that matches the ID in searchFor
         */
        private <A extends BasicDTO> A getInstanceById(A searchFor, List<A> listToSearch) {
            A returned = null;
            for (A a : listToSearch) {
                if (searchFor.getId() == a.getId()) {
                    returned = a;
                    break;
                }
            }
            return returned;
        }

    }
}