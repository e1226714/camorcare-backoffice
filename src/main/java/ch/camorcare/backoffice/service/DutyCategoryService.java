package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.DutyCategory;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's DutyCategory table.
 */
public interface DutyCategoryService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.DutyCategory} to the persistence layer to be inserted into the
     * database.
     *
     * @param original DutyCategory to be created
     * @return Created DutyCategory
     * @throws ServiceException
     * @throws ValidationException
     */
    public DutyCategory create(DutyCategory original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.DutyCategory} to the persistence layer whose data is updated in
     * the database.
     *
     * @param original DutyCategory to be updated
     * @return Updated DutyCategory
     * @throws ServiceException
     * @throws ValidationException
     */
    public DutyCategory update(DutyCategory original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.DutyCategory} to the persistence layer to be deleted.
     *
     * @param original DutyCategory to be deleted
     * @throws ServiceException
     */
    public void delete(DutyCategory original) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.DutyCategory} objects in the database.
     *
     * @return List of DutyCategory objects
     * @throws ServiceException
     */
    public List<DutyCategory> findAllDutyCategories() throws ServiceException;
}
