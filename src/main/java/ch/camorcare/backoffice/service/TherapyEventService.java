package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's TherapyEvent table.
 */
public interface TherapyEventService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyEvent} to the persistence layer to be inserted into the
     * database.
     *
     * @param original TherapyEvent to be created
     * @return Created TherapyEvent
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapyEvent create(TherapyEvent original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyEvent} to the persistence layer whose data is updated in
     * the database.
     *
     * @param original TherapyEvent to be updated
     * @return Updated TherapyEvent
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapyEvent update(TherapyEvent original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyEvent} to the persistence layer to be deleted.
     *
     * @param original TherapyEvent to be deleted
     * @throws ServiceException
     */
    public void delete(TherapyEvent original) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.TherapyEvent} objects in the database.
     *
     * @return List of TherapyEvent objects
     * @throws ServiceException
     */
    public List<TherapyEvent> findAllTherapyEvents() throws ServiceException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyPlan} to the persistence layer and then returns a list of
     * all {@link ch.camorcare.backoffice.entities.TherapyEvent} objects that belong to the passed TherapyPlan.
     *
     * @param therapyPlan The TherapyPlan to be passed
     * @return A list of TherapyEvent objects
     * @throws ServiceException
     */
    public List<TherapyEvent> findAllTherapyEventsByTherapyPlan(TherapyPlan therapyPlan) throws ServiceException;

    /**
     * Filters a given {@link ch.camorcare.backoffice.entities.TherapyPlan} by the {@link
     * ch.camorcare.backoffice.entities.TherapyEvent} objects that take place in a specific {@link
     * ch.camorcare.backoffice.entities.TherapyRoom}. Converts these TherapyEvent objects into {@link
     * ch.camorcare.backoffice.entities.TherapyEvent} objects and returns them as a list to be finally output as a PDF.
     *
     * @param therapyPlan The TherapyPlan that needs to be filtered and converted
     * @param therapyRoom The TherapyRoom by which the filtering takes placed
     * @return A list of TherapyEvent objects
     */
    public List<TherapyEvent> getTherapyEventsByTherapyPlanAndTherapyRoom(TherapyPlan therapyPlan, TherapyRoom therapyRoom) throws ServiceException, ValidationException;

    /**
     * Filters a given {@link ch.camorcare.backoffice.entities.TherapyPlan} by the {@link
     * ch.camorcare.backoffice.entities.TherapyEvent} objects that are held by a specific {@link
     * ch.camorcare.backoffice.entities.Therapist}. Converts these TherapyEvent objects into {@link
     * ch.camorcare.backoffice.entities.TherapyEvent} objects and returns them as a list to be finally output as a PDF.
     *
     * @param therapyPlan The TherapyPlan that needs to be filtered and converted
     * @param therapist   The Therapist by which the filtering takes placed
     * @return A list of TherapyEvent objects
     */
    public List<TherapyEvent> getTherapyEventsByTherapyPlanAndTherapist(TherapyPlan therapyPlan, Therapist therapist) throws ServiceException, ValidationException;

    /**
     * Filters a given {@link ch.camorcare.backoffice.entities.TherapyPlan} by the {@link
     * ch.camorcare.backoffice.entities.TherapyEvent} objects that are attended by a specific {@link
     * ch.camorcare.backoffice.entities.Patient}. Converts these TherapyEvent objects into {@link
     * ch.camorcare.backoffice.entities.TherapyEvent} objects and returns them as a list to be finally output as a PDF.
     *
     * @param therapyPlan The TherapyPlan that needs to be filtered and converted
     * @param patient     The Patient by which the filtering takes placed
     * @return A list of TherapyEvent objects
     */
    public List<TherapyEvent> getTherapyEventsByTherapyPlanAndPatient(TherapyPlan therapyPlan, Patient patient) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapy} to the persistence layer to be assigned to the {@link
     * ch.camorcare.backoffice.entities.TherapyEvent} that is also passed.
     *
     * @param therapyEvent The TherapyEvent to be passed and to which the Therapy is assigned
     * @param therapy      The Therapy to be passed and which is to be assigned to the TherapyEvent
     * @return <code>true</code> if successful, else <code>false</code>
     * @throws ServiceException
     * @throws ValidationException
     */
    public boolean createParticipation(TherapyEvent therapyEvent, Therapy therapy) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapy} to the persistence layer whose assignment to the {@link
     * ch.camorcare.backoffice.entities.TherapyEvent} that is also passed is removed.
     *
     * @param therapyEvent The TherapyEvent to be passed and to which the Therapy is no longer assigned
     * @param therapy      The Therapy to be passed and which is to be no longer assigned to the TherapyEvent
     * @return <code>true</code> if successful, else <code>false</code>
     * @throws ServiceException
     * @throws ValidationException
     */
    public boolean deleteParticipation(TherapyEvent therapyEvent, Therapy therapy) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapyEvent} to the persistence layer and returns a list of all
     * participants of this event in the form of {@link ch.camorcare.backoffice.entities.Therapy} objects.
     *
     * @param therapyEvent The TherapyEvent to be passed and used in the search function
     * @return A list of Therapy objects
     * @throws ServiceException
     * @throws ValidationException
     */
    public List<Therapy> getParticipants(TherapyEvent therapyEvent) throws ServiceException, ValidationException;


    /**
     * Returns all TimeBlocks that are available for a specific Events
     *
     * @param therapyEvent
     * @return
     * @throws ch.camorcare.backoffice.service.exception.ServiceException
     * @throws ch.camorcare.backoffice.util.validator.ValidationException
     */
    public List<TimeBlock> findAllAvailableTimeBlocksByTherapyEvent(TherapyEvent therapyEvent) throws ServiceException, ValidationException;

}
