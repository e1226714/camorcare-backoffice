package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's Therapist table.
 */
public interface TherapistService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapist} to the persistence layer to be inserted into the
     * database.
     *
     * @param original Therapist to be created
     * @return Created Therapist
     * @throws ServiceException
     * @throws ValidationException
     */
    public Therapist create(Therapist original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapist} to the persistence layer whose data is updated in the
     * database.
     *
     * @param original Therapist to be updated
     * @return Updated Therapist
     * @throws ServiceException
     * @throws ValidationException
     */
    public Therapist update(Therapist original) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapist} to the persistence layer to be deleted.
     *
     * @param original Therapist to be deleted
     * @throws ServiceException
     */
    public void delete(Therapist original) throws ServiceException;

    /**
     * Returns all {@link ch.camorcare.backoffice.entities.Therapist} objects in the database.
     *
     * @return List of Therapist objects
     * @throws ServiceException
     */
    public List<Therapist> findAllTherapists() throws ServiceException;

    /**
     * Reads all {@link ch.camorcare.backoffice.entities.Therapist} objects who can hold the given {@link
     * ch.camorcare.backoffice.entities.TherapyType}.
     *
     * @param therapyType The TherapyType to be used in the search function
     * @return list of therapists
     * @throws ServiceException
     */
    public List<Therapist> findAllTherapistsByTherapyType(TherapyType therapyType) throws ServiceException;

    /**
     * Reads all {@link ch.camorcare.backoffice.entities.Therapist} objects who hold therapies in the given {@link
     * ch.camorcare.backoffice.entities.TherapyPlan}.
     *
     * @param therapyPlan
     * @return list of therapists
     * @throws ServiceException
     */
    public List<Therapist> findAllTherapistsByTherapyPlan(TherapyPlan therapyPlan) throws ServiceException;

    /**
     *
     * @param therapist
     * @return
     * @throws ServiceException
     */
    public Therapist findOneById(Therapist therapist) throws ServiceException;

}
