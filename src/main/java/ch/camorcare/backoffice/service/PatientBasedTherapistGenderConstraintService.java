package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's PatientBasedTherapistGenderConstraint
 * table.
 */
public interface PatientBasedTherapistGenderConstraintService {

    /**
     * Passes a list of {@link ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint} to the
     * persistence layer along with the {@link ch.camorcare.backoffice.entities.Patient} for whom the constraints are
     * relevant.
     *
     * @param constraints The PatientBasedTherapistGenderConstraints to be added to the database
     * @param patient     The Patient for whom the constraints are relevant
     * @return A list of complete PatientBasedTherapistGenderConstraint objects
     * @throws ServiceException
     * @throws ValidationException
     */
    public List<PatientBasedTherapistGenderConstraint> create(List<PatientBasedTherapistGenderConstraint> constraints, Patient patient) throws ServiceException, ValidationException;

    /**
     * Passes a specific {@link ch.camorcare.backoffice.entities.Patient} to the persistence layer so that way a list of
     * all {@link ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint} objects relevant for a specific
     * Patient can be found.
     *
     * @param patient The Patient used for the search function
     * @return A list of PatientBasedTherapistGenderConstraint objects
     * @throws ServiceException
     * @throws ValidationException
     */
    public List<PatientBasedTherapistGenderConstraint> findAllTherapistGenderConstraintsByPatient(Patient patient) throws ServiceException, ValidationException;

}
