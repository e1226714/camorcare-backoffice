package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.service.exception.ServiceException;

import java.util.List;
import java.util.Map;

/**
 * Provides methods to allow the user interface to communicate with the database's TimeBlock table.
 */
public interface TimeBlockService {

    /**
     * Retrieves all {@link ch.camorcare.backoffice.entities.TimeBlock} objects in the database and groups them by
     * {@link ch.camorcare.backoffice.entities.type.Day}.
     *
     * @return A map with TimeBlocks grouped by Days
     * @throws ServiceException
     */
    public Map<Day, List<TimeBlock>> findAllTimeBlocksGroupedByDay() throws ServiceException;

    /**
     * Retrieves all {@link ch.camorcare.backoffice.entities.TimeBlock} objects from the database.
     *
     * @return A list of TimeBlock objects
     * @throws ServiceException
     */
    public List<TimeBlock> findAllTimeBlocks() throws ServiceException;

}
