package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Provides methods to allow the user interface to communicate with the database's
 * TherapistBasedTherapyTypeOccurrenceConstraint table.
 */
public interface TherapistBasedTherapyTypeOccurrenceConstraintService {

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.Therapist} object to the persistence layer to be used in a
     * search function that returns all {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint}
     * objects stored in the database.
     *
     * @return List of all TherapistBasedTherapyTypeOccurrenceConstraints
     * @throws ServiceException
     */
    public List<TherapistBasedTherapyTypeOccurrenceConstraint> findAllTherapistBasedTherapyTypeOccurrenceConstraintsByTherapist(Therapist therapist) throws ServiceException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint} to the
     * persistence layer to be inserted into the database.
     *
     * @param constraint TherapistBasedTherapyTypeOccurrenceConstraint to be created
     * @return Created TherapistBasedTherapyTypeOccurrenceConstraint
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapistBasedTherapyTypeOccurrenceConstraint create(TherapistBasedTherapyTypeOccurrenceConstraint constraint) throws ServiceException, ValidationException;

    /**
     * Passes a list of {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint} to the
     * persistence layer along with the {@link ch.camorcare.backoffice.entities.Therapist} for whom the constraints are
     * relevant.
     *
     * @param constraints The TherapistBasedTherapyTypeOccurrenceConstraints to be added to the database
     * @param therapist   The Therapist for whom the constraints are relevant
     * @return A list of complete TherapistBasedTherapyTypeOccurrenceConstraint objects
     * @throws ServiceException
     * @throws ValidationException
     */
    public List<TherapistBasedTherapyTypeOccurrenceConstraint> createTherapyTypeOccurrenceConstraints(List<TherapistBasedTherapyTypeOccurrenceConstraint> constraints, Therapist therapist) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint} to the
     * persistence layer whose data is updated in the database.
     *
     * @param constraint TherapistBasedTherapyTypeOccurrenceConstraint to be updated
     * @return Updated TherapistBasedTherapyTypeOccurrenceConstraint
     * @throws ServiceException
     * @throws ValidationException
     */
    public TherapistBasedTherapyTypeOccurrenceConstraint update(TherapistBasedTherapyTypeOccurrenceConstraint constraint) throws ServiceException, ValidationException;

    /**
     * Passes a {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint} to the
     * persistence layer to be deleted.
     *
     * @param constraint TherapistBasedTherapyTypeOccurrenceConstraint to be deleted
     * @throws ServiceException
     */
    public void delete(TherapistBasedTherapyTypeOccurrenceConstraint constraint) throws ServiceException;
}
