package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.entities.TherapyEvent;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.exception.PlanningException;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;
import java.util.Map;

/**
 * Interface for scheduling the Therapy Plans
 */
public interface TherapyScheduleService {

    /**
     * Assign {@link ch.camorcare.backoffice.entities.TherapyRoom} objects and {@link
     * ch.camorcare.backoffice.entities.TimeBlock} to the given list of {@link ch.camorcare.backoffice.entities.TherapyEvent}
     * objects that already have {@link ch.camorcare.backoffice.entities.Therapist} and participating {@link
     * ch.camorcare.backoffice.entities.Patient} assigned constraints:
     * <ul>
     * <li>A therapy Event can only be assigned to a single TimeBlock</li>
     * <li>No event can be assigned to more than one event at once</li>
     * <li>The free time of Therapists is respected</li>
     * </ul>
     *
     * @param plan The therapyPlan containing the Events to assign
     * @return The completed list of TherapyEvent objects
     */
    public List<TherapyEvent> schedule (TherapyPlan plan) throws ServiceException, PlanningException, ValidationException;


    /**
     * Create {@link ch.camorcare.backoffice.entities.TherapyEvent} objects for a whole week.
     * <p/>
     * The method assigns {@link ch.camorcare.backoffice.entities.Patient} objects and {@link
     * ch.camorcare.backoffice.entities.Therapist} objects to those events according to the constraints:
     * <ul>
     * <li>All Patients have to be assigned to all their Therapies</li>
     * <li>All Therapists only manage TherapyTypes they are qualified</li>
     * <li>All Therapist only manage up to a maximum amount of TherapyTypes</li>
     * <li>For all Events the Therapist has to be according to the preferences</li>
     * <li>For all events the preferred Therapist gender is optional</li>
     * </ul>
     *
     * @param plan The completed plan
     * @return
     */
    public Map<TherapyEvent, List<Therapy>> createEventsForWeek(TherapyPlan plan) throws ServiceException, PlanningException, ValidationException;

    /**
     * Passes a completed plan to the database to be persisted.
     *
     * @param events Events with list of participants
     */
    public void createAllEventsWithParticipants(Map<TherapyEvent, List<Therapy>> events) throws ValidationException, ServiceException;

    /**
     * delete all Events and their participants
     * @param plan plan for which all Events have to be deleted
     * @return
     */
    public void deleteAllEventsWithParticipants(TherapyPlan plan) throws ValidationException, ServiceException;
}
