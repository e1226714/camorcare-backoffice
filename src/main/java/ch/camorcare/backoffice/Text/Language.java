package ch.camorcare.backoffice.Text;

import java.util.Locale;

/**
 * Defines the language to be used in the program for internationalization purposes.
 */
public class Language {

    private String language;
    private String country;

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Locale getLocale() {
        return new Locale(language, country);
    }
}
