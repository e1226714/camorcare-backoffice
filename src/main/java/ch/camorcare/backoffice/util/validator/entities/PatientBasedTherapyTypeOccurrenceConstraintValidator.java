package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint} object.
 */
public class PatientBasedTherapyTypeOccurrenceConstraintValidator extends EntityValidator<PatientBasedTherapyTypeOccurrenceConstraint> {

    public PatientBasedTherapyTypeOccurrenceConstraintValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(PatientBasedTherapyTypeOccurrenceConstraint toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(PatientBasedTherapyTypeOccurrenceConstraint toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Therapy.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return PatientBasedTherapyTypeOccurrenceConstraint.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        PatientBasedTherapyTypeOccurrenceConstraint dto = (PatientBasedTherapyTypeOccurrenceConstraint) o;
        ValidationRules.rejectIfNullOrIdNotExistent(e, "patient", "no ID", dto.getPatient());
        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapyType", "no ID", dto.getTherapyType());
        ValidationRules.rejectIfNull(e, "therapyMode", "is Null", dto.getTherapyMode());
        ValidationRules.rejectIfLeqThanZero(e, "maxOccurrencePerWeek", "less or equal zero", dto.getMaxOccurencePerWeek());
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }
}
