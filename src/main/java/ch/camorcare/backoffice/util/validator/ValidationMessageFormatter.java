package ch.camorcare.backoffice.util.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.List;

/**
 * Formats the Validation errors in a readable format
 */
public class ValidationMessageFormatter {

    public static String formatErrorsObject(Errors errors){
        String retVal = "";
        List<ObjectError> globalErrorList = errors.getGlobalErrors();
        for (ObjectError error: globalErrorList){
            retVal += "\n" + error.getDefaultMessage();
        }
        List<FieldError> errorList = errors.getFieldErrors();
        for (FieldError error: errorList){
            retVal += "\n" + error.getField() + ": " + error.getDefaultMessage();
        }
        return retVal;
    }
}
