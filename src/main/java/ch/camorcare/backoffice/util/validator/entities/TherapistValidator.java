package ch.camorcare.backoffice.util.validator.entities;


import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.Therapist} object.
 */
public class TherapistValidator extends EntityValidator<Therapist> {

    public TherapistValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(Therapist toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(Therapist toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Therapist.class.getName());
        this.validate(toValidate, err);
        return err;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return Therapist.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        Therapist dto = (Therapist) o;

        ValidationRules.rejectIfLengthOutOfBounds(e, "firstName", "string length out of bounds", dto.getFirstName(), 1, 100);
        ValidationRules.rejectIfLengthOutOfBounds(e, "lastName", "string length out of bounds", dto.getLastName(), 1, 100);
        ValidationRules.rejectIfNull(e, "gender", "is null", dto.getGender());
        ValidationRules.rejectIfLengthOutOfBounds(e, "email", "string length out of bounds", dto.getEmail(), 5, 200);
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
