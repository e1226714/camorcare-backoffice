package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint} object.
 */
public class TherapistBasedTherapyTypeOccurrenceConstraintValidator extends EntityValidator<TherapistBasedTherapyTypeOccurrenceConstraint> {

    public TherapistBasedTherapyTypeOccurrenceConstraintValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(TherapistBasedTherapyTypeOccurrenceConstraint toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(TherapistBasedTherapyTypeOccurrenceConstraint toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Therapy.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return TherapistBasedTherapyTypeOccurrenceConstraint.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        TherapistBasedTherapyTypeOccurrenceConstraint dto = (TherapistBasedTherapyTypeOccurrenceConstraint) o;

        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapist", "no ID", dto.getTherapist());
        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapyType", "no ID", dto.getTherapyType());
        ValidationRules.rejectIfNull(e, "therapyMode", "is Null", dto.getTherapyMode());
        ValidationRules.rejectIfNull(e, "maxOccurrencePerDay", "is Null", dto.getMaxOccurrencePerDay());
        ValidationRules.rejectIfNumberOutOfBounds(e, "maxOccurrencePerDay", "out of bounds [0,6]", dto.getMaxOccurrencePerDay(), 0, 6);

        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }
}
