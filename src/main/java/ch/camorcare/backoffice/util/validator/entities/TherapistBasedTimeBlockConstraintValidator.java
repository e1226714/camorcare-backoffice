package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} object.
 */
public class TherapistBasedTimeBlockConstraintValidator extends EntityValidator<TherapistBasedTimeBlockConstraint> {

    public TherapistBasedTimeBlockConstraintValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(TherapistBasedTimeBlockConstraint toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(TherapistBasedTimeBlockConstraint toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Therapy.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return TherapistBasedTimeBlockConstraint.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        TherapistBasedTimeBlockConstraint dto = (TherapistBasedTimeBlockConstraint) o;

        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapist", "no ID", dto.getTherapist());
        ValidationRules.rejectIfNullOrIdNotExistent(e, "timeBlock", "no ID", dto.getTimeBlock());

        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }
}

