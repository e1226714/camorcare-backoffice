package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.TherapyEvent;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.TherapyEvent} object.
 */
public class TherapyEventValidator extends EntityValidator<TherapyEvent> {

    public TherapyEventValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(TherapyEvent toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(TherapyEvent toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, TherapyEvent.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return TherapyEvent.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        TherapyEvent dto = (TherapyEvent) o;

        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapyPlan", "no ID", dto.getTherapyPlan());
        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapyType", "no ID", dto.getTherapyType());
        ValidationRules.rejectIfNull(e, "therapyMode", "is null", dto.getTherapyMode());

        /**
         * If this therapy event already has an referenced time block or a therapy room,
         * we insist on having the therapist set.
         */
        if (dto.getTherapist() != null || dto.getTherapyTimeBlock() != null || dto.getTherapyRoom() != null) {
            ValidationRules.rejectIfNullOrIdNotExistent(e, "therapist", "no ID", dto.getTherapist());
        }

        /**
         * If a therapy time block is set, it has to have an ID.
         */
        if (dto.getTherapyTimeBlock() != null) {
            ValidationRules.rejectIfNullOrIdNotExistent(e, "therapyTimeBlock", "no ID", dto.getTherapyTimeBlock());
            ValidationRules.rejectIfNull(e, "therapyTime", "is null", dto.getTherapyTime());
        }

        /**
         * If a therapy room is set, it has to have an ID.
         */
        if (dto.getTherapyRoom() != null) {
            ValidationRules.rejectIfNullOrIdNotExistent(e, "therapyRoom", "no ID", dto.getTherapyRoom());
        }

        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
