package ch.camorcare.backoffice.util.validator.entities;


import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.TherapyType} object.
 */
public class TherapyTypeValidator extends EntityValidator<TherapyType> {

    public TherapyTypeValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(TherapyType toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(TherapyType toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, TherapyType.class.getName());
        this.validate(toValidate, err);
        return err;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return TherapyType.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        TherapyType dto = (TherapyType) o;

        ValidationRules.rejectIfLengthOutOfBounds(e, "name", "string length is out of bounds", dto.getName(), 1, 100);
        ValidationRules.rejectIfNull(e, "individualTherapyDuration", "is null", dto.getIndividualTherapyDuration());
        ValidationRules.rejectIfNull(e, "groupTherapyDuration", "is null", dto.getGroupTherapyDuration());
        ValidationRules.rejectIfNull(e, "minGroupSize", "is null", dto.getMinGroupSize());
        ValidationRules.rejectIfNull(e, "maxGroupSize", "is null", dto.getMaxGroupSize());
        if (dto.isSuitableForIndividuals()) {
            ValidationRules.rejectIfLeqThanZero(e, "individualTherapyDuration", "is less or equal 0", dto.getIndividualTherapyDuration());
        }
        if (dto.isSuitableForGroups()) {
            ValidationRules.rejectIfLeqThanZero(e, "groupTherapyDuration", "is less or equal 0", dto.getGroupTherapyDuration());
            ValidationRules.rejectIfLeqThanZero(e, "minGroupSize", "is less or equal 0", dto.getMinGroupSize());
            ValidationRules.rejectIfLeqThanZero(e, "maxGroupSize", "is less or equal 0", dto.getMaxGroupSize());
            ValidationRules.rejectIfNumbersHaveWrongOrder(e, "maxGroupSize", "is less than minGroupSize", dto.getMinGroupSize(), dto.getMaxGroupSize());
        }
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
