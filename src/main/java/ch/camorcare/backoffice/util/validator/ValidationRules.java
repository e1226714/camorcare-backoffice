package ch.camorcare.backoffice.util.validator;

import ch.camorcare.backoffice.entities.BasicDTO;
import org.joda.time.DateTime;
import org.springframework.validation.Errors;

/**
 * Rules often used in Validators
 */
public class ValidationRules {

    /**
     * Reject if o is null
     *
     * @param e
     * @param rejectMessage
     * @param o
     */
    public static void rejectIfNull(Errors e, String field, String rejectMessage, Object o) {
        if (o == null) {
            e.rejectValue(field, "1", rejectMessage);
        }
    }

    /**
     * Reject if string is null or length is out of bounds
     *
     * @param e
     * @param rejectMessage
     * @param s
     * @param minLength
     * @param maxLength
     */
    public static void rejectIfLengthOutOfBounds(Errors e, String field, String rejectMessage, String s, int minLength, int maxLength) {
        if (s == null || s.length() < minLength || s.length() > maxLength) {
            e.rejectValue(field, "2", rejectMessage);
        }
    }

    /**
     * Reject if young Date is older than old Date
     *
     * @param e
     * @param rejectMessage
     * @param oldDate
     * @param youngDate
     */
    public static void rejectIfDateIsNotYoungerOrNull(Errors e, String field, String rejectMessage, DateTime oldDate, DateTime youngDate) {
        if (youngDate != null && (oldDate == null || oldDate.getMillis() > youngDate.getMillis())) {
            e.rejectValue(field, "3", rejectMessage);
        }
    }

    /**
     * Reject if numberLow is greater than numberHigh
     *
     * @param e
     * @param rejectMessage
     * @param numberLow
     * @param numberHigh
     */
    public static void rejectIfNumbersHaveWrongOrder(Errors e, String field, String rejectMessage, Integer numberLow, Integer numberHigh) {
        if (numberHigh != null && (numberLow == null || numberLow > numberHigh)) {
            e.rejectValue(field, "4", rejectMessage);
        }
    }

    /**
     * Reject if Integer is null or <=0
     *
     * @param e
     * @param rejectMessage
     * @param i
     */
    public static void rejectIfLeqThanZero(Errors e, String field, String rejectMessage, Integer i) {
        if (i == null || i <= 0) {
            e.rejectValue(field, "5", rejectMessage);
        }
    }

    /**
     * Reject if the dto is null, or the id is missing
     *
     * @param e
     * @param rejectMessage
     * @param dto
     */
    public static void rejectIfNullOrIdNotExistent(Errors e, String field, String rejectMessage, BasicDTO dto) {
        if (dto == null || dto.getId() == null) {
            e.rejectValue(field, "6", rejectMessage);
        }
    }

    /**
     * Reject if number is out of given range
     * <p/>
     * lowerBound <= number <= upperBound
     *
     * @param e
     * @param rejectMessage
     * @param number
     * @param lowerBound
     */
    public static void rejectIfNumberOutOfBounds(Errors e, String field, String rejectMessage, Integer number, Integer lowerBound, Integer upperBound) {
        if (number != null && (number < lowerBound || number > upperBound)) {
            e.rejectValue(field, "6", rejectMessage);
        }
    }
}
