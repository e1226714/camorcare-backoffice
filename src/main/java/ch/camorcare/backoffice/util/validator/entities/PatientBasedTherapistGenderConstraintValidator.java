package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint} object.
 */
public class PatientBasedTherapistGenderConstraintValidator extends EntityValidator<PatientBasedTherapistGenderConstraint> {

    public PatientBasedTherapistGenderConstraintValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(PatientBasedTherapistGenderConstraint toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(PatientBasedTherapistGenderConstraint toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Therapy.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return PatientBasedTherapistGenderConstraint.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        PatientBasedTherapistGenderConstraint dto = (PatientBasedTherapistGenderConstraint) o;

        ValidationRules.rejectIfNullOrIdNotExistent(e, "patient", "no ID", dto.getPatient());
        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapyType", "no ID", dto.getTherapyType());
        ValidationRules.rejectIfNull(e, "gender", "is null", dto.getGender());

        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }
}
