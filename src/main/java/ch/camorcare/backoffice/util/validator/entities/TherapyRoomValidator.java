package ch.camorcare.backoffice.util.validator.entities;


import ch.camorcare.backoffice.entities.TherapyRoom;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.TherapyRoom} object.
 */
public class TherapyRoomValidator extends EntityValidator<TherapyRoom> {

    public TherapyRoomValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(TherapyRoom toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(TherapyRoom toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, TherapyRoom.class.getName());
        this.validate(toValidate, err);
        return err;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return TherapyRoom.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        TherapyRoom dto = (TherapyRoom) o;

        ValidationRules.rejectIfLengthOutOfBounds(e, "name", "string length is out of bounds", dto.getName(), 1, 100);
        ValidationRules.rejectIfLeqThanZero(e, "capacity", "less or equal zero", dto.getCapacity());
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
