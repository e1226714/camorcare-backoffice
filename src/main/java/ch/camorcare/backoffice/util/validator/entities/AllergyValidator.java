package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.Allergy;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates an {@link ch.camorcare.backoffice.entities.Allergy} object.
 */
public class AllergyValidator extends EntityValidator<Allergy> {

    public AllergyValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(Allergy toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(Allergy toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Allergy.class.getName());
        this.validate(toValidate, err);
        return err;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return Allergy.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        Allergy dto = (Allergy) o;
        ValidationRules.rejectIfLengthOutOfBounds(e, "name", "string length out of bounds", dto.getName(), 1, 250);

        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
