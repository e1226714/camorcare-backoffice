package ch.camorcare.backoffice.util.validator.entities;


import ch.camorcare.backoffice.entities.DutyCategory;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.DutyCategory} object.
 */
public class DutyCategoryValidator extends EntityValidator<DutyCategory> {

    public DutyCategoryValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(DutyCategory toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(DutyCategory toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, DutyCategory.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return DutyCategory.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        DutyCategory dto = (DutyCategory) o;
        ValidationRules.rejectIfLengthOutOfBounds(e, "name", "string length is out of bounds", dto.getName(), 1, 50);
        ValidationRules.rejectIfLeqThanZero(e, "duration", "less or equal zero", dto.getDuration());
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }
}
