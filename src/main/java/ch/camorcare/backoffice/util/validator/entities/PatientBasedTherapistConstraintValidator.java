package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint} object.
 */
public class PatientBasedTherapistConstraintValidator extends EntityValidator<PatientBasedTherapistConstraint> {

    public PatientBasedTherapistConstraintValidator(Validator basicValidator) {
		super(basicValidator);
	}

	@Override
	public void validate(PatientBasedTherapistConstraint toValidate) throws ValidationException {
		Errors errors = this.getErrors(toValidate);
		if (errors.getErrorCount() > 0) {
			throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
		}
	}

	@Override
	public Errors getErrors(PatientBasedTherapistConstraint toValidate) {
		Map<String, String> map = new HashMap<>();
		MapBindingResult err = new MapBindingResult(map, PatientBasedTherapistConstraint.class.getName());

		this.validate(toValidate, err);

		return err;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return PatientBasedTherapistConstraint.class.equals(clazz);
	}

    @Override
    public void validate(Object o, Errors e) {
        PatientBasedTherapistConstraint dto = (PatientBasedTherapistConstraint) o;
        ValidationRules.rejectIfNullOrIdNotExistent(e, "patient", "no ID", dto.getPatient());
        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapyType", "no ID", dto.getTherapyType());
        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapist", "no ID", dto.getTherapist());
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }
}
