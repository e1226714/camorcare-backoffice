package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.BasicDTO;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.BasicDTO} object.
 */
public class BasicValidator implements Validator {

    public boolean supports(Class clazz) {
        return BasicDTO.class.isAssignableFrom(clazz);
    }

    public void validate(Object obj, Errors e) {
        BasicDTO dto = (BasicDTO) obj;
        ValidationRules.rejectIfDateIsNotYoungerOrNull(e, "timeModified", "ModifyOlderThanCreate", dto.getTimeCreated(), dto.getTimeModified());
        ValidationRules.rejectIfDateIsNotYoungerOrNull(e, "timeDeleted", "DeleteOlderThanCreate", dto.getTimeCreated(), dto.getTimeDeleted());
        ValidationRules.rejectIfDateIsNotYoungerOrNull(e, "timeDeleted", "DeleteOlderThanModify", dto.getTimeModified(), dto.getTimeDeleted());
    }

}
