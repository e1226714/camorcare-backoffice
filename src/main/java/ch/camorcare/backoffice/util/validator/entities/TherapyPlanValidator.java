package ch.camorcare.backoffice.util.validator.entities;


import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.TherapyPlan} object.
 */
public class TherapyPlanValidator extends EntityValidator<TherapyPlan> {

    public TherapyPlanValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(TherapyPlan toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(TherapyPlan toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, TherapyPlan.class.getName());
        this.validate(toValidate, err);
        return err;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return TherapyPlan.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        TherapyPlan dto = (TherapyPlan) o;

        ValidationRules.rejectIfNull(e, "dateFrom", "is null", dto.getDateFrom());
        ValidationRules.rejectIfNull(e, "dateUntil", "is null", dto.getDateUntil());
        ValidationRules.rejectIfDateIsNotYoungerOrNull(e, "dateUntil", "dateUntil is older than dateFrom", dto.getDateFrom(), dto.getDateUntil());

        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
