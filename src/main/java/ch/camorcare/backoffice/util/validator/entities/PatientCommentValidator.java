package ch.camorcare.backoffice.util.validator.entities;


import ch.camorcare.backoffice.entities.PatientComment;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.PatientComment} object.
 */
public class PatientCommentValidator extends EntityValidator<PatientComment> {

    public PatientCommentValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(PatientComment toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(PatientComment toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, PatientComment.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return PatientComment.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        PatientComment dto = (PatientComment) o;
        ValidationRules.rejectIfLengthOutOfBounds(e, "content", "string length out of bounds", dto.getContent(), 1, 1000);
        ValidationRules.rejectIfNullOrIdNotExistent(e, "patient", "no ID", dto.getPatient());
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
