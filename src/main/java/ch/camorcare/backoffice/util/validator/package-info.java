package ch.camorcare.backoffice.util.validator;

/**
 * The validator package contains a series of validator tools used to check whether or not a given entity/DTO is able
 * to be further used by any layer. This ensures that a minimal amount of code must be written to check the validity of
 * any single entity and does not require complicated checks on the part of the developer.
 * <p/>
 * In order to be specific with error reporting an exception is provided to report any errors reported by these tools.
 * There is also a message formatter to make error reporting even more comfortable and easier to read for the developer.
 */