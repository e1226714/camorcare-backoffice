package ch.camorcare.backoffice.util.validator.entities;


import ch.camorcare.backoffice.entities.Address;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates an {@link ch.camorcare.backoffice.entities.Address} object.
 */
public class AddressValidator extends EntityValidator<Address> {

    public AddressValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(Address toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(Address toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Address.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Address.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        Address dto = (Address) o;
        ValidationRules.rejectIfLengthOutOfBounds(e, "street", "string length out of bounds", dto.getStreet(), 1, 150);
        ValidationRules.rejectIfLengthOutOfBounds(e, "zip", "string length out of bounds", dto.getZip(), 1, 10);
        ValidationRules.rejectIfLengthOutOfBounds(e, "city", "string length out of bounds", dto.getCity(), 1, 50);
        ValidationRules.rejectIfLengthOutOfBounds(e, "countryCode", "string length out of bounds", dto.getCountryCode(), 2, 2);
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }
}
