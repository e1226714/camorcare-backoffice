package ch.camorcare.backoffice.util.validator.entities;


import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.Patient} object.
 */
public class PatientValidator extends EntityValidator<Patient> {

    public PatientValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(Patient toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(Patient toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Patient.class.getName());
        this.validate(toValidate, err);
        return err;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return Patient.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        Patient dto = (Patient) o;
        ValidationRules.rejectIfLengthOutOfBounds(e, "firstName", "string length out of bounds", dto.getFirstName(), 1, 100);
        ValidationRules.rejectIfLengthOutOfBounds(e, "lastName", "string length out of bounds", dto.getLastName(), 1, 100);
        ValidationRules.rejectIfNull(e, "gender", "is null", dto.getGender());
        ValidationRules.rejectIfLengthOutOfBounds(e, "socialSecurityNumber", "string length out of bounds", dto.getSocialSecurityNumber(), 5, 20);
        ValidationRules.rejectIfNull(e, "birthdate", "is null", dto.getBirthdate());
        ValidationRules.rejectIfLengthOutOfBounds(e, "phone", "string length out of bounds", dto.getPhone(), 5, 30);
        ValidationRules.rejectIfLengthOutOfBounds(e, "email", "string length out of bounds", dto.getEmail(), 5, 200);
        ValidationRules.rejectIfNullOrIdNotExistent(e, "address", "no ID", dto.getAddress());
        ValidationRules.rejectIfLengthOutOfBounds(e, "religion", "string length out of bounds", dto.getReligion(), 1, 100);
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }
}
