package ch.camorcare.backoffice.util.validator;

/**
 * An exception to be thrown by a Validator.
 */
public class ValidationException extends Exception {

    private static final long serialVersionUID = 2395432254670363809L;

    public ValidationException(String message) {
        super(message);
    }
}
