package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.TimeBlock} object.
 */
public class TimeBlockValidator extends EntityValidator<TimeBlock> {

    public TimeBlockValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(TimeBlock toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(TimeBlock toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, TimeBlock.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return TimeBlock.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        TimeBlock dto = (TimeBlock) o;

        ValidationRules.rejectIfNull(e, "day", "is null", dto.getDay());
        ValidationRules.rejectIfNull(e, "timeStart", "is null", dto.getTimeStart());

        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }
}
