package ch.camorcare.backoffice.util.validator.entities;


import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates a {@link ch.camorcare.backoffice.entities.Therapy} object.
 */
public class TherapyValidator extends EntityValidator<Therapy> {

    public TherapyValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(Therapy toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(Therapy toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Therapy.class.getName());
        this.validate(toValidate, err);
        return err;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return Therapy.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        Therapy dto = (Therapy) o;
        ValidationRules.rejectIfNullOrIdNotExistent(e, "patient", "no ID", dto.getPatient());
        ValidationRules.rejectIfNull(e, "dateSickLeave", "is null", dto.getDateSickLeave());
        ValidationRules.rejectIfNull(e, "dateFrom", "is null", dto.getDateFrom());
        ValidationRules.rejectIfDateIsNotYoungerOrNull(e, "dateFrom", "dateFrom is older than dateSickLeave", dto.getDateSickLeave(), dto.getDateFrom());
        ValidationRules.rejectIfDateIsNotYoungerOrNull(e, "dateUntil", "dateUntil is older than dateFrom", dto.getDateFrom(), dto.getDateUntil());
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
