package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.Invoice;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import ch.camorcare.backoffice.util.validator.ValidationRules;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates an {@link ch.camorcare.backoffice.entities.Invoice} object.
 */
public class InvoiceValidator extends EntityValidator<Invoice> {

    public InvoiceValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(Invoice toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(Invoice toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Invoice.class.getName());
        this.validate(toValidate, err);
        return err;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Invoice.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        Invoice dto = (Invoice) o;
        ValidationRules.rejectIfNullOrIdNotExistent(e, "address", "no ID", dto.getAddress());
        ValidationRules.rejectIfNullOrIdNotExistent(e, "therapy", "no ID", dto.getTherapy());
        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
