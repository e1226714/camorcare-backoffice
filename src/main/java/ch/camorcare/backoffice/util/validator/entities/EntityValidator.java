package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.BasicDTO;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Provides an abstract class to validate any entity.
 */
public abstract class EntityValidator<Entity> implements Validator {
    protected final Validator basicValidator;

    public EntityValidator(Validator basicValidator) {
        if (basicValidator == null) {
            throw new IllegalArgumentException("The supplied [Validator] is " +
                    "required and must not be null.");
        }
        if (!basicValidator.supports(BasicDTO.class)) {
            throw new IllegalArgumentException("The supplied [Validator] must " +
                    "support the validation of [BasicDTO] instances.");
        }
        this.basicValidator = basicValidator;
    }

    public abstract void validate(Entity toValidate) throws ValidationException;

    public abstract Errors getErrors(Entity toValidate);

}
