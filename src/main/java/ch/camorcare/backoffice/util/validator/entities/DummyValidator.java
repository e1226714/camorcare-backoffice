package ch.camorcare.backoffice.util.validator.entities;

import ch.camorcare.backoffice.entities.BasicDTO;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.ValidationMessageFormatter;
import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates all entities (no validation faults)
 */
public class DummyValidator extends EntityValidator<BasicDTO> {

    public DummyValidator(Validator basicValidator) {
        super(basicValidator);
    }

    @Override
    public void validate(BasicDTO toValidate) throws ValidationException {
        Errors errors = this.getErrors(toValidate);
        if (errors.getErrorCount() > 0) {
            throw new ValidationException(ValidationMessageFormatter.formatErrorsObject(errors));
        }
    }

    @Override
    public Errors getErrors(BasicDTO toValidate) {
        Map<String, String> map = new HashMap<>();
        MapBindingResult err = new MapBindingResult(map, Therapy.class.getName());
        this.validate(toValidate, err);
        return err;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return BasicDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors e) {
        BasicDTO dto = (BasicDTO) o;

        try {
            e.pushNestedPath("basic");
            ValidationUtils.invokeValidator(this.basicValidator, dto, e);
        } finally {
            e.popNestedPath();
        }
    }

}
