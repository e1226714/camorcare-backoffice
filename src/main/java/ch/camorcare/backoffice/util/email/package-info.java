package ch.camorcare.backoffice.util.email;

/**
 * The {@link ch.camorcare.backoffice.util.email.EmailService} class provides methods to send either a TherapyPlan (a
 * weekly agenda) or an invoice. The service is based on JavaMail and further works with Spring's dependency injection.
 */