package ch.camorcare.backoffice.util.email;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * Provides the necessary utilities to send an email with or without an attachment from a pre-specified email address.
 */
public class EmailService {

    // Mail objects
    private JavaMailSender mailSender;
    // Provides logging functionality
    private Logger log = LogManager.getLogger(EmailService.class.getName());



    /**
     * @param mailSender The JavaMailSender to be used by the EmailService
     */
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * Sends an email with an attachment using the email address stored in the database for a specific person. The
     * attachment is the generated PDF for the weekly plan.
     * <p/>
     * The person can be a {@link ch.camorcare.backoffice.entities.Therapist} or a {@link
     * ch.camorcare.backoffice.entities.Patient}
     *
     * @param email      The email address that will receive the attachment
     * @param weekNumber The calendar week number for the weekly plan
     * @param filePath   The path with the filename of the PDF to be attached and sent
     * @throws MessagingException
     */
    public void sendTherapyPlan(String email, Integer weekNumber, String filePath) throws MessagingException {
        MimeMessage msg = mailSender.createMimeMessage();
        // Use the true flag to indicate a multipart message is needed
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);

        log.debug("Setting from email address 'no-reply@camorcare.ch' to email");
        helper.setFrom("no-reply@camorcare.ch");

        log.debug("Setting receiver's email '" + email + "'");
        helper.setTo(email);

        log.debug("Setting message body and email subject");
        helper.setText("Anbei des aktuellen Wochenplans.");
        helper.setSubject("Wochentlicher Plan für " + weekNumber + ". Kalenderwoche");

        log.debug("Getting attachment from filepath");
        FileSystemResource fileSystemResource = new FileSystemResource(new File(filePath));

        log.debug("Adding attachment to message");
        helper.addAttachment("Wochenplan_" + weekNumber + ".pdf", fileSystemResource);

        log.debug("Sending message");
        this.mailSender.send(msg);
    }

    /**
     * Sends an email with an attachment using an email address stored in the database for a specific {@link
     * ch.camorcare.backoffice.entities.Patient}. The attachment is the generated PDF for an invoice.
     * <p/>
     * This method is only being implemented due to requirements detailed in the project wiki. In reality this method
     * would never be used as all invoices must be printed and sent as a hardcopy. In order to send electronic
     * invoices,
     * digital signatures using an approved certificate authority must be used.
     *
     * @param email         The email address that will receive the attachment
     * @param invoiceNumber The invoice number of a specific invoice
     * @param filePath      The path with the filename of the PDF to be attached and sent
     * @throws MessagingException
     */
    public void sendInvoice(String email, Integer invoiceNumber, String filePath) throws MessagingException {
        MimeMessage msg = mailSender.createMimeMessage();
        // Use the true flag to indicate a multipart message is needed
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);

        log.debug("Setting from email address 'no-reply@camorcare.ch' to email");
        helper.setFrom("no-reply@camorcare.ch");

        log.debug("Setting receiver's email '" + email + "'");
        helper.setTo(email);

        log.debug("Setting message body and email subject");
        helper.setText("Anbei der Rechnung #" + invoiceNumber + ".");
        helper.setSubject("Rechnung #" + invoiceNumber);

        log.debug("Getting attachment from filepath");
        FileSystemResource fileSystemResource = new FileSystemResource(new File(filePath));

        log.debug("Adding attachment to message");
        helper.addAttachment("Rechnung_" + invoiceNumber, fileSystemResource);

        log.debug("Sending message");
        this.mailSender.send(msg);
    }
}
