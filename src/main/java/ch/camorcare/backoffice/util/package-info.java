package ch.camorcare.backoffice.util;

/**
 * The util package contains a number of tools that are used throughout the program to perform specialized tasks that
 * are not explicitly part of the 3-layer architecture. Please refer to each package's package-info file for more
 * information on each package itself.
 */