package ch.camorcare.backoffice.util.matchers;

import ch.camorcare.backoffice.entities.TherapyType;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;

/**
 * Matcher to match therapyType ID
 */
public class TherapyTypeIdEqualsMatcher extends TypeSafeMatcher<TherapyType> {
    private final int expectedValue;

    public TherapyTypeIdEqualsMatcher(int expectedValue) {
        this.expectedValue = expectedValue;
    }

    @Factory
    public static TherapyTypeIdEqualsMatcher therapyTypeIdEquals(int id) {
        return new TherapyTypeIdEqualsMatcher(id);

    }

    @Override
    public void describeTo(Description description) {
        description.appendText("not a number");
    }

    @Override
    protected boolean matchesSafely(TherapyType dto) {
        return dto.getId() == expectedValue;
    }

}