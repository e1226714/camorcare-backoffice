package ch.camorcare.backoffice.util.matchers;

import ch.camorcare.backoffice.entities.Patient;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;

/**
 * Matcher to match patient ID
 */
public class PatientIdEqualsMatcher extends TypeSafeMatcher<Patient> {
    private final int expectedValue;

    public PatientIdEqualsMatcher(int expectedValue) {
        this.expectedValue = expectedValue;
    }

    @Factory
    public static PatientIdEqualsMatcher patientIdEquals(int id) {
        return new PatientIdEqualsMatcher(id);

    }

    @Override
    public void describeTo(Description description) {
        description.appendText("not a number");
    }

    @Override
    protected boolean matchesSafely(Patient dto) {
        return dto.getId() == expectedValue;
    }

}