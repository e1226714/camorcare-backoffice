package ch.camorcare.backoffice.util.matchers;

import ch.camorcare.backoffice.entities.TherapyEvent;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;

/**
 * Matcher to match therapyEvent ID
 */
public class TherapyEventIdEqualsMatcher extends TypeSafeMatcher<TherapyEvent> {
    private final int expectedValue;

    public TherapyEventIdEqualsMatcher(int expectedValue) {
        this.expectedValue = expectedValue;
    }

    @Factory
    public static TherapyEventIdEqualsMatcher therapyEventIdEquals(int id) {
        return new TherapyEventIdEqualsMatcher(id);

    }

    @Override
    public void describeTo(Description description) {
        description.appendText("not a number");
    }

    @Override
    protected boolean matchesSafely(TherapyEvent dto) {
        return dto.getId() == expectedValue;
    }

}