package ch.camorcare.backoffice.util.matchers;

/**
 * Provides various matching utilities to accurately compare entities within the program.
 */