package ch.camorcare.backoffice.util.converter;

/**
 * The {@link ch.camorcare.backoffice.util.converter.DateTimeConverter} provides a series of methods to allow the
 * developer to easily convert between date formats relevant for the user interface, the persistence layer and other
 * places in the program as needed.
 */