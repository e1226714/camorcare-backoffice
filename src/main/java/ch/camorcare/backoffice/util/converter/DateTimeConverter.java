package ch.camorcare.backoffice.util.converter;

import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TimeBlock;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Locale;

/**
 * Class provides methods for conversion between SQL and JODA time formats.
 */
public class DateTimeConverter {

    /**
     * Converts a {@link org.joda.time.DateTime} to a {@link java.sql.Time} object.
     *
     * @param dateTime The DateTime to be converted
     * @return An SQL Time representation of the passed parameter
     */
    public static Time dateTimeToSqlTime(DateTime dateTime) {
        if (dateTime != null) {
            return new Time(dateTime.toDate().getTime());
        }
        else {
            return null;
        }
    }

    /**
     * Converts a {@link org.joda.time.DateTime} to an {@link java.sql.Date} object.
     *
     * @param dateTime The DateTime to be converted
     * @return An SQL Date representation of the passed parameter
     */
    public static Date dateTimeToSqlDate(DateTime dateTime) {
        if (dateTime != null) {
            return new java.sql.Date(dateTime.toDate().getTime());
        }
        else {
            return null;
        }
    }

    /**
     * Converts a {@link java.sql.Time} object to a {@link org.joda.time.DateTime} object.
     *
     * @param time The Time object to be converted
     * @return A DateTime representation of the passed parameter
     */
    public static DateTime sqlTimeToDateTime(Time time) {
        if (time != null) {
            DateTime temp = new DateTime();
            temp = temp.withTimeAtStartOfDay().plus(time.getTime());
            return temp.plusHours(1); // time zone hack
        }

        return null;
    }

    /**
     * Converts a {@link java.sql.Date} object to a {@link org.joda.time.DateTime} object.
     *
     * @param date The Date object to be converted
     * @return A DateTime representation of the passed parameter
     */
    public static DateTime sqlDateToDateTime(Date date) {
        if (date != null) {
            return LocalDate.fromDateFields(date).toDateTimeAtStartOfDay();
        }
        else {
            return null;
        }
    }

    /**
     * Converts a {@link org.joda.time.DateTime} to a {@link java.sql.Timestamp} object.
     *
     * @param dateTime The DateTime to be converted
     * @return An SQL Timestamp representation of the passed parameter
     */
    public static Timestamp dateTimeToSqlTimestamp(DateTime dateTime) {
        if (dateTime != null) {
            return new Timestamp(dateTime.getMillis());
        }
        else {
            return null;
        }
    }

    /**
     * Converts a {@link java.sql.Timestamp} object to a {@link org.joda.time.DateTime} object.
     *
     * @param timestamp The Timestamp object to be converted
     * @return A DateTime representation of the passed parameter
     */
    public static DateTime timeStampToDateTime(Timestamp timestamp) {
        if (timestamp != null) {
            return new DateTime(timestamp);
        }
        else {
            return null;
        }
    }

    /**
     * Provides a String representation of a {@link org.joda.time.DateTime} object's date information in the following
     * format: dd.MM.YYYY
     *
     * @param dateTime The DateTime to be converted
     * @return The formatted String
     */
    public static String dateTimeToString(DateTime dateTime) {
        if (dateTime != null) {
            DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.yyyy");
            return fmt.print(dateTime);
        }
        else {
            return null;
        }
    }

    /**
     * Converts a {@link org.joda.time.DateTime} object's time informaton into the following format: HH:mm
     *
     * @param dateTime The DateTime to be converted
     * @return The formatted String
     */
    public static String timeToString(DateTime dateTime) {
        if (dateTime != null) {
            DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");
            return fmt.print(dateTime);
        }
        else {
            return null;
        }
    }

    /**
     * Converts a {@link org.joda.time.DateTime} object to {@link java.time.LocalDate}.
     *
     * @param dateTime The DateTime to be converted
     * @return The LocalDate representation of the parameter
     */
    public static java.time.LocalDate dateTimeToLocalDate(DateTime dateTime) {
        if (dateTime != null) {
            DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.yyyy");
            java.time.format.DateTimeFormatter fmt2 =
                    java.time.format.DateTimeFormatter.ofPattern("dd.MM.yyyy");
            return java.time.LocalDate.parse(fmt.print(dateTime), fmt2);
        }
        else {
            return null;
        }
    }

    /**
     * Converts a {@link java.time.LocalDate} object to a {@link org.joda.time.DateTime}.
     *
     * @param localDate The LocalDate to be converted
     * @return The DateTime representation of the parameter
     */
    public static DateTime localDateToDateTime(java.time.LocalDate localDate) {
        if (localDate != null) {
            return new DateTime(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth(), 0, 0);
        }
        else {
            return null;
        }
    }

    /**
     * Creates a {@link org.joda.time.DateTime} from the year and week given in the therapyPlan and combines it with the
     * Day and time of day in the time block
     *
     * @param therapyPlan Year and week
     * @param timeBlock   Day and time of day
     * @return The generated DateTime
     */
    public static DateTime dateTimeFromTherapyPlanAndTimeBlock(TherapyPlan therapyPlan, TimeBlock timeBlock) {

        final Calendar planCalendar = therapyPlan.getDateFrom().toCalendar(Locale.getDefault());
        final Calendar retVal = Calendar.getInstance();

        retVal.set(Calendar.YEAR, planCalendar.get(Calendar.YEAR));
        retVal.set(Calendar.WEEK_OF_YEAR, planCalendar.get(Calendar.WEEK_OF_YEAR));
        switch (timeBlock.getDay()) {
            case MONDAY:
                retVal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                break;
            case TUESDAY:
                retVal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
                break;
            case WEDNESDAY:
                retVal.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
                break;
            case THURSDAY:
                retVal.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
                break;
            case FRIDAY:
                retVal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
                break;
            case SATURDAY:
                retVal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
                break;
            case SUNDAY:
                retVal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                break;
        }

        return new DateTime(retVal).withMillisOfDay(timeBlock.getTimeStart().getMillisOfDay());
    }
}
