package ch.camorcare.backoffice.util.pdftools;

import ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate;
import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * This class generates a PDF document, that contains the weekly schedule for a patient,
 * a therapist or a therapy room.
 * A PDF always contains the plan of a whole week.
 */
public class PdfPlanGenerator {

    private static Logger log = LogManager.getLogger(PdfPlanGenerator.class.getName());

    /**
     * This method generates a PDF file representing a weekly agenda with all events passed to the plan generator.
     *
     * @param title The title printed at the top of the page
     * @param dates The individual events printed in the agenda
     * @param outfile The output file path
     * @throws PdfException
     */
    public static void generatePDF(String title, List<PdfPlanDate> dates, String outfile) throws PdfException {
        try {
            doWork(title, dates, outfile);
        } catch (IOException | COSVisitorException e) {
            log.error(e.getMessage());
            throw new PdfException(e.getMessage());
        }
    }

    private static void doWork(String title, List<PdfPlanDate> dates, String outfile) throws IOException, COSVisitorException {
        // the document
        PDDocument doc = null;
        try {
            doc = new PDDocument();

            PDFont font = PDType1Font.HELVETICA;
            PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
            page.setRotation(90);
            doc.addPage(page);
            PDRectangle pageSize = page.getMediaBox();
            float pageWidth = pageSize.getWidth();
            float fontSize = 9;
            float headlineSize = 25;
            //float stringWidth = font.getStringWidth( message )*fontSize/1000f;
            float startX = 40;
            float startY = 540;
            PDPageContentStream contentStream = new PDPageContentStream(doc, page, false, false);
            // add the rotation using the current transformation matrix
            // including a translation of pageWidth to use the lower left corner as 0,0 reference
            contentStream.concatenate2CTM(0, 1, -1, 0, pageWidth, 0);
            contentStream.setFont(font, headlineSize);

            // headline
            contentStream.beginText();
            contentStream.moveTextPositionByAmount(startX, startY);
            contentStream.drawString(title);

            // caption
            contentStream.setFont(font, fontSize);
            contentStream.moveTextPositionByAmount(0, -68);
            contentStream.drawString("06:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("07:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("08:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("09:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("10:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("11:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("12:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("13:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("14:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("15:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("16:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("17:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("18:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("19:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("20:00");
            contentStream.moveTextPositionByAmount(0, -29);
            contentStream.drawString("21:00");

            contentStream.moveTextPositionByAmount(90, 464);
            contentStream.drawString("Montag");
            contentStream.moveTextPositionByAmount(144, 0);
            contentStream.drawString("Dienstag");
            contentStream.moveTextPositionByAmount(144, 0);
            contentStream.drawString("Mittwoch");
            contentStream.moveTextPositionByAmount(134, 0);
            contentStream.drawString("Donnerstag");
            contentStream.moveTextPositionByAmount(154, 0);
            contentStream.drawString("Freitag");

            contentStream.endText();

            // frame
            contentStream.setStrokingColor(0.9);
            contentStream.drawLine(startX + 40, startY - 50, startX + 760, startY - 50);
            contentStream.drawLine(startX + 40, startY - 79, startX + 760, startY - 79);
            contentStream.drawLine(startX + 40, startY - 108, startX + 760, startY - 108);
            contentStream.drawLine(startX + 40, startY - 137, startX + 760, startY - 137);
            contentStream.drawLine(startX + 40, startY - 166, startX + 760, startY - 166);
            contentStream.drawLine(startX + 40, startY - 195, startX + 760, startY - 195);
            contentStream.drawLine(startX + 40, startY - 224, startX + 760, startY - 224);
            contentStream.drawLine(startX + 40, startY - 253, startX + 760, startY - 253);
            contentStream.drawLine(startX + 40, startY - 282, startX + 760, startY - 282);
            contentStream.drawLine(startX + 40, startY - 311, startX + 760, startY - 311);
            contentStream.drawLine(startX + 40, startY - 340, startX + 760, startY - 340);
            contentStream.drawLine(startX + 40, startY - 369, startX + 760, startY - 369);
            contentStream.drawLine(startX + 40, startY - 398, startX + 760, startY - 398);
            contentStream.drawLine(startX + 40, startY - 427, startX + 760, startY - 427);
            contentStream.drawLine(startX + 40, startY - 456, startX + 760, startY - 456);
            contentStream.drawLine(startX + 40, startY - 485, startX + 760, startY - 485);
            contentStream.drawLine(startX + 40, startY - 514, startX + 760, startY - 514);

            contentStream.setStrokingColor(0);
            contentStream.drawLine(startX + 40, startY - 50, startX + 40, startY - 514);
            contentStream.drawLine(startX + 184, startY - 50, startX + 184, startY - 514);
            contentStream.drawLine(startX + 328, startY - 50, startX + 328, startY - 514);
            contentStream.drawLine(startX + 472, startY - 50, startX + 472, startY - 514);
            contentStream.drawLine(startX + 616, startY - 50, startX + 616, startY - 514);
            contentStream.drawLine(startX + 760, startY - 50, startX + 760, startY - 514);

            // content
            Iterator<PdfPlanDate> it = dates.iterator();

            while (it.hasNext()) {
                PdfPlanDate date = it.next();

                float timeFrom = date.getTimeFrom() - 6;
                float timeTo = date.getTimeTo() - 6;
                float textPos = (timeFrom + timeTo) / 2;

                contentStream.beginText();
                contentStream.moveTextPositionByAmount(startX + 47 + (date.getDay() * 144), startY - 54 - (textPos * 29));
                contentStream.drawString(date.getDateName());
                contentStream.endText();

                contentStream.drawLine(startX + 40 + (date.getDay() * 144), startY - 50 - (timeFrom * 29), startX + 184 + (date.getDay() * 144), startY - 50 - (timeFrom * 29));
                contentStream.drawLine(startX + 40 + (date.getDay() * 144), startY - 50 - (timeTo * 29), startX + 184 + (date.getDay() * 144), startY - 50 - (timeTo * 29));

            }

            contentStream.close();

            doc.save(outfile);

        } finally {
            if (doc != null) {
                doc.close();
            }
        }

    }

}
