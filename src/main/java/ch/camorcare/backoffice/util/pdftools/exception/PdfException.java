package ch.camorcare.backoffice.util.pdftools.exception;

/**
 * Exception to by thrown whenever using a class from PDF package.
 */
public class PdfException extends Exception {
    public PdfException(String message) {
        super(message);
    }
}
