package ch.camorcare.backoffice.util.pdftools.entity;

/**
 * The class contains all information necessary to draw a {@link ch.camorcare.backoffice.entities.TherapyEvent} in
 * PdfPlanGenerator after being converted using {@link ch.camorcare.backoffice.util.pdftools.PdfPlanDateConverter}.
 */
public class PdfPlanDate {

    // 0 = Monday, 1 = Tuesday,..., 4 = Friday
    private int day;
    // time between 6:00 and 22:00
    private float timeFrom;
    private float timeTo;
    // The title to be shown within the PdfPlanDate in the PDF file
    private String dateName;

    public PdfPlanDate() {

    }

    /**
     * Creates a new PdfPlanDate.
     *
     * @param day      day of the week. 0 = Monday, 1 = Tuesday, 2 = Wednesday, 3 = Thursday, 4 = Friday
     * @param timeFrom the time when the date starts. Number between 6 and 22.
     * @param timeTo   the time when the date ends. Number between 6 and 22.
     * @param dateName the title of the date.
     */
    public PdfPlanDate(int day, float timeFrom, float timeTo, String dateName) throws IllegalArgumentException {
        if (day < 0 || day > 4) {
            throw new IllegalArgumentException("Invalid day");
        }
        if (timeFrom < 6 || timeFrom > 22 || timeTo < 6 || timeTo > 22) {
            throw new IllegalArgumentException("Invalid start or end time");
        }
        this.day = day;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.dateName = dateName;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public float getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(float timeFrom) {
        this.timeFrom = timeFrom;
    }

    public float getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(float timeTo) {
        this.timeTo = timeTo;
    }

    public String getDateName() {
        return dateName;
    }

    public void setDateName(String dateName) {
        this.dateName = dateName;
    }

}
