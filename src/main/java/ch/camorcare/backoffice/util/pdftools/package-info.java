package ch.camorcare.backoffice.util.pdftools;

/**
 * Different types of documents represented electronically in the program can also be output as a PDF for further
 * usage by a user. This includes generating a bill/invoice, a PDF containing all of the chores that must be carried out
 * in a given week or converting a TherapyPlan into a weekly agenda that can be printed and also emailed. The package
 * further contains a utility to allow the program to print directly from itself, which allows the program to generate
 * a desired PDF and then print and/or email the document at the click of a button.
 * <p/>
 * Due to the multitude of exceptions that can be thrown by the various functions required to generate a PDF, a unified
 * exception was created to allow simple error reporting.
 * <p/>
 * There is also an entity unique to this package that is used when converting a TherapyEvent into a format readable
 * for the generation of a weekly agenda PDF. The package also contains the converters necessary to change a DTO into
 * a PDF-legible format.
 */