package ch.camorcare.backoffice.util.pdftools;

import ch.camorcare.backoffice.entities.TherapyEvent;
import ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate;
import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * This service provides methods to convert either a list of {@link ch.camorcare.backoffice.entities.TherapyEvent}
 * objects or an individual object into {@link ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate} objects, which are then
 * used in the services to generate PDFs.
 */
public class PdfPlanDateConverter {

    private static Logger log = LogManager.getLogger(PdfPlanDateConverter.class.getName());

    /**
     * Converts a list of {@link ch.camorcare.backoffice.entities.TherapyEvent} objects to a list of {@link
     * ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate} objects. The <code>criteria</code> flag is used to indicate what
     * type of PDF is to be generated. If 1 a PDF should be generated according to the {@link
     * ch.camorcare.backoffice.entities.Patient} perspective, if 2 according to the {@link
     * ch.camorcare.backoffice.entities.Therapist} perspective and if 3 according to the {@link
     * ch.camorcare.backoffice.entities.TherapyRoom} perspective. No other values will be expected for this flag.
     *
     * @param toConvert The list of TherapyEvent objects to be converted
     * @param criteria  The criteria flag to convert the list to a specific perspective
     * @return A list of PdfPlanDate objects
     * @throws PdfException
     */
    public static List<PdfPlanDate> convert(List<TherapyEvent> toConvert, int criteria) throws PdfException {
        if (toConvert == null) {
            throw new PdfException("List of TherapyEvent objects is null!");
        }

        if (toConvert.size() <= 0) {
            throw new PdfException("List of TherapyEvent objects is empty!");
        }

        ArrayList<PdfPlanDate> returnList = new ArrayList<>();

        for (TherapyEvent therapyEvent : toConvert) {
            /*
            All of the time-based attributes are the same regardless of the perspective to be generated, so these should be set first.
             */
            if (therapyEvent == null) {
                throw new PdfException("TherapyEvent is null!");
            }

            if (therapyEvent.getTherapyTime() == null) {
                throw new PdfException("TherapyTime in TherapyEvent " + therapyEvent.getId() + " is null!");
            }

            returnList.add(convert(therapyEvent, criteria));
        }

        return returnList;
    }

    /**
     * Converts a single {@link ch.camorcare.backoffice.entities.TherapyEvent} object to a list of {@link
     * ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate} objects. The <code>criteria</code> flag is used to indicate what
     * type of PDF is to be generated. If 1 a PDF should be generated according to the {@link
     * ch.camorcare.backoffice.entities.Patient} perspective, if 2 according to the {@link
     * ch.camorcare.backoffice.entities.Therapist} perspective and if 3 according to the {@link
     * ch.camorcare.backoffice.entities.TherapyRoom} perspective. No other values will be expected for this flag.
     *
     * @param toConvert The TherapyEvent object to be converted
     * @param criteria  The criteria flag to convert the list to a specific perspective
     * @return A PdfPlanDate object
     * @throws PdfException
     */
    public static PdfPlanDate convert(TherapyEvent toConvert, int criteria) throws PdfException {
        if (toConvert == null) {
            log.error("TherapyEvent object to convert is null!");
            throw new PdfException("TherapyEvent object to convert is null!");
        }

        if (toConvert.getTherapyTime() == null) {
            log.error("TherapyTime in TherapyEvent " + toConvert.getId() + " is null!");
            throw new PdfException("TherapyTime in TherapyEvent " + toConvert.getId() + " is null!");
        }

        PdfPlanDate pdfPlanDate = new PdfPlanDate();

        pdfPlanDate.setDay(getDayOfWeek(toConvert.getTherapyTime()));
        pdfPlanDate.setTimeFrom(getFloatFromDateTimeHour(toConvert.getTherapyTime()));
        pdfPlanDate.setTimeTo(pdfPlanDate.getTimeFrom() + 0.75f);

        switch (criteria) {
            case 1:
                return convertByPatientPerspective(pdfPlanDate, toConvert);
            case 2:
                return convertByTherapistPerspective(pdfPlanDate, toConvert);
            case 3:
                return convertByTherapyRoomPerspective(pdfPlanDate, toConvert);
            default:
                log.error("Criteria must be 1, 2 or 3!");
                throw new PdfException("Criteria must be 1, 2 or 3!");
        }
    }

    /**
     * Fills a {@link ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate} object with a String appropriate for representing a
     * {@link ch.camorcare.backoffice.entities.TherapyEvent} in a PDF from the perspective of a {@link
     * ch.camorcare.backoffice.entities.Patient}.
     *
     * @param pdfPlanDate The PdfPlanDate to be filled
     * @param toConvert   The TherapyEvent with the necessary attributes to generate the String
     * @return A completed PdfPlanDate
     * @throws PdfException
     */
    private static PdfPlanDate convertByPatientPerspective(PdfPlanDate pdfPlanDate, TherapyEvent toConvert) throws PdfException {
        String title = "";

        if (toConvert.getTherapyType() == null) {
            log.error("TherapyType in TherapyEvent is null!");
            throw new PdfException("TherapyType in TherapyEvent is null!");
        }

        if (toConvert.getTherapist() == null) {
            log.error("Therapist in TherapyEvent is null!");
            throw new PdfException("Therapist in TherapyEvent is null!");
        }

        if (toConvert.getTherapyRoom() == null) {
            log.error("TherapyRoom in TherapyEvent is null!");
            throw new PdfException("TherapyRoom in TherapyEvent is null!");
        }

        title += toConvert.getTherapyType().getName() + ", ";
        title += toConvert.getTherapyRoom().getName();

        pdfPlanDate.setDateName(title);

        return pdfPlanDate;
    }

    /**
     * Fills a {@link ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate} object with a String appropriate for representing a
     * {@link ch.camorcare.backoffice.entities.TherapyEvent} in a PDF from the perspective of a {@link
     * ch.camorcare.backoffice.entities.Therapist}.
     *
     * @param pdfPlanDate The PdfPlanDate to be filled
     * @param toConvert   The TherapyEvent with the necessary attributes to generate the String
     * @return A completed PdfPlanDate
     * @throws PdfException
     */
    private static PdfPlanDate convertByTherapistPerspective(PdfPlanDate pdfPlanDate, TherapyEvent toConvert) throws PdfException {
        String title = "";

        if (toConvert.getTherapyType() == null) {
            log.error("TherapyType in TherapyEvent is null!");
            throw new PdfException("TherapyType in TherapyEvent is null!");
        }

        if (toConvert.getTherapyMode() == null) {
            log.error("TherapyMode in TherapyEvent is null!");
            throw new PdfException("TherapyMode in TherapyEvent is null!");
        }

        if (toConvert.getTherapyRoom() == null) {
            log.error("TherapyRoom in TherapyEvent is null!");
            throw new PdfException("TherapyRoom in TherapyEvent is null!");
        }

        title += toConvert.getTherapyType().getName() + ", ";
        title += toConvert.getTherapyRoom().getName();

        pdfPlanDate.setDateName(title);

        return pdfPlanDate;
    }

    /**
     * Fills a {@link ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate} object with a String appropriate for representing a
     * {@link ch.camorcare.backoffice.entities.TherapyEvent} in a PDF from the perspective of a {@link
     * ch.camorcare.backoffice.entities.TherapyRoom}.
     *
     * @param pdfPlanDate The PdfPlanDate to be filled
     * @param toConvert   The TherapyEvent with the necessary attributes to generate the String
     * @return A completed PdfPlanDate
     * @throws PdfException
     */
    private static PdfPlanDate convertByTherapyRoomPerspective(PdfPlanDate pdfPlanDate, TherapyEvent toConvert) throws PdfException {
        String title = "";

        if (toConvert.getTherapyType() == null) {
            log.error("TherapyType in TherapyEvent is null!");
            throw new PdfException("TherapyType in TherapyEvent is null!");
        }

        if (toConvert.getTherapyMode() == null) {
            log.error("TherapyMode in TherapyEvent is null!");
            throw new PdfException("TherapyMode in TherapyEvent is null!");
        }

        if (toConvert.getTherapist() == null) {
            log.error("Therapist in TherapyEvent is null!");
            throw new PdfException("Therapist in TherapyEvent is null!");
        }

        title += toConvert.getTherapyType().getName() + ", ";
        title += toConvert.getTherapist().getLastName();

        pdfPlanDate.setDateName(title);

        return pdfPlanDate;
    }

    /**
     * Gets an Integer representation of a {@link org.joda.time.DateTime} dayOfWeek Property.
     * <p/>
     * 0 for Monday, 1 for Tuesday, 2 for Wednesday, 3 for Thursday, 4 for Friday.
     *
     * @param dateTime The DateTime whose dayOfWeek needs to be converted.
     * @return An Integer representing a day of the week
     * @throws PdfException
     */
    private static Integer getDayOfWeek(DateTime dateTime) throws PdfException {
        if (dateTime == null) {
            log.error("DateTime in TherapyEvent is null!");
            throw new PdfException("DateTime in TherapyEvent is null!");
        }

        switch (dateTime.getDayOfWeek()) {
            case DateTimeConstants.MONDAY:
                return 0;
            case DateTimeConstants.TUESDAY:
                return 1;
            case DateTimeConstants.WEDNESDAY:
                return 2;
            case DateTimeConstants.THURSDAY:
                return 3;
            case DateTimeConstants.FRIDAY:
                return 4;
            default:
                log.error("Invalid DateTime set in TherapyEvent");
                throw new PdfException("Invalid DateTime set in TherapyEvent");
        }
    }

    /**
     * Gets a Float representation of a {@link org.joda.time.DateTime} hourOfDay Property.
     *
     * @param dateTime The DateTime whose hourOfDay needs to be converted.
     * @return A Float representation of an hour of the day
     */
    private static Float getFloatFromDateTimeHour(DateTime dateTime) {
        float hour = Float.valueOf(dateTime.getHourOfDay());
        float minute = (float) dateTime.getMinuteOfHour() / 60;

        return hour + minute;
    }
}
