package ch.camorcare.backoffice.util.pdftools;

import ch.camorcare.backoffice.entities.DutyCategory;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * This class converts a list of {@link ch.camorcare.backoffice.entities.DutyCategory} objects into a data structure
 * that represents a week plan of the various chores that need to be completed each day.
 */
public class PdfDutyCategoryConverter {

	private static Logger log = LogManager.getLogger(PdfDutyCategoryConverter.class.getName());

	/**
	 * Converts a list of {@link ch.camorcare.backoffice.entities.DutyCategory} objects into a nested List of String
	 * Lists. The outer list has a maximum size of 5 with 0 representing Monday, 1 Tuesday, 2 Wednesday, 3 Thursday and
	 * 4 Friday. The inner list contains the chores that need to be completed each day. The {@link
	 * org.joda.time.DateTime} parameter indicates the starting day for the plan to be printed out. The starting day
	 * should be Monday and will end on Friday but can be any day of the week and will always end on Friday.
	 * <p/>
	 * The user can further decide whether or not the inner lists should all be identical or whether differentiation
	 * should be made based on the list of {@link ch.camorcare.backoffice.entities.type.Day} objects stored inside each
	 * DutyCategory.
	 *
	 * @param dutyCategories         The list of chores to be converted into a nested list
	 * @param startDate              The starting date for the plan
	 * @param ignoreDutyCategoryDays Variable to decide whether to ignore list of Days stored in each DutyCategory
	 * @return A nested list with a maximum of five inner lists
	 * @throws PdfException
	 */
	public static List<List<String>> convert(List<DutyCategory> dutyCategories, DateTime startDate, boolean ignoreDutyCategoryDays) throws PdfException {
		if (dutyCategories == null || dutyCategories.size() <= 0) {
			throw new PdfException("List of DutyCategory objects must not be null or empty");
		}

		if (startDate == null) {
			throw new PdfException("startDate cannot be null!");
		}

		List<List<String>> converted = new ArrayList<>();

		Integer start = getDayOfWeek(startDate);

		for (int i = start; i < 5; i++) {
			converted.add(new ArrayList<>());
		}

		int dayOfMonth = startDate.dayOfMonth().get();

		if (ignoreDutyCategoryDays) {
			switch (start) {
				case 0:
					return startDateMondayIgnoreDays(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				case 1:
					return startDateTuesdayIgnoreDays(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				case 2:
					return startDateWednesdayIgnoreDays(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				case 3:
					return startDateThursdayIgnoreDays(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				case 4:
					return startDateFridayIgnoreDays(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				default:
					log.error("Start date is not a weekday!");
					throw new PdfException("Starting date is not a weekday!");
			}
		}
		else {
			switch (start) {
				case 0:
					return startDateMonday(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				case 1:
					return startDateTuesday(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				case 2:
					return startDateWednesday(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				case 3:
					return startDateThursday(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				case 4:
					return startDateFriday(converted, dutyCategories, dayOfMonth, startDate.monthOfYear().get());
				default:
					log.error("Starting date is not a weekday!");
					throw new PdfException("Starting date is not a weekday!");
			}
		}

	}

	// IGNORE DAYS

	private static List<List<String>> startDateMondayIgnoreDays(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Mo, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 1:
					converted.get(i).add("Di, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 2:
					converted.get(i).add("Mi, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 3:
					converted.get(i).add("Do, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 4:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for(int i = 0; i < converted.size(); i++) {
				converted.get(i).add(dc.getName());
			}
		}

		return converted;
	}

	private static List<List<String>> startDateTuesdayIgnoreDays(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Di, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 1:
					converted.get(i).add("Mi, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 2:
					converted.get(i).add("Do, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 3:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for(int i = 0; i < converted.size(); i++) {
				converted.get(i).add(dc.getName());
			}
		}

		return converted;
	}

	private static List<List<String>> startDateWednesdayIgnoreDays(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Mi, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 1:
					converted.get(i).add("Do, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 2:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for(int i = 0; i < converted.size(); i++) {
				converted.get(i).add(dc.getName());
			}
		}

		return converted;
	}

	private static List<List<String>> startDateThursdayIgnoreDays(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Do, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 1:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for(int i = 0; i < converted.size(); i++) {
				converted.get(i).add(dc.getName());
			}
		}

		return converted;
	}

	private static List<List<String>> startDateFridayIgnoreDays(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for(int i = 0; i < converted.size(); i++) {
				converted.get(i).add(dc.getName());
			}
		}

		return converted;
	}

	// DO NOT IGNORE DAYS

	private static List<List<String>> startDateMonday(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Mo, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 1:
					converted.get(i).add("Di, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 2:
					converted.get(i).add("Mi, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 3:
					converted.get(i).add("Do, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 4:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for (Day day : dc.getWeekdays()) {
				switch (day) {
					case MONDAY:
						converted.get(0).add(dc.getName());
						break;
					case TUESDAY:
						converted.get(1).add(dc.getName());
						break;
					case WEDNESDAY:
						converted.get(2).add(dc.getName());
						break;
					case THURSDAY:
						converted.get(3).add(dc.getName());
						break;
					case FRIDAY:
						converted.get(4).add(dc.getName());
						break;
					default:
						log.error("Invalid day stored inside " + dc.getId());
						throw new PdfException("Invalid day stored inside " + dc.getId());
				}
			}
		}

		return converted;
	}

	private static List<List<String>> startDateTuesday(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Di, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 1:
					converted.get(i).add("Mi, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 2:
					converted.get(i).add("Do, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 3:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					log.error("Invalid i");
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for (Day day : dc.getWeekdays()) {
				switch (day) {
					case MONDAY:
						break;
					case TUESDAY:
						converted.get(0).add(dc.getName());
						break;
					case WEDNESDAY:
						converted.get(1).add(dc.getName());
						break;
					case THURSDAY:
						converted.get(2).add(dc.getName());
						break;
					case FRIDAY:
						converted.get(3).add(dc.getName());
						break;
					default:
						log.error("Invalid day stored inside " + dc.getId());
						throw new PdfException("Invalid day stored inside " + dc.getId());
				}
			}
		}

		return converted;
	}

	private static List<List<String>> startDateWednesday(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Mi, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 1:
					converted.get(i).add("Do, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 2:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					log.error("Invalid i");
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for (Day day : dc.getWeekdays()) {
				switch (day) {
					case MONDAY:
						break;
					case TUESDAY:
						break;
					case WEDNESDAY:
						converted.get(0).add(dc.getName());
						break;
					case THURSDAY:
						converted.get(1).add(dc.getName());
						break;
					case FRIDAY:
						converted.get(2).add(dc.getName());
						break;
					default:
						log.error("Invalid day stored inside " + dc.getId());
						throw new PdfException("Invalid day stored inside " + dc.getId());
				}
			}
		}

		return converted;
	}

	private static List<List<String>> startDateThursday(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Do, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				case 1:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					log.error("Invalid i");
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for (Day day : dc.getWeekdays()) {
				switch (day) {
					case MONDAY:
						break;
					case TUESDAY:
						break;
					case WEDNESDAY:
						break;
					case THURSDAY:
						converted.get(0).add(dc.getName());
						break;
					case FRIDAY:
						converted.get(1).add(dc.getName());
						break;
					default:
						log.error("Invalid day stored inside " + dc.getId());
						throw new PdfException("Invalid day stored inside " + dc.getId());
				}
			}
		}

		return converted;
	}

	private static List<List<String>> startDateFriday(List<List<String>> converted, List<DutyCategory> dutyCategories, int dayOfMonth, int monthOfYear) throws PdfException {
		/*for (int i = 0; i < converted.size(); i++) {
			int dayOfMonthTemp = dayOfMonth + i;
			switch (i) {
				case 0:
					converted.get(i).add("Fr, " + dayOfMonthTemp + "." + monthOfYear + ".");
					break;
				default:
					log.error("Invalid i");
					throw new PdfException("Invalid i");
			}
		}*/

		for (DutyCategory dc : dutyCategories) {
			for (Day day : dc.getWeekdays()) {
				switch (day) {
					case MONDAY:
						break;
					case TUESDAY:
						break;
					case WEDNESDAY:
						break;
					case THURSDAY:
						break;
					case FRIDAY:
						converted.get(0).add(dc.getName());
						break;
					default:
						log.error("Invalid day stored inside: " + dc.getId());
						throw new PdfException("Invalid day stored inside " + dc.getId());
				}
			}
		}

		return converted;
	}

	/**
	 * Gets an Integer representation of a {@link org.joda.time.DateTime} dayOfWeek Property.
	 * <p/>
	 * 0 for Monday, 1 for Tuesday, 2 for Wednesday, 3 for Thursday, 4 for Friday.
	 *
	 * @param dateTime The DateTime whose dayOfWeek needs to be converted.
	 * @return An Integer representing a day of the week
	 * @throws PdfException
	 */
	private static Integer getDayOfWeek(DateTime dateTime) throws PdfException {
		if (dateTime == null) {
			log.error("DateTime is null!");
			throw new PdfException("DateTime is null!");
		}

		switch (dateTime.getDayOfWeek()) {
			case DateTimeConstants.MONDAY:
				return 0;
			case DateTimeConstants.TUESDAY:
				return 1;
			case DateTimeConstants.WEDNESDAY:
				return 2;
			case DateTimeConstants.THURSDAY:
				return 3;
			case DateTimeConstants.FRIDAY:
				return 4;
			default:
				log.error("Invalid DateTime");
				throw new PdfException("Invalid DateTime");
		}
	}
}
