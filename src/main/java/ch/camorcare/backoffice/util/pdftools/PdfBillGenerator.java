package ch.camorcare.backoffice.util.pdftools;

import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.File;
import java.io.IOException;

/**
 * This class generates a PDF document, representing the bill for a patient.
 */
public class PdfBillGenerator {

    private static Logger log = LogManager.getLogger(PdfBillGenerator.class.getName());

    /**
     * This method generates the PDF file for a bill using the passed parameters.
     *
     * @param addressLine1 First line of the address
     * @param addressLine2 Second line of the address
     * @param addressLine3 Third line of the address
     * @param date Current date
     * @param stayPrice The price of the stay
     * @param therapyPrice The price of all therapies
     * @param outfile The output file path
     * @throws PdfException
     */
    public static void generatePDF(String addressLine1, String addressLine2, String addressLine3, String date, double stayPrice, double therapyPrice, String outfile) throws PdfException {
        try {
            doWork(addressLine1, addressLine2, addressLine3, date, stayPrice, therapyPrice, outfile);
        } catch (COSVisitorException | IOException e) {
            log.error(e.getMessage());
            throw new PdfException(e.getMessage());
        }
    }

    private static void doWork(String addressLine1, String addressLine2, String addressLine3, String date, double stayPrice, double therapyPrice, String outfile) throws IOException, COSVisitorException {
        // the document
        PDDocument doc = null;

            double priceSum = Math.round((stayPrice + therapyPrice) * 100.0) / 100.0;

            doc = PDDocument.load(new File("files/submissions/blankbill.PDF"));
            PDPage page = (PDPage) doc.getDocumentCatalog().getAllPages().get(0);
            PDFont font = PDType1Font.HELVETICA;
            PDFont fontBold = PDType1Font.HELVETICA_BOLD;
            PDPageContentStream contentStream = new PDPageContentStream(
                    doc, page, true, true);
            page.getContents().getStream();

            contentStream.beginText();
            contentStream.setFont(font, 9);

            // address
            contentStream.moveTextPositionByAmount(71, 650);
            contentStream.drawString(addressLine1);
            contentStream.moveTextPositionByAmount(0, -11);
            contentStream.drawString(addressLine2);
            contentStream.moveTextPositionByAmount(0, -11);
            contentStream.drawString(addressLine3);

            // date
            contentStream.moveTextPositionByAmount(408, -45);
            contentStream.drawString(date);

            // price
            contentStream.moveTextPositionByAmount(-5, -101);
            contentStream.drawString(Double.toString(stayPrice) + " CHF");

            contentStream.moveTextPositionByAmount(0, -20);
            contentStream.drawString(Double.toString(therapyPrice) + " CHF");

            contentStream.moveTextPositionByAmount(0, -20);
            contentStream.setFont(fontBold, 9);
            contentStream.drawString(Double.toString(priceSum) + " CHF");

            contentStream.endText();
            contentStream.close();
            doc.save(outfile);
            doc.close();
    }

}
