package ch.camorcare.backoffice.util.pdftools;

import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.awt.print.PrinterException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * This class generates a PDF document for the disposition of house services.
 */
public class PdfHouseServiceGenerator {

	private static Logger log = LogManager.getLogger(PdfHouseServiceGenerator.class.getName());

	/**
	 * This method generates a PDF document with the weekly schedule for house services.
	 *
	 * @param startDay The Date of the first day of the week
	 * @param services Has to contain at least 5 ArrayLists with services for each weekday. Additional ArrayLists will
	 *                 be ignored.
	 * @param outfile  The path where to save the PDF document
	 * @throws PdfException
	 */
	public static void generatePDF(DateTime startDay, List<List<String>> services, String outfile) throws PdfException {
		if (services.size() > 5) {
			throw new PdfException("Services is too large.");
		}

		try {
			doWork(startDay, services, outfile, false);
		} catch (COSVisitorException | PrinterException | IOException e) {
			log.error(e.getMessage());
			throw new PdfException(e.getMessage());
		}
	}

	/**
	 * This method generates a PDF document with the weekly schedule for house services and then finally opens the print
	 * dialog to allow the user to print the PDF that has just been generated.
	 *
	 * @param startDay The Date of the first day of the week
	 * @param services Has to contain at least 5 ArrayLists with services for each weekday. Additional ArrayLists will
	 *                 be ignored.
	 * @param outfile  The path where to save the PDF document
	 * @throws PdfException
	 */
	public static void generateAndPrintPDF(DateTime startDay, List<List<String>> services, String outfile) throws PdfException {
		if (services.size() > 5) {
			throw new PdfException("Services is too large.");
		}

		try {
			doWork(startDay, services, outfile, true);
		} catch (COSVisitorException | PrinterException | IOException e) {
			log.error(e.getMessage());
			throw new PdfException(e.getMessage());
		}
	}

	private static void doWork(DateTime startDay, List<List<String>> services, String outfile, boolean printOnEnd) throws IOException, COSVisitorException, PrinterException {
		// The PDF document to be output
		PDDocument doc = null;

		float fontSize = 16;
		float headlineSize = 25;
		float startX = 40;
		float startY = 770;

		try {
			doc = new PDDocument();

			PDFont font = PDType1Font.HELVETICA;

			for (int i = 0; i < services.size(); i++) {

				String title = "Hausdienste für ";

				DateTimeFormatter f = DateTimeFormat.forPattern("E, dd.MM.yy");
				f = f.withLocale(Locale.GERMAN);
				title += f.print(startDay);

				PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
				doc.addPage(page);

				PDPageContentStream contentStream = new PDPageContentStream(doc, page, false, false);
				contentStream.setFont(font, headlineSize);

				// Headline
				contentStream.beginText();
				contentStream.moveTextPositionByAmount(startX, startY);
				contentStream.drawString(title);
				contentStream.endText();

				contentStream.setFont(font, fontSize);

				Iterator<String> it = services.get(i).iterator();
				int counter = 0;

				while (it.hasNext()) {
					String service = it.next();

					contentStream.beginText();
					contentStream.moveTextPositionByAmount(startX + 15, startY - 85 - (counter * 60));
					contentStream.drawString(service + ":");
					contentStream.endText();

					contentStream.drawLine(startX, startY - 50 - (counter * 60), startX + 517, startY - 50 - (counter * 60));
					contentStream.drawLine(startX, startY - 110 - (counter * 60), startX + 517, startY - 110 - (counter * 60));

					counter++;

				}

				contentStream.close();

				startDay = startDay.plusDays(1);
			}

			if (printOnEnd) {
				doc.silentPrint();
			}
			doc.save(outfile);
		} finally {
			if (doc != null) {
				doc.close();
			}
		}
	}
}
