package ch.camorcare.backoffice.util.pdftools;

import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

public class PdfPrinter {

    private static Logger log = LogManager.getLogger(PdfPrinter.class.getName());

    /**
     * Method opens a print dialog box and prints the PDF document at the file path passed to the method.
     *
     * @param fileName Path of the PDF document to print
     * @throws PdfException
     */
    public static void printPDF(String fileName) throws PdfException {
        try {
            PDDocument pdDoc = PDDocument.load(fileName);
            pdDoc.print();
            pdDoc.close();
        } catch (IOException | PrinterException e) {
            log.error(e.getMessage());
            throw new PdfException(e.getMessage());
        }
    }

    /**
     * This method prints the given PDF on the system default printer. There is no dialog shown!
     *
     * @param fileName Path of the PDF document to print
     * @throws PdfException
     */
    public static void silentPrintPDF(String fileName) throws PdfException {
        try {
            PDDocument pdDoc = PDDocument.load(fileName);
            pdDoc.silentPrint();
            pdDoc.close();
        } catch (IOException | PrinterException e) {
            log.error(e.getMessage());
            throw new PdfException(e.getMessage());
        }
    }

    /**
     * This method opens a print dialog for the given PDF document
     * This method uses the java print dialog instead of the system dialog.
     * <p/>
     * Only recommended to use if printPDF(String fileName) does not work!
     *
     * @param fileName Path of the PDF document to print
     * @throws PdfException
     */
    public static void printPDFWithJavaDialog(String fileName) throws PdfException {
        try {
            PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
            DocFlavor flavor = DocFlavor.URL.AUTOSENSE;

            // Printer dialog
            PrintService printService[] =
                    PrintServiceLookup.lookupPrintServices(flavor, pras);
            PrintService defaultService =
                    PrintServiceLookup.lookupDefaultPrintService();
            PrintService service = null;
            service = ServiceUI.printDialog(null, 200, 200,
                    printService, defaultService, flavor, pras);

            // Return if aborted in dialog
            if (service == null) {
                return;
            }

            // Read and print PDF file
            DocPrintJob printerJob = service.createPrintJob();
            File pdfFile = new File(fileName);
            SimpleDoc simpleDoc = new SimpleDoc(pdfFile.toURI().toURL(), flavor, null);
            printerJob.print(simpleDoc, null);
        } catch (MalformedURLException | PrintException e) {
            log.error(e.getMessage());
            throw new PdfException(e.getMessage());
        }
    }

    /**
     * This method prints the given PDF on the system default printer. there is no dialog shown!
     * This method uses the java print engine instead of the system engine.
     * <p/>
     * Only recommended to use if silentPrintPDF(String fileName) does not work!
     *
     * @param fileName Path of the PDF document to print
     * @throws PdfException
     */
    public static void silentPrintPDFWithJavaInterface(String fileName) throws PdfException {
        try {
            PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
            DocFlavor flavor = DocFlavor.URL.AUTOSENSE;

            // Printer dialog
            PrintService service = PrintServiceLookup.lookupDefaultPrintService();

            // Return if aborted in dialog
            if (service == null) {
                throw new PrinterException("no default printer found");
            }

            // Read and print PDF file
            DocPrintJob printerJob = service.createPrintJob();
            File pdfFile = new File(fileName);
            SimpleDoc simpleDoc = new SimpleDoc(pdfFile.toURI().toURL(), flavor, null);
            printerJob.print(simpleDoc, null);
        } catch (MalformedURLException | PrintException | PrinterException e) {
            log.error(e.getMessage());
            throw new PdfException(e.getMessage());
        }
    }

}
