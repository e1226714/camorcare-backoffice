package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.PatientContact;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.PatientContact} DTO. It
 * complements it with ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class PatientContactAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private PatientContact entity;

    /**
     * String property for first name
     */
    private StringProperty firstNameProperty;

    /**
     * String property for last name
     */
    private StringProperty lastNameProperty;

    /**
     * String property for telephone number
     */
    private StringProperty phoneProperty;

    /**
     * String property for email address
     */
    private StringProperty emailProperty;

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.PatientContactAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public PatientContactAdapter(PatientContact entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.PatientContact}, which is wrapped by this adapter.
     *
     * @return The wrapped entity
     */
    public PatientContact getEntity() {
        return entity;
    }

    /**
     * Sets the first name in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param firstName The first name to be wrapped
     */
    public void setFirstName(String firstName) {
        this.entity.setFirstName(firstName);
        this.firstNameProperty.set(firstName);
    }

    /**
     * Returns the first name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return firstNameProperty The wrapped first name
     */
    public StringProperty getFirstNameProperty() {
        if (firstNameProperty == null) {
            firstNameProperty = new SimpleStringProperty(entity.getFirstName());
        }
        return firstNameProperty;
    }

    /**
     * Sets the last name in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param lastName The last name to be wrapped
     */
    public void setLastName(String lastName) {
        this.entity.setLastName(lastName);
        this.lastNameProperty.set(lastName);
    }

    /**
     * Returns the last name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return lastNameProperty The wrapped last name
     */
    public StringProperty getLastNameProperty() {
        if (lastNameProperty == null) {
            lastNameProperty = new SimpleStringProperty(entity.getLastName());
        }
        return lastNameProperty;
    }


    /**
     * Sets the phone number in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param phone The phone number to be wrapped
     */
    public void setPhone(String phone) {
        this.entity.setPhone(phone);
        this.phoneProperty.set(phone);
    }

    /**
     * Returns the phone number as a {@link javafx.beans.property.StringProperty}.
     *
     * @return phoneProperty The wrapped phone number
     */
    public StringProperty getPhoneProperty() {
        if (phoneProperty == null) {
            phoneProperty = new SimpleStringProperty(entity.getPhone());
        }
        return phoneProperty;
    }

    /**
     * Sets the patient email in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param email The email to be wrapped
     */
    public void setEmail(String email) {
        this.entity.setEmail(email);
        this.emailProperty.set(email);
    }

    /**
     * Returns the patient email as a {@link javafx.beans.property.StringProperty}.
     *
     * @return emailProperty The wrapped email
     */
    public StringProperty getEmailProperty() {
        if (emailProperty == null) {
            emailProperty = new SimpleStringProperty(entity.getEmail());
        }
        return emailProperty;
    }
}
