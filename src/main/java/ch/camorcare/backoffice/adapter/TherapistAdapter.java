package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyType;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.Therapist} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class TherapistAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private Therapist entity;

    /**
     * String property for the name
     */
    private StringProperty nameProperty;

    /**
     * String property for the first name
     */
    private StringProperty firstNameProperty;

    /**
     * String property for the last name
     */
    private StringProperty lastNameProperty;

    /**
     * String property for the email
     */
    private StringProperty emailProperty;

    /**
     * List of TherapyTypeAdapter objects that wrap TherapyType objects
     */
    private List<TherapyTypeAdapter> therapyTypesProperty;

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.TherapistAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public TherapistAdapter(Therapist entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.Therapist}, which is wrapped by this adapter.
     *
     * @return The wrapped entity
     */
    public Therapist getEntity() {
        return entity;
    }

    /**
     * Sets the Therapist first name in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param firstName The first name to be wrapped
     */
    public void setFirstName(String firstName) {
        this.entity.setFirstName(firstName);
        this.firstNameProperty.set(firstName);
    }

    /**
     * Returns the Therapist first name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return firstNameProperty The wrapped first name
     */
    public StringProperty getFirstNameProperty() {
        if (firstNameProperty == null) {
            firstNameProperty = new SimpleStringProperty(entity.getFirstName());
        }
        return firstNameProperty;
    }

    /**
     * Sets the Therapist last name in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param lastName The last name to be wrapped
     */
    public void setLastName(String lastName) {
        this.entity.setLastName(lastName);
        this.lastNameProperty.set(lastName);
    }

    /**
     * Returns the Therapist last name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return lastNameProperty The wrapped last name
     */
    public StringProperty getLastNameProperty() {
        if (lastNameProperty == null) {
            lastNameProperty = new SimpleStringProperty(entity.getLastName());
        }
        return lastNameProperty;
    }

    /**
     * Returns the Therapist full name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return nameProperty The wrapped full name
     */
    public StringProperty getNameProperty() {
        if (nameProperty == null) {
            nameProperty = new SimpleStringProperty(entity.getLastName() + " " + entity.getFirstName());
        }
        return nameProperty;
    }

    /**
     * Sets the Therapist email in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param email The email to be wrapped
     */
    public void setEmail(String email) {
        this.entity.setEmail(email);
        this.emailProperty.set(email);
    }

    /**
     * Returns the Therapist email as a {@link javafx.beans.property.StringProperty}.
     *
     * @return emailProperty The wrapped email
     */
    public StringProperty getEmailProperty() {
        if (emailProperty == null) {
            emailProperty = new SimpleStringProperty(entity.getEmail());
        }
        return emailProperty;
    }

    /**
     * Sets the Therapist's list of {@link ch.camorcare.backoffice.entities.TherapyType} objects in the entity and
     * updates the list of {@link ch.camorcare.backoffice.adapter.TherapyTypeAdapter} objects.
     *
     * @param therapyTypes The TherapyType objects to be wrapped
     */
    public void setTherapyTypeList(List<TherapyType> therapyTypes) {
        this.entity.setTherapyTypes(therapyTypes);
        therapyTypesProperty = new ArrayList<TherapyTypeAdapter>();
        for (TherapyType therapyType : therapyTypes) {
            therapyTypesProperty.add(new TherapyTypeAdapter(therapyType));
        }
    }

    /**
     * Returns the Therapist's list of {@link ch.camorcare.backoffice.entities.TherapyType} objects as a {@link
     * ch.camorcare.backoffice.adapter.TherapyTypeAdapter}.
     *
     * @return TherapyTypeListProperty The wrapped TherapyType objects
     */
    public List<TherapyTypeAdapter> getTherapyTypeListProperty() {
        if (therapyTypesProperty == null) {
            therapyTypesProperty = new ArrayList<TherapyTypeAdapter>();
            List<TherapyType> therapyTypes = entity.getTherapyTypes();
            for (TherapyType therapyType : therapyTypes) {
                therapyTypesProperty.add(new TherapyTypeAdapter(therapyType));
            }
        }
        return therapyTypesProperty;
    }

    @Override
    public String toString() {
        return entity.getLastName() + " " + entity.getFirstName();
    }
}
