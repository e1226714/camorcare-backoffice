package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.Therapy;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.joda.time.DateTime;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.Therapy} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class TherapyAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private Therapy entity;

    /**
     * Adapter that wraps a Patient
     */
    private PatientAdapter patientAdapter;

    /**
     * ObjectProperty that wraps the starting time
     */
    private ObjectProperty<DateTime> dateFromProperty;

    /**
     * ObjectProperty that wraps the end time
     */
    private ObjectProperty<DateTime> dateUntilProperty;

    /**
     * ObjectProperty that wraps the start of a patient's sick leave
     */
    private ObjectProperty<DateTime> dateSickLeaveProperty;

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.TherapyAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public TherapyAdapter(Therapy entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity, which is wrapped by this adapter.
     *
     * @return The wrapped entity
     */
    public Therapy getEntity() {
        return entity;
    }

    /**
     * Sets the {@link ch.camorcare.backoffice.entities.Patient} in the entity and updates the {@link ch.camorcare.backoffice.adapter.PatientAdapter}.
     *
     * @param patient The Patient to be wrapped
     */
    public void setPatient(Patient patient) {
        this.entity.setPatient(patient);
        this.patientAdapter = new PatientAdapter(patient);
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.entities.Patient} as a {@link ch.camorcare.backoffice.adapter.PatientAdapter}.
     *
     * @return patientProperty The wrapped Patient
     */
    public PatientAdapter getPatientAdapter() {
        if (patientAdapter == null) {
            patientAdapter = new PatientAdapter(entity.getPatient());
        }
        return patientAdapter;
    }

    /**
     * Sets the dateFrom of type {@link org.joda.time.DateTime} in the entity and updates the {@link javafx.beans.property.ObjectProperty}.
     *
     * @param dateFrom The DateTime to be wrapped
     */
    public void setDateFrom(DateTime dateFrom) {
        this.entity.setDateFrom(dateFrom);
        this.dateFromProperty.set(dateFrom);
    }

    /**
     * Returns the dateFrom of type {@link org.joda.time.DateTime} as a {@link javafx.beans.property.ObjectProperty}.
     *
     * @return dateFromProperty The wrapped DateTime
     */
    public ObjectProperty<DateTime> getDateFromProperty() {
        if (dateFromProperty == null) {
            dateFromProperty = new SimpleObjectProperty<DateTime>(entity.getDateFrom());
        }
        return dateFromProperty;
    }

    /**
     * Sets the dateUntil of type {@link org.joda.time.DateTime} in the entity and updates the {@link javafx.beans.property.ObjectProperty}.
     *
     * @param dateUntil The DateTime to be wrapped
     */
    public void setDateUntil(DateTime dateUntil) {
        this.entity.setDateUntil(dateUntil);
        this.dateUntilProperty.set(dateUntil);
    }

    /**
     * Returns the dateUntil of type {@link org.joda.time.DateTime} as a {@link javafx.beans.property.ObjectProperty}.
     *
     * @return dateUntilProperty The wrapped DateTime
     */
    public ObjectProperty<DateTime> getDateUntilProperty() {
        if (dateUntilProperty == null) {
            dateUntilProperty = new SimpleObjectProperty<DateTime>(entity.getDateUntil());
        }
        return dateUntilProperty;
    }

    /**
     * Sets the dateSickLeave of type {@link org.joda.time.DateTime} in the entity and updates the {@link javafx.beans.property.ObjectProperty}.
     *
     * @param dateSickLeave The DateTime to be wrapped
     */
    public void setDateSickLeave(DateTime dateSickLeave) {
        this.entity.setDateSickLeave(dateSickLeave);
        this.dateSickLeaveProperty.set(dateSickLeave);
    }

    /**
     * Returns the dateSickLeave of type {@link org.joda.time.DateTime} as a {@link javafx.beans.property.ObjectProperty}.
     *
     * @return dateSickLeaveProperty The wrapped DateTime
     */
    public ObjectProperty<DateTime> getDateSickLeaveProperty() {
        if (dateSickLeaveProperty == null) {
            dateSickLeaveProperty = new SimpleObjectProperty<DateTime>(entity.getDateSickLeave());
        }
        return dateSickLeaveProperty;
    }

    @Override
    public String toString() {
        return entity.getPatient().getLastName() + " " + entity.getPatient().getFirstName();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass().getName().equals(TherapyAdapter.class.getName())) {
            TherapyAdapter other = (TherapyAdapter) obj;

            if (this.getEntity().getId().equals(other.getEntity().getId())) {
                return true;
            }
        }
        return false;
    }
}
