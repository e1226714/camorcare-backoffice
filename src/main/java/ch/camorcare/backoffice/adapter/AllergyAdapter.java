package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.Allergy;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.Allergy} DTO. It complements it with ObjectProperties necessary for the data binding in the
 * frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class AllergyAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private Allergy entity;

    /**
     * String property for the name
     */
    private StringProperty nameProperty;

    /**
     * Creates a new allergy adapter.
     *
     * @param entity The entity to be wrapped
     */
    public AllergyAdapter(Allergy entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.Allergy}, which is wrapped by this adapter.
     *
     * @return The wrapped entity
     */
    public Allergy getEntity() {
        return entity;
    }

    /**
     * Sets the name in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param name The name to be wrapped
     */
    public void setName(String name) {
        this.entity.setName(name);
        this.nameProperty.set(name);
    }

    /**
     * Returns the name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return nameProperty The wrapped name
     */
    public StringProperty getNameProperty() {
        if (nameProperty == null) {
            nameProperty = new SimpleStringProperty(entity.getName());
        }
        return nameProperty;
    }
}
