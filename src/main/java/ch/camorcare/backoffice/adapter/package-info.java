package ch.camorcare.backoffice.adapter;

/**
 * The classes found in this package provide adapters that interface with the entities (DTOs) of the project and allow
 * their attributes and variables to be represented in JavaFX, which requires slightly different variable types known
 * as Properties.
 */