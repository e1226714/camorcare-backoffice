package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.TherapyPlan;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.joda.time.DateTime;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.TherapyPlan} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class TherapyPlanAdapter {

    /**
     * The entity wrapped by this adapter
     */
    private TherapyPlan entity;

    /**
     * A DateTime wrapped by an ObjectProperty for the start date of a TherapyPlan
     */
    private ObjectProperty<DateTime> propertyStartDate;

    /**
     * A DateTime wrapped by an ObjectProperty for the end date of a TherapyPlan
     */
    private ObjectProperty<DateTime> propertyEndDate;

    /**
     * Returns a new {@link ch.camorcare.backoffice.adapter.TherapyPlanAdapter}
     *
     * @param therapyPlan The entity to be wrapped
     */
    public TherapyPlanAdapter(TherapyPlan therapyPlan) {
        this.entity = therapyPlan;
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.TherapyPlan} wrapped in this adapter
     *
     * @return The wrapped entity
     */
    public TherapyPlan getEntity() {
        return entity;
    }

    /**
     * Sets the start date of type {@link org.joda.time.DateTime} in the entity and wraps it in an {@link
     * javafx.beans.property.ObjectProperty}
     *
     * @param date The date to be set and wrapped
     */
    public void setStartDate(DateTime date) {
        this.getEntity().setDateFrom(date);
        this.getStartDateProperty().set(date);
    }

    /**
     * Returns the start date of type {@link org.joda.time.DateTime} of the entity as an {@link
     * javafx.beans.property.ObjectProperty}
     *
     * @return The wrapped start date
     */
    public ObjectProperty<DateTime> getStartDateProperty() {
        if (propertyStartDate == null) {
            propertyStartDate = new SimpleObjectProperty<>(entity.getDateFrom());
        }
        return propertyStartDate;
    }

    /**
     * Sets and wraps the end date of type {@link org.joda.time.DateTime} as an {@link
     * javafx.beans.property.ObjectProperty}
     *
     * @param date The end date to be set and wrapped
     */
    public void setEndDate(DateTime date) {
        this.getEntity().setDateUntil(date);
        this.getEndDateProperty().set(date);
    }

    /**
     * Returns the end date of type {@link org.joda.time.DateTime} of the entity as an {@link
     * javafx.beans.property.ObjectProperty}
     *
     * @return The wrapped end date
     */
    public ObjectProperty<DateTime> getEndDateProperty() {
        if (propertyEndDate == null) {
            propertyEndDate = new SimpleObjectProperty<>(entity.getDateUntil());
        }
        return propertyEndDate;
    }

    @Override
    public String toString() {
        return entity.getDateFrom().toString("dd/MM/YYY") + " - " + entity.getDateUntil().toString("dd/MM/YYY");
    }
}
