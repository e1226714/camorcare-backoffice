package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint} DTO.
 * It complements it with ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class PatientBasedTherapyTypeOccurrenceConstraintAdapter {

    /**
     * The entity wrapped by this adapter
     */
    private PatientBasedTherapyTypeOccurrenceConstraint entity;

    /**
     * The adapter that wraps the TherapyType
     */
    private TherapyTypeAdapter therapyTypeAdapter;

    /**
     * An ObjectProperty that wraps a TherapyMode
     */
    private ObjectProperty<TherapyMode> therapyModeProperty;

    /**
     * Integer property that wraps the maximum number of occurrences per week of a TherapyType
     */
    private IntegerProperty maxOccurrencePerWeekProperty;

    /**
     * Returns a new {@link ch.camorcare.backoffice.adapter.PatientBasedTherapyTypeOccurrenceConstraintAdapter}
     *
     * @param entity The entity to be wrapped
     */
    public PatientBasedTherapyTypeOccurrenceConstraintAdapter(PatientBasedTherapyTypeOccurrenceConstraint entity) {
        this.entity = entity;
        this.therapyTypeAdapter = new TherapyTypeAdapter(entity.getTherapyType());
    }

    /**
     * Gets the entity of type {@link ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint}
     * stored within this adapter
     *
     * @return The wrapped entity
     */
    public PatientBasedTherapyTypeOccurrenceConstraint getEntity() {
        return this.entity;
    }

    /**
     * Returns the {@link TherapyTypeAdapter} stored in this wrapper
     *
     * @return The wrapped TherapyType
     */
    public TherapyTypeAdapter getTherapyTypeAdapter() {
        return therapyTypeAdapter;
    }

    /**
     * Gets the {@link javafx.beans.property.ObjectProperty} that wraps a {@link ch.camorcare.backoffice.entities.type.TherapyMode}
     *
     * @return The wrapped TherapyMode
     */
    public ObjectProperty<TherapyMode> getTherapyModeProperty() {
        if (therapyModeProperty == null) {
            therapyModeProperty = new SimpleObjectProperty<>(entity.getTherapyMode());
        }

        return therapyModeProperty;
    }

    /**
     * Sets the {@link javafx.beans.property.ObjectProperty} that wraps a {@link ch.camorcare.backoffice.entities.type.TherapyMode}
     *
     * @param therapyMode The TherapyMode to be wrapped
     */
    public void setTherapyModeProperty(TherapyMode therapyMode) {
        entity.setTherapyMode(therapyMode);
        therapyModeProperty.set(therapyMode);
    }

    /**
     * Returns the maximum number of occurrences of a {@link ch.camorcare.backoffice.entities.TherapyType} as an {@link
     * javafx.beans.property.IntegerProperty}
     *
     * @return The wrapped maximum number of occurrences per day
     */
    public IntegerProperty getMaxOccurrencePerWeekProperty() {
        if (maxOccurrencePerWeekProperty == null) {
            maxOccurrencePerWeekProperty = new SimpleIntegerProperty(entity.getMaxOccurencePerWeek());
        }

        return maxOccurrencePerWeekProperty;
    }

    /**
     * Sets the maximum number of occurrences of a {@link ch.camorcare.backoffice.entities.TherapyType} as an {@link
     * javafx.beans.property.IntegerProperty}
     *
     * @param maxOccurrencePerWeek The number of occurrences per day to be wrapped
     */
    public void setMaxOccurrencePerWeekProperty(int maxOccurrencePerWeek) {
        entity.setMaxOccurencePerWeek(maxOccurrencePerWeek);
        maxOccurrencePerWeekProperty.set(maxOccurrencePerWeek);
    }
}
