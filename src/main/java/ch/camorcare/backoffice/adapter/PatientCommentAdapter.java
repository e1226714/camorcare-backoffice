package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.PatientComment;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.PatientComment} DTO. It
 * complements it with ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class PatientCommentAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private PatientComment entity;

    /**
     * String property for its content
     */
    private StringProperty contentProperty;

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.PatientCommentAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public PatientCommentAdapter(PatientComment entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.PatientComment}, which is wrapped by this adapter.
     *
     * @return wrapped entity
     */
    public PatientComment getEntity() {
        return entity;
    }

    /**
     * Sets the content in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param content The content to be wrapped
     */
    public void setContent(String content) {
        this.entity.setContent(content);
        this.contentProperty.set(content);
    }

    /**
     * Returns the content as a {@link javafx.beans.property.StringProperty}.
     *
     * @return contentProperty The wrapped content
     */
    public StringProperty getContentProperty() {
        if (contentProperty == null) {
            contentProperty = new SimpleStringProperty(entity.getContent());
        }
        return contentProperty;
    }
}
