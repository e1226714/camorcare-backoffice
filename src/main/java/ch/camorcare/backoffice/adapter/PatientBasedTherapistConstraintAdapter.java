package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint} DTO. It
 * complements it with ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class PatientBasedTherapistConstraintAdapter {

    /**
     * The entity wrapped by this adapter
     */
    private PatientBasedTherapistConstraint entity;

    /**
     * The adapter that wraps a TherapyType and delivers its attributes as properties
     */
    private TherapyTypeAdapter therapyTypeAdapter;

    /**
     * The adapter that wraps a Therapist and delivers its attributes as properties
     */
    private TherapistAdapter therapistAdapter;

    /**
     * Returns a {@link ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint}
     *
     * @param entity The entity to be wrapped
     */
    public PatientBasedTherapistConstraintAdapter(PatientBasedTherapistConstraint entity) {
        this.entity = entity;
        this.therapyTypeAdapter = new TherapyTypeAdapter(entity.getTherapyType());
        this.therapistAdapter = new TherapistAdapter(entity.getTherapist());
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint} wrapped by this adapter
     *
     * @return The wrapped entity
     */
    public PatientBasedTherapistConstraint getEntity() {
        return this.entity;
    }

    /**
     * Gets the {@link ch.camorcare.backoffice.adapter.TherapyTypeAdapter} that wraps {@link ch.camorcare.backoffice.entities.TherapyType} stored in this wrapper
     *
     * @return The wrapped TherapyType
     */
    public TherapyTypeAdapter getTherapyTypeAdapter() {
        return therapyTypeAdapter;
    }

    /**
     * Gets the {@link ch.camorcare.backoffice.adapter.TherapistAdapter} that wraps {@link ch.camorcare.backoffice.entities.Therapist} stored in this wrapper
     *
     * @return The wrapped Therapist
     */
    public TherapistAdapter getTherapistAdapter() {
        return therapistAdapter;
    }
}
