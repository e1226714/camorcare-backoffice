package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.TherapyRoom;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.TherapyRoom} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class TherapyRoomAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private TherapyRoom entity;

    /**
     * String property for name
     */
    private StringProperty nameProperty;

    /**
     * Adapter for capacity
     */
    private IntegerProperty capacityProperty;

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.TherapyRoomAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public TherapyRoomAdapter(TherapyRoom entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.TherapyRoom}, which is wrapped by this
     * adapter.
     *
     * @return The wrapped entity
     */
    public TherapyRoom getEntity() {
        return entity;
    }

    /**
     * Sets the {@link ch.camorcare.backoffice.entities.TherapyRoom} name in the entity and updates the {@link
     * javafx.beans.property.StringProperty}.
     *
     * @param name The name to be set and wrapped
     */
    public void setName(String name) {
        this.entity.setName(name);
        this.nameProperty.set(name);
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.entities.TherapyRoom} name as a {@link
     * javafx.beans.property.StringProperty}.
     *
     * @return nameProperty The wrapped name
     */
    public StringProperty getNameProperty() {
        if (nameProperty == null) {
            nameProperty = new SimpleStringProperty(entity.getName());
        }
        return nameProperty;
    }

    /**
     * Sets the {@link ch.camorcare.backoffice.entities.TherapyRoom} capacity in the entity and updates the {@link
     * javafx.beans.property.IntegerProperty}.
     *
     * @param capacity The capacity to be set and wrapped
     */
    public void setCapacity(int capacity) {
        this.entity.setCapacity(capacity);
        this.capacityProperty.set(capacity);
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.entities.TherapyRoom} capacity as a {@link
     * javafx.beans.property.IntegerProperty}.
     *
     * @return capacityProperty The wrapped capacity
     */
    public IntegerProperty getCapacityProperty() {
        if (capacityProperty == null) {
            capacityProperty = new SimpleIntegerProperty(entity.getCapacity());
        }
        return capacityProperty;
    }

    @Override
    public String toString() {
        return entity.getName();
    }
}
