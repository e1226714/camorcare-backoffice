package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint;
import ch.camorcare.backoffice.entities.type.Gender;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint} DTO. It
 * complements it with ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class PatientBasedTherapistGenderConstraintAdapter {

    /**
     * The entity wrapped by this adapter.
     */
    private PatientBasedTherapistGenderConstraint entity;

    /**
     * The adapter that wraps a TherapyType
     */
    private TherapyTypeAdapter therapyTypeAdapter;

    /**
     * The wrapped Gender
     */
    private ObjectProperty<Gender> genderProperty;

    /**
     * Returns a new {@link ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint}
     *
     * @param entity The entity to be wrapped
     */
    public PatientBasedTherapistGenderConstraintAdapter(PatientBasedTherapistGenderConstraint entity) {
        this.entity = entity;
        this.therapyTypeAdapter = new TherapyTypeAdapter(entity.getTherapyType());
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint} stored in this wrapper
     *
     * @return The wrapped entity
     */
    public PatientBasedTherapistGenderConstraint getEntity() {
        return this.entity;
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.adapter.TherapyTypeAdapter} stored in this wrapper
     *
     * @return The wrapped TherapyType
     */
    public TherapyTypeAdapter getTherapyTypeAdapter() {
        return therapyTypeAdapter;
    }

    /**
     * Returns the {@link javafx.beans.property.ObjectProperty} that wraps {@link ch.camorcare.backoffice.entities.type.Gender}
     * stored in this wrapper
     *
     * @return The wrapped Gender
     */
    public ObjectProperty<Gender> getGenderProperty() {
        if (genderProperty == null) {
            genderProperty = new SimpleObjectProperty<>(entity.getGender());
        }
        return genderProperty;
    }
}
