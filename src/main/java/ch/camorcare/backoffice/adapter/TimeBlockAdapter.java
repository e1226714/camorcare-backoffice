package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.entities.type.Day;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.joda.time.DateTime;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.TimeBlock} DTO. It
 * complements it with ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class TimeBlockAdapter {

    /**
     * The entity wrapped by the adapter
     */
    private TimeBlock entity;

    /**
     * The String property for the day
     */
    private StringProperty dayProperty;

    /**
     * The object property for the time start
     */
    private ObjectProperty<DateTime> timeStartProperty;

    /**
     * Returns a new {@link ch.camorcare.backoffice.adapter.TimeBlockAdapter}
     *
     * @param entity The entity to be wrapped
     */
    public TimeBlockAdapter(TimeBlock entity) {
        this.entity = entity;
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.entities.type.Day} as a {@link javafx.beans.property.StringProperty}
     *
     * @return The wrapped Day
     */
    public StringProperty getDayProperty() {
        if(dayProperty == null) {
            dayProperty = new SimpleStringProperty(entity.getDay().toString());
        }
        return dayProperty;
    }

    /**
     * Sets the {@link ch.camorcare.backoffice.entities.type.Day} as a {@link javafx.beans.property.StringProperty}
     *
     * @param day The Day to be wrapped
     */
    public void setDay(String day) {
        this.entity.setDay(Day.valueOf(day));
        this.dayProperty.set(day);
    }

    /**
     * Gets the starting time of the block as an {@link javafx.beans.property.ObjectProperty} that wraps a {@link
     * org.joda.time.DateTime} object
     *
     * @return The wrapped DateTime
     */
    public ObjectProperty<DateTime> getTimeStartProperty() {
        if(timeStartProperty == null) {
            timeStartProperty = new SimpleObjectProperty<>(entity.getTimeStart());
        }
        return timeStartProperty;
    }

    /**
     * Sets and wraps a {@link org.joda.time.DateTime} as an {@link javafx.beans.property.ObjectProperty}
     *
     * @param timeStart The DateTime to be wrapped
     */
    public void setTimeStart(DateTime timeStart) {
        this.entity.setTimeStart(timeStart);
        this.timeStartProperty.set(timeStart);
    }
}
