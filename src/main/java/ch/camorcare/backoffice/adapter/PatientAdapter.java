package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.Address;
import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.type.Gender;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.joda.time.DateTime;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.Patient} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class PatientAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private Patient entity;

    /**
     * String property for first name
     */
    private StringProperty firstNameProperty;

    /**
     * String property for last name
     */
    private StringProperty lastNameProperty;

    /**
     * String property for the name
     */
    private StringProperty nameProperty;

    /**
     * Object property for the gender
     */
    private ObjectProperty<Gender> genderProperty;

    /**
     * Object property for the birthday
     */
    private ObjectProperty<DateTime> birthdayProperty;

    /**
     * Address adapter for the address to deliver its attributes as properties
     */
    private AddressAdapter addressAdapter;

    /**
     * String property for social security number
     */
    private StringProperty socialSecurityNumberProperty;

    /**
     * String property for telephone number
     */
    private StringProperty phoneProperty;

    /**
     * String property for the email address
     */
    private StringProperty emailProperty;

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.PatientAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public PatientAdapter(Patient entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity, which is wrapped by this adapter.
     *
     * @return The wrapped entity
     */
    public Patient getEntity() {
        return entity;
    }

    /**
     * Sets the patient first name in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param firstName The first name to be wrapped
     */
    public void setFirstName(String firstName) {
        this.entity.setFirstName(firstName);
        this.firstNameProperty.set(firstName);
    }

    /**
     * Returns the patient first name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return firstNameProperty The wrapped first name
     */
    public StringProperty getFirstNameProperty() {
        if (firstNameProperty == null) {
            firstNameProperty = new SimpleStringProperty(entity.getFirstName());
        }
        return firstNameProperty;
    }

    /**
     * Sets the patient last name in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param lastName The last name to be wrapped
     */
    public void setLastName(String lastName) {
        this.entity.setLastName(lastName);
        this.lastNameProperty.set(lastName);
    }

    /**
     * Returns the patient last name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return lastNameProperty The wrapped last name
     */
    public StringProperty getLastNameProperty() {
        if (lastNameProperty == null) {
            lastNameProperty = new SimpleStringProperty(entity.getLastName());
        }
        return lastNameProperty;
    }

    /**
     * Returns the patient's full name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return nameProperty The wrapped full name
     */
    public StringProperty getNameProperty() {
        if (nameProperty == null) {
            nameProperty = new SimpleStringProperty(entity.getLastName() + " " + entity.getFirstName());
        }
        return nameProperty;
    }

    /**
     * Sets the patient {@link ch.camorcare.backoffice.entities.type.Gender} in the entity and updates the {@link
     * javafx.beans.property.ObjectProperty}.
     *
     * @param gender The gender to be wrapped
     */
    public void setGender(Gender gender) {
        this.entity.setGender(gender);
        this.genderProperty.set(gender);
    }

    /**
     * Returns the patient {@link ch.camorcare.backoffice.entities.type.Gender} as a {@link
     * javafx.beans.property.ObjectProperty}.
     *
     * @return genderProperty The wrapped gender
     */
    public ObjectProperty<Gender> getGenderProperty() {
        if (genderProperty == null) {
            genderProperty = new SimpleObjectProperty<Gender>(entity.getGender());
        }
        return genderProperty;
    }

    /**
     * Sets the patient birthday of type {@link org.joda.time.DateTime} in the entity and updates the {@link
     * javafx.beans.property.ObjectProperty}.
     *
     * @param birthday The birthday to be wrapped
     */
    public void setBirthday(DateTime birthday) {
        this.entity.setBirthdate(birthday);
        this.birthdayProperty.set(birthday);
    }

    /**
     * Returns the patient birthday of type {@link org.joda.time.DateTime} as a {@link
     * javafx.beans.property.ObjectProperty}.
     *
     * @return birthdayProperty The wrapped birthday
     */
    public ObjectProperty<DateTime> getBirthdayProperty() {
        if (birthdayProperty == null) {
            birthdayProperty = new SimpleObjectProperty<DateTime>(entity.getBirthdate());
        }
        return birthdayProperty;
    }

    /**
     * Sets the patient {@link ch.camorcare.backoffice.entities.Address} in the entity and updates the {@link
     * ch.camorcare.backoffice.adapter.AddressAdapter}.
     *
     * @param address The address to be wrapped
     */
    public void setAddress(Address address) {
        this.entity.setAddress(address);
        this.addressAdapter = new AddressAdapter(address);
    }

    /**
     * Returns the patient {@link ch.camorcare.backoffice.entities.Address} as an {@link
     * ch.camorcare.backoffice.adapter.AddressAdapter}.
     *
     * @return addressAdapter The wrapped address
     */
    public AddressAdapter getAddressAdapter() {
        if (addressAdapter == null) {
            addressAdapter = new AddressAdapter(entity.getAddress());
        }
        return addressAdapter;
    }

    /**
     * Sets the patient social security number in the entity and updates the {@link
     * javafx.beans.property.StringProperty}.
     *
     * @param socialSecurityNumber The social security number to be wrapped
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.entity.setSocialSecurityNumber(socialSecurityNumber);
        this.socialSecurityNumberProperty.set(socialSecurityNumber);
    }

    /**
     * Returns the patient social security number as a {@link javafx.beans.property.StringProperty}.
     *
     * @return socialSecurityNumberProperty The wrapped social security number
     */
    public StringProperty getSocialSecurityNumberProperty() {
        if (socialSecurityNumberProperty == null) {
            socialSecurityNumberProperty = new SimpleStringProperty(entity.getSocialSecurityNumber());
        }
        return socialSecurityNumberProperty;
    }

    /**
     * Sets the patient phone number in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param phone The phone number to be wrapped
     */
    public void setPhone(String phone) {
        this.entity.setPhone(phone);
        this.phoneProperty.set(phone);
    }

    /**
     * Returns the patient phone number as a {@link javafx.beans.property.StringProperty}.
     *
     * @return phoneProperty The wrapped phone number
     */
    public StringProperty getPhoneProperty() {
        if (phoneProperty == null) {
            phoneProperty = new SimpleStringProperty(entity.getPhone());
        }
        return phoneProperty;
    }

    /**
     * Sets the patient email in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param email The email to be wrapped
     */
    public void setEmail(String email) {
        this.entity.setEmail(email);
        this.emailProperty.set(email);
    }

    /**
     * Returns the patient email as a {@link javafx.beans.property.StringProperty}.
     *
     * @return emailProperty The wrapped email
     */
    public StringProperty getEmailProperty() {
        if (emailProperty == null) {
            emailProperty = new SimpleStringProperty(entity.getEmail());
        }
        return emailProperty;
    }

    @Override
    public String toString() {
        return entity.getLastName() + " " + entity.getFirstName();
    }
}
