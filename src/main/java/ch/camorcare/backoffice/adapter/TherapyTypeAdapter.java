package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyType;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.TherapyType} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class TherapyTypeAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private TherapyType entity;

    /**
     * String property for the name
     */
    private StringProperty nameProperty;

    /**
     * Boolean property to distinguish individual therapy
     */
    private BooleanProperty individualTherapyProperty;

    /**
     * Boolean therapy to distinguish group therapy
     */
    private BooleanProperty groupTherapyProperty;

    /**
     * Gets a list of TherapistAdapter objects
     */
    private List<TherapistAdapter> therapistsProperty;

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.TherapyTypeAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public TherapyTypeAdapter(TherapyType entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.TherapyType}, which is wrapped by this
     * adapter.
     *
     * @return The wrapped entity
     */
    public TherapyType getEntity() {
        return entity;
    }

    /**
     * Sets the {@link ch.camorcare.backoffice.entities.TherapyType} name in the entity and updates the {@link
     * javafx.beans.property.StringProperty}.
     *
     * @param name The name to be set and wrapped
     */
    public void setName(String name) {
        this.entity.setName(name);
        if (nameProperty == null) {
            nameProperty = new SimpleStringProperty(entity.getName());
        }
        this.nameProperty.set(name);
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.entities.TherapyType} name as a {@link
     * javafx.beans.property.StringProperty}.
     *
     * @return nameProperty The wrapped name
     */
    public StringProperty getNameProperty() {
        if (nameProperty == null) {
            nameProperty = new SimpleStringProperty(entity.getName());
        }

        return nameProperty;
    }

    /**
     * Sets the value in the entity and updates the {@link javafx.beans.property.BooleanProperty}.
     *
     * @param individualTherapy The value to be set and wrapped
     */
    public void setIndividualTherapy(boolean individualTherapy) {
        this.entity.setSuitableForIndividuals(individualTherapy);
        if (individualTherapyProperty == null) {
            individualTherapyProperty = new SimpleBooleanProperty(entity.isSuitableForIndividuals());
        }
        this.individualTherapyProperty.set(individualTherapy);
    }

    /**
     * Returns if the {@link ch.camorcare.backoffice.entities.TherapyType} is suitable for individual therapies.
     *
     * @return individualTherapyProperty <code>true</code> if suitable, else <code>false</code>
     */
    public BooleanProperty getIndividualTherapyProperty() {
        if (individualTherapyProperty == null) {
            individualTherapyProperty = new SimpleBooleanProperty(entity.isSuitableForIndividuals());
        }

        return individualTherapyProperty;
    }

    /**
     * Sets the value in the entity and updates the {@link javafx.beans.property.BooleanProperty}.
     *
     * @param groupTherapy
     */
    public void setGroupTherapy(boolean groupTherapy) {
        this.entity.setSuitableForGroups(groupTherapy);
        if (groupTherapyProperty == null) {
            groupTherapyProperty = new SimpleBooleanProperty(entity.isSuitableForGroups());
        }
        this.groupTherapyProperty.set(groupTherapy);
    }

    /**
     * Returns if the {@link ch.camorcare.backoffice.entities.TherapyType} is suitable for group therapies.
     *
     * @return individualTherapyProperty <code>true</code> if suitable, else <code>false</code>
     */
    public BooleanProperty getGroupTherapyProperty() {
        if (groupTherapyProperty == null) {
            groupTherapyProperty = new SimpleBooleanProperty(entity.isSuitableForGroups());
        }

        return groupTherapyProperty;
    }

    /**
     * Sets the list of {@link ch.camorcare.backoffice.entities.Therapist} objects in the entity and updates the list of
     * {@link ch.camorcare.backoffice.adapter.TherapistAdapter} objects.
     *
     * @param therapists The list of Therapist objects to be set and wrapped
     */
    public void setTherapists(List<Therapist> therapists) {
        this.entity.setTherapists(therapists);
        therapistsProperty = new ArrayList<TherapistAdapter>();

        for (Therapist therapist : therapists) {
            therapistsProperty.add(new TherapistAdapter(therapist));
        }
    }

    /**
     * Returns the list of {@link ch.camorcare.backoffice.entities.Therapist} objects as a list of {@link
     * ch.camorcare.backoffice.adapter.TherapyTypeAdapter} objects.
     *
     * @return therapistsProperty The list of wrapped Therapist objects
     */
    public List<TherapistAdapter> getTherapistsProperty() {
        if (therapistsProperty == null) {
            therapistsProperty = new ArrayList<TherapistAdapter>();

            for (Therapist therapist : entity.getTherapists()) {
                therapistsProperty.add(new TherapistAdapter(therapist));
            }
        }

        return therapistsProperty;
    }
}
