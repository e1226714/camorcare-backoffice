package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint}
 * DTO. It complements it with ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class TherapistBasedTherapyTypeOccurrenceConstraintAdapter {

    /**
     * The entity wrapped by this adapter
     */
    private TherapistBasedTherapyTypeOccurrenceConstraint entity;

    /**
     * The adapter that wraps the TherapyType
     */
    private TherapyTypeAdapter therapyTypeAdapter;

    /**
     * An ObjectProperty that wraps a TherapyMode
     */
    private ObjectProperty<TherapyMode> therapyModeProperty;

    /**
     * Integer property that wraps the maximum number of occurrences per day of a TherapyType
     */
    private IntegerProperty maxOccurrencePerDayProperty;

    /**
     * Returns a new {@link ch.camorcare.backoffice.adapter.TherapistBasedTherapyTypeOccurrenceConstraintAdapter}
     *
     * @param entity The entity to be wrapped
     */
    public TherapistBasedTherapyTypeOccurrenceConstraintAdapter(TherapistBasedTherapyTypeOccurrenceConstraint entity) {
        this.entity = entity;
        this.therapyTypeAdapter = new TherapyTypeAdapter(entity.getTherapyType());
    }

    /**
     * Gets the entity of type {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint}
     * stored within this adapter
     *
     * @return The wrapped entity
     */
    public TherapistBasedTherapyTypeOccurrenceConstraint getEntity() {
        return this.entity;
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.adapter.TherapyTypeAdapter} stored in this wrapper
     *
     * @return The wrapped TherapyType
     */
    public TherapyTypeAdapter getTherapyTypeAdapter() {
        return therapyTypeAdapter;
    }

    /**
     * Sets the {@link ch.camorcare.backoffice.adapter.TherapyTypeAdapter} stored in this wrapper
     *
     * @param therapyTypeAdapter The TherapyTypeAdapter to be stored
     */
    public void setTherapyTypeAdapter(TherapyTypeAdapter therapyTypeAdapter) {
        this.therapyTypeAdapter = therapyTypeAdapter;
    }

    /**
     * Gets the {@link javafx.beans.property.ObjectProperty} that wraps a {@link ch.camorcare.backoffice.entities.type.TherapyMode}
     *
     * @return The wrapped TherapyMode
     */
    public ObjectProperty<TherapyMode> getTherapyModeProperty() {
        if (therapyModeProperty == null) {
            therapyModeProperty = new SimpleObjectProperty<TherapyMode>(entity.getTherapyMode());
        }
        return therapyModeProperty;
    }

    /**
     * Sets the {@link javafx.beans.property.ObjectProperty} that wraps a {@link ch.camorcare.backoffice.entities.type.TherapyMode}
     *
     * @param therapyMode The TherapyMode to be wrapped
     */
    public void setTherapyModeProperty(TherapyMode therapyMode) {
        entity.setTherapyMode(therapyMode);
        if (therapyModeProperty == null) {
            therapyModeProperty = new SimpleObjectProperty<TherapyMode>(entity.getTherapyMode());
        }
        therapyModeProperty.set(therapyMode);
    }

    /**
     * Returns the maximum number of occurrences of a {@link ch.camorcare.backoffice.entities.TherapyType} as an {@link
     * javafx.beans.property.IntegerProperty}
     *
     * @return The wrapped maximum number of occurrences per day
     */
    public IntegerProperty getMaxOccurrencePerDayProperty() {
        if (maxOccurrencePerDayProperty == null) {
            maxOccurrencePerDayProperty = new SimpleIntegerProperty(entity.getMaxOccurrencePerDay());
        }
        return maxOccurrencePerDayProperty;
    }

    /**
     * Sets the maximum number of occurrences of a {@link ch.camorcare.backoffice.entities.TherapyType} as an {@link
     * javafx.beans.property.IntegerProperty}
     *
     * @param maxOccurrencePerDay The number of occurrences per day to be wrapped
     */
    public void setMaxOccurrencePerDayProperty(int maxOccurrencePerDay) {
        entity.setMaxOccurrencePerDay(maxOccurrencePerDay);
        if (maxOccurrencePerDayProperty == null) {
            maxOccurrencePerDayProperty = new SimpleIntegerProperty(entity.getMaxOccurrencePerDay());
        }
        maxOccurrencePerDayProperty.set(maxOccurrencePerDay);
    }
}
