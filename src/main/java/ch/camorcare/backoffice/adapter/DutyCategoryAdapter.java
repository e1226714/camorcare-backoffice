package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.DutyCategory;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.DutyCategory} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class DutyCategoryAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private DutyCategory entity;

    /**
     * String property for the name
     */
    private StringProperty nameProperty;

    /**
     * Integer property for the duraton
     */
    private IntegerProperty durationProperty;

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.DutyCategoryAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public DutyCategoryAdapter(DutyCategory entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.DutyCategory}, which is wrapped by this adapter.
     *
     * @return The wrapped entity
     */
    public DutyCategory getEntity() {
        return entity;
    }

    /**
     * Sets the DutyCategory name in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param name The name to be wrapped
     */
    public void setName(String name) {
        this.entity.setName(name);
        this.nameProperty.set(name);
    }

    /**
     * Returns the DutyCategory name as a {@link javafx.beans.property.StringProperty}.
     *
     * @return nameProperty The wrapped name
     */
    public StringProperty getNameProperty() {
        if (nameProperty == null) {
            nameProperty = new SimpleStringProperty(entity.getName());
        }
        return nameProperty;
    }

    /**
     * Sets the DutyCategory duration in the entity and updates the {@link javafx.beans.property.IntegerProperty}.
     *
     * @param duration The duration to be wrapped
     */
    public void setDuration(int duration) {
        this.entity.setDuration(duration);
        this.durationProperty.set(duration);
    }

    /**
     * Returns the DutyCategory duration as a {@link javafx.beans.property.IntegerProperty}.
     *
     * @return durationProperty The wrapped duration
     */
    public IntegerProperty getDurationProperty() {
        if (durationProperty == null) {
            durationProperty = new SimpleIntegerProperty(entity.getDuration());
        }
        return durationProperty;
    }
}
