package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class TherapistBasedTimeBlockConstraintAdapter {

    /**
     * The entity wrapped by this adapter
     */
    private TherapistBasedTimeBlockConstraint entity;

    /**
     * The adapter that wraps a TimeBlock
     */
    private TimeBlockAdapter timeBlockAdapter;

    /**
     * Returns a new {@link ch.camorcare.backoffice.adapter.TherapistBasedTimeBlockConstraintAdapter}
     *
     * @param entity The entity to be wrapped
     */
    public TherapistBasedTimeBlockConstraintAdapter(TherapistBasedTimeBlockConstraint entity) {
        this.entity = entity;
        this.timeBlockAdapter = new TimeBlockAdapter(entity.getTimeBlock());
    }

    /**
     * Gets the {@link ch.camorcare.backoffice.adapter.TimeBlockAdapter} stored in this wrapper
     *
     * @return The wrapped TimeBlock
     */
    public TimeBlockAdapter getTimeBlockAdapter() {
        return timeBlockAdapter;
    }

    /**
     * Sets a {@link ch.camorcare.backoffice.adapter.TimeBlockAdapter} to be stored in the wrapper
     *
     * @param timeBlockAdapter The adapter stored in this wrapper
     */
    public void setTimeBlockAdapter(TimeBlockAdapter timeBlockAdapter) {
        this.timeBlockAdapter = timeBlockAdapter;
    }
}
