package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.Address;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.Address} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class AddressAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private Address entity;

    /**
     * StringProperty for the street.
     */
    private StringProperty streetProperty;

    /**
     * StringProperty for the zip code.
     */
    private StringProperty zipProperty;

    /**
     * StringProperty for the city.
     */
    private StringProperty cityProperty;

    /**
     * StringProperty for the country code.
     */
    private StringProperty countryCodeProperty;

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.AddressAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public AddressAdapter(Address entity) {
        this.entity = entity;
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.entities.Address} entity, which is wrapped by this adapter.
     *
     * @return The wrapped entity
     */
    public Address getEntity() {
        return entity;
    }

    /**
     * Sets the street in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param street The street to be wrapped
     */
    public void setStreet(String street) {
        this.entity.setStreet(street);
        if (streetProperty == null) {
            streetProperty = new SimpleStringProperty(entity.getStreet());
        }
        this.streetProperty.set(street);
    }

    /**
     * Returns the street as a {@link javafx.beans.property.StringProperty}.
     *
     * @return firstNameProperty The wrapped first name
     */
    public StringProperty getStreetProperty() {
        if (streetProperty == null) {
            streetProperty = new SimpleStringProperty(entity.getStreet());
        }
        return streetProperty;
    }

    /**
     * Sets the zip in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param zip The zip to be wrapped
     */
    public void setZip(String zip) {
        this.entity.setZip(zip);
        if (zipProperty == null) {
            zipProperty = new SimpleStringProperty(entity.getZip());
        }
        this.zipProperty.set(zip);
    }

    /**
     * Returns the zip as a {@link javafx.beans.property.StringProperty}.
     *
     * @return zipProperty The wrapped zip
     */
    public StringProperty getZipProperty() {
        if (zipProperty == null) {
            zipProperty = new SimpleStringProperty(entity.getZip());
        }
        return zipProperty;
    }

    /**
     * Sets the city in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param city The city to be wrapped
     */
    public void setCity(String city) {
        this.entity.setCity(city);
        if (cityProperty == null) {
            cityProperty = new SimpleStringProperty(entity.getCity());
        }
        this.cityProperty.set(city);
    }

    /**
     * Returns the city as a {@link javafx.beans.property.StringProperty}.
     *
     * @return cityProperty The wrapped city
     */
    public StringProperty getCityProperty() {
        if (cityProperty == null) {
            cityProperty = new SimpleStringProperty(entity.getCity());
        }
        return cityProperty;
    }

    /**
     * Sets the country code in the entity and updates the {@link javafx.beans.property.StringProperty}.
     *
     * @param countryCode The country code to be wrapped
     */
    public void setCountryCode(String countryCode) {
        this.entity.setCountryCode(countryCode);
        if (countryCodeProperty == null) {
            countryCodeProperty = new SimpleStringProperty(entity.getCountryCode());
        }
        this.countryCodeProperty.set(countryCode);
    }

    /**
     * Returns the country code as a {@link javafx.beans.property.StringProperty}.
     *
     * @return countryCodeProperty The wrapped country code
     */
    public StringProperty getCountryCodeProperty() {
        if (countryCodeProperty == null) {
            countryCodeProperty = new SimpleStringProperty(entity.getCountryCode());
        }

        return countryCodeProperty;
    }
}