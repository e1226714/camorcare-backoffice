package ch.camorcare.backoffice.adapter;

import ch.camorcare.backoffice.entities.*;
import javafx.beans.property.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * This is an adapter for the {@link ch.camorcare.backoffice.entities.TherapyEvent} DTO. It complements it with
 * ObjectProperties necessary for the data binding in the frontend.
 * <p/>
 * For more information on adapters, @see  <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Wikipedia: Adapter
 * Pattern</a>
 */
public class TherapyEventAdapter {

    /**
     * Contains the entity, which is wrapped by this adapter.
     */
    private TherapyEvent entity;

    /**
     * Adapter for a Therapist
     */
    private TherapistAdapter therapistProperty;

    /**
     * Adapter for a TherapyType
     */
    private TherapyTypeAdapter therapyTypeProperty;

    /**
     * Adapter for a TherapyRoom
     */
    private TherapyRoomAdapter therapyRoomProperty;

    /**
     * Adapter for a TimeBlock
     */
    private TimeBlockAdapter therapyTimeProperty;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapyEventAdapter.class.getName());

    /**
     * Creates a new {@link ch.camorcare.backoffice.adapter.TherapyEventAdapter}.
     *
     * @param entity The entity to be wrapped
     */
    public TherapyEventAdapter(TherapyEvent entity) {
        this.entity = entity;
    }

    /**
     * Returns the entity of type {@link ch.camorcare.backoffice.entities.TherapyEvent}, which is wrapped by this
     * adapter.
     *
     * @return The wrapped entity
     */
    public TherapyEvent getEntity() {
        return entity;
    }

    /**
     * Sets the {@link ch.camorcare.backoffice.entities.Therapist} in the entity and updates the {@link
     * ch.camorcare.backoffice.adapter.TherapistAdapter}.
     *
     * @param therapistAdapter The Therapist to be wrapped
     */
    public void setTherapist(TherapistAdapter therapistAdapter) {
        this.entity.setTherapist(therapistAdapter.getEntity());
        this.therapistProperty = therapistAdapter;
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.entities.Therapist} as a {@link ch.camorcare.backoffice.adapter.TherapistAdapter}.
     *
     * @return therapistProperty The wrapped Therapist
     */
    public TherapistAdapter getTherapistProperty() {
        if (therapistProperty == null) {
            therapistProperty = new TherapistAdapter(entity.getTherapist());
        }

        return therapistProperty;
    }

    /**
     * Sets the {@link ch.camorcare.backoffice.entities.TherapyType} in the entity and updates the {@link
     * ch.camorcare.backoffice.adapter.TherapyTypeAdapter}.
     *
     * @param therapyType The TherapyType to be wrapped
     */
    public void setTherapyType(TherapyType therapyType) {
        this.entity.setTherapyType(therapyType);
        this.therapyTypeProperty = new TherapyTypeAdapter(therapyType);
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.entities.TherapyType} as a {@link
     * ch.camorcare.backoffice.adapter.TherapyTypeAdapter}.
     *
     * @return therapyTypeProperty The wrapped TherapyType
     */
    public TherapyTypeAdapter getTherapyTypeProperty() {
        if (therapyTypeProperty == null) {
            therapyTypeProperty = new TherapyTypeAdapter(entity.getTherapyType());
        }

        return therapyTypeProperty;
    }

    /**
     * Sets the {@link ch.camorcare.backoffice.entities.TherapyRoom} in the entity and updates the {@link
     * ch.camorcare.backoffice.adapter.TherapyRoomAdapter}.
     *
     * @param therapyRoom The TherapyRoom to be wrapped
     */
    public void setTherapyRoom(TherapyRoom therapyRoom) {
        this.entity.setTherapyRoom(therapyRoom);
        this.therapyRoomProperty = new TherapyRoomAdapter(therapyRoom);
    }

    /**
     * Returns the {@link ch.camorcare.backoffice.entities.TherapyRoom} as a {@link
     * ch.camorcare.backoffice.adapter.TherapyRoomAdapter}.
     *
     * @return therapyRoomProperty The wrapped TherapyRoom
     */
    public TherapyRoomAdapter getTherapyRoomProperty() {
        if (therapyRoomProperty == null) {
            therapyRoomProperty = new TherapyRoomAdapter(entity.getTherapyRoom());
        }

        return therapyRoomProperty;
    }

    /**
     * Sets the therapyTime of type {@link ch.camorcare.backoffice.entities.TimeBlock} in the entity and updates the
     * {@link ch.camorcare.backoffice.adapter.TimeBlockAdapter}.
     *
     * @param therapyTime The TimeBlock to be wrapped
     */
    public void setTherapyTime(TimeBlock therapyTime) {
        this.entity.setTherapyTimeBlock(therapyTime);
        this.therapyTimeProperty = new TimeBlockAdapter(therapyTime);
    }

    /**
     * Returns the therapyTime of type {@link ch.camorcare.backoffice.entities.TimeBlock} as a {@link
     * ch.camorcare.backoffice.adapter.TimeBlockAdapter}.
     *
     * @return therapyTimeProperty The wrapped TimeBlock
     */
    public TimeBlockAdapter getTherapyTimeProperty() {
        if (therapyTimeProperty == null) {
            therapyTimeProperty = new TimeBlockAdapter(entity.getTherapyTimeBlock());
        }

        return therapyTimeProperty;
    }

    /**
     * Returns the name of the {@link ch.camorcare.backoffice.entities.TherapyType} of this {@link
     * ch.camorcare.backoffice.entities.TherapyEvent} as a {@link javafx.beans.property.StringProperty}
     *
     * @return The wrapped TherapyType name
     */
    public StringProperty getTherapyName() {
        //log.debug(entity.getTherapyType().getName());

        return new SimpleStringProperty(entity.getTherapyType().getName());
    }

    /**
     * Returns if there is a {@link ch.camorcare.backoffice.entities.Therapist} assigned for this event as a {@link
     * javafx.beans.property.BooleanProperty}
     *
     * @return <code>true</code> if therapist assigned, else <code>false</code>
     */
    public BooleanProperty isTherapistAssigned() {
        if (entity.getTherapist() == null) {
            return new SimpleBooleanProperty(false);
        }
        //log.debug( entity.getTherapist().getFirstName());

        return new SimpleBooleanProperty(true);
    }

    /**
     * Returns the number of participants for this event as an {@link javafx.beans.property.IntegerProperty}
     *
     * @return The number of participants
     */
    public IntegerProperty getParticipantNumber() {
        List<Therapy> cl = entity.getTherapies();
        return new SimpleIntegerProperty(cl.size());
    }

    /**
     * Returns if the {@link ch.camorcare.backoffice.entities.TherapyEvent} is already allocated as a {@link
     * javafx.beans.property.BooleanProperty}
     *
     * @return <code>true</code> if allocated, else <code>false</code>
     */
    public BooleanProperty isAllocated() {
        if (entity.getTherapyTimeBlock() == null) {
            return new SimpleBooleanProperty(false);
        }

        return new SimpleBooleanProperty(true);
    }
}
