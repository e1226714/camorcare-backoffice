package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.BasicDTO} data access object implementations.
 */
public interface BasicDAO<DTO> {

    /**
     * Creates aan entry.
     *
     * @param toCreate The DTO to be inserted into the database
     * @return The DTO inserted into the database along with the generated key
     * @throws ch.camorcare.backoffice.persistence.exception.PersistenceException
     */
    public DTO create(DTO toCreate) throws PersistenceException, ValidationException;

    /**
     * Modifies an entry.
     *
     * @param toUpdate The DTO to be updated
     * @throws ch.camorcare.backoffice.persistence.exception.PersistenceException
     */
    public DTO update(DTO toUpdate) throws PersistenceException, ValidationException;

    /**
     * Deletes an entry.
     *
     * @param toDelete The DTO to be marked for deletion from the database
     * @throws ch.camorcare.backoffice.persistence.exception.PersistenceException
     */
    public void delete(DTO toDelete) throws PersistenceException;

    /**
     * Returns a list of all entry (including deleted ones).
     *
     * @return all duty categories
     */
    public List<DTO> findAll() throws PersistenceException;

    /**
     * Returns a single entry out of the database if the DTO exists, else null
     *
     * @param toFind The DTO to be found
     * @return a single entry
     */
    public DTO findOneById(DTO toFind) throws PersistenceException;

}
