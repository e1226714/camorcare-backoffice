package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;
import java.util.Map;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint} data access
 * object
 * implementations.
 */
public interface PatientBasedTherapyTypeOccurrenceConstraintDAO extends BasicDAO<PatientBasedTherapyTypeOccurrenceConstraint> {

    /**
     * Deletes all {@link ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint} entries in the
     * database that belong to a specific {@link ch.camorcare.backoffice.entities.Patient}.
     *
     * @param patient The Patient used in the search function
     * @throws PersistenceException
     */
    public void deleteAllByPatient(Patient patient) throws PersistenceException;

    /**
     * Returns a list of all {@link ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint}
     * related to a specific {@link ch.camorcare.backoffice.entities.Patient}.
     *
     * @param patient      The Patient by which filtering should take place
     * @param eagerLoading
     * @return A list of PatientBasedTherapyTypeOccurrenceConstraint objects
     * @throws ch.camorcare.backoffice.persistence.exception.PersistenceException
     */
    public List<PatientBasedTherapyTypeOccurrenceConstraint> findAllByPatient(Patient patient, boolean eagerLoading) throws PersistenceException;

    /**
     * Returns the statistics by a specific {@link ch.camorcare.backoffice.entities.TherapyPlan}.
     *
     * @param therapyPlan The TherapyPlan by which filtering should take place
     * @return The statistics in the database
     * @throws PersistenceException
     */
    public Map<TherapyType, Map<String, Integer>> findAllStatsByTherapyPlanGroupedByTherapyType(TherapyPlan therapyPlan) throws PersistenceException;

    /**
     * Searches the database for all {@link ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint}
     * objects by a specific {@link ch.camorcare.backoffice.entities.TherapyPlan}.
     *
     * @param therapyPlan The TherapyPlan by which filtering should be done
     * @return The list of PatientBasedTherapyTypeOccurrenceConstraint objects
     * @throws PersistenceException
     */
    public List<PatientBasedTherapyTypeOccurrenceConstraint> findAllByTherapyPlanGroupedByTherapyType(TherapyPlan therapyPlan) throws PersistenceException;

}
