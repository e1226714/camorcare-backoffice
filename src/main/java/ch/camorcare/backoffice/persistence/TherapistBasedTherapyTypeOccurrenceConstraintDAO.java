package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint} data access
 * object implementations.
 */
public interface TherapistBasedTherapyTypeOccurrenceConstraintDAO extends BasicDAO<TherapistBasedTherapyTypeOccurrenceConstraint> {

    /**
     * Returns a list of all {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint}
     * entries by a specific {@link ch.camorcare.backoffice.entities.Therapist}.
     *
     * @return A list of TherapistBasedTherapyTypeOccurrenceConstraint objects
     */
    public List<TherapistBasedTherapyTypeOccurrenceConstraint> findAllByTherapist(Therapist therapist) throws PersistenceException;

    /**
     * Deletes all {@link ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint} objects stored
     * in the database that belong to a specific {@link ch.camorcare.backoffice.entities.Therapist}.
     *
     * @param therapist The Therapist by which the deletions should take place
     * @throws PersistenceException
     */
    public void deleteByTherapist(Therapist therapist) throws PersistenceException;

    public List<TherapistBasedTherapyTypeOccurrenceConstraint> findAllByTherapyType(TherapyType therapyType) throws PersistenceException;
}
