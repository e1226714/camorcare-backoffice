package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Invoice;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.Invoice} data access object implementations.
 */
public interface InvoiceDAO extends BasicDAO<Invoice> {
}
