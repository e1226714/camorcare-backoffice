package ch.camorcare.backoffice.persistence.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Manager for the H2 Database Connections.
 */
public class ConnectionManager {

    /**
     * Contains the database connection.
     */
    private String dbName;
    private boolean autocommit;
    private Connection connection;
    private Logger log = LogManager.getLogger(ConnectionManager.class.getName());

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public void setAutocommit(boolean autocommit) {
        this.autocommit = autocommit;
    }

    /**
     * Returns the connection to the database.
     *
     * @return connection to the database
     * @throws java.sql.SQLException
     */
    public Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            try {
                Class.forName("org.h2.Driver");
            } catch (Exception e) {
                return null;
            }
            log.debug("Connecting to database: '" + dbName + "' - autocommit is " + autocommit);
            connection = DriverManager.getConnection("jdbc:h2:~/" + dbName, "camorcare", "n3n1M0q(H*hu");
            connection.setAutoCommit(autocommit);
        }
        return connection;
    }

    public void close() throws SQLException {
        if (connection != null) {
            if (!connection.isClosed()) {
                log.debug("closing connection to database: " + dbName);
                connection.close();
            }
            connection = null;
        }

    }

    public void rollback() throws SQLException {
        log.debug("rollback on database: " + dbName);
        getConnection().rollback();
    }
}
