package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Allergy;
import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.Allergy} data access object implementations.
 */
public interface AllergyDAO extends BasicDAO<Allergy> {
    /**
     * Finds all of the allergies specific to a {@link ch.camorcare.backoffice.entities.Patient} and return them as a
     * list of {@link ch.camorcare.backoffice.entities.Allergy} objects.
     *
     * @param patient The Patient by which all allergy rows should be filtered
     * @return A list of Allergy objects that belong to a specific Patient
     * @throws PersistenceException
     */
    public List<Allergy> findAllByPatient(Patient patient) throws PersistenceException;

    /**
     * Deletes all of the allergies stored in the database by a specific {@link ch.camorcare.backoffice.entities.Patient}.
     *
     * @param patient The Patient by which all of the allergy rows should be deleted
     * @throws PersistenceException
     */
    public void deleteByPatient(Patient patient) throws PersistenceException;
}
