package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.PatientContact;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.PatientContact} data access object implementations.
 */
public interface PatientContactDAO extends BasicDAO<PatientContact> {

}
