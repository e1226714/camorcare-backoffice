package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.DutyCategory;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.DutyCategory} data access object implementations.
 */
public interface DutyCategoryDAO extends BasicDAO<DutyCategory> {

}
