package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Address;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.Address} data access object implementations.
 */
public interface AddressDAO extends BasicDAO<Address> {

}
