package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;
import java.util.Map;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.TimeBlock} data access object implementations.
 */
public interface TimeBlockDAO extends BasicDAO<TimeBlock> {

    /**
     * Finds all {@link ch.camorcare.backoffice.entities.TimeBlock} objects stored in the database and groups them by
     * {@link ch.camorcare.backoffice.entities.type.Day}.
     *
     * @return A map of Days and the lists of TimeBlocks
     * @throws PersistenceException
     */
    public Map<Day, List<TimeBlock>> findAllGroupedByDay() throws PersistenceException;

    /**
     * Returns a list of all {@link ch.camorcare.backoffice.entities.TimeBlock} obejcts for a specific {@link
     * ch.camorcare.backoffice.entities.type.Day}.
     *
     * @return A list of TimeBlock objects
     */
    public List<TimeBlock> findAllByDay(Day day) throws PersistenceException;

    /**
     * Returns a list of all {@link ch.camorcare.backoffice.entities.TimeBlock} objects stored
     * in the database that are not related to a specific therapist by a time block constraint.
     *
     * @return A list of TimeBlock objects
     * @param therapist
     * @throws PersistenceException
     */
    public List<TimeBlock> findAllNegativeByTherapist(Therapist therapist) throws PersistenceException;
}
