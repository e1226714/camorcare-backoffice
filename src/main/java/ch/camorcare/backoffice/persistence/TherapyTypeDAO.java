package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.TherapyType} data access object implementations.
 */
public interface TherapyTypeDAO extends BasicDAO<TherapyType> {

    /**
     * Finds all {@link ch.camorcare.backoffice.entities.TherapyType} objects for which a specific {@link
     * ch.camorcare.backoffice.entities.Therapist} is responsible.
     *
     * @param therapist The Therapist used in the search function
     * @return A list of TherapyType objects
     * @throws PersistenceException
     */
    public List<TherapyType> findAllByTherapist(Therapist therapist) throws PersistenceException;
}