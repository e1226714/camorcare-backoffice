package ch.camorcare.backoffice.persistence.exception;

/**
 * Defines an exception that occurs at the persistence level of the program.
 */
public class PersistenceException extends Exception {

    private static final long serialVersionUID = 1L;

    public PersistenceException() {
        super();
    }

    public PersistenceException(String string) {
        super(string);
    }
}
