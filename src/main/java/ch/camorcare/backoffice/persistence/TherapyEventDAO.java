package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.TherapyEvent} data access object implementations.
 */
public interface TherapyEventDAO extends BasicDAO<TherapyEvent> {

    /**
     * Establishes a participation relationship between a {@link ch.camorcare.backoffice.entities.TherapyEvent} and a
     * {@link ch.camorcare.backoffice.entities.Therapy} in a specific table.
     *
     * @param therapyEvent The TherapyEvent to be inserted into the table
     * @param therapy      The Therapy to be inserted into the table
     * @return <code>true</code> if insert successful, else <code>false</code>
     * @throws PersistenceException
     * @throws ValidationException
     */
    public boolean createParticipation(TherapyEvent therapyEvent, Therapy therapy) throws PersistenceException, ValidationException;

    /**
     * Removes a {@link ch.camorcare.backoffice.entities.TherapyEvent} and {@link ch.camorcare.backoffice.entities.Therapy}
     * pair from the table.
     *
     * @param therapyEvent The TherapyEvent to be removed
     * @param therapy      The Therapy to be removed
     * @return <code>true</code> if delete successful, else <code>false</code>
     * @throws PersistenceException
     * @throws ValidationException
     */
    public boolean deleteParticipation(TherapyEvent therapyEvent, Therapy therapy) throws PersistenceException, ValidationException;

    /**
     * Gets all of the {@link ch.camorcare.backoffice.entities.Therapy} objects stored in the table that participate in
     * a specific {@link ch.camorcare.backoffice.entities.TherapyEvent}.
     *
     * @param therapyEvent The TherapyEvent used to filter
     * @param eagerLoading <code>true</code> if eager loading should be applied, else <code>false</code>
     * @return A list of Therapy objects
     * @throws PersistenceException
     * @throws ValidationException
     */
    public List<Therapy> getParticipants(TherapyEvent therapyEvent, boolean eagerLoading) throws PersistenceException, ValidationException;

    /**
     * Finds all {@link ch.camorcare.backoffice.entities.TherapyEvent} objects in the database by a specific {@link
     * ch.camorcare.backoffice.entities.TherapyPlan}. If <code>eagerLoading</code> is <code>true</code>, all referenced
     * objects will be loaded as well.
     *
     * @param therapyPlan  The TherapyPlan object used for searching
     * @param eagerLoading <code>true</code> if eager loading should be applied, else <code>false</code>
     * @return A list of TherapyEvent objects
     * @throws PersistenceException
     * @throws ValidationException
     */
    public List<TherapyEvent> findAllByTherapyPlan(TherapyPlan therapyPlan, boolean eagerLoading) throws PersistenceException, ValidationException;

    /**
     * Returns a list of {@link ch.camorcare.backoffice.entities.TherapyEvent} objects related to a specific {@link
     * ch.camorcare.backoffice.entities.Therapist} and {@link ch.camorcare.backoffice.entities.TherapyPlan}. If
     * <code>eagerLoading</code> is <code>true</code>, all referenced objects will be loaded as well.
     *
     * @param therapist    The Therapist used in the search function
     * @param therapyPlan  The TherapyPlan used in the search function
     * @param eagerLoading <code>true</code> is eager loading should be applied, else <code>false</code>
     * @return A list of TherapyEvent objects
     * @throws PersistenceException
     * @throws ValidationException
     */
    public List<TherapyEvent> findAllByTherapistAndTherapyPlan(Therapist therapist, TherapyPlan therapyPlan, boolean eagerLoading) throws PersistenceException, ValidationException;

    /**
     * Returns a list of {@link ch.camorcare.backoffice.entities.TherapyEvent} objects related to a specific {@link
     * ch.camorcare.backoffice.entities.Therapy} and {@link ch.camorcare.backoffice.entities.TherapyPlan}. If
     * <code>eagerLoading</code> is <code>true</code>, all referenced objects will be loaded as well.
     *
     * @param therapy      The Therapy used in the search function
     * @param therapyPlan  The TherapyPlan used in the search function
     * @param eagerLoading <code>true</code> is eager loading should be applied, else <code>false</code>
     * @return A list of TherapyEvent objects
     * @throws PersistenceException
     * @throws ValidationException
     */
    public List<TherapyEvent> findAllByTherapyAndTherapyPlan(Therapy therapy, TherapyPlan therapyPlan, boolean eagerLoading) throws PersistenceException, ValidationException;

    /**
     * Returns a list of {@link ch.camorcare.backoffice.entities.TherapyEvent} objects related to a specific {@link
     * ch.camorcare.backoffice.entities.TherapyRoom} and {@link ch.camorcare.backoffice.entities.TherapyPlan}. If
     * <code>eagerLoading</code> is <code>true</code>, all referenced objects will be loaded as well.
     *
     * @param therapyRoom  The TherapyRoom used in the search function
     * @param therapyPlan  The TherapyPlan used in the search function
     * @param eagerLoading <code>true</code> is eager loading should be applied, else <code>false</code>
     * @return A list of TherapyEvent objects
     * @throws PersistenceException
     * @throws ValidationException
     */
    public List<TherapyEvent> findAllByTherapyRoomAndTherapyPlan(TherapyRoom therapyRoom, TherapyPlan therapyPlan, boolean eagerLoading) throws PersistenceException, ValidationException;

}
