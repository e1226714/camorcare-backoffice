package ch.camorcare.backoffice.persistence;

/**
 * The persistence package contains all of the data access object (DAO) interfaces, their implementations, the database
 * connection manager and the exception(s) specific to this layer of the program.
 * <p/>
 * Each DAO accesses a tables in the database specific to itself (or in some cases related) in order to retrieve the
 * database information in a form that can be represented by a data transfer object (DTO; see entities). Aside from the
 * necessary logic to validate all parameters and provide database access, these DAOs have no other function. Their sole
 * purpose is to interface with whatever database on which the program is built.
 */