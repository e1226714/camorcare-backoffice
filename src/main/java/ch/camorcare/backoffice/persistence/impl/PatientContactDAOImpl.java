package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientContact;
import ch.camorcare.backoffice.persistence.PatientContactDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the PatientContact table stored
 * in the database.
 */
public class PatientContactDAOImpl implements PatientContactDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(PatientContactDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<PatientContact> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<PatientContact> validator) {
        this.validator = validator;
    }

    @Override
    public PatientContact create(PatientContact toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);
        PatientContact copy = new PatientContact(toCreate);
        validator.validate(toCreate);
        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }
        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO PATIENT_CONTACT (PATIENT, FIRST_NAME, LAST_NAME, PHONE, EMAIL, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setInt(1, toCreate.getPatient().getId());
            preparedStatementCreate.setString(2, toCreate.getFirstName());
            preparedStatementCreate.setString(3, toCreate.getLastName());
            preparedStatementCreate.setString(4, toCreate.getPhone());
            preparedStatementCreate.setString(5, toCreate.getEmail());
            preparedStatementCreate.setTimestamp(6, ts);
            preparedStatementCreate.setTimestamp(7, ts);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public PatientContact update(PatientContact toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE patient_contact SET first_name = ?, last_name = ?, phone = ?, email = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementUpdate.setString(1, toUpdate.getFirstName());
            preparedStatementUpdate.setString(2, toUpdate.getLastName());
            preparedStatementUpdate.setString(3, toUpdate.getPhone());
            preparedStatementUpdate.setString(4, toUpdate.getEmail());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementUpdate.setTimestamp(5, ts);
            preparedStatementUpdate.setInt(6, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);
            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(PatientContact toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("PatientContact was already deleted.");
            throw new PersistenceException("PatientContact was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE patient_contact SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<PatientContact> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<PatientContact> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, patient, first_name, last_name, phone, email, time_created, time_modified FROM patient_contact WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));

                PatientContact contact = new PatientContact();
                contact.setId(rs.getInt(1));
                contact.setPatient(patient);
                contact.setFirstName(rs.getString(3));
                contact.setLastName(rs.getString(4));
                contact.setPhone(rs.getString(5));
                contact.setEmail(rs.getString(6));
                contact.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                contact.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(8)));

                result.add(contact);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public PatientContact findOneById(PatientContact toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);
        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        PatientContact result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, first_name, last_name, phone, email, time_created, time_modified, time_deleted, patient FROM patient_contact WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                Patient patient = new Patient();
                patient.setId(rs.getInt(9));

                result = new PatientContact();
                result.setId(rs.getInt(1));
                result.setPatient(patient);
                result.setFirstName(rs.getString(2));
                result.setLastName(rs.getString(3));
                result.setPhone(rs.getString(4));
                result.setEmail(rs.getString(5));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(8)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
