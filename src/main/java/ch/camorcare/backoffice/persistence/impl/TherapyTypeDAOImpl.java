package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.TherapyTypeDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the TherapyType table stored in
 * the database.
 */
public class TherapyTypeDAOImpl implements TherapyTypeDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAllByTherapist = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapyTypeDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<TherapyType> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<TherapyType> validator) {
        this.validator = validator;
    }

    @Override
    public TherapyType create(TherapyType toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);
        validator.validate(toCreate);

        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        TherapyType retVal = new TherapyType(toCreate);
        Timestamp now = new Timestamp(new java.util.Date().getTime());

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO therapy_type (name, individual_therapy, group_therapy, group_therapy_min_size, group_therapy_max_size, individual_therapist_fixed, group_therapist_fixed, time_created, time_modified, individual_therapy_duration, group_therapy_duration) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            preparedStatementCreate.setString(1, toCreate.getName());
            preparedStatementCreate.setBoolean(2, toCreate.isSuitableForIndividuals());
            preparedStatementCreate.setBoolean(3, toCreate.isSuitableForGroups());
            preparedStatementCreate.setInt(4, toCreate.getMinGroupSize());
            preparedStatementCreate.setInt(5, toCreate.getMaxGroupSize());
            preparedStatementCreate.setBoolean(6, toCreate.isIndividualTherapistFixed());
            preparedStatementCreate.setBoolean(7, toCreate.isGroupTherapistFixed());
            preparedStatementCreate.setTimestamp(8, now);
            preparedStatementCreate.setTimestamp(9, now);
            preparedStatementCreate.setInt(10, toCreate.getIndividualTherapyDuration());
            preparedStatementCreate.setInt(11, toCreate.getGroupTherapyDuration());

            log.debug("resulting statement: " + preparedStatementCreate);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                retVal.setId(keyResultSet.getInt(1));
                log.debug("resulting key: " + retVal.getId());
            }

            keyResultSet.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return retVal;
    }

    @Override
    public TherapyType update(TherapyType toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE therapy_type SET name = ?, individual_therapy = ?, group_therapy = ?, group_therapy_min_size = ?, group_therapy_max_size = ?, individual_therapist_fixed = ?, group_therapist_fixed = ?, time_modified = ?, individual_therapy_duration = ?, group_therapy_duration = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());

            preparedStatementUpdate.setString(1, toUpdate.getName());
            preparedStatementUpdate.setBoolean(2, toUpdate.isSuitableForIndividuals());
            preparedStatementUpdate.setBoolean(3, toUpdate.isSuitableForGroups());
            preparedStatementUpdate.setInt(4, toUpdate.getMinGroupSize());
            preparedStatementUpdate.setInt(5, toUpdate.getMaxGroupSize());
            preparedStatementUpdate.setBoolean(6, toUpdate.isIndividualTherapistFixed());
            preparedStatementUpdate.setBoolean(7, toUpdate.isGroupTherapistFixed());
            preparedStatementUpdate.setTimestamp(8, now);
            preparedStatementUpdate.setInt(9, toUpdate.getIndividualTherapyDuration());
            preparedStatementUpdate.setInt(10, toUpdate.getGroupTherapyDuration());
            preparedStatementUpdate.setInt(11, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);
            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(TherapyType toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("TherapyType was already deleted.");
            throw new PersistenceException("TherapyType was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE therapy_type SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, now);
            preparedStatementDelete.setTimestamp(2, now);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<TherapyType> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<TherapyType> result = new ArrayList<>();
        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, name, individual_therapy, group_therapy, group_therapy_min_size, group_therapy_max_size, individual_therapist_fixed, group_therapist_fixed, time_created, time_modified, time_deleted, individual_therapy_duration, group_therapy_duration FROM therapy_type WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(1));
                therapyType.setName(rs.getString(2));
                therapyType.setSuitableForIndividuals(rs.getBoolean(3));
                therapyType.setSuitableForGroups(rs.getBoolean(4));
                therapyType.setMinGroupSize(rs.getInt(5));
                therapyType.setMaxGroupSize(rs.getInt(6));
                therapyType.setIndividualTherapistFixed(rs.getBoolean(7));
                therapyType.setGroupTherapistFixed(rs.getBoolean(8));
                therapyType.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(9)));
                therapyType.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));
                therapyType.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(11)));
                therapyType.setIndividualTherapyDuration(rs.getInt(12));
                therapyType.setGroupTherapyDuration(rs.getInt(13));

                result.add(therapyType);
            }

            log.debug("found entries: " + result.size());

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TherapyType> findAllByTherapist(Therapist therapist) throws PersistenceException {
        log.debug("reading all entries");
        List<TherapyType> result = new ArrayList<>();
        try {
            if (preparedStatementFindAllByTherapist == null) {
                String sql = "SELECT id, name, individual_therapy, group_therapy, group_therapy_min_size, group_therapy_max_size, individual_therapist_fixed, group_therapist_fixed, time_created, time_modified, time_deleted, individual_therapy_duration, group_therapy_duration FROM therapy_type JOIN JOIN_THERAPIST_THERAPY_TYPE ON (JOIN_THERAPIST_THERAPY_TYPE.THERAPY_TYPE = therapy_type.id) WHERE time_deleted IS NULL AND THERAPIST = ?";
                preparedStatementFindAllByTherapist = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapist != null && therapist.getId() != null) {
                preparedStatementFindAllByTherapist.setInt(1, therapist.getId());
            }
            else {
                throw new PersistenceException("Therapist ID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapist);
            ResultSet rs = preparedStatementFindAllByTherapist.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(1));
                therapyType.setName(rs.getString(2));
                therapyType.setSuitableForIndividuals(rs.getBoolean(3));
                therapyType.setSuitableForGroups(rs.getBoolean(4));
                therapyType.setMinGroupSize(rs.getInt(5));
                therapyType.setMaxGroupSize(rs.getInt(6));
                therapyType.setIndividualTherapistFixed(rs.getBoolean(7));
                therapyType.setGroupTherapistFixed(rs.getBoolean(8));
                therapyType.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(9)));
                therapyType.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));
                therapyType.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(11)));
                therapyType.setIndividualTherapyDuration(rs.getInt(12));
                therapyType.setGroupTherapyDuration(rs.getInt(13));

                result.add(therapyType);
            }

            log.debug("found entries: " + result.size());

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public TherapyType findOneById(TherapyType toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);
        if (toFind == null) {
            throw new NullPointerException();
        }
        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        TherapyType result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, name, individual_therapy, group_therapy, group_therapy_min_size, group_therapy_max_size, individual_therapist_fixed, group_therapist_fixed, time_created, time_modified, time_deleted, individual_therapy_duration, group_therapy_duration FROM therapy_type WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new TherapyType();
                result.setId(rs.getInt(1));
                result.setName(rs.getString(2));
                result.setSuitableForIndividuals(rs.getBoolean(3));
                result.setSuitableForGroups(rs.getBoolean(4));
                result.setMinGroupSize(rs.getInt(5));
                result.setMaxGroupSize(rs.getInt(6));
                result.setIndividualTherapistFixed(rs.getBoolean(7));
                result.setGroupTherapistFixed(rs.getBoolean(8));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(9)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(11)));
                result.setIndividualTherapyDuration(rs.getInt(12));
                result.setGroupTherapyDuration(rs.getInt(13));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
