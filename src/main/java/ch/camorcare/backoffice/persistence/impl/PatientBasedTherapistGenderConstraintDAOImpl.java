package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.persistence.PatientBasedTherapistGenderConstraintDAO;
import ch.camorcare.backoffice.persistence.TherapyTypeDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the
 * PatientBasedTherapistGenderConstraint table stored in the database.
 */
public class PatientBasedTherapistGenderConstraintDAOImpl implements PatientBasedTherapistGenderConstraintDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDeleteAllByPatient = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items by patient.
     */
    private static PreparedStatement preparedStatementFindAllByPatient = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     *
     */
    private TherapyTypeDAO therapyTypeDAO;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(PatientBasedTherapistGenderConstraintDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<PatientBasedTherapistGenderConstraint> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public void setValidator(EntityValidator<PatientBasedTherapistGenderConstraint> validator) {
        this.validator = validator;
    }

    public void setTherapyTypeDAO(TherapyTypeDAO therapyTypeDAO) {
        this.therapyTypeDAO = therapyTypeDAO;
    }

    @Override
    public PatientBasedTherapistGenderConstraint create(PatientBasedTherapistGenderConstraint toCreate) throws PersistenceException, ValidationException {
        PatientBasedTherapistGenderConstraint copy = new PatientBasedTherapistGenderConstraint(toCreate);
        log.debug("persisting new entry: " + copy);

        if (copy.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        validator.validate(toCreate);

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO PBC_THERAPIST_GENDER (PATIENT, THERAPY_TYPE, GENDER, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            preparedStatementCreate.setInt(1, copy.getPatient().getId());
            preparedStatementCreate.setInt(2, copy.getTherapyType().getId());
            preparedStatementCreate.setString(3, copy.getGender().name());
            preparedStatementCreate.setTimestamp(4, ts);
            preparedStatementCreate.setTimestamp(5, ts);

            log.debug("resulting statement: " + preparedStatementCreate);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public PatientBasedTherapistGenderConstraint update(PatientBasedTherapistGenderConstraint toUpdate) throws PersistenceException, ValidationException {
        throw new PersistenceException("Update is not supported for PBC_THERAPIST_GENDER.");
    }

    @Override
    public void delete(PatientBasedTherapistGenderConstraint toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Constraint has already been deleted.");
            throw new PersistenceException("Constraint has already been deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE PBC_THERAPIST_GENDER SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE ID = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public void deleteAllByPatient(Patient patient) throws PersistenceException {
        log.debug("deleting entries for patient: " + patient);

        if (patient == null || patient.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementDeleteAllByPatient == null) {
                String sql = "UPDATE PBC_THERAPIST_GENDER SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE PATIENT = ?";
                preparedStatementDeleteAllByPatient = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDeleteAllByPatient.setTimestamp(1, ts);
            preparedStatementDeleteAllByPatient.setTimestamp(2, ts);
            preparedStatementDeleteAllByPatient.setInt(3, patient.getId());

            log.debug("resulting statement: " + preparedStatementDeleteAllByPatient);
            preparedStatementDeleteAllByPatient.execute();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<PatientBasedTherapistGenderConstraint> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<PatientBasedTherapistGenderConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT ID, PATIENT, THERAPY_TYPE, GENDER, TIME_CREATED, TIME_MODIFIED FROM PBC_THERAPIST_GENDER WHERE TIME_DELETED IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));

                PatientBasedTherapistGenderConstraint constraint = new PatientBasedTherapistGenderConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapyType(therapyType);
                constraint.setGender(Gender.valueOf(rs.getString(4)));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<PatientBasedTherapistGenderConstraint> findAllByPatient(Patient patient, boolean eagerLoading) throws PersistenceException {
        log.debug("reading all entries");
        List<PatientBasedTherapistGenderConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByPatient == null) {
                String sql = "SELECT ID, THERAPY_TYPE, GENDER, TIME_CREATED, TIME_MODIFIED FROM PBC_THERAPIST_GENDER WHERE TIME_DELETED IS NULL AND PATIENT = ?";
                preparedStatementFindAllByPatient = connectionManager.getConnection().prepareStatement(sql);
            }

            if (patient != null && patient.getId() != null) {
                preparedStatementFindAllByPatient.setInt(1, patient.getId());
            }
            else {
                throw new PersistenceException("Patient ID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByPatient);
            ResultSet rs = preparedStatementFindAllByPatient.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(2));

                if (eagerLoading) {
                    therapyType = therapyTypeDAO.findOneById(therapyType);
                }

                PatientBasedTherapistGenderConstraint constraint = new PatientBasedTherapistGenderConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapyType(therapyType);
                constraint.setGender(Gender.valueOf(rs.getString(3)));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public PatientBasedTherapistGenderConstraint findOneById(PatientBasedTherapistGenderConstraint toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);

        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        PatientBasedTherapistGenderConstraint constraint = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT ID, PATIENT, THERAPY_TYPE, GENDER, TIME_CREATED, TIME_MODIFIED, TIME_DELETED FROM PBC_THERAPIST_GENDER WHERE ID = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));

                constraint = new PatientBasedTherapistGenderConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapyType(therapyType);
                constraint.setGender(Gender.valueOf(rs.getString(4)));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
            }

            if (constraint == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return constraint;
    }
}
