package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint;
import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.PatientBasedTherapistConstraintDAO;
import ch.camorcare.backoffice.persistence.TherapistDAO;
import ch.camorcare.backoffice.persistence.TherapyTypeDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the PatientBasedTherapistConstraint table stored in the database.
 */
public class PatientBasedTherapistConstraintDAOImpl implements PatientBasedTherapistConstraintDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDeleteByPatient = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items by patient.
     */
    private static PreparedStatement preparedStatementFindAllByPatient = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(PatientBasedTherapistConstraintDAOImpl.class.getName());

    private TherapistDAO therapistDAO;

    private TherapyTypeDAO therapyTypeDAO;

    /**
     * Validates a DTO.
     */
    private EntityValidator<PatientBasedTherapistConstraint> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public void setValidator(EntityValidator<PatientBasedTherapistConstraint> validator) {
        this.validator = validator;
    }

    public void setTherapistDAO(TherapistDAO therapistDAO) {
        this.therapistDAO = therapistDAO;
    }

    public void setTherapyTypeDAO(TherapyTypeDAO therapyTypeDAO) {
        this.therapyTypeDAO = therapyTypeDAO;
    }

    @Override
    public PatientBasedTherapistConstraint create(PatientBasedTherapistConstraint toCreate) throws PersistenceException, ValidationException {
        PatientBasedTherapistConstraint copy = new PatientBasedTherapistConstraint(toCreate);
        log.debug("persisting new entry: " + copy);

        validator.validate(toCreate);

        if (copy.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO PBC_THERAPIST (PATIENT, THERAPIST, THERAPY_TYPE, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            preparedStatementCreate.setInt(1, copy.getPatient().getId());
            preparedStatementCreate.setInt(2, copy.getTherapist().getId());
            preparedStatementCreate.setInt(3, copy.getTherapyType().getId());
            preparedStatementCreate.setTimestamp(4, ts);
            preparedStatementCreate.setTimestamp(5, ts);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
           // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public PatientBasedTherapistConstraint update(PatientBasedTherapistConstraint toUpdate) throws PersistenceException, ValidationException {
        throw new PersistenceException("Update is not supported for PBC_THERAPIST.");
    }

    @Override
    public void delete(PatientBasedTherapistConstraint toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Constraint has already been deleted.");
            throw new PersistenceException("Constraint has already been deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE PBC_THERAPIST SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE ID = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
           // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public void deleteAllByPatient(Patient patient) throws PersistenceException {
        if (patient == null || patient.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        log.debug("deleting entries for patient: " + patient.getId());

        try {
            if (preparedStatementDeleteByPatient == null) {
                String sql = "UPDATE PBC_THERAPIST SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE PATIENT = ?";
                preparedStatementDeleteByPatient = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDeleteByPatient.setTimestamp(1, ts);
            preparedStatementDeleteByPatient.setTimestamp(2, ts);
            preparedStatementDeleteByPatient.setInt(3, patient.getId());

            log.debug("resulting statement: " + preparedStatementDeleteByPatient);
            preparedStatementDeleteByPatient.execute();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<PatientBasedTherapistConstraint> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<PatientBasedTherapistConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, patient, therapist, therapy_type, time_created, time_modified FROM pbc_therapist WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));

                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(3));

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(4));

                PatientBasedTherapistConstraint constraint = new PatientBasedTherapistConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapist(therapist);
                constraint.setTherapyType(therapyType);
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<PatientBasedTherapistConstraint> findAllByPatient(Patient patient, boolean eagerLoading) throws PersistenceException {
        log.debug("reading all entries for patient");
        List<PatientBasedTherapistConstraint> result = new ArrayList<>();

        if (patient == null || patient.getId() == null) {
            log.error("Patient (or ID) is null");
            throw new PersistenceException("Patient (or ID) is null");
        }

        try {
            if (preparedStatementFindAllByPatient == null) {
                String sql = "SELECT id, therapist, therapy_type, time_created, time_modified FROM pbc_therapist WHERE time_deleted IS NULL AND patient = ?";
                preparedStatementFindAllByPatient = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindAllByPatient.setInt(1, patient.getId());
            log.debug("resulting statement: " + preparedStatementFindAllByPatient);
            ResultSet rs = preparedStatementFindAllByPatient.executeQuery();

            while (rs.next()) {
                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(2));

                if(eagerLoading) {
                    therapist = therapistDAO.findOneById(therapist);
                }

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));

                if(eagerLoading) {
                    therapyType = therapyTypeDAO.findOneById(therapyType);
                }

                PatientBasedTherapistConstraint constraint = new PatientBasedTherapistConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapist(therapist);
                constraint.setTherapyType(therapyType);
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public PatientBasedTherapistConstraint findOneById(PatientBasedTherapistConstraint toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);

        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        PatientBasedTherapistConstraint constraint = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, patient, therapist, therapy_type, time_created, time_modified, time_deleted FROM pbc_therapist WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));

                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(3));

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(4));

                constraint = new PatientBasedTherapistConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapist(therapist);
                constraint.setTherapyType(therapyType);
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
            }

            if (constraint == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return constraint;
    }
}
