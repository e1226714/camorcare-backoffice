package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Address;
import ch.camorcare.backoffice.entities.Invoice;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.persistence.InvoiceDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the Invoice table stored in the
 * database.
 */
public class InvoiceDAOImpl implements InvoiceDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(InvoiceDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<Invoice> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<Invoice> validator) {
        this.validator = validator;
    }

    @Override
    public Invoice create(Invoice toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);
        Invoice copy = new Invoice(toCreate);
        validator.validate(toCreate);
        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }
        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO INVOICE (THERAPY,ADDRESS,VAT_INCLUDED, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }
            // Set PreparedStatement
            preparedStatementCreate.setInt(1, toCreate.getTherapy().getId());
            preparedStatementCreate.setInt(2, toCreate.getAddress().getId());
            preparedStatementCreate.setBoolean(3, toCreate.isVatIncluded());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setTimestamp(4, ts);
            preparedStatementCreate.setTimestamp(5, ts);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public Invoice update(Invoice toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE INVOICE SET THERAPY = ?,ADDRESS = ?, VAT_INCLUDED = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementUpdate.setInt(1, toUpdate.getTherapy().getId());
            preparedStatementUpdate.setInt(2, toUpdate.getAddress().getId());
            preparedStatementUpdate.setBoolean(3, toUpdate.isVatIncluded());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementUpdate.setTimestamp(4, ts);
            preparedStatementUpdate.setInt(5, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);
            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(Invoice toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Invoice was already deleted.");
            throw new PersistenceException("Invoice was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE INVOICE SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<Invoice> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<Invoice> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, therapy, address, vat_included, time_created, time_modified FROM invoice WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Invoice invoice = new Invoice();
                invoice.setId(rs.getInt(1));
                Therapy therapy = new Therapy();
                therapy.setId(rs.getInt(2));
                invoice.setTherapy(therapy);
                Address address = new Address();
                address.setId(rs.getInt(3));
                invoice.setAddress(address);
                invoice.setVatIncluded(rs.getBoolean(4));
                invoice.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                invoice.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));

                result.add(invoice);
            }

            log.debug("found entries: " + result.size());

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public Invoice findOneById(Invoice toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);
        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        Invoice result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, THERAPY,ADDRESS,VAT_INCLUDED, time_created, time_modified, time_deleted FROM invoice WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new Invoice();
                result.setId(rs.getInt(1));
                Therapy therapy = new Therapy();
                therapy.setId(rs.getInt(2));
                result.setTherapy(therapy);
                Address address = new Address();
                address.setId(rs.getInt(3));
                result.setAddress(address);
                result.setVatIncluded(rs.getBoolean(4));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
