package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyRoom;
import ch.camorcare.backoffice.persistence.TherapyRoomDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the PatientRoom table stored in
 * the database.
 */
public class TherapyRoomDAOImpl implements TherapyRoomDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAllByTherapyPlan = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapyRoomDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<TherapyRoom> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<TherapyRoom> validator) {
        this.validator = validator;
    }

    @Override
    public TherapyRoom create(TherapyRoom toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);
        validator.validate(toCreate);

        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        TherapyRoom retVal = new TherapyRoom(toCreate);
        String sql = "INSERT INTO THERAPY_ROOM (name, capacity, time_created, time_modified) VALUES (?, ?, ?, ?)";

        try {
            preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatementCreate.setString(1, toCreate.getName());
            preparedStatementCreate.setInt(2, toCreate.getCapacity());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setTimestamp(3, ts);
            preparedStatementCreate.setTimestamp(4, ts);
            log.debug("resulting statement: " + preparedStatementCreate);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();
            if (keyResultSet.next()) {
                retVal.setId(keyResultSet.getInt(1));
                log.debug("resulting key: " + retVal.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return retVal;
    }

    @Override
    public TherapyRoom update(TherapyRoom toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        String sql = "UPDATE THERAPY_ROOM SET name = ?, capacity = ?, time_modified = ? WHERE id = ?";

        try {
            preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql);
            preparedStatementUpdate.setString(1, toUpdate.getName());
            preparedStatementUpdate.setInt(2, toUpdate.getCapacity());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementUpdate.setTimestamp(3, ts);
            preparedStatementUpdate.setInt(4, toUpdate.getId());
            log.debug("resulting statement: " + preparedStatementUpdate);
            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(TherapyRoom toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("TherapyRoom was already deleted.");
            throw new PersistenceException("TherapyRoom was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE THERAPY_ROOM SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);

            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<TherapyRoom> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<TherapyRoom> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, name, capacity, time_created, time_modified, time_deleted FROM THERAPY_ROOM WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                TherapyRoom therapyRoom = new TherapyRoom();
                therapyRoom.setId(rs.getInt(1));
                therapyRoom.setName(rs.getString(2));
                therapyRoom.setCapacity(rs.getInt(3));
                therapyRoom.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                therapyRoom.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(therapyRoom);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public TherapyRoom findOneById(TherapyRoom toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);
        if (toFind == null) {
            throw new NullPointerException();
        }
        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        TherapyRoom result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, name, capacity, time_created, time_modified, time_deleted FROM THERAPY_ROOM WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());
            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new TherapyRoom();

                result.setId(rs.getInt(1));
                result.setName(rs.getString(2));
                result.setCapacity(rs.getInt(3));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TherapyRoom> findAllByTherapyPlan(TherapyPlan therapyPlan) throws PersistenceException {
        log.debug("findAllByTherapyPlan");
        List<TherapyRoom> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapyPlan == null) {
                String sql = "SELECT DISTINCT THERAPY_ROOM.id, name, capacity, THERAPY_ROOM.time_created, THERAPY_ROOM.time_modified FROM THERAPY_ROOM JOIN THERAPY_EVENT ON (THERAPY_EVENT.THERAPY_ROOM = THERAPY_ROOM.ID) WHERE THERAPY_ROOM.time_deleted IS NULL AND THERAPY_EVENT.THERAPY_PLAN = ?";
                preparedStatementFindAllByTherapyPlan = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyPlan != null && therapyPlan.getId() != null) {
                preparedStatementFindAllByTherapyPlan.setInt(1, therapyPlan.getId());
            }
            else {
                throw new PersistenceException("TherapyPlan ID is null");
            }
            log.debug("resulting statement: " + preparedStatementFindAllByTherapyPlan);
            ResultSet rs = preparedStatementFindAllByTherapyPlan.executeQuery();

            while (rs.next()) {
                TherapyRoom therapyRoom = new TherapyRoom();
                therapyRoom.setId(rs.getInt(1));
                therapyRoom.setName(rs.getString(2));
                therapyRoom.setCapacity(rs.getInt(3));
                therapyRoom.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                therapyRoom.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(therapyRoom);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
