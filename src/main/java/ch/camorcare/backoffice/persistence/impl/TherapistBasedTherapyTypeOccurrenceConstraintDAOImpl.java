package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.persistence.TherapistBasedTherapyTypeOccurrenceConstraintDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the
 * TherapistBasedTherapyTypeOccurrenceConstraint table stored in the database.
 */
public class TherapistBasedTherapyTypeOccurrenceConstraintDAOImpl implements TherapistBasedTherapyTypeOccurrenceConstraintDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDeleteByTherapist = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items for a therapist.
     */
    private static PreparedStatement preparedStatementFindAllByTherapist = null;

    /**
     * Prepared statement for finding all items for a therapy type.
     */
    private static PreparedStatement preparedStatementFindAllByTherapyType = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapistBasedTherapyTypeOccurrenceConstraintDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<TherapistBasedTherapyTypeOccurrenceConstraint> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public void setValidator(EntityValidator<TherapistBasedTherapyTypeOccurrenceConstraint> validator) {
        this.validator = validator;
    }

    @Override
    public TherapistBasedTherapyTypeOccurrenceConstraint create(TherapistBasedTherapyTypeOccurrenceConstraint toCreate) throws PersistenceException, ValidationException {
        TherapistBasedTherapyTypeOccurrenceConstraint copy = new TherapistBasedTherapyTypeOccurrenceConstraint(toCreate);
        log.debug("persisting new entry: " + copy);

        if (copy.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        validator.validate(toCreate);

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO TBC_THERAPY_TYPE_OCCURRENCE (THERAPIST, THERAPY_TYPE, THERAPY_MODE, MAX_PER_DAY, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            preparedStatementCreate.setInt(1, copy.getTherapist().getId());
            preparedStatementCreate.setInt(2, copy.getTherapyType().getId());
            preparedStatementCreate.setString(3, copy.getTherapyMode().name());
            preparedStatementCreate.setInt(4, copy.getMaxOccurrencePerDay());
            preparedStatementCreate.setTimestamp(5, ts);
            preparedStatementCreate.setTimestamp(6, ts);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    /**
     * Updates the maximum occurrence value of this constraint.
     *
     * @param toUpdate The DTO to be updated
     * @return
     * @throws PersistenceException
     * @throws ValidationException
     */
    @Override
    public TherapistBasedTherapyTypeOccurrenceConstraint update(TherapistBasedTherapyTypeOccurrenceConstraint toUpdate) throws PersistenceException, ValidationException {
        TherapistBasedTherapyTypeOccurrenceConstraint copy = new TherapistBasedTherapyTypeOccurrenceConstraint(toUpdate);
        log.debug("persisting updated entry: " + copy);

        if (copy.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        validator.validate(toUpdate);

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE TBC_THERAPY_TYPE_OCCURRENCE SET THERAPY_MODE = ?, MAX_PER_DAY = ?, TIME_MODIFIED = ? WHERE ID = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            preparedStatementUpdate.setString(1, copy.getTherapyMode().name());
            preparedStatementUpdate.setInt(2, copy.getMaxOccurrencePerDay());
            preparedStatementUpdate.setTimestamp(3, ts);
            preparedStatementUpdate.setInt(4, copy.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);

            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public void delete(TherapistBasedTherapyTypeOccurrenceConstraint toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Constraint has already been deleted.");
            throw new PersistenceException("Constraint has already been deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE TBC_THERAPY_TYPE_OCCURRENCE SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE ID = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public void deleteByTherapist(Therapist therapist) throws PersistenceException {
        log.debug("deleting entries by therapist");

        if (therapist == null || therapist.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementDeleteByTherapist == null) {
                String sql = "UPDATE TBC_THERAPY_TYPE_OCCURRENCE SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE THERAPIST = ?";
                preparedStatementDeleteByTherapist = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDeleteByTherapist.setTimestamp(1, ts);
            preparedStatementDeleteByTherapist.setTimestamp(2, ts);
            preparedStatementDeleteByTherapist.setInt(3, therapist.getId());

            log.debug("resulting statement: " + preparedStatementDeleteByTherapist);
            preparedStatementDeleteByTherapist.execute();

            if (preparedStatementDeleteByTherapist.getUpdateCount() <= 0) {
                log.error("Nothing deleted");
                throw new PersistenceException("Nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<TherapistBasedTherapyTypeOccurrenceConstraint> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<TherapistBasedTherapyTypeOccurrenceConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT ID, THERAPIST, THERAPY_TYPE, THERAPY_MODE, MAX_PER_DAY, TIME_CREATED, TIME_MODIFIED FROM TBC_THERAPY_TYPE_OCCURRENCE WHERE TIME_DELETED IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(2));

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));

                TherapistBasedTherapyTypeOccurrenceConstraint constraint = new TherapistBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setTherapist(therapist);
                constraint.setTherapyType(therapyType);
                constraint.setTherapyMode(TherapyMode.valueOf(rs.getString(4).toUpperCase()));
                constraint.setMaxOccurrencePerDay(rs.getInt(5));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TherapistBasedTherapyTypeOccurrenceConstraint> findAllByTherapist(Therapist therapist) throws PersistenceException {
        log.debug("reading all entries");
        List<TherapistBasedTherapyTypeOccurrenceConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapist == null) {
                String sql = "SELECT ID, THERAPY_TYPE, THERAPY_MODE, MAX_PER_DAY, TIME_CREATED, TIME_MODIFIED FROM TBC_THERAPY_TYPE_OCCURRENCE WHERE TIME_DELETED IS NULL AND THERAPIST = ?";
                preparedStatementFindAllByTherapist = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapist != null && therapist.getId() != null) {
                preparedStatementFindAllByTherapist.setInt(1, therapist.getId());
            }
            else {
                throw new PersistenceException("Therapist ID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapist);
            ResultSet rs = preparedStatementFindAllByTherapist.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(2));

                TherapistBasedTherapyTypeOccurrenceConstraint constraint = new TherapistBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setTherapist(therapist);
                constraint.setTherapyType(therapyType);
                constraint.setTherapyMode(TherapyMode.valueOf(rs.getString(3).toUpperCase()));
                constraint.setMaxOccurrencePerDay(rs.getInt(4));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TherapistBasedTherapyTypeOccurrenceConstraint> findAllByTherapyType(TherapyType therapyType) throws PersistenceException {
        log.debug("reading all entries");
        List<TherapistBasedTherapyTypeOccurrenceConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapyType == null) {
                String sql = "SELECT ID, THERAPIST, THERAPY_MODE, MAX_PER_DAY, TIME_CREATED, TIME_MODIFIED FROM TBC_THERAPY_TYPE_OCCURRENCE WHERE TIME_DELETED IS NULL AND THERAPY_TYPE = ?";
                preparedStatementFindAllByTherapyType = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyType != null && therapyType.getId() != null) {
                preparedStatementFindAllByTherapyType.setInt(1, therapyType.getId());
            }
            else {
                throw new PersistenceException("Therapist ID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapyType);
            ResultSet rs = preparedStatementFindAllByTherapyType.executeQuery();

            while (rs.next()) {
                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(2));

                TherapistBasedTherapyTypeOccurrenceConstraint constraint = new TherapistBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setTherapist(therapist);
                constraint.setTherapyType(therapyType);
                constraint.setTherapyMode(TherapyMode.valueOf(rs.getString(3).toUpperCase()));
                constraint.setMaxOccurrencePerDay(rs.getInt(4));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public TherapistBasedTherapyTypeOccurrenceConstraint findOneById(TherapistBasedTherapyTypeOccurrenceConstraint toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);

        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        TherapistBasedTherapyTypeOccurrenceConstraint constraint = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT ID, THERAPIST, THERAPY_TYPE, MAX_PER_DAY, TIME_CREATED, TIME_MODIFIED, TIME_DELETED FROM TBC_THERAPY_TYPE_OCCURRENCE WHERE ID = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(2));

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));

                constraint = new TherapistBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setTherapist(therapist);
                constraint.setTherapyType(therapyType);
                constraint.setMaxOccurrencePerDay(rs.getInt(4));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                constraint.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
            }

            if (constraint == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return constraint;
    }
}
