package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.persistence.*;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the Therapy table stored in the
 * database.
 */
public class TherapyEventDAOImpl implements TherapyEventDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the creation of an event participation.
     */
    private static PreparedStatement preparedStatementFindParticipants = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAllByTherapyPlan = null;

    /**
     *
     */
    private static PreparedStatement preparedStatementFindAllByTherapistAndTherapyPlan = null;

    /**
     *
     */
    private static PreparedStatement preparedStatementFindAllByTherapyAndTherapyPlan = null;

    /**
     *
     */
    private static PreparedStatement preparedStatementFindAllByTherapyRoomAndTherapyPlan = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Prepared statement for the creation of an event participation.
     */
    private static PreparedStatement preparedStatementCreateParticipation = null;

    /**
     * Prepared statement for the deletion of an event participation.
     */
    private static PreparedStatement preparedStatementDeleteParticipation = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapyEventDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<TherapyEvent> validator;

    private TherapyDAO therapyDAO;

    private TherapyPlanDAO therapyPlanDAO;

    private TherapyTypeDAO therapyTypeDAO;

    private TherapistDAO therapistDAO;

    private TherapyRoomDAO therapyRoomDAO;

    private TimeBlockDAO timeBlockDAO;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<TherapyEvent> validator) {
        this.validator = validator;
    }

    public void setTherapyPlanDAO(TherapyPlanDAO therapyPlanDAO) {
        this.therapyPlanDAO = therapyPlanDAO;
    }

    public void setTherapyTypeDAO(TherapyTypeDAO therapyTypeDAO) {
        this.therapyTypeDAO = therapyTypeDAO;
    }

    public void setTherapistDAO(TherapistDAO therapistDAO) {
        this.therapistDAO = therapistDAO;
    }

    public void setTherapyRoomDAO(TherapyRoomDAO therapyRoomDAO) {
        this.therapyRoomDAO = therapyRoomDAO;
    }

    public void setTimeBlockDAO(TimeBlockDAO timeBlockDAO) {
        this.timeBlockDAO = timeBlockDAO;
    }

    public void setTherapyDAO(TherapyDAO therapyDAO) {
        this.therapyDAO = therapyDAO;
    }

    @Override
    public TherapyEvent create(TherapyEvent toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);

        TherapyEvent copy = new TherapyEvent(toCreate);
        validator.validate(toCreate);

        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO therapy_event (therapy_plan, therapy_type, therapy_mode, time_created, time_modified) VALUES (?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());

            if (toCreate.getTherapyPlan() != null && toCreate.getTherapyPlan().getId() != null) {
                preparedStatementCreate.setInt(1, toCreate.getTherapyPlan().getId());
            }
            else {
                throw new ValidationException("TherapyPlan ID is null");
            }

            if (toCreate.getTherapyType() != null && toCreate.getTherapyType().getId() != null) {
                preparedStatementCreate.setInt(2, toCreate.getTherapyType().getId());
            }
            else {
                throw new ValidationException("TherapyType ID is null");
            }

            if (toCreate.getTherapyMode() != null && toCreate.getTherapyMode().name() != null) {
                preparedStatementCreate.setString(3, toCreate.getTherapyMode().name());
            }
            else {
                throw new ValidationException("TherapyMode is null");
            }

            preparedStatementCreate.setTimestamp(4, now);
            preparedStatementCreate.setTimestamp(5, now);

            log.debug("resulting statement: " + preparedStatementCreate);

            // @todo: start transaction

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();
            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("resulting key: " + copy.getId());
            }

            for (Therapy therapy : copy.getTherapies()) {
                createParticipation(copy, therapy);
            }

            // @todo: commit transaction

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public TherapyEvent update(TherapyEvent toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        TherapyEvent copy = new TherapyEvent(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE therapy_event SET therapy_type = ?, therapy_mode = ?, therapist = ?, therapy_room = ?, therapy_time = ?, therapy_time_block = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            if (toUpdate.getTherapyType() != null && toUpdate.getTherapyType().getId() != null) {
                preparedStatementUpdate.setInt(1, toUpdate.getTherapyType().getId());
            }
            else {
                throw new ValidationException("TherapyType ID is null");
            }

            // therapy mode
            if (toUpdate.getTherapyMode() != null) {
                preparedStatementUpdate.setString(2, toUpdate.getTherapyMode().name());
            }
            else {
                preparedStatementUpdate.setNull(2, Types.VARCHAR);
            }

            // therapist
            if (toUpdate.getTherapist() != null) {
                preparedStatementUpdate.setInt(3, toUpdate.getTherapist().getId());
            }
            else {
                preparedStatementUpdate.setNull(3, Types.INTEGER);
            }

            // therapy room
            if (toUpdate.getTherapyRoom() != null) {
                preparedStatementUpdate.setInt(4, toUpdate.getTherapyRoom().getId());
            }
            else {
                preparedStatementUpdate.setNull(4, Types.INTEGER);
            }

            // therapy time
            if (toUpdate.getTherapyTime() != null) {
                preparedStatementUpdate.setTimestamp(5, DateTimeConverter.dateTimeToSqlTimestamp(toUpdate.getTherapyTime()));
            }
            else {
                preparedStatementUpdate.setNull(5, Types.INTEGER);
            }

            // therapy time block
            if (toUpdate.getTherapyTimeBlock() != null) {
                preparedStatementUpdate.setInt(6, toUpdate.getTherapyTimeBlock().getId());
            }
            else {
                preparedStatementUpdate.setNull(6, Types.INTEGER);
            }

            preparedStatementUpdate.setTimestamp(7, now);
            preparedStatementUpdate.setInt(8, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);

            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public void delete(TherapyEvent toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("TherapyEvent was already deleted.");
            throw new PersistenceException("TherapyEvent was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE therapy_event SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, now);
            preparedStatementDelete.setTimestamp(2, now);
            preparedStatementDelete.setInt(3, toDelete.getId());
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<TherapyEvent> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<TherapyEvent> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, therapy_plan, therapy_type, therapy_mode, therapist, therapy_room, therapy_time, therapy_time_block, time_created, time_modified FROM therapy_event WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                TherapyPlan therapyPlan = new TherapyPlan();
                therapyPlan.setId(rs.getInt(2));
                if (rs.wasNull()) {
                    therapyPlan = null;
                }

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));
                if (rs.wasNull()) {
                    therapyType = null;
                }

                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(5));
                if (rs.wasNull()) {
                    therapist = null;
                }

                TherapyRoom therapyRoom = new TherapyRoom();
                therapyRoom.setId(rs.getInt(6));
                if (rs.wasNull()) {
                    therapyRoom = null;
                }

                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(8));
                if (rs.wasNull()) {
                    timeBlock = null;
                }

                TherapyMode therapyMode = null;
                String therapyModeName = rs.getString(4);
                if (!rs.wasNull()) {
                    therapyMode = TherapyMode.valueOf(therapyModeName);
                }

                TherapyEvent therapyEvent = new TherapyEvent();
                therapyEvent.setId(rs.getInt(1));
                therapyEvent.setTherapyPlan(therapyPlan);
                therapyEvent.setTherapyType(therapyType);
                therapyEvent.setTherapyMode(therapyMode);
                therapyEvent.setTherapist(therapist);
                therapyEvent.setTherapyRoom(therapyRoom);
                therapyEvent.setTherapyTime(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                therapyEvent.setTherapyTimeBlock(timeBlock);
                therapyEvent.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(9)));
                therapyEvent.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));

                result.add(therapyEvent);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TherapyEvent> findAllByTherapyPlan(TherapyPlan therapyPlan, boolean eagerLoading) throws PersistenceException, ValidationException {
        log.debug("reading all entries");
        List<TherapyEvent> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapyPlan == null) {
                String sql = "SELECT id, therapy_plan, therapy_type, therapy_mode, therapist, therapy_room, therapy_time, therapy_time_block, "
                        + "time_created, time_modified FROM therapy_event WHERE time_deleted IS NULL AND therapy_plan = ?";
                preparedStatementFindAllByTherapyPlan = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyPlan != null && therapyPlan.getId() != null) {
                preparedStatementFindAllByTherapyPlan.setInt(1, therapyPlan.getId());
            }
            else {
                throw new PersistenceException("TherapyPlan ID is invalid");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapyPlan);
            ResultSet rs = preparedStatementFindAllByTherapyPlan.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));
                if (rs.wasNull()) {
                    therapyType = null;
                }
                else if (eagerLoading) {
                    therapyType = therapyTypeDAO.findOneById(therapyType);
                }

                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(5));
                if (rs.wasNull()) {
                    therapist = null;
                }
                else if (eagerLoading) {
                    therapist = therapistDAO.findOneById(therapist);
                }

                TherapyRoom therapyRoom = new TherapyRoom();
                therapyRoom.setId(rs.getInt(6));
                if (rs.wasNull()) {
                    therapyRoom = null;
                }
                else if (eagerLoading) {
                    therapyRoom = therapyRoomDAO.findOneById(therapyRoom);
                }

                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(8));
                if (rs.wasNull()) {
                    timeBlock = null;
                }
                else if (eagerLoading) {
                    timeBlock = timeBlockDAO.findOneById(timeBlock);
                }

                TherapyMode therapyMode = null;
                String therapyModeName = rs.getString(4);
                if (!rs.wasNull()) {
                    therapyMode = TherapyMode.valueOf(therapyModeName);
                }

                TherapyEvent therapyEvent = new TherapyEvent();
                therapyEvent.setId(rs.getInt(1));
                therapyEvent.setTherapyPlan(therapyPlan);
                therapyEvent.setTherapyType(therapyType);
                therapyEvent.setTherapyMode(therapyMode);
                therapyEvent.setTherapist(therapist);
                therapyEvent.setTherapyRoom(therapyRoom);
                therapyEvent.setTherapyTime(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                therapyEvent.setTherapyTimeBlock(timeBlock);
                therapyEvent.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(9)));
                therapyEvent.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));

                result.add(therapyEvent);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TherapyEvent> findAllByTherapistAndTherapyPlan(Therapist therapist, TherapyPlan therapyPlan, boolean eagerLoading) throws PersistenceException, ValidationException {
        log.debug("reading all entries");
        List<TherapyEvent> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapistAndTherapyPlan == null) {
                String sql = "SELECT id, therapy_plan, therapy_type, therapy_mode, therapist, therapy_room, therapy_time, therapy_time_block, "
                        + "time_created, time_modified FROM therapy_event WHERE time_deleted IS NULL AND therapy_plan = ? AND therapist = ?";
                preparedStatementFindAllByTherapistAndTherapyPlan = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyPlan != null && therapyPlan.getId() != null) {
                preparedStatementFindAllByTherapistAndTherapyPlan.setInt(1, therapyPlan.getId());
            }
            else {
                throw new PersistenceException("TherapyPlan ID is invalid!");
            }

            if (therapist != null && therapist.getId() != null) {
                preparedStatementFindAllByTherapistAndTherapyPlan.setInt(2, therapist.getId());
            }
            else {
                throw new PersistenceException("Therapist ID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapistAndTherapyPlan);
            ResultSet rs = preparedStatementFindAllByTherapistAndTherapyPlan.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));
                if (rs.wasNull()) {
                    therapyType = null;
                }
                else if (eagerLoading) {
                    therapyType = therapyTypeDAO.findOneById(therapyType);
                }

                TherapyRoom therapyRoom = new TherapyRoom();
                therapyRoom.setId(rs.getInt(6));
                if (rs.wasNull()) {
                    therapyRoom = null;
                }
                else if (eagerLoading) {
                    therapyRoom = therapyRoomDAO.findOneById(therapyRoom);
                }

                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(8));
                if (rs.wasNull()) {
                    timeBlock = null;
                }
                else if (eagerLoading) {
                    timeBlock = timeBlockDAO.findOneById(timeBlock);
                }

                TherapyMode therapyMode = null;
                String therapyModeName = rs.getString(4);
                if (!rs.wasNull()) {
                    therapyMode = TherapyMode.valueOf(therapyModeName);
                }

                TherapyEvent therapyEvent = new TherapyEvent();
                therapyEvent.setId(rs.getInt(1));
                therapyEvent.setTherapyPlan(therapyPlan);
                therapyEvent.setTherapyType(therapyType);
                therapyEvent.setTherapyMode(therapyMode);
                therapyEvent.setTherapist(therapist);
                therapyEvent.setTherapyRoom(therapyRoom);
                therapyEvent.setTherapyTime(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                therapyEvent.setTherapyTimeBlock(timeBlock);
                therapyEvent.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(9)));
                therapyEvent.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));

                result.add(therapyEvent);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TherapyEvent> findAllByTherapyAndTherapyPlan(Therapy therapy, TherapyPlan therapyPlan, boolean eagerLoading) throws PersistenceException, ValidationException {
        log.debug("reading all entries");
        List<TherapyEvent> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapyAndTherapyPlan == null) {
                String sql = "SELECT id, therapy_plan, therapy_type, therapy_mode, therapist, therapy_room, therapy_time, therapy_time_block, "
                        + "time_created, time_modified FROM therapy_event JOIN join_therapy_event_therapy ON id = therapy_event_id " +
                        "WHERE time_deleted IS NULL AND therapy_id = ? AND therapy_plan = ?";
                preparedStatementFindAllByTherapyAndTherapyPlan = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapy != null && therapy.getId() != null) {
                preparedStatementFindAllByTherapyAndTherapyPlan.setInt(1, therapy.getId());
            }
            else {
                throw new PersistenceException("Therapy ID is invalid!");
            }
            if (therapyPlan != null && therapyPlan.getId() != null) {
                preparedStatementFindAllByTherapyAndTherapyPlan.setInt(2, therapyPlan.getId());
            }
            else {
                throw new PersistenceException("TherapyPlan ID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapyAndTherapyPlan);
            ResultSet rs = preparedStatementFindAllByTherapyAndTherapyPlan.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));
                if (rs.wasNull()) {
                    therapyType = null;
                }
                else if (eagerLoading) {
                    therapyType = therapyTypeDAO.findOneById(therapyType);
                }

                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(5));
                if (rs.wasNull()) {
                    therapist = null;
                }
                else if (eagerLoading) {
                    therapist = therapistDAO.findOneById(therapist);
                }

                TherapyRoom therapyRoom = new TherapyRoom();
                therapyRoom.setId(rs.getInt(6));
                if (rs.wasNull()) {
                    therapyRoom = null;
                }
                else if (eagerLoading) {
                    therapyRoom = therapyRoomDAO.findOneById(therapyRoom);
                }

                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(8));
                if (rs.wasNull()) {
                    timeBlock = null;
                }
                else if (eagerLoading) {
                    timeBlock = timeBlockDAO.findOneById(timeBlock);
                }

                TherapyMode therapyMode = null;
                String therapyModeName = rs.getString(4);
                if (!rs.wasNull()) {
                    therapyMode = TherapyMode.valueOf(therapyModeName);
                }

                TherapyEvent therapyEvent = new TherapyEvent();
                therapyEvent.setId(rs.getInt(1));
                therapyEvent.setTherapyPlan(therapyPlan);
                therapyEvent.setTherapyType(therapyType);
                therapyEvent.setTherapyMode(therapyMode);
                therapyEvent.setTherapist(therapist);
                therapyEvent.setTherapyRoom(therapyRoom);
                therapyEvent.setTherapyTime(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                therapyEvent.setTherapyTimeBlock(timeBlock);
                therapyEvent.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(9)));
                therapyEvent.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));

                result.add(therapyEvent);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TherapyEvent> findAllByTherapyRoomAndTherapyPlan(TherapyRoom therapyRoom, TherapyPlan therapyPlan, boolean eagerLoading) throws PersistenceException, ValidationException {
        log.debug("reading all entries");
        List<TherapyEvent> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapyRoomAndTherapyPlan == null) {
                String sql = "SELECT id, therapy_plan, therapy_type, therapy_mode, therapist, therapy_room, therapy_time, therapy_time_block, "
                        + "time_created, time_modified FROM therapy_event WHERE time_deleted IS NULL AND therapy_plan = ? AND therapy_room = ?";
                preparedStatementFindAllByTherapyRoomAndTherapyPlan = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyPlan != null && therapyPlan.getId() != null) {
                preparedStatementFindAllByTherapyRoomAndTherapyPlan.setInt(1, therapyPlan.getId());
            }
            else {
                throw new PersistenceException("TherapyPlan ID is invalid!");
            }
            if (therapyRoom != null && therapyRoom.getId() != null) {
                preparedStatementFindAllByTherapyRoomAndTherapyPlan.setInt(2, therapyRoom.getId());
            }
            else {
                throw new PersistenceException("TherapyRoom ID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapyRoomAndTherapyPlan);
            ResultSet rs = preparedStatementFindAllByTherapyRoomAndTherapyPlan.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));
                if (rs.wasNull()) {
                    therapyType = null;
                }
                else if (eagerLoading) {
                    therapyType = therapyTypeDAO.findOneById(therapyType);
                }

                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(5));
                if (rs.wasNull()) {
                    therapist = null;
                }
                else if (eagerLoading) {
                    therapist = therapistDAO.findOneById(therapist);
                }

                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(8));
                if (rs.wasNull()) {
                    timeBlock = null;
                }
                else if (eagerLoading) {
                    timeBlock = timeBlockDAO.findOneById(timeBlock);
                }

                TherapyMode therapyMode = null;
                String therapyModeName = rs.getString(4);
                if (!rs.wasNull()) {
                    therapyMode = TherapyMode.valueOf(therapyModeName);
                }

                TherapyEvent therapyEvent = new TherapyEvent();
                therapyEvent.setId(rs.getInt(1));
                therapyEvent.setTherapyPlan(therapyPlan);
                therapyEvent.setTherapyType(therapyType);
                therapyEvent.setTherapyMode(therapyMode);
                therapyEvent.setTherapist(therapist);
                therapyEvent.setTherapyRoom(therapyRoom);
                therapyEvent.setTherapyTime(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                therapyEvent.setTherapyTimeBlock(timeBlock);
                therapyEvent.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(9)));
                therapyEvent.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));

                result.add(therapyEvent);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public TherapyEvent findOneById(TherapyEvent toFind) throws PersistenceException {
        log.debug("reading entry with ID: " + toFind.getId());
        if (toFind == null) {
            throw new NullPointerException();
        }
        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        TherapyEvent therapyEvent = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, therapy_plan, therapy_type, therapy_mode, therapist, therapy_room, therapy_time, therapy_time_block, time_created, time_modified FROM therapy_event WHERE time_deleted IS NULL AND id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());
            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                TherapyPlan therapyPlan = new TherapyPlan();
                therapyPlan.setId(rs.getInt(2));
                if (rs.wasNull()) {
                    therapyPlan = null;
                }

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));
                if (rs.wasNull()) {
                    therapyType = null;
                }

                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(5));
                if (rs.wasNull()) {
                    therapist = null;
                }

                TherapyRoom therapyRoom = new TherapyRoom();
                therapyRoom.setId(rs.getInt(6));
                if (rs.wasNull()) {
                    therapyRoom = null;
                }

                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(8));
                if (rs.wasNull()) {
                    timeBlock = null;
                }

                TherapyMode therapyMode = null;
                String therapyModeName = rs.getString(4);
                if (!rs.wasNull()) {
                    therapyMode = TherapyMode.valueOf(therapyModeName);
                }

                therapyEvent = new TherapyEvent(toFind);
                therapyEvent.setTherapyPlan(therapyPlan);
                therapyEvent.setTherapyType(therapyType);
                therapyEvent.setTherapyMode(therapyMode);
                therapyEvent.setTherapist(therapist);
                therapyEvent.setTherapyRoom(therapyRoom);
                therapyEvent.setTherapyTime(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                therapyEvent.setTherapyTimeBlock(timeBlock);
                therapyEvent.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(9)));
                therapyEvent.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));
            }

            if (therapyEvent == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return therapyEvent;
    }

    @Override
    public boolean createParticipation(TherapyEvent therapyEvent, Therapy therapy) throws PersistenceException, ValidationException {
        log.debug("create participation for event");

        try {

            /**
             * @todo: delete previous entries!
             */

            if (preparedStatementCreateParticipation == null) {
                String sql = "INSERT INTO JOIN_THERAPY_EVENT_THERAPY (THERAPY_EVENT_ID, THERAPY_ID) VALUES (?, ?)";
                preparedStatementCreateParticipation = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyEvent != null && therapyEvent.getId() != null) {
                preparedStatementCreateParticipation.setInt(1, therapyEvent.getId());
            }
            else {
                throw new PersistenceException("TherapyEvent ID is invalid!");
            }

            if (therapy != null && therapy.getId() != null) {
                preparedStatementCreateParticipation.setInt(2, therapy.getId());
            }
            else {
                throw new PersistenceException("TherapyID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementCreateParticipation);

            if (preparedStatementCreateParticipation.executeUpdate() == 1) {
                return true;
            }
            else {
                return false;
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public boolean deleteParticipation(TherapyEvent therapyEvent, Therapy therapy) throws PersistenceException, ValidationException {
        try {

            if (preparedStatementDeleteParticipation == null) {
                String sql = "DELETE FROM JOIN_THERAPY_EVENT_THERAPY WHERE THERAPY_EVENT_ID = ? AND THERAPY_ID = ?";
                preparedStatementDeleteParticipation = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyEvent != null && therapyEvent.getId() != null) {
                preparedStatementDeleteParticipation.setInt(1, therapyEvent.getId());
            }
            else {
                throw new PersistenceException("TherapyEvent ID is invalid!");
            }

            if (therapy != null && therapy.getId() != null) {
                preparedStatementDeleteParticipation.setInt(2, therapy.getId());
            }
            else {
                throw new PersistenceException("TherapyID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementDeleteParticipation);

            preparedStatementDeleteParticipation.execute();
            if (preparedStatementDeleteParticipation.getUpdateCount() <= 0) {
                return false;
            }
            else {
                return true;
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<Therapy> getParticipants(TherapyEvent therapyEvent, boolean eagerLoading) throws PersistenceException, ValidationException {
        log.debug("find participants for event");
        List<Therapy> result = new ArrayList<>();

        try {
            if (preparedStatementFindParticipants == null) {
                String sql = "SELECT THERAPY_ID FROM JOIN_THERAPY_EVENT_THERAPY WHERE THERAPY_EVENT_ID = ?";
                preparedStatementFindParticipants = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyEvent != null && therapyEvent.getId() != null) {
                preparedStatementFindParticipants.setInt(1, therapyEvent.getId());
            }
            else {
                throw new PersistenceException("TherapyEvent ID is invalid!");
            }

            log.debug("resulting statement: " + preparedStatementFindParticipants);
            ResultSet rs = preparedStatementFindParticipants.executeQuery();

            while (rs.next()) {
                Therapy therapy = new Therapy();
                therapy.setId(rs.getInt(1));

                if (eagerLoading) {
                    therapy = therapyDAO.findOneById(therapy);
                }

                result.add(therapy);
            }
            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
