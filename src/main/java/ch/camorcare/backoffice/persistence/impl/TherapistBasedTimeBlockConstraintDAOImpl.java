package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint;
import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.persistence.TherapistBasedTimeBlockConstraintDAO;
import ch.camorcare.backoffice.persistence.TimeBlockDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the
 * TherapistBasedTimeBlockConstraint table stored in the database.
 */
public class TherapistBasedTimeBlockConstraintDAOImpl implements TherapistBasedTimeBlockConstraintDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDeleteByTherapist = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items for a therapist.
     */
    private static PreparedStatement preparedStatementFindAllByTherapist = null;

    /**
     * Prepared statement for finding all items for a therapist.
     */
    private static PreparedStatement preparedStatementFindAllNegativeByTherapist = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     *
     */
    private TimeBlockDAO timeBlockDAO;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapistBasedTimeBlockConstraintDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<TherapistBasedTimeBlockConstraint> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public void setValidator(EntityValidator<TherapistBasedTimeBlockConstraint> validator) {
        this.validator = validator;
    }

    public void setTimeBlockDAO(TimeBlockDAO timeBlockDAO) {
        this.timeBlockDAO = timeBlockDAO;
    }

    @Override
    public TherapistBasedTimeBlockConstraint create(TherapistBasedTimeBlockConstraint toCreate) throws PersistenceException, ValidationException {
        TherapistBasedTimeBlockConstraint copy = new TherapistBasedTimeBlockConstraint(toCreate);
        log.debug("persisting new entry: " + copy);

        if (copy.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        validator.validate(toCreate);

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO TBC_TIME_BLOCK (THERAPIST, TIME_BLOCK, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            preparedStatementCreate.setInt(1, copy.getTherapist().getId());
            preparedStatementCreate.setInt(2, copy.getTimeBlock().getId());
            preparedStatementCreate.setTimestamp(3, ts);
            preparedStatementCreate.setTimestamp(4, ts);

            log.debug("resulting statement: " + preparedStatementCreate);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public TherapistBasedTimeBlockConstraint update(TherapistBasedTimeBlockConstraint toUpdate) throws PersistenceException, ValidationException {
        throw new PersistenceException("Update is not supported for TBC_TIME_BLOCK.");
    }

    @Override
    public void delete(TherapistBasedTimeBlockConstraint toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Constraint has already been deleted.");
            throw new PersistenceException("Constraint has already been deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE TBC_TIME_BLOCK SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE ID = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public void deleteByTherapist(Therapist therapist) throws PersistenceException {
        log.debug("deleting entries by therapist");

        if (therapist == null || therapist.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("Therapist ID is null");
        }

        try {
            if (preparedStatementDeleteByTherapist == null) {
                String sql = "UPDATE TBC_TIME_BLOCK SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE TIME_DELETED IS NULL AND THERAPIST = ?";
                preparedStatementDeleteByTherapist = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDeleteByTherapist.setTimestamp(1, ts);
            preparedStatementDeleteByTherapist.setTimestamp(2, ts);
            preparedStatementDeleteByTherapist.setInt(3, therapist.getId());

            log.debug("resulting statement: " + preparedStatementDeleteByTherapist);
            preparedStatementDeleteByTherapist.executeUpdate();

            if (preparedStatementDeleteByTherapist.getUpdateCount() <= 0) {
                throw new PersistenceException("No rows were deleted from the database");
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<TherapistBasedTimeBlockConstraint> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<TherapistBasedTimeBlockConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT ID, THERAPIST, TIME_BLOCK, TIME_CREATED, TIME_MODIFIED FROM TBC_TIME_BLOCK WHERE TIME_DELETED IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(2));

                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(3));

                TherapistBasedTimeBlockConstraint constraint = new TherapistBasedTimeBlockConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setTherapist(therapist);
                constraint.setTimeBlock(timeBlock);
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TherapistBasedTimeBlockConstraint> findAllByTherapist(Therapist therapist, boolean eagerLoading) throws PersistenceException {
        log.debug("reading all entries for therapist");
        List<TherapistBasedTimeBlockConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapist == null) {
                String sql = "SELECT DISTINCT TBC_TIME_BLOCK.ID, TIME_BLOCK, TBC_TIME_BLOCK.TIME_CREATED, TBC_TIME_BLOCK.TIME_MODIFIED, DAY, TIME_BLOCK.TIME_START FROM TBC_TIME_BLOCK JOIN TIME_BLOCK ON (TIME_BLOCK.ID = TBC_TIME_BLOCK.TIME_BLOCK) WHERE TBC_TIME_BLOCK.TIME_DELETED IS NULL AND THERAPIST = ? ORDER BY DAY, TIME_BLOCK.TIME_START ASC";
                preparedStatementFindAllByTherapist = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapist != null && therapist.getId() != null) {
                preparedStatementFindAllByTherapist.setInt(1, therapist.getId());
            } else {
                throw new PersistenceException("Therapist ID is not valid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapist);
            ResultSet rs = preparedStatementFindAllByTherapist.executeQuery();

            while (rs.next()) {
                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(2));

                if (eagerLoading) {
                    timeBlock = timeBlockDAO.findOneById(timeBlock);
                }

                TherapistBasedTimeBlockConstraint constraint = new TherapistBasedTimeBlockConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setTherapist(therapist);
                constraint.setTimeBlock(timeBlock);
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(3)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public TherapistBasedTimeBlockConstraint findOneById(TherapistBasedTimeBlockConstraint toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);

        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        TherapistBasedTimeBlockConstraint constraint = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT ID, THERAPIST, TIME_BLOCK, TIME_CREATED, TIME_MODIFIED, TIME_DELETED FROM TBC_TIME_BLOCK WHERE ID = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                Therapist therapist = new Therapist();
                therapist.setId(rs.getInt(2));

                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(3));

                constraint = new TherapistBasedTimeBlockConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setTherapist(therapist);
                constraint.setTimeBlock(timeBlock);
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
            }

            if (constraint == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return constraint;
    }
}
