package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.persistence.TimeBlockDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides an implementation of a Data Access Object to specifically access the TimeBlock table stored in
 * the database.
 */
public class TimeBlockDAOImpl implements TimeBlockDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items by day.
     */
    private static PreparedStatement preparedStatementFindAllByDay = null;

    /**
     *
     */
    private static PreparedStatement preparedStatementFindAllNegativeByTherapist = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TimeBlockDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<TimeBlock> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<TimeBlock> validator) {
        this.validator = validator;
    }

    @Override
    public TimeBlock create(TimeBlock toCreate) throws PersistenceException, ValidationException {
        TimeBlock copy = new TimeBlock(toCreate);
        log.debug("persisting new entry: " + copy);
        validator.validate(toCreate);

        if (copy.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO TIME_BLOCK (DAY, TIME_START, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            if (copy.getDay() != null && copy.getDay().name() != null) {
                preparedStatementCreate.setString(1, copy.getDay().name());
            }
            else {
                throw new ValidationException("Day is null");
            }
            preparedStatementCreate.setTime(2, DateTimeConverter.dateTimeToSqlTime(copy.getTimeStart()));
            preparedStatementCreate.setTimestamp(3, ts);
            preparedStatementCreate.setTimestamp(4, ts);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    /**
     * Updates the start time of this time block.
     *
     * @param toUpdate The DTO to be updated
     * @return
     * @throws PersistenceException
     * @throws ValidationException
     */
    @Override
    public TimeBlock update(TimeBlock toUpdate) throws PersistenceException, ValidationException {
        TimeBlock copy = new TimeBlock(toUpdate);
        log.debug("persisting updated entry: " + copy);
        validator.validate(toUpdate);

        if (copy.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE TIME_BLOCK SET TIME_START = ?, TIME_MODIFIED = ? WHERE ID = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            if (copy.getTimeStart() != null) {
                preparedStatementUpdate.setTime(1, DateTimeConverter.dateTimeToSqlTime(copy.getTimeStart()));
            }
            else {
                throw new ValidationException("TimeStart is null!");
            }
            preparedStatementUpdate.setTimestamp(2, ts);
            preparedStatementUpdate.setInt(3, copy.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);

            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public void delete(TimeBlock toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Constraint has already been deleted.");
            throw new PersistenceException("Constraint has already been deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE TIME_BLOCK SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE ID = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<TimeBlock> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<TimeBlock> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT ID, DAY, TIME_START, TIME_CREATED, TIME_MODIFIED FROM TIME_BLOCK WHERE TIME_DELETED IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(1));
                timeBlock.setDay(Day.valueOf(rs.getString(2).toUpperCase()));
                timeBlock.setTimeStart(DateTimeConverter.sqlTimeToDateTime(rs.getTime(3)));
                timeBlock.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                timeBlock.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(timeBlock);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public Map<Day, List<TimeBlock>> findAllGroupedByDay() throws PersistenceException {
        log.debug("reading all entries");
        Map<Day, List<TimeBlock>> result = new HashMap<>();

        for (Day day : Day.values()) {
            result.put(day, new ArrayList<>());
        }

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT ID, DAY, TIME_START, TIME_CREATED, TIME_MODIFIED FROM TIME_BLOCK WHERE TIME_DELETED IS NULL ORDER BY TIME_START ASC";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(1));
                timeBlock.setDay(Day.valueOf(rs.getString(2).toUpperCase()));
                timeBlock.setTimeStart(DateTimeConverter.sqlTimeToDateTime(rs.getTime(3)));
                timeBlock.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                timeBlock.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.get(timeBlock.getDay()).add(timeBlock);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<TimeBlock> findAllByDay(Day day) throws PersistenceException {
        log.debug("reading all entries");
        List<TimeBlock> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByDay == null) {
                String sql = "SELECT ID, DAY, TIME_START, TIME_CREATED, TIME_MODIFIED FROM TIME_BLOCK WHERE TIME_DELETED IS NULL AND DAY = ?";
                preparedStatementFindAllByDay = connectionManager.getConnection().prepareStatement(sql);
            }

            if (day != null) {
                //noinspection JpaQueryApiInspection
                preparedStatementFindAllByDay.setString(1, day.name());
            }
            else {
                throw new PersistenceException("Day is null!");
            }
            log.debug("resulting statement: " + preparedStatementFindAllByDay);
            ResultSet rs = preparedStatementFindAllByDay.executeQuery();

            while (rs.next()) {
                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(1));
                timeBlock.setDay(Day.valueOf(rs.getString(2).toUpperCase()));
                timeBlock.setTimeStart(DateTimeConverter.sqlTimeToDateTime(rs.getTime(3)));
                timeBlock.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                timeBlock.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(timeBlock);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public TimeBlock findOneById(TimeBlock toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);

        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        TimeBlock timeBlock = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT ID, DAY, TIME_START, TIME_CREATED, TIME_MODIFIED FROM TIME_BLOCK WHERE ID = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            //noinspection JpaQueryApiInspection
            preparedStatementFindOneById.setInt(1, toFind.getId());
            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(1));
                timeBlock.setDay(Day.valueOf(rs.getString(2).toUpperCase()));
                timeBlock.setTimeStart(DateTimeConverter.sqlTimeToDateTime(rs.getTime(3)));
                timeBlock.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                timeBlock.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
            }

            if (timeBlock == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return timeBlock;
    }

    @Override
    public List<TimeBlock> findAllNegativeByTherapist(Therapist therapist) throws PersistenceException {
        log.debug("reading all entries which are not part of time block constraints for a therapist");
        List<TimeBlock> result = new ArrayList<>();

        if (therapist == null || therapist.getId() == null) {
            throw new PersistenceException("Therapist ID is not valid!");
        }

        try {
            if (preparedStatementFindAllNegativeByTherapist == null) {
                String sql = "SELECT TIME_BLOCK.ID, TIME_BLOCK.DAY, TIME_BLOCK.TIME_START, TIME_BLOCK.TIME_CREATED, TIME_BLOCK.TIME_MODIFIED FROM TIME_BLOCK WHERE TIME_BLOCK.TIME_DELETED IS NULL AND NOT EXISTS (SELECT TBC_TIME_BLOCK.ID FROM TBC_TIME_BLOCK WHERE TBC_TIME_BLOCK.TIME_DELETED IS NULL AND TBC_TIME_BLOCK.TIME_BLOCK = TIME_BLOCK.ID AND TBC_TIME_BLOCK.THERAPIST = ?)";
                preparedStatementFindAllNegativeByTherapist = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindAllNegativeByTherapist.setInt(1, therapist.getId());
            log.debug("resulting statement: " + preparedStatementFindAllNegativeByTherapist);
            ResultSet rs = preparedStatementFindAllNegativeByTherapist.executeQuery();

            while (rs.next()) {
                TimeBlock timeBlock = new TimeBlock();
                timeBlock.setId(rs.getInt(1));
                timeBlock.setDay(Day.valueOf(rs.getString(2).toUpperCase()));
                timeBlock.setTimeStart(DateTimeConverter.sqlTimeToDateTime(rs.getTime(3)));
                timeBlock.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                timeBlock.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(timeBlock);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

}
