package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientComment;
import ch.camorcare.backoffice.persistence.PatientCommentDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the PatientComment table stored
 * in the database.
 */
public class PatientCommentDAOImpl implements PatientCommentDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items for a patient.
     */
    private static PreparedStatement preparedStatementFindAllByPatient = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(PatientCommentDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<PatientComment> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<PatientComment> validator) {
        this.validator = validator;
    }

    @Override
    public PatientComment create(PatientComment toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);
        PatientComment copy = new PatientComment(toCreate);
        validator.validate(toCreate);
        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }
        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO PATIENT_COMMENT (PATIENT, CONTENT, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            // Set PreparedStatement
            preparedStatementCreate.setInt(1, toCreate.getPatient().getId());
            preparedStatementCreate.setString(2, toCreate.getContent());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setTimestamp(3, ts);
            preparedStatementCreate.setTimestamp(4, ts);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public PatientComment update(PatientComment toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE patient_comment SET patient = ?, content = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementUpdate.setInt(1, toUpdate.getPatient().getId());
            preparedStatementUpdate.setString(2, toUpdate.getContent());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementUpdate.setTimestamp(3, ts);
            preparedStatementUpdate.setInt(4, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);
            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(PatientComment toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("PatientComment was already deleted.");
            throw new PersistenceException("PatientComment was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE patient_comment SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<PatientComment> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<PatientComment> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, patient, content, time_created, time_modified, time_deleted FROM patient_comment WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                PatientComment patientComment = new PatientComment();
                patientComment.setId(rs.getInt(1));
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));
                patientComment.setPatient(patient);
                patientComment.setContent(rs.getString(3));
                patientComment.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                patientComment.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                patientComment.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));

                result.add(patientComment);
            }

            log.debug("found entries: " + result.size());

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<PatientComment> findAllByPatient(Patient patient) throws PersistenceException {
        log.debug("reading all entries for patient #" + patient.getId());
        List<PatientComment> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByPatient == null) {
                String sql = "SELECT id, content, time_created, time_modified, time_deleted FROM patient_comment WHERE patient = ? AND time_deleted IS NULL";
                preparedStatementFindAllByPatient = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindAllByPatient.setInt(1, patient.getId());
            log.debug("resulting statement: " + preparedStatementFindAllByPatient);
            ResultSet rs = preparedStatementFindAllByPatient.executeQuery();

            while (rs.next()) {
                PatientComment patientComment = new PatientComment();
                patientComment.setId(rs.getInt(1));
                patientComment.setPatient(patient);
                patientComment.setContent(rs.getString(2));
                patientComment.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(3)));
                patientComment.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));

                result.add(patientComment);
            }

            log.debug("found entries: " + result.size());

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public PatientComment findOneById(PatientComment toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);
        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        PatientComment result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, patient, content, time_created, time_modified, time_deleted FROM patient_comment WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new PatientComment();
                result.setId(rs.getInt(1));
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));
                result.setPatient(patient);
                result.setContent(rs.getString(3));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
