package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Address;
import ch.camorcare.backoffice.persistence.AddressDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the Address table stored in the
 * database.
 */
public class AddressDAOImpl implements AddressDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(AddressDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<Address> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<Address> validator) {
        this.validator = validator;
    }

    @Override
    public Address create(Address toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);
        Address copy = new Address(toCreate);
        validator.validate(toCreate);

        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }
        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO ADDRESS (STREET, ZIP, CITY, COUNTRY, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            preparedStatementCreate.setString(1, toCreate.getStreet());
            preparedStatementCreate.setString(2, toCreate.getZip());
            preparedStatementCreate.setString(3, toCreate.getCity());
            preparedStatementCreate.setString(4, toCreate.getCountryCode());
            preparedStatementCreate.setTimestamp(5, new Timestamp(new java.util.Date().getTime()));
            preparedStatementCreate.setTimestamp(6, new Timestamp(new java.util.Date().getTime()));

            log.debug("resulting statement: " + preparedStatementCreate);
            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

            keyResultSet.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public Address update(Address toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        Address copy = new Address(toUpdate);
        validator.validate(copy);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE ADDRESS SET STREET = ?, ZIP = ?, CITY = ?, COUNTRY = ?, TIME_MODIFIED = ? WHERE ID = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementUpdate.setString(1, toUpdate.getStreet());
            preparedStatementUpdate.setString(2, toUpdate.getZip());
            preparedStatementUpdate.setString(3, toUpdate.getCity());
            preparedStatementUpdate.setString(4, toUpdate.getCountryCode());
            preparedStatementUpdate.setTimestamp(5, new Timestamp(new java.util.Date().getTime()));
            preparedStatementUpdate.setInt(6, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);
            preparedStatementUpdate.execute();
            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public void delete(Address toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Address was already deleted.");
            throw new PersistenceException("Address was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE ADDRESS SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE ID = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementDelete.setTimestamp(1, new Timestamp(new java.util.Date().getTime()));
            preparedStatementDelete.setTimestamp(2, new Timestamp(new java.util.Date().getTime()));
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<Address> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<Address> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT ID, STREET, ZIP, CITY, COUNTRY, TIME_CREATED, TIME_MODIFIED, TIME_DELETED FROM ADDRESS";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Address address = new Address();
                address.setId(rs.getInt(1));
                address.setStreet(rs.getString(2));
                address.setZip(rs.getString(3));
                address.setCity(rs.getString(4));
                address.setCountryCode(rs.getString(5));
                address.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                address.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                address.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(8)));
                if (address.getTimeDeleted() == null) {
                    result.add(address);
                }
            }

            log.debug("found entries: " + result.size());

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public Address findOneById(Address toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);
        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        Address result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT ID, STREET, ZIP, CITY, COUNTRY, TIME_CREATED, TIME_MODIFIED, TIME_DELETED FROM ADDRESS WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new Address();
                result.setId(rs.getInt(1));
                result.setStreet(rs.getString(2));
                result.setZip(rs.getString(3));
                result.setCity(rs.getString(4));
                result.setCountryCode(rs.getString(5));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(8)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
