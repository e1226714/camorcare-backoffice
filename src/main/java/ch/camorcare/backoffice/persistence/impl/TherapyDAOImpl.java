package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.persistence.TherapyDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the Therapy table stored in the
 * database.
 */
public class TherapyDAOImpl implements TherapyDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items by a given therapy plan.
     */
    private static PreparedStatement preparedStatementFindAllByTherapyPlan = null;

    /**
     * Prepared statement for finding all items by a given patient.
     */
    private static PreparedStatement preparedStatementFindAllByPatient = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(PatientDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<Therapy> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<Therapy> validator) {
        this.validator = validator;
    }

    @Override
    public Therapy create(Therapy toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);

        Therapy copy = new Therapy(toCreate);
        validator.validate(toCreate);

        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO therapy (patient, date_sick_leave, date_from, date_until, time_created, time_modified) VALUES (?, ?, ?, ?, ?, ?);";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setInt(1, toCreate.getPatient().getId());
            preparedStatementCreate.setDate(2, DateTimeConverter.dateTimeToSqlDate(toCreate.getDateSickLeave()));
            preparedStatementCreate.setDate(3, DateTimeConverter.dateTimeToSqlDate(toCreate.getDateFrom()));
            preparedStatementCreate.setDate(4, DateTimeConverter.dateTimeToSqlDate(toCreate.getDateUntil()));
            preparedStatementCreate.setTimestamp(5, now);
            preparedStatementCreate.setTimestamp(6, now);

            log.debug("resulting statement: " + preparedStatementCreate);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();
            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("resulting key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public Therapy update(Therapy toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE therapy SET patient = ?, date_sick_leave = ?, date_from = ?, date_until = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementUpdate.setInt(1, toUpdate.getPatient().getId());
            preparedStatementUpdate.setDate(2, DateTimeConverter.dateTimeToSqlDate(toUpdate.getDateSickLeave()));
            preparedStatementUpdate.setDate(3, DateTimeConverter.dateTimeToSqlDate(toUpdate.getDateFrom()));
            preparedStatementUpdate.setDate(4, DateTimeConverter.dateTimeToSqlDate(toUpdate.getDateUntil()));
            preparedStatementUpdate.setTimestamp(5, now);
            preparedStatementUpdate.setInt(6, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);

            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(Therapy toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Therapy was already deleted.");
            throw new PersistenceException("Therapy was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE therapy SET time_modified = ?, time_deleted = ? WHERE id = ?;";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, now);
            preparedStatementDelete.setTimestamp(2, now);
            preparedStatementDelete.setInt(3, toDelete.getId());
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<Therapy> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<Therapy> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, patient, date_sick_leave, date_from, date_until, time_created, time_modified, time_deleted FROM therapy WHERE time_deleted IS NULL ORDER BY date_from ASC";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Therapy therapy = new Therapy();
                therapy.setId(rs.getInt(1));
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));
                therapy.setPatient(patient);
                therapy.setDateSickLeave(DateTimeConverter.sqlDateToDateTime(rs.getDate(3)));
                therapy.setDateFrom(DateTimeConverter.sqlDateToDateTime(rs.getDate(4)));
                therapy.setDateUntil(DateTimeConverter.sqlDateToDateTime(rs.getDate(5)));
                therapy.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                therapy.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                therapy.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(8)));

                if (therapy.getTimeDeleted() == null) {
                    result.add(therapy);
                }
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<Therapy> findAllByTherapyPlan(TherapyPlan original) throws PersistenceException {
        log.debug("reading all entries");
        List<Therapy> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapyPlan == null) {
                String sql = "SELECT id, patient, date_sick_leave, date_from, date_until, time_created, time_modified FROM therapy "
                        + "WHERE time_deleted IS NULL AND (? >= date_from AND ? <= date_until)";
                preparedStatementFindAllByTherapyPlan = connectionManager.getConnection().prepareStatement(sql);
            }

            if (original != null && original.getDateFrom() != null) {
                preparedStatementFindAllByTherapyPlan.setDate(1, DateTimeConverter.dateTimeToSqlDate(original.getDateFrom()));
                preparedStatementFindAllByTherapyPlan.setDate(2, DateTimeConverter.dateTimeToSqlDate(original.getDateFrom()));
            }
            else {
                throw new PersistenceException("TherapyPlan DateFrom invalid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapyPlan);
            ResultSet rs = preparedStatementFindAllByTherapyPlan.executeQuery();

            while (rs.next()) {
                Therapy therapy = new Therapy();
                therapy.setId(rs.getInt(1));
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));
                therapy.setPatient(patient);
                therapy.setDateSickLeave(DateTimeConverter.sqlDateToDateTime(rs.getDate(3)));
                therapy.setDateFrom(DateTimeConverter.sqlDateToDateTime(rs.getDate(4)));
                therapy.setDateUntil(DateTimeConverter.sqlDateToDateTime(rs.getDate(5)));
                therapy.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                therapy.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));

                result.add(therapy);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<Therapy> findAllByPatient(Patient patient) throws PersistenceException {
        log.debug("reading all entries");
        List<Therapy> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByPatient == null) {
                String sql = "SELECT id, patient, date_sick_leave, date_from, date_until, time_created, time_modified FROM therapy "
                        + "WHERE time_deleted IS NULL AND patient = ?";
                preparedStatementFindAllByPatient = connectionManager.getConnection().prepareStatement(sql);
            }

            if (patient != null && patient.getId() != null) {
                preparedStatementFindAllByPatient.setInt(1, patient.getId());
            }
            else {
                throw new PersistenceException("Patient ID is invalid");
            }
            log.debug("resulting statement: " + preparedStatementFindAllByPatient);
            ResultSet rs = preparedStatementFindAllByPatient.executeQuery();

            while (rs.next()) {
                Therapy therapy = new Therapy();
                therapy.setId(rs.getInt(1));
                therapy.setPatient(patient);
                therapy.setDateSickLeave(DateTimeConverter.sqlDateToDateTime(rs.getDate(3)));
                therapy.setDateFrom(DateTimeConverter.sqlDateToDateTime(rs.getDate(4)));
                therapy.setDateUntil(DateTimeConverter.sqlDateToDateTime(rs.getDate(5)));
                therapy.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                therapy.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));

                result.add(therapy);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public Therapy findOneById(Therapy toFind) throws PersistenceException {
        log.debug("reading entry with ID: " + toFind.getId());
        if (toFind == null) {
            throw new NullPointerException();
        }
        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        Therapy result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, patient, date_sick_leave, date_from, date_until, time_created, time_modified, time_deleted FROM therapy WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());
            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new Therapy();
                result.setId(rs.getInt(1));
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));
                result.setPatient(patient);
                result.setDateSickLeave(DateTimeConverter.sqlDateToDateTime(rs.getDate(3)));
                result.setDateFrom(DateTimeConverter.sqlDateToDateTime(rs.getDate(4)));
                result.setDateUntil(DateTimeConverter.sqlDateToDateTime(rs.getDate(5)));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(8)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
