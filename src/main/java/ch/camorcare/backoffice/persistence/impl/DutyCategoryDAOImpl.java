package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.DutyCategory;
import ch.camorcare.backoffice.persistence.DutyCategoryDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the DutyCategory table stored in
 * the database.
 */
public class DutyCategoryDAOImpl implements DutyCategoryDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the creation of the referenced weekdays.
     */
    private static PreparedStatement preparedStatementCreateReferences = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the update of the referenced weekdays.
     */
    private static PreparedStatement preparedStatementUpdateReferences = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for the deletion of the referenced weekdays.
     */
    private static PreparedStatement preparedStatementDeleteReferences = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all related weekday items.
     */
    private static PreparedStatement preparedStatementFindAllReferences = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(DutyCategoryDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<DutyCategory> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be set
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<DutyCategory> validator) {
        this.validator = validator;
    }

    @Override
    public DutyCategory create(DutyCategory toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);
        validator.validate(toCreate);

        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        DutyCategory retVal = new DutyCategory(toCreate);

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO duty_category (name, duration, time_created, time_modified) "
                        + "VALUES (?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }
            preparedStatementCreate.setString(1, toCreate.getName());
            preparedStatementCreate.setInt(2, toCreate.getDuration());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setTimestamp(3, ts);
            preparedStatementCreate.setTimestamp(4, ts);

            log.debug("resulting statement: " + preparedStatementCreate);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                retVal.setId(keyResultSet.getInt(1));
                log.debug("resulting key: " + retVal.getId());
            }

            keyResultSet.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
        return retVal;
    }

    @Override
    public DutyCategory update(DutyCategory toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        DutyCategory copy = new DutyCategory(toUpdate);
        validator.validate(copy);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE duty_category SET name = ?, duration = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }
            preparedStatementUpdate.setString(1, toUpdate.getName());
            preparedStatementUpdate.setInt(2, toUpdate.getDuration());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementUpdate.setTimestamp(3, ts);
            preparedStatementUpdate.setInt(4, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);
            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public void delete(DutyCategory toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("DutyCategory was already deleted.");
            throw new PersistenceException("DutyCategory was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE duty_category SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<DutyCategory> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<DutyCategory> result = new ArrayList<>();
        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, name, duration, time_created, time_modified FROM duty_category WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                DutyCategory dutyCategory = new DutyCategory();
                dutyCategory.setId(rs.getInt(1));
                dutyCategory.setName(rs.getString(2));
                dutyCategory.setDuration(rs.getInt(3));
                dutyCategory.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                dutyCategory.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(dutyCategory);
            }

            log.debug("found entries: " + result.size());

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public DutyCategory findOneById(DutyCategory toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);
        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        DutyCategory result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, name, duration, time_created, time_modified, time_deleted FROM duty_category WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new DutyCategory();
                result.setId(rs.getInt(1));
                result.setName(rs.getString(2));
                result.setDuration(rs.getInt(3));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
             throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            //throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
