package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.persistence.TherapistDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the Therapist table stored in
 * the database.
 */
public class TherapistDAOImpl implements TherapistDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all therapists by therapy type.
     */
    private static PreparedStatement preparedStatementFindAllByTherapyType = null;

    /**
     * Prepared statement for finding all therapists by therapy plan.
     */
    private static PreparedStatement preparedStatementFindAllByTherapyPlan = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapistDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<Therapist> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<Therapist> validator) {
        this.validator = validator;
    }

    @Override
    public Therapist create(Therapist toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);

        Therapist copy = new Therapist(toCreate);
        validator.validate(toCreate);

        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO therapist (first_name, last_name, gender, email, time_created, time_modified) VALUES (?, ?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setString(1, toCreate.getFirstName());
            preparedStatementCreate.setString(2, toCreate.getLastName());
            preparedStatementCreate.setString(3, toCreate.getGender().name());
            preparedStatementCreate.setString(4, toCreate.getEmail());
            preparedStatementCreate.setTimestamp(5, now);
            preparedStatementCreate.setTimestamp(6, now);

            log.debug("resulting statement: " + preparedStatementCreate);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();
            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("resulting key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public Therapist update(Therapist toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE therapist SET first_name = ?, last_name = ?, gender = ?, email = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementUpdate.setString(1, toUpdate.getFirstName());
            preparedStatementUpdate.setString(2, toUpdate.getLastName());
            preparedStatementUpdate.setString(3, toUpdate.getGender().name());
            preparedStatementUpdate.setString(4, toUpdate.getEmail());
            preparedStatementUpdate.setTimestamp(5, now);
            preparedStatementUpdate.setInt(6, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);

            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(Therapist toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Therapist was already deleted.");
            throw new PersistenceException("Therapist was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE therapist SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, now);
            preparedStatementDelete.setTimestamp(2, now);
            preparedStatementDelete.setInt(3, toDelete.getId());
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<Therapist> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<Therapist> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, first_name, last_name, gender, email, time_created, time_modified, time_deleted FROM therapist WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Therapist therapist = new Therapist();

                therapist.setId(rs.getInt(1));
                therapist.setFirstName(rs.getString(2));
                therapist.setLastName(rs.getString(3));
                therapist.setGender(Gender.valueOf(rs.getString(4).toUpperCase()));
                therapist.setEmail(rs.getString(5));
                therapist.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                therapist.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                therapist.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(8)));

                result.add(therapist);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<Therapist> findAllByTherapyType(TherapyType therapyType) throws PersistenceException {
        log.debug("reading all entries filtered by therapy type");
        List<Therapist> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapyType == null) {
                String sql = "SELECT id, first_name, last_name, gender, email, time_created, time_modified " +
                        "FROM therapist JOIN join_therapist_therapy_type ON therapist.id = join_therapist_therapy_type.therapist " +
                        "WHERE join_therapist_therapy_type.therapy_type = ? AND time_deleted IS NULL";
                preparedStatementFindAllByTherapyType = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyType != null && therapyType.getId() != null) {
                preparedStatementFindAllByTherapyType.setInt(1, therapyType.getId());
            }
            else {
                throw new PersistenceException("TherapyType ID is invalid");
            }
            log.debug("resulting statement: " + preparedStatementFindAllByTherapyType);
            ResultSet rs = preparedStatementFindAllByTherapyType.executeQuery();

            while (rs.next()) {
                Therapist therapist = new Therapist();

                therapist.setId(rs.getInt(1));
                therapist.setFirstName(rs.getString(2));
                therapist.setLastName(rs.getString(3));
                therapist.setGender(Gender.valueOf(rs.getString(4).toUpperCase()));
                therapist.setEmail(rs.getString(5));
                therapist.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                therapist.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));

                result.add(therapist);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<Therapist> findAllByTherapyPlan(TherapyPlan therapyPlan) throws PersistenceException {
        log.debug("reading all entries filtered by therapy plan");
        List<Therapist> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByTherapyPlan == null) {
                String sql = "SELECT DISTINCT therapist.id, first_name, last_name, gender, email, therapist.time_created, therapist.time_modified " +
                        "FROM therapist JOIN therapy_event ON therapist.id = therapy_event.therapist " +
                        "WHERE therapy_event.therapy_plan = ? AND therapist.time_deleted IS NULL AND therapy_event.time_deleted IS NULL ORDER BY last_name ASC";
                preparedStatementFindAllByTherapyPlan = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyPlan != null && therapyPlan.getId() != null) {
                preparedStatementFindAllByTherapyPlan.setInt(1, therapyPlan.getId());
            }
            else {
                throw new PersistenceException("TherapyPlan ID is invalid!");
            }
            log.debug("resulting statement: " + preparedStatementFindAllByTherapyPlan);
            ResultSet rs = preparedStatementFindAllByTherapyPlan.executeQuery();

            while (rs.next()) {
                Therapist therapist = new Therapist();

                therapist.setId(rs.getInt(1));
                therapist.setFirstName(rs.getString(2));
                therapist.setLastName(rs.getString(3));
                therapist.setGender(Gender.valueOf(rs.getString(4).toUpperCase()));
                therapist.setEmail(rs.getString(5));
                therapist.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                therapist.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));

                result.add(therapist);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public Therapist findOneById(Therapist toFind) throws PersistenceException {
        log.debug("reading entry with ID: " + toFind.getId());
        if (toFind == null) {
            throw new NullPointerException();
        }
        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        Therapist result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, first_name, last_name, gender, email, time_created, time_modified, time_deleted FROM therapist WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());
            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new Therapist();
                result.setId(rs.getInt(1));
                result.setFirstName(rs.getString(2));
                result.setLastName(rs.getString(3));
                result.setGender(Gender.valueOf(rs.getString(4).toUpperCase()));
                result.setEmail(rs.getString(5));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(8)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
