package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Allergy;
import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.persistence.AllergyDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the Allergy table stored in the
 * database.
 */
public class AllergyDAOImpl implements AllergyDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items by patient.
     */
    private static PreparedStatement preparedStatementFindAllByPatient = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(AllergyDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<Allergy> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<Allergy> validator) {
        this.validator = validator;
    }

    @Override
    public Allergy create(Allergy toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);
        Allergy copy = new Allergy(toCreate);
        validator.validate(toCreate);
        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }
        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO ALLERGY (NAME, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }
            // Set PreparedStatement
            preparedStatementCreate.setString(1, toCreate.getName());
            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setTimestamp(2, ts);
            preparedStatementCreate.setTimestamp(3, ts);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public Allergy update(Allergy toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE ALLERGY SET name = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            preparedStatementUpdate.setString(1, toUpdate.getName());
            preparedStatementUpdate.setTimestamp(2, ts);
            preparedStatementUpdate.setInt(3, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);

            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(Allergy toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Allergy was already deleted.");
            throw new PersistenceException("Allergy was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE ALLERGY SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("Nothing deleted");
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<Allergy> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<Allergy> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, name, time_created, time_modified FROM patient_allergy WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Allergy Allergy = new Allergy();
                Allergy.setId(rs.getInt(1));
                Allergy.setName(rs.getString(2));
                Allergy.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(3)));
                Allergy.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));

                result.add(Allergy);
            }

            log.debug("found entries: " + result.size());

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public Allergy findOneById(Allergy toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);
        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        Allergy result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, name, time_created, time_modified, time_deleted FROM patient_allergy WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new Allergy();
                result.setId(rs.getInt(1));
                result.setName(rs.getString(2));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(3)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<Allergy> findAllByPatient(Patient patient) throws PersistenceException {
        log.debug("reading all entries for patient #" + patient.getId());
        List<Allergy> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByPatient == null) {
                String sql = "SELECT id, name, time_created, time_modified, time_deleted FROM patient_allergy JOIN allergy ON (patient_allergy.allergy_id = allergy.id) WHERE patient_id = ? AND time_deleted IS NULL";
                preparedStatementFindAllByPatient = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindAllByPatient.setInt(1, patient.getId());
            log.debug("resulting statement: " + preparedStatementFindAllByPatient);
            ResultSet rs = preparedStatementFindAllByPatient.executeQuery();

            while (rs.next()) {
                Allergy allergy = new Allergy();
                allergy.setId(rs.getInt(1));
                allergy.setName(rs.getString(2));
                allergy.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(3)));
                allergy.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));

                result.add(allergy);
            }

            log.debug("found entries: " + result.size());
            rs.close();
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            // throw new PersistenceException("SQL Exception thrown: " + e);
            throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public void deleteByPatient(Patient patient) throws PersistenceException {
        throw new PersistenceException("This command is not supported!");
    }
}
