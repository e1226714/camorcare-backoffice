package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.persistence.TherapyPlanDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the TherapyPlan table stored in
 * the database.
 */
public class TherapyPlanDAOImpl implements TherapyPlanDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(TherapyPlanDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<TherapyPlan> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<TherapyPlan> validator) {
        this.validator = validator;
    }

    @Override
    public TherapyPlan create(TherapyPlan toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);

        TherapyPlan copy = new TherapyPlan(toCreate);
        validator.validate(toCreate);

        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO therapy_plan (date_from, date_until, time_created, time_modified) VALUES (?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setDate(1, DateTimeConverter.dateTimeToSqlDate(toCreate.getDateFrom()));
            preparedStatementCreate.setDate(2, DateTimeConverter.dateTimeToSqlDate(toCreate.getDateUntil()));
            preparedStatementCreate.setTimestamp(3, now);
            preparedStatementCreate.setTimestamp(4, now);

            log.debug("resulting statement: " + preparedStatementCreate);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();
            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("resulting key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public TherapyPlan update(TherapyPlan toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE therapy_plan SET date_from = ?, date_until = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementUpdate.setDate(1, DateTimeConverter.dateTimeToSqlDate(toUpdate.getDateFrom()));
            preparedStatementUpdate.setDate(2, DateTimeConverter.dateTimeToSqlDate(toUpdate.getDateUntil()));
            preparedStatementUpdate.setTimestamp(3, now);
            preparedStatementUpdate.setInt(4, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);

            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(TherapyPlan toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("TherapyPlan was already deleted.");
            throw new PersistenceException("TherapyPlan was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE therapy_plan SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, now);
            preparedStatementDelete.setTimestamp(2, now);
            preparedStatementDelete.setInt(3, toDelete.getId());
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<TherapyPlan> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<TherapyPlan> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, date_from, date_until, time_created, time_modified, time_deleted FROM therapy_plan WHERE time_deleted IS NULL ORDER BY date_from ASC";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                TherapyPlan therapyPlan = new TherapyPlan();
                therapyPlan.setId(rs.getInt(1));
                therapyPlan.setDateFrom(DateTimeConverter.sqlDateToDateTime(rs.getDate(2)));
                therapyPlan.setDateUntil(DateTimeConverter.sqlDateToDateTime(rs.getDate(3)));
                therapyPlan.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                therapyPlan.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));

                result.add(therapyPlan);
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public TherapyPlan findOneById(TherapyPlan toFind) throws PersistenceException {
        log.debug("reading entry with ID: " + toFind.getId());

        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        TherapyPlan result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, date_from, date_until, time_created, time_modified, time_deleted FROM therapy_plan WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());
            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new TherapyPlan();
                result.setId(rs.getInt(1));
                result.setDateFrom(DateTimeConverter.sqlDateToDateTime(rs.getDate(2)));
                result.setDateUntil(DateTimeConverter.sqlDateToDateTime(rs.getDate(3)));
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(4)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

}
