package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.persistence.PatientBasedTherapyTypeOccurrenceConstraintDAO;
import ch.camorcare.backoffice.persistence.TherapyTypeDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides an implementation of a Data Access Object to specifically access the
 * PatientBasedTherapyTypeOccurrenceConstraint table stored in the database.
 */
public class PatientBasedTherapyTypeOccurrenceConstraintDAOImpl implements PatientBasedTherapyTypeOccurrenceConstraintDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDeleteByPatient = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding all items for a patient.
     */
    private static PreparedStatement preparedStatementFindAllByPatient = null;

    /**
     *
     */
    private static PreparedStatement preparedStatementFindAllByTherapyPlanGroupedByTherapyType = null;

    /**
     *
     */
    private static PreparedStatement preparedStatementFindAllGroupConstraintsByTherapyPlan = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(PatientBasedTherapyTypeOccurrenceConstraintDAOImpl.class.getName());

    /**
     *
     */
    private TherapyTypeDAO therapyTypeDAO;

    /**
     * Validates a DTO.
     */
    private EntityValidator<PatientBasedTherapyTypeOccurrenceConstraint> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public void setValidator(EntityValidator<PatientBasedTherapyTypeOccurrenceConstraint> validator) {
        this.validator = validator;
    }

    public void setTherapyTypeDAO(TherapyTypeDAO therapyTypeDAO) {
        this.therapyTypeDAO = therapyTypeDAO;
    }

    @Override
    public PatientBasedTherapyTypeOccurrenceConstraint create(PatientBasedTherapyTypeOccurrenceConstraint toCreate) throws PersistenceException, ValidationException {
        PatientBasedTherapyTypeOccurrenceConstraint copy = new PatientBasedTherapyTypeOccurrenceConstraint(toCreate);
        log.debug("persisting new entry: " + copy);

        if (copy.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        validator.validate(toCreate);

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO PBC_THERAPY_TYPE_OCCURRENCE (PATIENT, THERAPY_TYPE, THERAPY_MODE, MAX_PER_WEEK, TIME_CREATED, TIME_MODIFIED) VALUES (?, ?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            preparedStatementCreate.setInt(1, copy.getPatient().getId());
            preparedStatementCreate.setInt(2, copy.getTherapyType().getId());
            preparedStatementCreate.setString(3, copy.getTherapyMode().name());
            preparedStatementCreate.setInt(4, copy.getMaxOccurencePerWeek());
            preparedStatementCreate.setTimestamp(5, ts);
            preparedStatementCreate.setTimestamp(6, ts);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();

            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("Generated primary key: " + copy.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    /**
     * Updates the maximum occurrence value of this constraint.
     *
     * @param toUpdate The DTO to be updated
     * @return
     * @throws PersistenceException
     * @throws ValidationException
     */
    @Override
    public PatientBasedTherapyTypeOccurrenceConstraint update(PatientBasedTherapyTypeOccurrenceConstraint toUpdate) throws PersistenceException, ValidationException {
        PatientBasedTherapyTypeOccurrenceConstraint copy = new PatientBasedTherapyTypeOccurrenceConstraint(toUpdate);
        log.debug("persisting updated entry: " + copy);

        if (copy.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        validator.validate(toUpdate);

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE PBC_THERAPY_TYPE_OCCURRENCE SET MAX_PER_WEEK = ?, TIME_MODIFIED = ? WHERE ID = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());

            preparedStatementUpdate.setInt(1, copy.getMaxOccurencePerWeek());
            preparedStatementUpdate.setTimestamp(2, ts);
            preparedStatementUpdate.setInt(3, copy.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);

            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public void delete(PatientBasedTherapyTypeOccurrenceConstraint toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Constraint has already been deleted.");
            throw new PersistenceException("Constraint has already been deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE PBC_THERAPY_TYPE_OCCURRENCE SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE TIME_DELETED IS NULL AND ID = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, ts);
            preparedStatementDelete.setTimestamp(2, ts);
            preparedStatementDelete.setInt(3, toDelete.getId());

            log.debug("resulting statement: " + preparedStatementDelete);
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public void deleteAllByPatient(Patient patient) throws PersistenceException {
        log.debug("deleting entries for patient");

        if (patient == null || patient.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementDeleteByPatient == null) {
                String sql = "UPDATE PBC_THERAPY_TYPE_OCCURRENCE SET TIME_MODIFIED = ?, TIME_DELETED = ? WHERE TIME_DELETED IS NULL AND PATIENT = ?";
                preparedStatementDeleteByPatient = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp ts = new Timestamp(new java.util.Date().getTime());
            preparedStatementDeleteByPatient.setTimestamp(1, ts);
            preparedStatementDeleteByPatient.setTimestamp(2, ts);
            preparedStatementDeleteByPatient.setInt(3, patient.getId());

            log.debug("resulting statement: " + preparedStatementDeleteByPatient);
            preparedStatementDeleteByPatient.execute();

            if (preparedStatementDeleteByPatient.getUpdateCount() <= 0) {
                throw new PersistenceException("No rows deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<PatientBasedTherapyTypeOccurrenceConstraint> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<PatientBasedTherapyTypeOccurrenceConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT ID, PATIENT, THERAPY_TYPE, THERAPY_MODE, MAX_PER_WEEK, TIME_CREATED, TIME_MODIFIED FROM PBC_THERAPY_TYPE_OCCURRENCE WHERE TIME_DELETED IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));

                PatientBasedTherapyTypeOccurrenceConstraint constraint = new PatientBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapyType(therapyType);
                constraint.setTherapyMode(TherapyMode.valueOf(rs.getString(4)));
                constraint.setMaxOccurencePerWeek(rs.getInt(5));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<PatientBasedTherapyTypeOccurrenceConstraint> findAllByPatient(Patient patient, boolean eagerLoading) throws PersistenceException {
        log.debug("reading all entries");
        List<PatientBasedTherapyTypeOccurrenceConstraint> result = new ArrayList<>();

        try {
            if (preparedStatementFindAllByPatient == null) {
                String sql = "SELECT ID, THERAPY_TYPE, THERAPY_MODE, MAX_PER_WEEK, TIME_CREATED, TIME_MODIFIED FROM PBC_THERAPY_TYPE_OCCURRENCE WHERE TIME_DELETED IS NULL AND PATIENT = ?";
                preparedStatementFindAllByPatient = connectionManager.getConnection().prepareStatement(sql);
            }

            if (patient != null && patient.getId() != null) {
                preparedStatementFindAllByPatient.setInt(1, patient.getId());
            }
            else {
                throw new PersistenceException("Patient ID is invalid!");
            }
            log.debug("resulting statement: " + preparedStatementFindAllByPatient);
            ResultSet rs = preparedStatementFindAllByPatient.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(2));

                if(eagerLoading) {
                    therapyType = therapyTypeDAO.findOneById(therapyType);
                }

                PatientBasedTherapyTypeOccurrenceConstraint constraint = new PatientBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapyType(therapyType);
                constraint.setTherapyMode(TherapyMode.valueOf(rs.getString(3)));
                constraint.setMaxOccurencePerWeek(rs.getInt(4));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public Map<TherapyType, Map<String, Integer>> findAllStatsByTherapyPlanGroupedByTherapyType(TherapyPlan therapyPlan) throws PersistenceException {
        Map<TherapyType, Map<String, Integer>> result = new HashMap<>();

        try {
            if (preparedStatementFindAllByTherapyPlanGroupedByTherapyType == null) {
                String sql = "SELECT THERAPY_TYPE, MIN(MAX_PER_WEEK) AS MIN, MAX(MAX_PER_WEEK) AS MAX, SUM(MAX_PER_WEEK) AS SUM, COUNT(*) AS COUNT "
                        + "FROM PBC_THERAPY_TYPE_OCCURRENCE WHERE THERAPY_MODE = 'GROUP' AND TIME_DELETED IS NULL AND PATIENT IN ("
                        + "   SELECT PATIENT FROM THERAPY WHERE TIME_DELETED IS NULL AND ("
                        + "       (SELECT DATE_FROM FROM THERAPY_PLAN WHERE THERAPY_PLAN.ID = ?) >= DATE_FROM AND "
                        + "       (SELECT DATE_UNTIL FROM THERAPY_PLAN WHERE THERAPY_PLAN.ID = ?) <= DATE_UNTIL"
                        + "   )"
                        + ") GROUP BY THERAPY_TYPE";

                preparedStatementFindAllByTherapyPlanGroupedByTherapyType = connectionManager.getConnection().prepareStatement(sql);
            }

            if (therapyPlan != null && therapyPlan.getId() != null) {
                //noinspection JpaQueryApiInspection
                preparedStatementFindAllByTherapyPlanGroupedByTherapyType.setInt(1, therapyPlan.getId());
                //noinspection JpaQueryApiInspection
                preparedStatementFindAllByTherapyPlanGroupedByTherapyType.setInt(2, therapyPlan.getId());
            }
            else {
                throw new PersistenceException("TherapyPlan ID is not valid!");
            }

            log.debug("resulting statement: " + preparedStatementFindAllByTherapyPlanGroupedByTherapyType);
            ResultSet rs = preparedStatementFindAllByTherapyPlanGroupedByTherapyType.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(1));

                if (!result.containsKey(therapyType)) {
                    result.put(therapyType, new HashMap<>());
                }

                result.get(therapyType).put("MIN", rs.getInt(2));
                result.get(therapyType).put("MAX", rs.getInt(3));
                result.get(therapyType).put("SUM", rs.getInt(4));
                result.get(therapyType).put("COUNT", rs.getInt(5));
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public List<PatientBasedTherapyTypeOccurrenceConstraint> findAllByTherapyPlanGroupedByTherapyType(TherapyPlan therapyPlan) throws PersistenceException {
        // TODO Please implement
        return null;
    }

    public List<PatientBasedTherapyTypeOccurrenceConstraint> findAllGroupConstraintsByTherapyPlanGroupedTherapyType(TherapyPlan therapyPlan) throws PersistenceException {
        // TODO Implement?
       /*
        log.debug("reading all entries");
        Map<TherapyType, List<PatientBasedTherapyTypeOccurrenceConstraint>> result = new HashMap<>();

        try {
            if (preparedStatementFindAllGroupConstraintsByTherapyPlan == null) {
                String sql = "SELECT ID, THERAPY_TYPE, THERAPY_MODE, MAX_PER_WEEK, TIME_CREATED, TIME_MODIFIED FROM PBC_THERAPY_TYPE_OCCURRENCE "
                           + "WHERE TIME_DELETED IS NULL AND THERAPY_MODE = 'GROUP' AND PATIENT IN ("
                           + "  SELECT PATIENT FROM THERAPY WHERE TIME_DELETED IS NULL AND ("
                           + "    (SELECT DATE_FROM  FROM THERAPY_PLAN WHERE THERAPY_PLAN.ID = ?) >= DATE_FROM AND "
                           + "    (SELECT DATE_UNTIL FROM THERAPY_PLAN WHERE THERAPY_PLAN.ID = ?) <= DATE_UNTIL"
                           + "  )) GROUP BY PBC_THERAPY_TYPE_OCCURRENCE.THERAPY_TYPE";

                preparedStatementFindAllGroupConstraintsByTherapyPlan = connectionManager.getConnection().prepareStatement(sql);
            }

            /*

            SELECT THERAPY_TYPE, MAX(MAX_PER_WEEK) FROM PBC_THERAPY_TYPE_OCCURRENCE WHERE THERAPY_MODE = 'GROUP' AND PATIENT IN (
                    SELECT patient FROM therapy WHERE time_deleted IS NULL AND
                            ((SELECT DATE_FROM FROM THERAPY_PLAN WHERE THERAPY_PLAN.ID = 6) >= date_from AND (SELECT DATE_UNTIL FROM THERAPY_PLAN WHERE THERAPY_PLAN.ID = 6) <= date_until)
            ) GROUP BY THERAPY_TYPE


            * /
            preparedStatementFindAllGroupConstraintsByTherapyPlan.setInt(1, therapyPlan.getId());
            preparedStatementFindAllGroupConstraintsByTherapyPlan.setInt(2, therapyPlan.getId());
            log.debug("resulting statement: " + preparedStatementFindAllGroupConstraintsByTherapyPlan);
            ResultSet rs = preparedStatementFindAllGroupConstraintsByTherapyPlan.executeQuery();

            while (rs.next()) {
                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(2));

                if(!result.containsKey(therapyType)) {
                    result.put(therapyType, new ArrayList<>());
                }

                List<PatientBasedTherapyTypeOccurrenceConstraint> constraints = result.get(therapyType);



                PatientBasedTherapyTypeOccurrenceConstraint constraint = new PatientBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapyType(therapyType);
                constraint.setTherapyMode(TherapyMode.valueOf(rs.getString(3)));
                constraint.setMaxOccurencePerWeek(rs.getInt(4));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));

                result.add(constraint);
            }

            log.debug("found entries: " + result.size());
            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e);
        }

        return result;
        */
        return null;
    }

    @Override
    public PatientBasedTherapyTypeOccurrenceConstraint findOneById(PatientBasedTherapyTypeOccurrenceConstraint toFind) throws PersistenceException {
        log.debug("looking up entry: " + toFind);

        if (toFind == null) {
            throw new NullPointerException();
        }

        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        PatientBasedTherapyTypeOccurrenceConstraint constraint = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT ID, PATIENT, THERAPY_TYPE, MAX_PER_WEEK, TIME_CREATED, TIME_MODIFIED, TIME_DELETED FROM PBC_THERAPY_TYPE_OCCURRENCE WHERE ID = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());

            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                Patient patient = new Patient();
                patient.setId(rs.getInt(2));

                TherapyType therapyType = new TherapyType();
                therapyType.setId(rs.getInt(3));

                constraint = new PatientBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(rs.getInt(1));
                constraint.setPatient(patient);
                constraint.setTherapyType(therapyType);
                constraint.setMaxOccurencePerWeek(rs.getInt(4));
                constraint.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(5)));
                constraint.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(6)));
                constraint.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(7)));
            }

            if (constraint == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

            rs.close();

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return constraint;
    }
}
