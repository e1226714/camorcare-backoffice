package ch.camorcare.backoffice.persistence.impl;

import ch.camorcare.backoffice.entities.Address;
import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.persistence.PatientDAO;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.converter.DateTimeConverter;
import ch.camorcare.backoffice.util.validator.ValidationException;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an implementation of a Data Access Object to specifically access the Patient table stored in the
 * database.
 */
public class PatientDAOImpl implements PatientDAO {

    /**
     * Prepared statement for the creation.
     */
    private static PreparedStatement preparedStatementCreate = null;

    /**
     * Prepared statement for the update.
     */
    private static PreparedStatement preparedStatementUpdate = null;

    /**
     * Prepared statement for the deletion.
     */
    private static PreparedStatement preparedStatementDelete = null;

    /**
     * Prepared statement for finding all items.
     */
    private static PreparedStatement preparedStatementFindAll = null;

    /**
     * Prepared statement for finding one item by id.
     */
    private static PreparedStatement preparedStatementFindOneById = null;

    /**
     * Prepared statement for finding one item by ssnr.
     */
    private static PreparedStatement preparedStatementFindOneBySsnr = null;

    /**
     * Provides a reference to the JDBC Connection Manager.
     */
    private ConnectionManager connectionManager;

    /**
     * Provides logging functionality
     */
    private Logger log = LogManager.getLogger(PatientDAOImpl.class.getName());

    /**
     * Validates a DTO.
     */
    private EntityValidator<Patient> validator;

    /**
     * Sets a reference to the JDBC Connection Manager.
     *
     * @param connectionManager The JDBC Connection Manager to be used
     */
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Sets a reference to the validator.
     *
     * @param validator The Validator to be used
     */
    public void setValidator(EntityValidator<Patient> validator) {
        this.validator = validator;
    }

    @Override
    public Patient create(Patient toCreate) throws PersistenceException, ValidationException {
        log.debug("persisting new entry: " + toCreate);

        Patient copy = new Patient(toCreate);
        validator.validate(toCreate);

        if (toCreate.getId() != null) {
            log.error("ID already assigned");
            throw new PersistenceException("ID not null");
        }

        try {
            if (preparedStatementCreate == null) {
                String sql = "INSERT INTO patient (first_name, last_name, gender, birthdate, ssn, phone, email, address, time_created, time_modified, religion) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                preparedStatementCreate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementCreate.setString(1, toCreate.getFirstName());
            preparedStatementCreate.setString(2, toCreate.getLastName());
            preparedStatementCreate.setString(3, toCreate.getGender().name());
            preparedStatementCreate.setDate(4, DateTimeConverter.dateTimeToSqlDate(toCreate.getBirthdate()));
            preparedStatementCreate.setString(5, toCreate.getSocialSecurityNumber());
            preparedStatementCreate.setString(6, toCreate.getPhone());
            preparedStatementCreate.setString(7, toCreate.getEmail());
            preparedStatementCreate.setInt(8, toCreate.getAddress().getId());
            preparedStatementCreate.setTimestamp(9, now);
            preparedStatementCreate.setTimestamp(10, now);
            preparedStatementCreate.setString(11, toCreate.getReligion());

            log.debug("resulting statement: " + preparedStatementCreate);

            if (preparedStatementCreate.executeUpdate() != 1) {
                log.error("Create failed!");
                throw new PersistenceException("Create failed!");
            }

            ResultSet keyResultSet = preparedStatementCreate.getGeneratedKeys();
            if (keyResultSet.next()) {
                copy.setId(keyResultSet.getInt(1));
                log.debug("resulting key: " + copy.getId());
            }
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return copy;
    }

    @Override
    public Patient update(Patient toUpdate) throws PersistenceException, ValidationException {
        log.debug("persisting updated entry: " + toUpdate);
        validator.validate(toUpdate);

        if (toUpdate.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        try {
            if (preparedStatementUpdate == null) {
                String sql = "UPDATE patient SET first_name = ?, last_name = ?, gender = ?, birthdate = ?, ssn = ?, phone = ?, email = ?, address = ?, time_modified = ? WHERE id = ?";
                preparedStatementUpdate = connectionManager.getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementUpdate.setString(1, toUpdate.getFirstName());
            preparedStatementUpdate.setString(2, toUpdate.getLastName());
            preparedStatementUpdate.setString(3, toUpdate.getGender().name());
            preparedStatementUpdate.setDate(4, DateTimeConverter.dateTimeToSqlDate(toUpdate.getBirthdate()));
            preparedStatementUpdate.setString(5, toUpdate.getSocialSecurityNumber());
            preparedStatementUpdate.setString(6, toUpdate.getPhone());
            preparedStatementUpdate.setString(7, toUpdate.getEmail());
            preparedStatementUpdate.setInt(8, toUpdate.getAddress().getId());
            preparedStatementUpdate.setTimestamp(9, now);
            preparedStatementUpdate.setInt(10, toUpdate.getId());

            log.debug("resulting statement: " + preparedStatementUpdate);

            preparedStatementUpdate.execute();

            if (preparedStatementUpdate.getUpdateCount() <= 0) {
                log.error("nothing updated");
                throw new PersistenceException("nothing updated");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return null;
    }

    @Override
    public void delete(Patient toDelete) throws PersistenceException {
        log.debug("deleting entry: " + toDelete);

        if (toDelete.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }

        if (toDelete.getTimeDeleted() != null) {
            log.error("Patient was already deleted.");
            throw new PersistenceException("Patient was already deleted.");
        }

        try {
            if (preparedStatementDelete == null) {
                String sql = "UPDATE patient SET time_modified = ?, time_deleted = ? WHERE id = ?";
                preparedStatementDelete = connectionManager.getConnection().prepareStatement(sql);
            }

            Timestamp now = new Timestamp(new java.util.Date().getTime());
            preparedStatementDelete.setTimestamp(1, now);
            preparedStatementDelete.setTimestamp(2, now);
            preparedStatementDelete.setInt(3, toDelete.getId());
            preparedStatementDelete.execute();

            if (preparedStatementDelete.getUpdateCount() <= 0) {
                log.error("nothing deleted");
                throw new PersistenceException("nothing deleted");
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }
    }

    @Override
    public List<Patient> findAll() throws PersistenceException {
        log.debug("reading all entries");
        List<Patient> result = new ArrayList<>();

        try {
            if (preparedStatementFindAll == null) {
                String sql = "SELECT id, first_name, last_name, gender, birthdate, ssn, phone, email, religion, address, time_created, time_modified, time_deleted FROM patient WHERE time_deleted IS NULL";
                preparedStatementFindAll = connectionManager.getConnection().prepareStatement(sql);
            }

            log.debug("resulting statement: " + preparedStatementFindAll);
            ResultSet rs = preparedStatementFindAll.executeQuery();

            while (rs.next()) {
                Patient patient = new Patient();

                patient.setId(rs.getInt(1));
                patient.setFirstName(rs.getString(2));
                patient.setLastName(rs.getString(3));
                patient.setGender(Gender.valueOf(rs.getString(4).toUpperCase()));
                patient.setBirthdate(DateTimeConverter.sqlDateToDateTime(rs.getDate(5)));
                patient.setSocialSecurityNumber(rs.getString(6));
                patient.setPhone(rs.getString(7));
                patient.setEmail(rs.getString(8));
                patient.setReligion(rs.getString(9));
                Address address = new Address();
                address.setId(rs.getInt(10));
                patient.setAddress(address);
                patient.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(11)));
                patient.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(12)));
                patient.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(13)));
                if (patient.getTimeDeleted() == null) {
                    result.add(patient);
                }
            }

            log.debug("found entries: " + result.size());
        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    public Patient findOneById(Patient toFind) throws PersistenceException {
        log.debug("reading entry with ID: " + toFind.getId());
        if (toFind == null) {
            throw new NullPointerException();
        }
        if (toFind.getId() == null) {
            log.error("ID not assigned");
            throw new PersistenceException("ID is null");
        }
        Patient result = null;

        try {
            if (preparedStatementFindOneById == null) {
                String sql = "SELECT id, first_name, last_name, gender, birthdate, ssn, phone, email, address, time_created, time_modified, time_deleted FROM patient WHERE id = ?";
                preparedStatementFindOneById = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneById.setInt(1, toFind.getId());
            log.debug("resulting statement: " + preparedStatementFindOneById);
            ResultSet rs = preparedStatementFindOneById.executeQuery();

            while (rs.next()) {
                result = new Patient();
                result.setId(rs.getInt(1));
                result.setFirstName(rs.getString(2));
                result.setLastName(rs.getString(3));
                result.setGender(Gender.valueOf(rs.getString(4).toUpperCase()));
                result.setBirthdate(DateTimeConverter.sqlDateToDateTime(rs.getDate(5)));
                result.setSocialSecurityNumber(rs.getString(6));
                result.setPhone(rs.getString(7));
                result.setEmail(rs.getString(8));
                Address address = new Address();
                address.setId(rs.getInt(9));
                result.setAddress(address);
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(11)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(12)));
            }

            if (result == null) {
                log.error("no entry found with ID: " + toFind.getId());
                throw new PersistenceException("no entry found with ID: " + toFind.getId());
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }

    @Override
    public Patient findOneBySSNR(Patient patient) throws PersistenceException, ValidationException {
        if (patient == null) {
            throw new ValidationException("");
        }

        if (patient.getSocialSecurityNumber() == null) {
            log.error("ssnr not assigned");
            throw new ValidationException("ssnr is null");
        }

        log.debug("reading entry with ssnr: " + patient.getSocialSecurityNumber());
        Patient result = null;

        try {
            if (preparedStatementFindOneBySsnr == null) {
                String sql = "SELECT id, first_name, last_name, gender, birthdate, ssn, phone, email, address, time_created, time_modified, time_deleted FROM patient WHERE time_deleted IS NULL AND ssn = ?";
                preparedStatementFindOneBySsnr = connectionManager.getConnection().prepareStatement(sql);
            }

            preparedStatementFindOneBySsnr.setString(1, patient.getSocialSecurityNumber());
            log.debug("resulting statement: " + preparedStatementFindOneBySsnr);
            ResultSet rs = preparedStatementFindOneBySsnr.executeQuery();

            while (rs.next()) {
                result = new Patient();
                result.setId(rs.getInt(1));
                result.setFirstName(rs.getString(2));
                result.setLastName(rs.getString(3));
                result.setGender(Gender.valueOf(rs.getString(4).toUpperCase()));
                result.setBirthdate(DateTimeConverter.sqlDateToDateTime(rs.getDate(5)));
                result.setSocialSecurityNumber(rs.getString(6));
                result.setPhone(rs.getString(7));
                result.setEmail(rs.getString(8));
                Address address = new Address();
                address.setId(rs.getInt(9));
                result.setAddress(address);
                result.setTimeCreated(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(10)));
                result.setTimeModified(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(11)));
                result.setTimeDeleted(DateTimeConverter.timeStampToDateTime(rs.getTimestamp(12)));
            }

        } catch (SQLException e) {
            log.error("SQL Exception thrown: " + e);
            throw new PersistenceException("SQL Exception thrown: " + e.getMessage());
            // throw new PersistenceException("A database error has occurred. Please check database connection and try again.");
        }

        return result;
    }
}
