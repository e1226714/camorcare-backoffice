package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint} data access object
 * implementations.
 */
public interface PatientBasedTherapistGenderConstraintDAO extends BasicDAO<PatientBasedTherapistGenderConstraint> {

    /**
     * Returns a list of all {@link ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint} objects
     * stored in the database related to a specific {@link ch.camorcare.backoffice.entities.Patient}.
     *
     * @param patient      The Patient by which filtering should take place
     * @param eagerLoading <code>true</code> if eager loading should be applied, else <code>false</code>
     * @return list of constraints
     * @throws ch.camorcare.backoffice.persistence.exception.PersistenceException
     */
    public List<PatientBasedTherapistGenderConstraint> findAllByPatient(Patient patient, boolean eagerLoading) throws PersistenceException;

    /**
     * Delete all {@link ch.camorcare.backoffice.persistence.PatientBasedTherapistGenderConstraintDAO} objects stored in
     * the database that belong to a specific {@link ch.camorcare.backoffice.entities.Patient}.
     *
     * @param patient The Patient by which the deletions should take place.
     * @throws PersistenceException
     */
    public void deleteAllByPatient(Patient patient) throws PersistenceException;

}
