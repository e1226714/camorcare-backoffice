package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientComment;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.PatientComment} data access object implementations.
 */
public interface PatientCommentDAO extends BasicDAO<PatientComment> {

    /**
     * Finds all {@link ch.camorcare.backoffice.entities.PatientComment} objects stored in the database that belong to a
     * specific {@link ch.camorcare.backoffice.entities.Patient}.
     *
     * @param patient The Patient by which filtering should be done
     * @return List of PatientComment objects
     * @throws PersistenceException
     */
    public List<PatientComment> findAllByPatient(Patient patient) throws PersistenceException;
}
