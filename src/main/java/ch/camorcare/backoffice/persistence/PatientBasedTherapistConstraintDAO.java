package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint} data access object
 * implementations.
 */
public interface PatientBasedTherapistConstraintDAO extends BasicDAO<PatientBasedTherapistConstraint> {

    /**
     * Returns a list of all {@link ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint} objects related to
     * a specific {@link ch.camorcare.backoffice.entities.Patient}.
     *
     * @param patient      The Patient by which the database should be filtered
     * @param eagerLoading <code>true</code> if eager loading should be applied, else <code>false</code>
     * @return A list of PatientBasedTherapistConstraint objects
     * @throws ch.camorcare.backoffice.persistence.exception.PersistenceException
     */
    public List<PatientBasedTherapistConstraint> findAllByPatient(Patient patient, boolean eagerLoading) throws PersistenceException;

    /**
     * Deletes the {@link ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint} objects stored in the
     * database that belong to a specific {@link ch.camorcare.backoffice.entities.Patient}.
     *
     * @param patient The Patient by which the deletions should take place
     * @throws PersistenceException
     */
    public void deleteAllByPatient(Patient patient) throws PersistenceException;
}
