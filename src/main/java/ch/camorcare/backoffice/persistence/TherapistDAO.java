package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.Therapist} data access object implementations.
 */
public interface TherapistDAO extends BasicDAO<Therapist> {

    /**
     * Finds all {@link ch.camorcare.backoffice.entities.Therapist} objects in the database that are responsible for a
     * specific {@link ch.camorcare.backoffice.entities.TherapyType}.
     *
     * @param therapyType The TherapyType by which the Therapist objects should be filtered
     * @return A list of Therapist objects
     * @throws PersistenceException
     */
    public List<Therapist> findAllByTherapyType(TherapyType therapyType) throws PersistenceException;

    /**
     * Finds all {@link ch.camorcare.backoffice.entities.Therapist} objects in the database that are working in a given
     * week as represented by a specific {@link ch.camorcare.backoffice.entities.TherapyPlan}.
     *
     * @param therapyPlan The TherapyPlan by which the Therapist objects should be filtered
     * @return A list of Therapist objects
     * @throws PersistenceException
     */
    public List<Therapist> findAllByTherapyPlan(TherapyPlan therapyPlan) throws PersistenceException;
}
