package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.Therapy} data access object implementations.
 */
public interface TherapyDAO extends BasicDAO<Therapy> {

    /**
     * Returns a list of {@link ch.camorcare.backoffice.entities.Therapy} objects taking place in a given week as
     * represented by a specific {@link ch.camorcare.backoffice.entities.TherapyPlan}.
     *
     * @param original The TherapyPlan by which the Therapy objects should be filtered
     * @return A list of Therapy objects
     * @throws PersistenceException
     */
    public List<Therapy> findAllByTherapyPlan(TherapyPlan original) throws PersistenceException;

    /**
     * Finds all {@link ch.camorcare.backoffice.entities.Therapy} objects that a {@link
     * ch.camorcare.backoffice.entities.Patient} attended.
     *
     * @param entity The Patient by which the database should be searched
     * @return
     */
    public List<Therapy> findAllByPatient(Patient entity) throws PersistenceException;
}
