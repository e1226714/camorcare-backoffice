package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint;
import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} data access object
 * implementations.
 */
public interface TherapistBasedTimeBlockConstraintDAO extends BasicDAO<TherapistBasedTimeBlockConstraint> {

    /**
     * Returns a list of all {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} objects stored
     * in the database that belong to a specific {@link ch.camorcare.backoffice.entities.Therapist}.
     *
     * @return A list of TherapistBasedTimeBlockConstraint objects
     */
    public List<TherapistBasedTimeBlockConstraint> findAllByTherapist(Therapist therapist, boolean eagerLoading) throws PersistenceException;

    /**
     * Deletes all {@link ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint} objects stored in the
     * database that belong to a specific {@link ch.camorcare.backoffice.entities.Therapist}.
     *
     * @param therapist The Therapist by which the deletions should be done
     * @throws PersistenceException
     */
    public void deleteByTherapist(Therapist therapist) throws PersistenceException;
}
