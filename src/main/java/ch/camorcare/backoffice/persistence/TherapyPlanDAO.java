package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.TherapyPlan;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.TherapyPlan} data access object implementations.
 */
public interface TherapyPlanDAO extends BasicDAO<TherapyPlan> {

}