package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.Patient} data access object implementations.
 */
public interface PatientDAO extends BasicDAO<Patient> {

    /**
     * Finds a {@link ch.camorcare.backoffice.entities.Patient} by a specific social security number.
     *
     * @param patient The social security number stored inside this Patient is used in the search
     * @return The Patient found to have this social security number, else <code>null</code>
     * @throws PersistenceException
     * @throws ValidationException
     */
    public Patient findOneBySSNR(Patient patient) throws PersistenceException, ValidationException;
}
