package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyRoom;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;

import java.util.List;

/**
 * Interface for {@link ch.camorcare.backoffice.entities.TherapyRoom} data access object implementations.
 */
public interface TherapyRoomDAO extends BasicDAO<TherapyRoom> {

    /**
     * Finds all {@link ch.camorcare.backoffice.entities.TherapyRoom} objects in the database that are reserved in a
     * specific {@link ch.camorcare.backoffice.entities.TherapyPlan}.
     *
     * @param therapyPlan The TherapyPlan to be used in the search function
     * @return A list of TherapyRoom objects
     */
    public List<TherapyRoom> findAllByTherapyPlan(TherapyPlan therapyPlan) throws PersistenceException;
}