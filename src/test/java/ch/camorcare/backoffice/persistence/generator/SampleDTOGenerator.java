package ch.camorcare.backoffice.persistence.generator;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import org.joda.time.DateTime;

import java.util.ArrayList;

public class SampleDTOGenerator {

    private static SampleDTOGenerator instance = null;

    private static Address addressValid;
    private static Address addressInvalid;
    private static Allergy allergyValid;
    private static Allergy allergyInvalid;
    private static DutyCategory dutyCategoryValid;
    private static DutyCategory dutyCategoryInvalid;
    private static Invoice invoiceValid;
    private static Invoice invoiceInvalid;
    private static Patient patientValid;
    private static Patient patientInvalid;
    private static PatientComment patientCommentValid;
    private static PatientComment patientCommentInvalid;
    private static PatientContact patientContactValid;
    private static PatientContact patientContactInvalid;
    private static Therapist therapistValid;
    private static Therapist therapistInvalid;
    private static Therapy therapyValid;
    private static Therapy therapyInvalid;
    private static TherapyEvent therapyEventValid;
    private static TherapyEvent therapyEventInvalid;
    private static TherapyPlan therapyPlanValid;
    private static TherapyPlan therapyPlanInvalid;
    private static TherapyRoom therapyRoomValid;
    private static TherapyRoom therapyRoomInvalid;
    private static TherapyType therapyTypeValid;
    private static TherapyType therapyTypeInvalid;

    private static TimeBlock timeBlockValid;
    private static TimeBlock timeBlockInvalid;

    private static PatientBasedTherapistConstraint patientBasedTherapistConstraintValid;
    private static PatientBasedTherapistConstraint patientBasedTherapistConstraintInvalid;
    private static PatientBasedTherapistGenderConstraint patientBasedTherapistGenderConstraintValid;
    private static PatientBasedTherapistGenderConstraint patientBasedTherapistGenderConstraintInvalid;
    private static PatientBasedTherapyTypeOccurrenceConstraint patientBasedTherapyTypeOccurrenceConstraintValid;
    private static PatientBasedTherapyTypeOccurrenceConstraint patientBasedTherapyTypeOccurrenceConstraintInvalid;

    private static TherapistBasedTimeBlockConstraint therapistBasedTimeBlockConstraintValid;
    private static TherapistBasedTimeBlockConstraint therapistBasedTimeBlockConstraintInvalid;
    private static TherapistBasedTherapyTypeOccurrenceConstraint therapistBasedTherapyTypeOccurrenceConstraintValid;
    private static TherapistBasedTherapyTypeOccurrenceConstraint therapistBasedTherapyTypeOccurrenceConstraintInvalid;


    public static <DTO> DTO createValidDTO(DTO dynamicType) {
        init();
        if (dynamicType.getClass() == Address.class) {
            return (DTO) addressValid;
        } else if (dynamicType.getClass() == Allergy.class) {
            return (DTO) allergyValid;
        } else if (dynamicType.getClass() == DutyCategory.class) {
            return (DTO) dutyCategoryValid;
        } else if (dynamicType.getClass() == Invoice.class) {
            return (DTO) invoiceValid;
        } else if (dynamicType.getClass() == Patient.class) {
            return (DTO) patientValid;
        } else if (dynamicType.getClass() == PatientComment.class) {
            return (DTO) patientCommentValid;
        } else if (dynamicType.getClass() == PatientContact.class) {
            return (DTO) patientContactValid;
        } else if (dynamicType.getClass() == Therapist.class) {
            return (DTO) therapistValid;
        } else if (dynamicType.getClass() == Therapy.class) {
            return (DTO) therapyValid;
        } else if (dynamicType.getClass() == TherapyEvent.class) {
            return (DTO) therapyEventValid;
        } else if (dynamicType.getClass() == TherapyPlan.class) {
            return (DTO) therapyPlanValid;
        } else if (dynamicType.getClass() == TherapyRoom.class) {
            return (DTO) therapyRoomValid;
        } else if (dynamicType.getClass() == TherapyType.class) {
            return (DTO) therapyTypeValid;
        } else if (dynamicType.getClass() == PatientBasedTherapistConstraint.class) {
            return (DTO)  patientBasedTherapistConstraintValid;
        } else if (dynamicType.getClass() == PatientBasedTherapistGenderConstraint.class) {
            return (DTO) patientBasedTherapistGenderConstraintValid;
        }
        else if (dynamicType.getClass() == PatientBasedTherapyTypeOccurrenceConstraint.class) {
            return (DTO) patientBasedTherapyTypeOccurrenceConstraintValid;
        } else if (dynamicType.getClass() == TherapistBasedTimeBlockConstraint.class) {
            return (DTO) therapistBasedTimeBlockConstraintValid;
        } else if (dynamicType.getClass() == TherapistBasedTherapyTypeOccurrenceConstraint.class) {
            return (DTO) therapistBasedTherapyTypeOccurrenceConstraintValid;
        }
        else if (dynamicType.getClass() == TimeBlock.class) {
            return (DTO) timeBlockValid;
        } else {
            return null;
        }
    }

    public static <DTO> DTO createInvalidDTO(DTO dynamicType) {
        init();
        if (dynamicType.getClass() == Address.class) {
            return (DTO) addressInvalid;
        } else if (dynamicType.getClass() == Allergy.class) {
            return (DTO) allergyInvalid;
        } else if (dynamicType.getClass() == DutyCategory.class) {
            return (DTO) dutyCategoryInvalid;
        } else if (dynamicType.getClass() == Invoice.class) {
            return (DTO) invoiceInvalid;
        } else if (dynamicType.getClass() == Patient.class) {
            return (DTO) patientInvalid;
        } else if (dynamicType.getClass() == PatientComment.class) {
            return (DTO) patientCommentInvalid;
        } else if (dynamicType.getClass() == PatientContact.class) {
            return (DTO) patientContactInvalid;
        } else if (dynamicType.getClass() == Therapist.class) {
            return (DTO) therapistInvalid;
        } else if (dynamicType.getClass() == Therapy.class) {
            return (DTO) therapyInvalid;
        } else if (dynamicType.getClass() == TherapyEvent.class) {
            return (DTO) therapyEventInvalid;
        } else if (dynamicType.getClass() == TherapyPlan.class) {
            return (DTO) therapyPlanInvalid;
        } else if (dynamicType.getClass() == TherapyRoom.class) {
            return (DTO) therapyRoomInvalid;
        } else if (dynamicType.getClass() == TherapyType.class) {
            return (DTO) therapyTypeInvalid;
        } else if (dynamicType.getClass() == PatientBasedTherapistConstraint.class) {
            return (DTO)  patientBasedTherapistConstraintInvalid;
        } else if (dynamicType.getClass() == PatientBasedTherapistGenderConstraint.class) {
            return (DTO) patientBasedTherapistGenderConstraintInvalid;
        }
        else if (dynamicType.getClass() == PatientBasedTherapyTypeOccurrenceConstraint.class) {
            return (DTO) patientBasedTherapyTypeOccurrenceConstraintInvalid;
        } else if (dynamicType.getClass() == TherapistBasedTimeBlockConstraint.class) {
            return (DTO) therapistBasedTimeBlockConstraintInvalid;
        } else if (dynamicType.getClass() == TherapistBasedTherapyTypeOccurrenceConstraint.class) {
            return (DTO) therapistBasedTherapyTypeOccurrenceConstraintInvalid;
        }
        else if (dynamicType.getClass() == TimeBlock.class) {
            return (DTO) timeBlockInvalid;
        } else {
            return null;
        }
    }

    private static void init() {
        //if (instance == null) {
        //    instance = new SampleDTOGenerator();
        setAddressValid();
        setAddressInvalid();
        setAllergyValid();
        setAllergyInvalid();
        setDutyCategoryValid();
        setDutyCategoryInvalid();
        setInvoiceValid();
        setInvoiceInvalid();
        setPatientValid();
        setPatientInvalid();
        setPatientCommentValid();
        setPatientCommentInvalid();
        setPatientContactValid();
        setPatientContactInvalid();
        setTherapistValid();
        setTherapistInvalid();
        setTherapyValid();
        setTherapyInvalid();
        setTherapyEventValid();
        setTherapyEventInvalid();
        setTherapyPlanValid();
        setTherapyPlanInvalid();
        setTherapyRoomValid();
        setTherapyRoomInvalid();
        setTherapyTypeValid();
        setTherapyTypeInvalid();
        setTimeBlockValid();
        setTimeBlockInvalid();
        setPatientBasedTherapistConstraintValid();
        setPatientBasedTherapistConstraintInvalid();
        setPatientBasedTherapistGenderConstraintValid();
        setPatientBasedTherapistGenderConstraintInvalid();
        setPatientBasedTherapyTypeOccurrenceConstraintValid();
        setPatientBasedTherapyTypeOccurrenceConstraintInvalid();
        setTherapistBasedTherapyTypeOccurrenceConstraintValid();
        setTherapistBasedTherapyTypeOccurrenceConstraintInvalid();
        setTherapistBasedTimeBlockConstraintValid();
        setTherapistBasedTimeBlockConstraintInvalid();
        //}
    }

    private static void setAddressValid() {
        addressValid = new Address();
        addressValid.setStreet("Sample Street 1");
        addressValid.setZip("1234");
        addressValid.setCity("Sample City");
        addressValid.setCountryCode("CH");
    }

    private static void setDutyCategoryValid() {
        dutyCategoryValid = new DutyCategory();
        dutyCategoryValid.setName("Sample Category");
        dutyCategoryValid.setDuration(30);
    }

    private static void setInvoiceValid() {
        invoiceValid = new Invoice();
        invoiceValid.setAddress(new Address());
        invoiceValid.getAddress().setId(1);
        invoiceValid.setTherapy(new Therapy());
        invoiceValid.getTherapy().setId(1);
        invoiceValid.setVatIncluded(true);
    }

    private static void setPatientValid() {
        patientValid = new Patient();
        patientValid.setFirstName("Sample First");
        patientValid.setLastName("Sample Last");
        patientValid.setGender(Gender.FEMALE);
        patientValid.setBirthdate(new DateTime());
        patientValid.setAddress(new Address());
        patientValid.getAddress().setId(1);
        patientValid.setSocialSecurityNumber("SSN1234");
        patientValid.setPhone("+43123456");
        patientValid.setEmail("sample@mail.com");
        patientValid.setReligion("Atheist");
        patientValid.setAllergyList(new ArrayList<Allergy>());
        patientValid.setPatientCommentList(new ArrayList<PatientComment>());
    }

    private static void setAllergyValid() {
        allergyValid = new Allergy();
        allergyValid.setName("Sample Allergy");
    }

    private static void setPatientCommentValid() {
        patientCommentValid = new PatientComment();
        patientCommentValid.setContent("Sample Content");
        patientCommentValid.setPatient(new Patient());
        patientCommentValid.getPatient().setId(1);
    }

    private static void setPatientContactValid() {
        patientContactValid = new PatientContact();
        patientContactValid.setPatient(new Patient());
        patientContactValid.getPatient().setId(1);
        patientContactValid.setFirstName("Sample First");
        patientContactValid.setLastName("Sample Last");
        patientContactValid.setPhone("+43123456");
        patientContactValid.setEmail("sample@mail.com");
    }

    private static void setTherapistValid() {
        therapistValid = new Therapist();
        therapistValid.setFirstName("Sample First");
        therapistValid.setLastName("Sample Last");
        therapistValid.setGender(Gender.MALE);
        therapistValid.setEmail("sample@email.com");
        therapistValid.setTherapyTypes(new ArrayList<TherapyType>());
    }

    private static void setTherapyValid() {
        therapyValid = new Therapy();
        therapyValid.setPatient(new Patient());
        therapyValid.getPatient().setId(1);
        therapyValid.setDateSickLeave(new DateTime());
        therapyValid.setDateFrom(new DateTime());
        therapyValid.setDateUntil(new DateTime());
    }

    private static void setTherapyEventValid() {
        therapyEventValid = new TherapyEvent();
        therapyEventValid.setTherapyPlan(new TherapyPlan());
        therapyEventValid.getTherapyPlan().setId(1);
        therapyEventValid.setTherapyType(new TherapyType());
        therapyEventValid.getTherapyType().setId(1);
        therapyEventValid.setTherapyRoom(new TherapyRoom());
        therapyEventValid.getTherapyRoom().setId(1);
        therapyEventValid.setTherapist(new Therapist());
        therapyEventValid.getTherapist().setId(1);
        therapyEventValid.setTherapyTimeBlock(new TimeBlock());
        therapyEventValid.getTherapyTimeBlock().setId(1);
        therapyEventValid.setTherapyTime(new DateTime());
        therapyEventValid.setTherapyMode(TherapyMode.GROUP);
    }

    private static void setTherapyPlanValid() {
        therapyPlanValid = new TherapyPlan();
        therapyPlanValid.setDateFrom(new DateTime());
        therapyPlanValid.setDateUntil(new DateTime());
    }

    private static void setTherapyRoomValid() {
        therapyRoomValid = new TherapyRoom();
        therapyRoomValid.setName("Sample Room");
        therapyRoomValid.setCapacity(10);
    }

    private static void setTherapyTypeValid() {
        therapyTypeValid = new TherapyType();
        therapyTypeValid.setName("Sample Type");
        therapyTypeValid.setGroupTherapistFixed(true);
        therapyTypeValid.setGroupTherapyDuration(45);
        therapyTypeValid.setIndividualTherapistFixed(true);
        therapyTypeValid.setIndividualTherapyDuration(45);
        therapyTypeValid.setMaxGroupSize(10);
        therapyTypeValid.setMinGroupSize(1);
        therapyTypeValid.setSuitableForGroups(true);
        therapyTypeValid.setSuitableForIndividuals(true);
        therapyTypeValid.setTherapists(new ArrayList<Therapist>());
    }

    private static void setTimeBlockValid() {
        timeBlockValid = new TimeBlock();
        timeBlockValid.setDay(Day.MONDAY);
        timeBlockValid.setTimeStart(new DateTime(2015, 6, 8, 9, 0));
        timeBlockValid.setPredecessor(null);
    }

    private static void setPatientBasedTherapistConstraintValid() {
        patientBasedTherapistConstraintValid = new PatientBasedTherapistConstraint();
        patientBasedTherapistConstraintValid.setPatient(new Patient());
        patientBasedTherapistConstraintValid.getPatient().setId(1);
        patientBasedTherapistConstraintValid.setTherapyType(new TherapyType());
        patientBasedTherapistConstraintValid.getTherapyType().setId(1);
        patientBasedTherapistConstraintValid.setTherapist(new Therapist());
        patientBasedTherapistConstraintValid.getTherapist().setId(1);
    }

    private static void setPatientBasedTherapistGenderConstraintValid() {
        patientBasedTherapistGenderConstraintValid = new PatientBasedTherapistGenderConstraint();
        patientBasedTherapistGenderConstraintValid.setPatient(new Patient());
        patientBasedTherapistGenderConstraintValid.getPatient().setId(1);
        patientBasedTherapistGenderConstraintValid.setTherapyType(new TherapyType());
        patientBasedTherapistGenderConstraintValid.getTherapyType().setId(1);
        patientBasedTherapistGenderConstraintValid.setGender(Gender.FEMALE);
    }

    private static void setPatientBasedTherapyTypeOccurrenceConstraintValid() {
        patientBasedTherapyTypeOccurrenceConstraintValid = new PatientBasedTherapyTypeOccurrenceConstraint();
        patientBasedTherapyTypeOccurrenceConstraintValid.setPatient(new Patient());
        patientBasedTherapyTypeOccurrenceConstraintValid.getPatient().setId(1);
        patientBasedTherapyTypeOccurrenceConstraintValid.setTherapyType(new TherapyType());
        patientBasedTherapyTypeOccurrenceConstraintValid.getTherapyType().setId(1);
        patientBasedTherapyTypeOccurrenceConstraintValid.setTherapyMode(TherapyMode.SINGLE);
        patientBasedTherapyTypeOccurrenceConstraintValid.setMaxOccurencePerWeek(2);
    }

    private static void setTherapistBasedTimeBlockConstraintValid() {
        therapistBasedTimeBlockConstraintValid = new TherapistBasedTimeBlockConstraint();
        therapistBasedTimeBlockConstraintValid.setTherapist(new Therapist());
        therapistBasedTimeBlockConstraintValid.getTherapist().setId(1);
        therapistBasedTimeBlockConstraintValid.setTimeBlock(new TimeBlock());
        therapistBasedTimeBlockConstraintValid.getTimeBlock().setId(1);
    }

    private static void setTherapistBasedTherapyTypeOccurrenceConstraintValid() {
        therapistBasedTherapyTypeOccurrenceConstraintValid = new TherapistBasedTherapyTypeOccurrenceConstraint();
        therapistBasedTherapyTypeOccurrenceConstraintValid.setTherapist(new Therapist());
        therapistBasedTherapyTypeOccurrenceConstraintValid.getTherapist().setId(1);
        therapistBasedTherapyTypeOccurrenceConstraintValid.setTherapyType(new TherapyType());
        therapistBasedTherapyTypeOccurrenceConstraintValid.getTherapyType().setId(1);
        therapistBasedTherapyTypeOccurrenceConstraintValid.setTherapyMode(TherapyMode.SINGLE);
        therapistBasedTherapyTypeOccurrenceConstraintValid.setMaxOccurrencePerDay(2);
    }


    private static void setAddressInvalid() {
        addressInvalid = new Address();
    }

    private static void setDutyCategoryInvalid() {
        dutyCategoryInvalid = new DutyCategory();
    }

    private static void setInvoiceInvalid() {
        invoiceInvalid = new Invoice();
    }

    private static void setPatientInvalid() {
        patientInvalid = new Patient();
    }

    private static void setAllergyInvalid() {
        allergyInvalid = new Allergy();
    }

    private static void setPatientCommentInvalid() {
        patientCommentInvalid = new PatientComment();
    }

    private static void setPatientContactInvalid() {
        patientContactInvalid = new PatientContact();
    }

    private static void setTherapistInvalid() {
        therapistInvalid = new Therapist();
    }

    private static void setTherapyInvalid() {
        therapyInvalid = new Therapy();
    }

    private static void setTherapyEventInvalid() {
        therapyEventInvalid = new TherapyEvent();
    }

    private static void setTherapyPlanInvalid() {
        therapyPlanInvalid = new TherapyPlan();
    }

    private static void setTherapyRoomInvalid() {
        therapyRoomInvalid = new TherapyRoom();
    }

    private static void setTherapyTypeInvalid() {
        therapyTypeInvalid = new TherapyType();
    }

    private static void setTimeBlockInvalid() {
        timeBlockInvalid = new TimeBlock();
    }

    private static void setPatientBasedTherapistConstraintInvalid() {
        patientBasedTherapistConstraintInvalid = new PatientBasedTherapistConstraint();
    }

    private static void setPatientBasedTherapistGenderConstraintInvalid() {
        patientBasedTherapistGenderConstraintInvalid = new PatientBasedTherapistGenderConstraint();
    }

    private static void setPatientBasedTherapyTypeOccurrenceConstraintInvalid() {
        patientBasedTherapyTypeOccurrenceConstraintInvalid = new PatientBasedTherapyTypeOccurrenceConstraint();
    }

    private static void setTherapistBasedTimeBlockConstraintInvalid() {
        therapistBasedTimeBlockConstraintInvalid = new TherapistBasedTimeBlockConstraint();
    }

    private static void setTherapistBasedTherapyTypeOccurrenceConstraintInvalid() {
        therapistBasedTherapyTypeOccurrenceConstraintInvalid = new TherapistBasedTherapyTypeOccurrenceConstraint();
    }

}

