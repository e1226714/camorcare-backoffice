package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.BasicDTO;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Tests the basic functionality of any DAO
 */
public abstract class BasicDaoTest<DtoClass extends BasicDTO, DaoClass extends BasicDAO<DtoClass>> {

    private DtoClass dynamicType;
    protected DaoClass dao;
    @Rule
    public ExpectedException exception = ExpectedException.none();

    public void setDao(DaoClass dao) {
        this.dao = dao;
    }

    public void setDynamicType(DtoClass dynamicType) {
        this.dynamicType = dynamicType;
    }

    //##########################FIND ALL################################
    @Test
    public void findAllTest() throws PersistenceException {
        List<DtoClass> found = dao.findAll();
        assertThat("found entries", found.size(), greaterThan(0));
    }

    //##########################CREATE################################
    @Test
    public void createNull() throws PersistenceException, ValidationException {
        exception.expect(NullPointerException.class);
        dao.create(null);
    }

    @Test
    public void createValid() throws PersistenceException, ValidationException {
        DtoClass newEntry = SampleDTOGenerator.createValidDTO(dynamicType);
        DtoClass created = dao.create(newEntry);
        assertThat("assigned ID", created.getId(), notNullValue());
    }

    @Test
    public void createWithAssignedId() throws PersistenceException, ValidationException {
        DtoClass newEntry = SampleDTOGenerator.createValidDTO(dynamicType);
        newEntry.setId(140);
        exception.expect(PersistenceException.class);
        dao.create(newEntry);
    }

    @Test
    public void createInvalid() throws PersistenceException, ValidationException {
        DtoClass newEntry = SampleDTOGenerator.createInvalidDTO(dynamicType);
        exception.expect(ValidationException.class);
        dao.create(newEntry);
    }

    //##########################UPDATE################################
    @Test
    public void updateNull() throws PersistenceException, ValidationException {
        if ((dao instanceof PatientBasedTherapistConstraintDAO) || (dao instanceof PatientBasedTherapistGenderConstraintDAO) || (dao instanceof TherapistBasedTimeBlockConstraintDAO)) {
            exception.expect(PersistenceException.class);
            dao.update(null);
        }
        else {
            exception.expect(NullPointerException.class);
            dao.update(null);
        }
    }

    @Test
    public void updateExisting() throws PersistenceException, ValidationException {
        if ((dao instanceof PatientBasedTherapistConstraintDAO) || (dao instanceof PatientBasedTherapistGenderConstraintDAO) || (dao instanceof TherapistBasedTimeBlockConstraintDAO)) {
            List<DtoClass> found = dao.findAll();
            DtoClass sample = found.get(0);
            exception.expect(PersistenceException.class);
            dao.update(sample);
        }
        else {
            List<DtoClass> found = dao.findAll();
            DtoClass sample = found.get(0);
            dao.update(sample);
        }
    }

    @Test
    public void updateInvalid() throws PersistenceException, ValidationException {
        if ((dao instanceof PatientBasedTherapistConstraintDAO) || (dao instanceof PatientBasedTherapistGenderConstraintDAO) || (dao instanceof TherapistBasedTimeBlockConstraintDAO)) {
            List<DtoClass> found = dao.findAll();
            DtoClass sample = found.get(0);
            DtoClass invalid = SampleDTOGenerator.createInvalidDTO(dynamicType);
            invalid.setId(sample.getId());
            exception.expect(PersistenceException.class);
            dao.update(invalid);
        }
        else {
            List<DtoClass> found = dao.findAll();
            DtoClass sample = found.get(0);
            DtoClass invalid = SampleDTOGenerator.createInvalidDTO(dynamicType);
            invalid.setId(sample.getId());
            exception.expect(ValidationException.class);
            dao.update(invalid);
        }
    }

    @Test
    public void updateWithoutId() throws PersistenceException, ValidationException {
        List<DtoClass> found = dao.findAll();
        DtoClass sample = found.get(0);
        sample.setId(null);
        exception.expect(PersistenceException.class);
        dao.update(sample);
    }

    @Test
    public void updateWithNonExistingId() throws PersistenceException, ValidationException {
        List<DtoClass> found = dao.findAll();
        DtoClass sample = found.get(0);
        sample.setId(99999999);
        exception.expect(PersistenceException.class);
        dao.update(sample);
    }

    //##########################DELETE################################
    @Test
    public void deleteNull() throws PersistenceException, ValidationException {
        exception.expect(NullPointerException.class);
        dao.delete(null);
    }

    @Test
    public void deleteExisting() throws PersistenceException, ValidationException {
        List<DtoClass> found = dao.findAll();
        DtoClass sample = found.get(0);
        int before = found.size();
        dao.delete(sample);
        assertThat(dao.findAll().size(), equalTo(before - 1));
    }

    @Test
    public void deleteWithoutId() throws PersistenceException, ValidationException {
        List<DtoClass> found = dao.findAll();
        DtoClass sample = found.get(0);
        sample.setId(null);
        exception.expect(PersistenceException.class);
        dao.delete(sample);
    }

    @Test
    public void deleteWithNonExistingId() throws PersistenceException, ValidationException {
        List<DtoClass> found = dao.findAll();
        DtoClass sample = found.get(0);
        sample.setId(99999999);
        exception.expect(PersistenceException.class);
        dao.delete(sample);
    }

    @Test
    public void deleteDeleted() throws PersistenceException, ValidationException {
        List<DtoClass> found = dao.findAll();
        DtoClass sample = found.get(0);
        dao.delete(sample);
        exception.expect(PersistenceException.class);
        sample.setTimeDeleted(sample.getTimeCreated());
        dao.delete(sample);
    }

    //##########################FindOne################################
    @Test
    public void findOneByIdNull() throws PersistenceException, ValidationException {
        exception.expect(NullPointerException.class);
        dao.findOneById(null);
    }

    @Test
    public void findOneByIdIdNull() throws PersistenceException, ValidationException {
        List<DtoClass> found = dao.findAll();
        DtoClass search = found.get(0);
        search.setId(null);
        exception.expect(PersistenceException.class);
        dao.findOneById(search);
    }

    @Test
    public void findOneByIdExisting() throws PersistenceException, ValidationException {
        List<DtoClass> found = dao.findAll();
        DtoClass search = found.get(0);

        DtoClass foundByOneId = dao.findOneById(search);
        assertThat(foundByOneId.getTimeCreated(), notNullValue());
    }

    @Test
    public void findOneByIdNotExisting() throws PersistenceException, ValidationException {
        List<DtoClass> found = dao.findAll();
        DtoClass search = found.get(0);
        search.setId(999999999);
        exception.expect(PersistenceException.class);
        dao.findOneById(search);
    }
}
