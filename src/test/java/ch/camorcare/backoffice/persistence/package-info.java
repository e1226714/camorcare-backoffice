package ch.camorcare.backoffice.persistence;

/**
 * The tests contained in this package are specific to testing the DAOs used in the persistence layer of the program. A
 * BasicDaoTest exists to test common functionality shared by all of the DAOs (based Create, Read, Update and Delete
 * functions), which uses a generating tool to automatically generate test data.
 * <p/>
 * A test file exists for each DAO along with tests specific to the current database implementation (H2) of the program.
 */