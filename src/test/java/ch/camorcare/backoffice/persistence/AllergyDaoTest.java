package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Allergy;

/**
 * Placeholder for AllergyDaoTest. This test will always fail because it is not implemented in the database.
 */
public abstract class AllergyDaoTest extends BasicDaoTest<Allergy, AllergyDAO> {
}
