package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.joda.time.DateTime;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public abstract class TherapyDaoTest extends BasicDaoTest<Therapy, TherapyDAO> {
    //##########################findAllByTherapyPlan################################
    @Test
     public void findAllByTherapyPlan_ValidTherapyPlan() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(1);
        therapyPlan.setDateFrom(new DateTime(2015, 6, 1, 12, 0));
        List<Therapy> found = dao.findAllByTherapyPlan(therapyPlan);
        assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllByTherapyPlan_NotExistingTherapyPlanDateFrom() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(1);
        therapyPlan.setDateFrom(new DateTime(2012, 6, 1, 12, 0));
        List<Therapy> found = dao.findAllByTherapyPlan(therapyPlan);
        assertThat("found entries", found.size(), lessThanOrEqualTo(0));
    }

    @Test
    public void findAllByTherapyPlan_InvalidTherapyPlanDateFrom() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(1);
        therapyPlan.setDateFrom(null);
        exception.expect(PersistenceException.class);
        dao.findAllByTherapyPlan(therapyPlan);
    }

    @Test
    public void findAllByTherapyPlanBy_EmptyTherapyPlan() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByTherapyPlan(new TherapyPlan());
    }

    @Test
    public void findAllByTherapyPlanBy_NullTherapyPlan() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByTherapyPlan(null);
    }

    //##########################findAllByPatient################################
    @Test
    public void findAllByPatient_ValidPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(1);
        List<Therapy> found = dao.findAllByPatient(patient);
        assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllByPatient_NotExistingPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(999);
        List<Therapy> found = dao.findAllByPatient(patient);
        assertThat("found entries", found.size(), lessThanOrEqualTo(0));
    }

    @Test
    public void findAllByPatientBy_EmptyPatient() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByPatient(new Patient());
    }

    @Test
    public void findAllByPatientBy_NullPatient() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByPatient(null);
    }
}
