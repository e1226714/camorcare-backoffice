package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public abstract class TherapyEventDaoTest extends BasicDaoTest<TherapyEvent, TherapyEventDAO> {
	// ##################### TEST SEARCH BY THERAPIST #####################

	@Test
	public void findTherapyEventsBy_ValidTherapistAndTherapyPlan() throws ValidationException, PersistenceException {
		Therapist searchTherapist = new Therapist();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapist.setId(1);
		searchTherapyPlan.setId(1);

		List<TherapyEvent> found = dao.findAllByTherapistAndTherapyPlan(searchTherapist, searchTherapyPlan, true);
		assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
	}

	@Test
	public void findTherapyEventsBy_NullTherapistIdAnd_ValidTherapyPlan() throws ValidationException, PersistenceException {
		Therapist searchTherapist = new Therapist();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapist.setId(null);
		searchTherapyPlan.setId(1);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapistAndTherapyPlan(searchTherapist, searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_ValidTherapistAnd_NullTherapyPlanId() throws ValidationException, PersistenceException {
		Therapist searchTherapist = new Therapist();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapist.setId(1);
		searchTherapyPlan.setId(null);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapistAndTherapyPlan(searchTherapist, searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_NullTherapistIdAnd_NullTherapyPlanId() throws ValidationException, PersistenceException {
		Therapist searchTherapist = new Therapist();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapist.setId(null);
		searchTherapyPlan.setId(null);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapistAndTherapyPlan(searchTherapist, searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_NotExistingTherapist() throws ValidationException, PersistenceException {
		Therapist searchTherapist = new Therapist();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapist.setId(9999);
		searchTherapyPlan.setId(1);

		List<TherapyEvent> found = dao.findAllByTherapistAndTherapyPlan(searchTherapist, searchTherapyPlan, true);
		assertThat("found entries", found.size(), comparesEqualTo(0));
	}

	@Test
	public void findTherapyEventsBy_ValidTherapistAnd_NotExistingTherapyPlan() throws ValidationException, PersistenceException {
		Therapist searchTherapist = new Therapist();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapist.setId(1);
		searchTherapyPlan.setId(9999);

		List<TherapyEvent> found = dao.findAllByTherapistAndTherapyPlan(searchTherapist, searchTherapyPlan, true);
		assertThat("found entries", found.size(), comparesEqualTo(0));
	}

	@Test
	public void findTherapyEventsBy_NullTherapistAnd_NullTherapyPlan() throws ValidationException, PersistenceException{
		exception.expect(PersistenceException.class);
		dao.findAllByTherapistAndTherapyPlan(null, null, true);
	}

	// ##################### TEST SEARCH BY THERAPY/PATIENT #####################

	@Test
	public void findTherapyEventsBy_ValidTherapyAndTherapyPlan() throws ValidationException, PersistenceException {
		Therapy searchTherapy = new Therapy();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapy.setId(1);
		searchTherapyPlan.setId(1);

		List<TherapyEvent> found = dao.findAllByTherapyAndTherapyPlan(searchTherapy, searchTherapyPlan, true);
		assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
	}

	@Test
	public void findTherapyEventsBy_NullTherapyIdAnd_ValidTherapyPlan() throws ValidationException, PersistenceException {
		Therapy searchTherapy = new Therapy();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapy.setId(null);
		searchTherapyPlan.setId(1);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapyAndTherapyPlan(searchTherapy, searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_ValidTherapyAnd_NullTherapyPlanId() throws ValidationException, PersistenceException {
		Therapy searchTherapy = new Therapy();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapy.setId(1);
		searchTherapyPlan.setId(null);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapyAndTherapyPlan(searchTherapy, searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_NullTherapyIdAnd_NullTherapyPlanId() throws ValidationException, PersistenceException {
		Therapy searchTherapy = new Therapy();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapy.setId(null);
		searchTherapyPlan.setId(null);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapyAndTherapyPlan(searchTherapy, searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_NotExistingTherapy() throws ValidationException, PersistenceException {
		Therapy searchTherapy = new Therapy();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapy.setId(9999);
		searchTherapyPlan.setId(1);

		List<TherapyEvent> found = dao.findAllByTherapyAndTherapyPlan(searchTherapy, searchTherapyPlan, true);
		assertThat("found entries", found.size(), comparesEqualTo(0));
	}

	@Test
	public void findTherapyEventsBy_NullTherapyAnd_NullTherapyPlan() throws ValidationException, PersistenceException {
		exception.expect(PersistenceException.class);
		dao.findAllByTherapyAndTherapyPlan(null, null, true);
	}

	// ##################### TEST SEARCH BY THERAPY_ROOM #####################

	@Test
	public void findTherapyEventsBy_ValidTherapyRoomAndTherapyPlan() throws ValidationException, PersistenceException {
		TherapyRoom searchTherapyRoom = new TherapyRoom();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapyRoom.setId(1);
		searchTherapyPlan.setId(3);

		List<TherapyEvent> found = dao.findAllByTherapyRoomAndTherapyPlan(searchTherapyRoom, searchTherapyPlan, true);
		assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
	}

	@Test
	public void findTherapyEventsBy_NullTherapyRoomIdAnd_ValidTherapyPlan() throws ValidationException, PersistenceException {
		TherapyRoom searchTherapyRoom = new TherapyRoom();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapyRoom.setId(null);
		searchTherapyPlan.setId(1);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapyRoomAndTherapyPlan(searchTherapyRoom, searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_ValidTherapyRoomAnd_NullTherapyPlanId() throws ValidationException, PersistenceException {
		TherapyRoom searchTherapyRoom = new TherapyRoom();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapyRoom.setId(1);
		searchTherapyPlan.setId(null);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapyRoomAndTherapyPlan(searchTherapyRoom, searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_NullTherapyRoomIdAnd_NullTherapyPlanId() throws ValidationException, PersistenceException {
		TherapyRoom searchTherapyRoom = new TherapyRoom();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapyRoom.setId(null);
		searchTherapyPlan.setId(null);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapyRoomAndTherapyPlan(searchTherapyRoom, searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_NotExistingTherapyRoom() throws ValidationException, PersistenceException {
		TherapyRoom searchTherapyRoom = new TherapyRoom();
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapyRoom.setId(9999);
		searchTherapyPlan.setId(1);

		List<TherapyEvent> found = dao.findAllByTherapyRoomAndTherapyPlan(searchTherapyRoom, searchTherapyPlan, true);
		assertThat("found entries", found.size(), comparesEqualTo(0));
	}

	@Test
	public void findTherapyEventsBy_NullTherapyRoomAnd_NullTherapyPlan() throws ValidationException, PersistenceException {
		exception.expect(PersistenceException.class);
		dao.findAllByTherapyRoomAndTherapyPlan(null, null, true);
	}

	// ##################### TEST SEARCH ALL BY THERAPY_PLAN #####################
	@Test
	public void findTherapyEventsBy_ValidTherapyPlan() throws ValidationException, PersistenceException {
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapyPlan.setId(1);

		List<TherapyEvent> found = dao.findAllByTherapyPlan(searchTherapyPlan, true);
		assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
	}

	@Test
	public void findTherapyEventsBy_InvalidTherapyPlan() throws ValidationException, PersistenceException {
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapyPlan.setId(null);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapyPlan(searchTherapyPlan, true);
	}

	@Test
	public void findTherapyEventsBy_NotExistingTherapyPlan() throws ValidationException, PersistenceException {
		TherapyPlan searchTherapyPlan = new TherapyPlan();

		searchTherapyPlan.setId(999);

		List<TherapyEvent> found = dao.findAllByTherapyPlan(searchTherapyPlan, true);
		assertThat("found entries", found.size(), comparesEqualTo(0));
	}

	@Test
	public void findTherapyEventsBy_EmptyTherapyPlan() throws ValidationException, PersistenceException {
		exception.expect(PersistenceException.class);
		dao.findAllByTherapyPlan(new TherapyPlan(), true);
	}

	@Test
	public void findTherapyEventsBy_NullTherapyPlan() throws ValidationException, PersistenceException {
		exception.expect(PersistenceException.class);
		dao.findAllByTherapyPlan(null, true);
	}

	// ##################### TEST CREATE PARTICIPATION #####################
	@Test
	public void createParticipationWith_ValidTherapyEventAnd_ValidTherapy() throws ValidationException, PersistenceException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(2);
		therapy.setId(2);

		assertThat("create partication returns true", dao.createParticipation(therapyEvent, therapy), equalTo(true));
	}

	@Test
	public void createParticipationWith_ValidTherapyEventAnd_InvalidTherapy() throws ValidationException, PersistenceException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(2);
		therapy.setId(null);

		exception.expect(PersistenceException.class);
		dao.createParticipation(therapyEvent, therapy);
	}

	@Test
	public void createParticipationWith_InvalidTherapyEventAnd_ValidTherapy() throws ValidationException, PersistenceException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(null);
		therapy.setId(2);

		exception.expect(PersistenceException.class);
		dao.createParticipation(therapyEvent, therapy);
	}

	@Test
	public void createParticipationWith_InvalidTherapyEventAnd_InvalidTherapy() throws ValidationException, PersistenceException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(null);
		therapy.setId(null);

		exception.expect(PersistenceException.class);
		dao.createParticipation(therapyEvent, therapy);
	}

	@Test
	public void createParticipationWith_NullTherapyEventAnd_ValidTherapy() throws ValidationException, PersistenceException {
		Therapy therapy = new Therapy();
		therapy.setId(1);

		exception.expect(PersistenceException.class);
		dao.createParticipation(null, therapy);
	}

	@Test
	public void createParticipationWith_ValidTherapyEventAnd_NullTherapy() throws ValidationException, PersistenceException {
		TherapyEvent therapyEvent = new TherapyEvent();
		therapyEvent.setId(1);

		exception.expect(PersistenceException.class);
		dao.createParticipation(therapyEvent, null);
	}

	@Test
	public void createParticipationWith_NullTherapyEventAnd_NullTherapy() throws ValidationException, PersistenceException {
		exception.expect(PersistenceException.class);
		dao.createParticipation(null, null);
	}

	// ##################### TEST DELETE PARTICIPANTS #####################
	@Test
	public void deleteParticipationBy_ValidTherapyEventAnd_ValidTherapy() throws PersistenceException, ValidationException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(1);
		therapy.setId(1);

		assertThat("delete participation returns true", dao.deleteParticipation(therapyEvent, therapy), equalTo(true));
	}

	@Test
	public void deleteParticipationBy_InvalidTherapyEventAnd_ValidTherapy() throws PersistenceException, ValidationException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(null);
		therapy.setId(1);

		exception.expect(PersistenceException.class);
		dao.deleteParticipation(therapyEvent, therapy);
	}

	@Test
	public void deleteParticipationBy_ValidTherapyEventAnd_InvalidTherapy() throws PersistenceException, ValidationException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(1);
		therapy.setId(null);

		exception.expect(PersistenceException.class);
		dao.deleteParticipation(therapyEvent, therapy);
	}

	@Test
	public void deleteParticipationBy_ValidTherapyEventAnd_NotExistingTherapy() throws PersistenceException, ValidationException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(1);
		therapy.setId(999);

		assertThat("delete not existing", dao.deleteParticipation(therapyEvent, therapy), equalTo(false));
	}

	@Test
	public void deleteParticipationBy_NotExistingTherapyEventAnd_ValidTherapy() throws PersistenceException, ValidationException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(999);
		therapy.setId(1);

		assertThat("delete not existing", dao.deleteParticipation(therapyEvent, therapy), equalTo(false));
	}

	@Test
	public void deleteParticipationBy_NotExistingTherapyEventAnd_NotExistingTherapy() throws PersistenceException, ValidationException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(999);
		therapy.setId(999);

		assertThat("delete not existing", dao.deleteParticipation(therapyEvent, therapy), equalTo(false));
	}

	@Test
	public void deleteParticipationBy_EmptyTherapyEventAnd_ValidTherapy() throws PersistenceException, ValidationException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapy.setId(1);

		exception.expect(PersistenceException.class);
		dao.deleteParticipation(therapyEvent, therapy);
	}

	@Test
	public void deleteParticipationBy_ValidTherapyEventAnd_EmptyTherapy() throws PersistenceException, ValidationException {
		TherapyEvent therapyEvent = new TherapyEvent();
		Therapy therapy = new Therapy();

		therapyEvent.setId(1);

		exception.expect(PersistenceException.class);
		dao.deleteParticipation(therapyEvent, therapy);
	}

	@Test
	public void deleteParticipationBy_NullTherapyEventAnd_ValidTherapy() throws PersistenceException, ValidationException {
		Therapy therapy = new Therapy();

		therapy.setId(1);

		exception.expect(PersistenceException.class);
		dao.deleteParticipation(null, therapy);
	}

	@Test
	public void deleteParticipationBy_ValidTherapyEventAnd_NullTherapy() throws PersistenceException, ValidationException {
		TherapyEvent therapyEvent = new TherapyEvent();

		therapyEvent.setId(1);

		exception.expect(PersistenceException.class);
		dao.deleteParticipation(therapyEvent, null);
	}

	@Test
	public void deleteParticipationBy_NullTherapyEventAnd_NullTherapy() throws PersistenceException, ValidationException {
		exception.expect(PersistenceException.class);
		dao.deleteParticipation(null, null);
	}

	// ##################### TEST GET PARTICIPANTS #####################
	@Test
	public void getParticipantsBy_ValidTherapyEvent() throws ValidationException, PersistenceException {
		TherapyEvent therapyEvent = new TherapyEvent();

		therapyEvent.setId(1);

		List<Therapy> found = dao.getParticipants(therapyEvent, true);
		assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
	}

	@Test
	public void getParticipantsBy_InvalidTherapyEvent() throws ValidationException, PersistenceException {
		TherapyEvent therapyEvent = new TherapyEvent();

		therapyEvent.setId(null);

		exception.expect(PersistenceException.class);
		dao.getParticipants(therapyEvent, true);
	}

	@Test
	public void getParticipantsBy_EmptyTherapyEvent() throws ValidationException, PersistenceException {
		exception.expect(PersistenceException.class);
		dao.getParticipants(new TherapyEvent(), true);
	}

	@Test
	public void getParticipantsBy_Null() throws ValidationException, PersistenceException {
		exception.expect(PersistenceException.class);
		dao.getParticipants(null, true);
	}
}
