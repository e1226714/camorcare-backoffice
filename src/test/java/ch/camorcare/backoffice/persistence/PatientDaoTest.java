package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public abstract class PatientDaoTest extends BasicDaoTest<Patient, PatientDAO> {

    // ######################### findOneBySSNR ###########################
    @Test
    public void findOneBySSNRBy_ValidPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setSocialSecurityNumber("1234010782");
        assertThat("found patient", dao.findOneBySSNR(patient), notNullValue());
    }

    @Test
    public void findOneBySSNRBy_InvalidPatientSSNR() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setSocialSecurityNumber(null);
        exception.expect(ValidationException.class);
        dao.findOneBySSNR(patient);
    }

    @Test
    public void findOneBySSNRBy_EmptyPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        exception.expect(ValidationException.class);
        dao.findOneBySSNR(patient);
    }

    @Test
    public void findOneBySSNRBy_Null() throws PersistenceException, ValidationException {
        exception.expect(ValidationException.class);
        dao.findOneBySSNR(null);
    }

    @Test
    public void findOneBySSNRBy_NotExistingPatientSSNR() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setSocialSecurityNumber("AAAAA");
        assertThat("found patient is null", dao.findOneBySSNR(patient), is(nullValue()));
    }

}
