package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;


public abstract class TherapistDaoTest extends BasicDaoTest<Therapist, TherapistDAO> {
    //##########################FindByTherapyType################################
    @Test
    public void findAllByTherapyTypeBy_ValidTherapyType() throws PersistenceException, ValidationException {
        TherapyType therapyType = new TherapyType();
        therapyType.setId(1);

        List<Therapist> found = dao.findAllByTherapyType(therapyType);
        assertThat("found size", found.size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllByTherapyTypeByy_NotExistingTherapyType() throws PersistenceException, ValidationException {
        TherapyType therapyType = new TherapyType();
        therapyType.setId(999);

        List<Therapist> found = dao.findAllByTherapyType(therapyType);
        assertThat("found size", found.size(), lessThanOrEqualTo(0));
    }

    @Test
    public void findAllByTherapyTypeBy_InvalidTherapyType() throws PersistenceException, ValidationException {
        TherapyType therapyType = new TherapyType();
        therapyType.setId(null);

        exception.expect(PersistenceException.class);
        dao.findAllByTherapyType(therapyType);
    }

    @Test
    public void findAllByTherapyTypeBy_EmptyTherapyType() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByTherapyType(new TherapyType());
    }

    @Test
    public void findAllByTherapyTypeBy_NullTherapyType() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByTherapyType(null);
    }

    //##########################FindByTherapyPlan################################
    @Test
    public void findAllByTherapyPlanBy_ValidTherapyPlan() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(1);

        List<Therapist> found = dao.findAllByTherapyPlan(therapyPlan);
        assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllByTherapyPlanByy_NotExistingTherapyType() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(999);

        List<Therapist> found = dao.findAllByTherapyPlan(therapyPlan);
        assertThat("found size", found.size(), lessThanOrEqualTo(0));
    }

    @Test
    public void findAllByTherapyPlanBy_InvalidTherapyPlan() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(null);

        exception.expect(PersistenceException.class);
        dao.findAllByTherapyPlan(therapyPlan);
    }

    @Test
    public void findAllByTherapyPlanBy_EmptyTherapyPlan() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByTherapyPlan(new TherapyPlan());
    }

    @Test
    public void findAllByTherapyPlanBy_NullTherapyPlan() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByTherapyPlan(null);
    }
}
