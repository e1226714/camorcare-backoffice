package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public abstract class TherapyTypeDaoTest extends BasicDaoTest<TherapyType, TherapyTypeDAO> {
	//##########################FindAllByTherapist################################
	@Test
	public void findAllByTherapistBy_ValidTherapist() throws PersistenceException, ValidationException {
		Therapist therapist = new Therapist();
		therapist.setId(1);

		List<TherapyType> found = dao.findAllByTherapist(therapist);
		assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
	}

	@Test
	public void findAllByTherapistBy_NotExistingTherapist() throws PersistenceException, ValidationException {
		Therapist therapist = new Therapist();
		therapist.setId(999);

		List<TherapyType> found = dao.findAllByTherapist(therapist);
		assertThat("found entries", found.size(), lessThanOrEqualTo(0));
	}

	@Test
	public void findAllByTherapistBy_InvalidTherapist() throws PersistenceException, ValidationException {
		Therapist therapist = new Therapist();
		therapist.setId(null);

		exception.expect(PersistenceException.class);
		dao.findAllByTherapist(therapist);
	}

	@Test
	public void findAllByTherapistBy_EmptyTherapist() throws PersistenceException, ValidationException {
		Therapist therapist = new Therapist();

		exception.expect(PersistenceException.class);
		dao.findAllByTherapist(therapist);
	}

	@Test
	public void findAllByTherapistBy_NullTherapist() throws PersistenceException, ValidationException {
		exception.expect(PersistenceException.class);
		dao.findAllByTherapist(null);
	}
}
