package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public abstract class PatientBasedTherapyTypeOccurrenceConstraintDaoTest extends BasicDaoTest<PatientBasedTherapyTypeOccurrenceConstraint, PatientBasedTherapyTypeOccurrenceConstraintDAO> {
    //##########################deleteAllByPatient################################
    @Test
    public void deleteAllByPatientBy_ValidPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(1);

        dao.deleteAllByPatient(patient);
    }

    @Test
    public void deleteAllByPatientBy_InvalidPatient() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.deleteAllByPatient(new Patient());
    }

    @Test
    public void deleteAllByPatientBy_NullPatient() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.deleteAllByPatient(null);
    }

    @Test
    public void deleteAllByPatientBy_NotExistingPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(999);

        exception.expect(PersistenceException.class);
        dao.deleteAllByPatient(patient);
    }

    //##########################findAllByPatient################################
    @Test
    public void findAllByPatientBy_ValidPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(1);

        List<PatientBasedTherapyTypeOccurrenceConstraint> found = dao.findAllByPatient(patient, false);
        assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllByPatientBy_InvalidPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(null);

        exception.expect(PersistenceException.class);
        dao.findAllByPatient(patient, false);
    }

    @Test
    public void findAllByPatientBy_NotExistingPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(999);

        List<PatientBasedTherapyTypeOccurrenceConstraint> found = dao.findAllByPatient(patient, false);
        assertThat("found entries", found.size(), lessThanOrEqualTo(0));
    }

    @Test
    public void findAllPatientBy_NullPatient() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByPatient(null, false);
    }

    //##########################findAllStatsByTherapyPlanGroupedByTherapyType################################
    @Test
    public void findAllStatsByTherapyPlanGroupByTherapyTypeBy_ValidTherapyPlan() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(1);

        assertThat("map size", dao.findAllStatsByTherapyPlanGroupedByTherapyType(therapyPlan).size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllStatsByTherapyPlanGroupByTherapyTypeBy_NotExistingTherapyPlan() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(999);

        assertThat("map size", dao.findAllStatsByTherapyPlanGroupedByTherapyType(therapyPlan).size(), lessThanOrEqualTo(0));
    }

    @Test
    public void findAllStatsByTherapyPlanGroupByTherapyTypeBy_InvalidTherapyPlan() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(null);

        exception.expect(PersistenceException.class);
        dao.findAllStatsByTherapyPlanGroupedByTherapyType(therapyPlan);
    }

    @Test
    public void findAllStatsByTherapyPlanGroupByTherapyTypeBy_NullTherapyPlan() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllStatsByTherapyPlanGroupedByTherapyType(null);
    }

    //##########################findAllByTherapyPlanGroupedByTherapyType################################
    @Test
    public void findAllByTherapyPlanGroupedByTherapyTypeBy_ValidTherapyPlan() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(1);

        assertThat("map size", dao.findAllStatsByTherapyPlanGroupedByTherapyType(therapyPlan).size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllByTherapyPlanGroupedByTherapyTypeBy_NotExistingTherapyPlan() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(999);

        assertThat("list size", dao.findAllStatsByTherapyPlanGroupedByTherapyType(therapyPlan).size(), lessThanOrEqualTo(0));
    }

    @Test
    public void findAllByTherapyPlanGroupedByTherapyTypeBy_InvalidTherapyPlan() throws PersistenceException, ValidationException {
        TherapyPlan therapyPlan = new TherapyPlan();
        therapyPlan.setId(null);

        exception.expect(PersistenceException.class);
        dao.findAllStatsByTherapyPlanGroupedByTherapyType(therapyPlan);
    }

    @Test
    public void findAllByTherapyPlanGroupedByTherapyTypeBy_NullTherapyPlan() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllStatsByTherapyPlanGroupedByTherapyType(null);
    }
}
