package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.entities.TherapyRoom;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public abstract class TherapyRoomDaoTest extends BasicDaoTest<TherapyRoom, TherapyRoomDAO> {
	//##########################findAllByTherapyPlan################################
	@Test
	public void findAllByTherapyPlanBy_ValidTherapyPlan() throws PersistenceException, ValidationException {
		TherapyPlan therapyPlan = new TherapyPlan();
		therapyPlan.setId(1);

		List<TherapyRoom> found = dao.findAllByTherapyPlan(therapyPlan);
		assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
	}

	@Test
	public void findAllByTherapyPlanBy_InvalidTherapyPlan() throws PersistenceException, ValidationException {
		exception.expect(PersistenceException.class);
		dao.findAllByTherapyPlan(new TherapyPlan());
	}

	@Test
	public void findAllByTherapyPlanBy_NotExistingTherapyPlan() throws PersistenceException, ValidationException {
		TherapyPlan therapyPlan = new TherapyPlan();
		therapyPlan.setId(999);

		List<TherapyRoom> found = dao.findAllByTherapyPlan(therapyPlan);
		assertThat("found entries", found.size(), lessThanOrEqualTo(0));
	}

	@Test
	public void findAllByTherapyPlanBy_NullTherapyPlan() throws PersistenceException, ValidationException {
		exception.expect(PersistenceException.class);
		dao.findAllByTherapyPlan(null);
	}
}
