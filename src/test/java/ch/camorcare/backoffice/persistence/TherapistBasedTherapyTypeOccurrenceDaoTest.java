package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public abstract class TherapistBasedTherapyTypeOccurrenceDaoTest extends BasicDaoTest<TherapistBasedTherapyTypeOccurrenceConstraint, TherapistBasedTherapyTypeOccurrenceConstraintDAO> {
    //##########################findAllByTherapist################################
    @Test
    public void findAllByTherapistBy_ValidTherapist() throws PersistenceException, ValidationException {
        Therapist therapist = new Therapist();
        therapist.setId(1);

        List<TherapistBasedTherapyTypeOccurrenceConstraint> found = dao.findAllByTherapist(therapist);
        assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllByTherapistBy_InvalidTherapist() throws PersistenceException, ValidationException {
        Therapist therapist = new Therapist();
        therapist.setId(null);

        exception.expect(PersistenceException.class);
        dao.findAllByTherapist(therapist);
    }

    @Test
    public void findAllByTherapistBy_NotExistingTherapist() throws PersistenceException, ValidationException {
        Therapist therapist = new Therapist();
        therapist.setId(999);

        List<TherapistBasedTherapyTypeOccurrenceConstraint> found = dao.findAllByTherapist(therapist);
        assertThat("found entries", found.size(), lessThanOrEqualTo(0));
    }

    @Test
    public void findAllTherapistBy_NullTherapist() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByTherapist(null);
    }

    //##########################deleteByTherapist################################
    @Test
    public void deleteByTherapistBy_ValidTherapist() throws PersistenceException, ValidationException {
        Therapist therapist = new Therapist();
        therapist.setId(1);

        dao.deleteByTherapist(therapist);
    }

    @Test
    public void deleteByTherapistBy_InvalidTherapist() throws PersistenceException, ValidationException {
        Therapist therapist = new Therapist();
        therapist.setId(null);

        exception.expect(PersistenceException.class);
        dao.deleteByTherapist(therapist);
    }

    @Test
    public void deleteByTherapistBy_NotExistingTherapist() throws PersistenceException, ValidationException {
        Therapist therapist = new Therapist();
        therapist.setId(999);

        exception.expect(PersistenceException.class);
        dao.deleteByTherapist(therapist);
    }

    @Test
    public void deleteByTherapistBy_EmptyTherapist() throws PersistenceException, ValidationException {
        Therapist therapist = new Therapist();

        exception.expect(PersistenceException.class);
        dao.deleteByTherapist(therapist);
    }

    @Test
    public void deleteByTherapistBy_NullTherapist() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.deleteByTherapist(null);
    }
}
