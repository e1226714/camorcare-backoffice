package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.TimeBlock;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

public abstract class TimeBlockDaoTest extends BasicDaoTest<TimeBlock, TimeBlockDAO> {
    //##########################FindAllGroupedByDay################################
    @Test
    public void findAllGroupByDay() throws PersistenceException, ValidationException {
        assertThat("map size", dao.findAllGroupedByDay().size(), greaterThanOrEqualTo(1));
    }

    //##########################FindAllByDay################################
    @Test
    public void findAllByDayBy_ValidDay() throws PersistenceException, ValidationException {
        Day day = Day.TUESDAY;
        List<TimeBlock> found = dao.findAllByDay(day);
        assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllDayBy_NullDay() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByDay(null);
    }

}
