package ch.camorcare.backoffice.persistence.H2;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.persistence.TherapistDAO;
import ch.camorcare.backoffice.persistence.TherapistDaoTest;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class H2TherapistDaoTest extends TherapistDaoTest {

    public static ConnectionManager connectionManager;
    public static TherapistDAO dao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        connectionManager = (ConnectionManager) context.getBean("connectionManager");
        dao = (TherapistDAO) context.getBean("therapistDAO");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        connectionManager.close();
    }

    @Before
    public void setUpBefore() throws Exception {
        this.setDao(dao);
        this.setDynamicType(new Therapist());
    }

    @After
    public void tearDownAfter() throws Exception {
        connectionManager.rollback();
    }
}


