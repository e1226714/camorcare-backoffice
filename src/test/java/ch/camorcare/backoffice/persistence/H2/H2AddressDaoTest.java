package ch.camorcare.backoffice.persistence.H2;

import ch.camorcare.backoffice.entities.Address;
import ch.camorcare.backoffice.persistence.AddressDAO;
import ch.camorcare.backoffice.persistence.AddressDaoTest;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class H2AddressDaoTest extends AddressDaoTest {

    public static ConnectionManager connectionManager;
    public static AddressDAO dao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        connectionManager = (ConnectionManager) context.getBean("connectionManager");
        dao = (AddressDAO) context.getBean("addressDAO");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        connectionManager.close();
    }

    @Before
    public void setUpBefore() throws Exception {
        this.setDao(dao);
        this.setDynamicType(new Address());
    }

    /**
     * @throws Exception
     */
    @After
    public void tearDownAfter() throws Exception {
        connectionManager.rollback();
    }
}


