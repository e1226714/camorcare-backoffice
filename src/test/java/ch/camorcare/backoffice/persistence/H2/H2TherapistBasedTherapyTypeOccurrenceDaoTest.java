package ch.camorcare.backoffice.persistence.H2;

import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.persistence.TherapistBasedTherapyTypeOccurrenceConstraintDAO;
import ch.camorcare.backoffice.persistence.TherapistBasedTherapyTypeOccurrenceDaoTest;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class H2TherapistBasedTherapyTypeOccurrenceDaoTest extends TherapistBasedTherapyTypeOccurrenceDaoTest {
    public static ConnectionManager connectionManager;
    public static TherapistBasedTherapyTypeOccurrenceConstraintDAO dao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        connectionManager = (ConnectionManager) context.getBean("connectionManager");
        dao = (TherapistBasedTherapyTypeOccurrenceConstraintDAO) context.getBean("therapistBasedTherapyTypeOccurrenceConstraintDAO");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        connectionManager.close();
    }

    @Before
    public void setUpBefore() throws Exception {
        this.setDao(dao);
        this.setDynamicType(new TherapistBasedTherapyTypeOccurrenceConstraint());
    }

    @After
    public void tearDownAfter() throws Exception {
        connectionManager.rollback();
    }
}
