package ch.camorcare.backoffice.persistence.H2;

import ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint;
import ch.camorcare.backoffice.persistence.PatientBasedTherapistConstraintDAO;
import ch.camorcare.backoffice.persistence.PatientBasedTherapistConstraintDaoTest;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class H2PatientBasedTherapistConstraintDaoTest extends PatientBasedTherapistConstraintDaoTest {
    public static ConnectionManager connectionManager;
    public static PatientBasedTherapistConstraintDAO dao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        connectionManager = (ConnectionManager) context.getBean("connectionManager");
        dao = (PatientBasedTherapistConstraintDAO) context.getBean("patientBasedTherapistConstraintDAO");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        connectionManager.close();
    }

    @Before
    public void setUpBefore() throws Exception {
        this.setDao(dao);
        this.setDynamicType(new PatientBasedTherapistConstraint());
    }

    @After
    public void tearDownAfter() throws Exception {
        connectionManager.rollback();
    }
}
