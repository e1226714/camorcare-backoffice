package ch.camorcare.backoffice.persistence.H2;

import ch.camorcare.backoffice.entities.DutyCategory;
import ch.camorcare.backoffice.persistence.DutyCategoryDAO;
import ch.camorcare.backoffice.persistence.DutyCategoryDaoTest;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class H2DutyCategoryDaoTest extends DutyCategoryDaoTest {

    public static ConnectionManager connectionManager;
    public static DutyCategoryDAO dao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        connectionManager = (ConnectionManager) context.getBean("connectionManager");
        dao = (DutyCategoryDAO) context.getBean("dutyCategoryDAO");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        connectionManager.close();
    }

    @Before
    public void setUpBefore() throws Exception {
        this.setDao(dao);
        this.setDynamicType(new DutyCategory());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDownAfter() throws Exception {
        connectionManager.rollback();
    }
}


