package ch.camorcare.backoffice.persistence.H2;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.persistence.PatientDAO;
import ch.camorcare.backoffice.persistence.PatientDaoTest;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class H2PatientDaoTest extends PatientDaoTest {

    public static ConnectionManager connectionManager;
    public static PatientDAO dao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        connectionManager = (ConnectionManager) context.getBean("connectionManager");
        dao = (PatientDAO) context.getBean("patientDAO");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        connectionManager.close();
    }

    @Before
    public void setUpBefore() throws Exception {
        this.setDao(dao);
        this.setDynamicType(new Patient());
    }

    @After
    public void tearDownAfter() throws Exception {
        connectionManager.rollback();
    }
}


