package ch.camorcare.backoffice.persistence.H2;

import ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint;
import ch.camorcare.backoffice.persistence.TherapistBasedTimeBlockConstraintDAO;
import ch.camorcare.backoffice.persistence.TherapistBasedTimeBlockConstraintDaoTest;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class H2TherapistBasedTimeBlockConstraintDaoTest extends TherapistBasedTimeBlockConstraintDaoTest {
    public static ConnectionManager connectionManager;
    public static TherapistBasedTimeBlockConstraintDAO dao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        connectionManager = (ConnectionManager) context.getBean("connectionManager");
        dao = (TherapistBasedTimeBlockConstraintDAO) context.getBean("therapistBasedTimeBlockConstraintDAO");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        connectionManager.close();
    }

    @Before
    public void setUpBefore() throws Exception {
        this.setDao(dao);
        this.setDynamicType(new TherapistBasedTimeBlockConstraint());
    }

    @After
    public void tearDownAfter() throws Exception {
        connectionManager.rollback();
    }
}
