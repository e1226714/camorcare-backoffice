package ch.camorcare.backoffice.persistence.H2;

import ch.camorcare.backoffice.entities.PatientContact;
import ch.camorcare.backoffice.persistence.PatientContactDAO;
import ch.camorcare.backoffice.persistence.PatientContactDaoTest;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class H2PatientContactDaoTest extends PatientContactDaoTest {

    public static ConnectionManager connectionManager;
    public static PatientContactDAO dao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        connectionManager = (ConnectionManager) context.getBean("connectionManager");
        dao = (PatientContactDAO) context.getBean("patientContactDAO");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        connectionManager.close();
    }

    @Before
    public void setUpBefore() throws Exception {
        this.setDao(dao);
        this.setDynamicType(new PatientContact());
    }

    @After
    public void tearDownAfter() throws Exception {
        connectionManager.rollback();
    }
}


