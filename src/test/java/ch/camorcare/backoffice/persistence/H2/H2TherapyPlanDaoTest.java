package ch.camorcare.backoffice.persistence.H2;

import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.persistence.TherapyPlanDAO;
import ch.camorcare.backoffice.persistence.TherapyPlanDaoTest;
import ch.camorcare.backoffice.persistence.database.ConnectionManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class H2TherapyPlanDaoTest extends TherapyPlanDaoTest {

    public static ConnectionManager connectionManager;
    public static TherapyPlanDAO dao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        connectionManager = (ConnectionManager) context.getBean("connectionManager");
        dao = (TherapyPlanDAO) context.getBean("therapyPlanDAO");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        connectionManager.close();
    }

    @Before
    public void setUpBefore() throws Exception {
        this.setDao(dao);
        this.setDynamicType(new TherapyPlan());
    }

    @After
    public void tearDownAfter() throws Exception {
        connectionManager.rollback();
    }
}


