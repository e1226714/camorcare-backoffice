package ch.camorcare.backoffice.persistence;

import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.entities.PatientBasedTherapistConstraint;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public abstract class PatientBasedTherapistConstraintDaoTest extends BasicDaoTest<PatientBasedTherapistConstraint, PatientBasedTherapistConstraintDAO> {
    //##########################findAllByPatient################################
    @Test
    public void findAllByPatientBy_ValidPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(1);

        List<PatientBasedTherapistConstraint> found = dao.findAllByPatient(patient, false);
        assertThat("found entries", found.size(), greaterThanOrEqualTo(1));
    }

    @Test
    public void findAllByPatientBy_InvalidPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(null);

        exception.expect(PersistenceException.class);
        dao.findAllByPatient(patient, false);
    }

    @Test
    public void findAllByPatientBy_NotExistingPatient() throws PersistenceException, ValidationException {
        Patient patient = new Patient();
        patient.setId(999);

        List<PatientBasedTherapistConstraint> found = dao.findAllByPatient(patient, false);
        assertThat("found entries", found.size(), lessThanOrEqualTo(0));
    }

    @Test
    public void findAllPatientBy_NullPatient() throws PersistenceException, ValidationException {
        exception.expect(PersistenceException.class);
        dao.findAllByPatient(null, false);
    }
}
