package ch.camorcare.backoffice.service;

import ch.camorcare.backoffice.entities.*;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.entities.type.Gender;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.persistence.*;
import ch.camorcare.backoffice.persistence.exception.PersistenceException;
import ch.camorcare.backoffice.service.exception.PlanningException;
import ch.camorcare.backoffice.service.exception.ServiceException;
import ch.camorcare.backoffice.service.impl.TherapyScheduleServiceImpl;
import ch.camorcare.backoffice.util.validator.ValidationException;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ch.camorcare.backoffice.util.matchers.PatientIdEqualsMatcher.patientIdEquals;
import static ch.camorcare.backoffice.util.matchers.TherapyEventIdEqualsMatcher.therapyEventIdEquals;
import static ch.camorcare.backoffice.util.matchers.TherapyTypeIdEqualsMatcher.therapyTypeIdEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.when;

/**
 * Tests for TherapyScheduleService
 */
public class TherapyScheduleServiceTest {

    private static TherapyScheduleServiceImpl therapyScheduleService;
    private static List<Therapist> therapists;
    private static List<Patient> patients;
    private static List<Therapy> therapies;
    private static List<TherapyType> therapyTypes;
    private static List<TherapyEvent> events;
    private static List<TherapyRoom> rooms;
    private static List<TimeBlock> availableTimeBlocks;
    private static List<TherapistBasedTimeBlockConstraint> therapistTimeConstraints;
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private static void setUpAuxLists() {

        DateTimeFormatter tf = DateTimeFormat.forPattern("HH:mm");

        //sample TimeBlocks
        availableTimeBlocks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Day d = null;
            switch (i) {
                case 0:
                    d = Day.MONDAY;
                    break;
                case 1:
                    d = Day.TUESDAY;
                    break;
                case 2:
                    d = Day.WEDNESDAY;
                    break;
                case 3:
                    d = Day.THURSDAY;
                    break;
                case 4:
                    d = Day.FRIDAY;
                    break;
                case 5:
                    d = Day.SATURDAY;
                    break;
                case 6:
                    d = Day.SUNDAY;
                    break;
            }
            for (int j = 0; j < 6; j++) {
                TimeBlock t = new TimeBlock();
                t.setId((i + 1) * 10 + (j + 1));
                t.setDay(d);
                t.setTimeStart(tf.parseDateTime("11:00"));
                availableTimeBlocks.add(t);
            }
        }

        //sample TherapyTypes
        therapyTypes = new ArrayList<>();
        for (int i = 0; i < 3; i++) {//single Events
            TherapyType therapyType = new TherapyType();
            therapyType.setId(i + 1);
            therapyType.setSuitableForIndividuals(true);
            therapyType.setSuitableForGroups(false);
            therapyTypes.add(therapyType);
        }
        for (int i = 3; i < 5; i++) {//single Events
            TherapyType therapyType = new TherapyType();
            therapyType.setId(i + 1);
            therapyType.setSuitableForIndividuals(false);
            therapyType.setSuitableForGroups(true);
            therapyType.setMaxGroupSize(10);
            therapyTypes.add(therapyType);
        }

        //sample Therapists
        therapists = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Therapist therapist = new Therapist();
            therapist.setId(i + 1);
            if (i == 0) {
                therapist.setGender(Gender.MALE);
            } else {
                therapist.setGender(Gender.FEMALE);
            }
            therapist.setTherapyTypes(new ArrayList<>());
            for (int j = 0; j < 5; j++) {
                therapist.getTherapyTypes().add(therapyTypes.get(j));
            }
            therapists.add(therapist);
        }

        //sample Patients
        patients = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Patient patient = new Patient();
            patient.setId(i + 1);
            patients.add(patient);
        }

        //sample Therapies with Patients
        therapies = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Therapy therapy = new Therapy();
            therapy.setId(i + 1);
            therapy.setPatient(patients.get(i));
            therapies.add(therapy);
        }
        TherapyPlan plan = new TherapyPlan();
        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd");
        plan.setDateFrom(df.parseDateTime("2015-02-19"));

        //sample TherapyEvents
        events = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            TherapyEvent event = new TherapyEvent();
            event.setId(i + 1);
            event.setTherapist(therapists.get(i % 5));
            event.setTherapyPlan(plan);
            events.add(event);
        }
    }


    private static void mockRooms() throws PersistenceException {
        //mock TherapyRoomDAO
        rooms = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            TherapyRoom room = new TherapyRoom();
            room.setId(i + 1);
            room.setCapacity(1);
            rooms.add(room);
        }
        for (int i = 5; i < 10; i++) {
            TherapyRoom room = new TherapyRoom();
            room.setId(i + 1);
            room.setCapacity(10);
            rooms.add(room);
        }
        TherapyRoomDAO therapyRoomDAO = Mockito.mock(TherapyRoomDAO.class);
        when(therapyRoomDAO.findAll()).thenReturn(rooms);
        therapyScheduleService.setTherapyRoomDAO(therapyRoomDAO);
    }

    private static void mockTherapyEventsForPlanning() throws PersistenceException, ValidationException {

        //therapy events 1-10 with one participant
        Map<Integer, List<Therapy>> participantsForTherapyEvent = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            List<Therapy> participants = new ArrayList<>();
            Therapy therapy = new Therapy();
            therapy.setId(i + 1);
            participants.add(therapy);
            participantsForTherapyEvent.put(i + 1, participants);
        }
        //therapy events 11-20 with one participant
        for (int i = 0; i < 10; i++) {
            List<Therapy> participants = new ArrayList<>();
            Therapy therapy = new Therapy();
            therapy.setId(i + 1);
            participants.add(therapy);
            participantsForTherapyEvent.put(i + 11, participants);
        }
        //therapy events 21-30 with five participants
        for (int i = 0; i < 10; i++) {
            List<Therapy> participants = new ArrayList<>();
            //for each Event 5 Therapies
            for (int j = 0; j < 5; j++) {
                Therapy therapy = new Therapy();
                therapy.setId(j + i % 5);
                participants.add(therapy);
            }
            participantsForTherapyEvent.put(i + 21, participants);
        }

        TherapyEvent dummy = new TherapyEvent();

        TherapyEventDAO therapyEventDAO = Mockito.mock(TherapyEventDAO.class);
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(1)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(1));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(2)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(2));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(3)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(3));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(4)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(4));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(5)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(5));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(6)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(6));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(7)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(7));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(8)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(8));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(9)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(9));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(10)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(10));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(11)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(11));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(12)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(12));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(13)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(13));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(14)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(14));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(15)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(15));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(16)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(16));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(17)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(17));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(18)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(18));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(19)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(19));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(20)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(20));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(21)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(21));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(22)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(22));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(23)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(23));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(24)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(24));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(25)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(25));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(26)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(26));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(27)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(27));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(28)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(28));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(29)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(29));
        Mockito.when(therapyEventDAO.getParticipants(argThat(therapyEventIdEquals(30)), anyBoolean())).thenReturn(participantsForTherapyEvent.get(30));


        when(therapyEventDAO.findAllByTherapyPlan(Matchers.any(TherapyPlan.class), anyBoolean())).thenReturn(events);
        therapyScheduleService.setTherapyEventDAO(therapyEventDAO);
    }

    private static void mockTimeBlocks() throws PersistenceException {
        List<TimeBlock> forbiddenTimeBlocks = new ArrayList<>();

        forbiddenTimeBlocks.add(availableTimeBlocks.get(0));
        forbiddenTimeBlocks.add(availableTimeBlocks.get(1));
        forbiddenTimeBlocks.add(availableTimeBlocks.get(2));
        forbiddenTimeBlocks.add(availableTimeBlocks.get(3));
        forbiddenTimeBlocks.add(availableTimeBlocks.get(4));
        forbiddenTimeBlocks.add(availableTimeBlocks.get(5));
        forbiddenTimeBlocks.add(availableTimeBlocks.get(6));
        forbiddenTimeBlocks.add(availableTimeBlocks.get(7));
        forbiddenTimeBlocks.add(availableTimeBlocks.get(8));
        forbiddenTimeBlocks.add(availableTimeBlocks.get(9));


        //mock TimeBlockDAO
        TimeBlockDAO timeBlockDAO = Mockito.mock(TimeBlockDAO.class);
        when(timeBlockDAO.findAll()).thenReturn(availableTimeBlocks);
        when(timeBlockDAO.findAllNegativeByTherapist(Matchers.any(Therapist.class))).thenReturn(forbiddenTimeBlocks);
        therapyScheduleService.setTimeBlockDAO(timeBlockDAO);
    }

    /*
    private static void mockTherapistForbiddenBlocks() throws PersistenceException {
        //first ten timeblocks are forbidden for all therapists
        therapistTimeConstraints = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TherapistBasedTimeBlockConstraint ttb = new TherapistBasedTimeBlockConstraint();
            ttb.setId(i + 1);
            ttb.setTimeBlock(availableTimeBlocks.get(i));
            therapistTimeConstraints.add(ttb);
        }

        TherapistBasedTimeBlockConstraintDAO therapistBasedTimeBlockConstraintDAO = Mockito.mock(TherapistBasedTimeBlockConstraintDAO.class);
        when(therapistBasedTimeBlockConstraintDAO.findAllNegativeByTherapist(any(Therapist.class), anyBoolean())).thenReturn(therapistTimeConstraints);
        therapyScheduleService.setTherapistBasedTimeBlockConstraintDAO(therapistBasedTimeBlockConstraintDAO);
    }
    */

    private static void mockTherapistAvailableBlocksIsAlwaysAvailable() throws PersistenceException {
        //Therapist available for all time Blocks
        therapistTimeConstraints = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            TherapistBasedTimeBlockConstraint ttb = new TherapistBasedTimeBlockConstraint();
            ttb.setId(i + 1);
            ttb.setTimeBlock(availableTimeBlocks.get(i));
            therapistTimeConstraints.add(ttb);
        }

        TherapistBasedTimeBlockConstraintDAO therapistBasedTimeBlockConstraintDAO = Mockito.mock(TherapistBasedTimeBlockConstraintDAO.class);
        when(therapistBasedTimeBlockConstraintDAO.findAllByTherapist(Matchers.any(Therapist.class), anyBoolean())).thenReturn(therapistTimeConstraints);
        therapyScheduleService.setTherapistBasedTimeBlockConstraintDAO(therapistBasedTimeBlockConstraintDAO);
    }

    private static void mockTherapistProfession() throws PersistenceException {
        //Therapist available for all time Blocks
        List<TherapistBasedTherapyTypeOccurrenceConstraint> tbcTherapyTypes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            TherapistBasedTherapyTypeOccurrenceConstraint tbc = new TherapistBasedTherapyTypeOccurrenceConstraint();
            tbc.setId(i + 1);
            tbc.setTherapyType(therapyTypes.get(i));
            tbcTherapyTypes.add(tbc);
        }

        TherapistBasedTherapyTypeOccurrenceConstraintDAO therapistBasedTherapyTypeOccurrenceConstraintDAO = Mockito.mock(TherapistBasedTherapyTypeOccurrenceConstraintDAO.class);
        when(therapistBasedTherapyTypeOccurrenceConstraintDAO.findAllByTherapist(Matchers.any(Therapist.class))).thenReturn(tbcTherapyTypes);
        therapyScheduleService.setTherapistBasedTherapyTypeOccurrenceConstraintDAO(therapistBasedTherapyTypeOccurrenceConstraintDAO);
    }

    private static void mockTherapies() throws PersistenceException {
        TherapyDAO therapyDAO = Mockito.mock(TherapyDAO.class);
        Mockito.when(therapyDAO.findAllByTherapyPlan(Matchers.any(TherapyPlan.class))).thenReturn(therapies);
        therapyScheduleService.setTherapyDAO(therapyDAO);
    }

    private static void mockTherapists() throws PersistenceException {
        TherapistDAO therapistDAO = Mockito.mock(TherapistDAO.class);
        Mockito.when(therapistDAO.findAll()).thenReturn(therapists);
        therapyScheduleService.setTherapistDAO(therapistDAO);
    }

    private static void mockPatientBaseTherapyTypeOccurenceConstraints() throws PersistenceException {
        Map<Integer, List<PatientBasedTherapyTypeOccurrenceConstraint>> constraintsForTherapies = new HashMap<>();
        for (int j = 0; j < 10; j++) {
            List<PatientBasedTherapyTypeOccurrenceConstraint> constraints = new ArrayList<>();
            constraintsForTherapies.put(j, constraints);
            for (int i = 0; i < 3; i++) {
                PatientBasedTherapyTypeOccurrenceConstraint constraint = new PatientBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(j * 10 + i);
                constraint.setMaxOccurencePerWeek(i % 2 + 1);//1 or 2 occurences
                constraint.setTherapyType(therapyTypes.get(i));
                constraint.setTherapyMode(TherapyMode.SINGLE);
                constraints.add(constraint);
            }
            for (int i = 3; i < 5; i++) {
                PatientBasedTherapyTypeOccurrenceConstraint constraint = new PatientBasedTherapyTypeOccurrenceConstraint();
                constraint.setId(j * 10 + i);
                constraint.setMaxOccurencePerWeek(i % 2 + 1);//1 or 2 occurences
                constraint.setTherapyType(therapyTypes.get(i));
                constraint.setTherapyMode(TherapyMode.GROUP);
                constraints.add(constraint);
            }

        }

        PatientBasedTherapyTypeOccurrenceConstraintDAO dao = Mockito.mock(PatientBasedTherapyTypeOccurrenceConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(constraintsForTherapies.get(0));
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(constraintsForTherapies.get(1));
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(constraintsForTherapies.get(2));
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(constraintsForTherapies.get(3));
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(constraintsForTherapies.get(4));
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(constraintsForTherapies.get(5));
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(constraintsForTherapies.get(6));
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(constraintsForTherapies.get(7));
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(constraintsForTherapies.get(8));
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(constraintsForTherapies.get(9));
        therapyScheduleService.setPatientBasedTherapyTypeOccurrenceConstraintDAO(dao);


    }

    private static void mockTherapyTypes() throws PersistenceException {

        TherapyTypeDAO dao = Mockito.mock(TherapyTypeDAO.class);
        Mockito.when(dao.findOneById(argThat(therapyTypeIdEquals(1)))).thenReturn(therapyTypes.get(0));
        Mockito.when(dao.findOneById(argThat(therapyTypeIdEquals(2)))).thenReturn(therapyTypes.get(1));
        Mockito.when(dao.findOneById(argThat(therapyTypeIdEquals(3)))).thenReturn(therapyTypes.get(2));
        Mockito.when(dao.findOneById(argThat(therapyTypeIdEquals(4)))).thenReturn(therapyTypes.get(3));
        Mockito.when(dao.findOneById(argThat(therapyTypeIdEquals(5)))).thenReturn(therapyTypes.get(4));
        therapyScheduleService.setTherapyTypeDAO(dao);
    }

    private static void mockTherapyEventCreate() throws PersistenceException, ValidationException {
        TherapyEvent dummy = new TherapyEvent();

        TherapyEventDAO therapyEventDAO = Mockito.mock(TherapyEventDAO.class);
        Mockito.when(therapyEventDAO.create(Matchers.any(TherapyEvent.class))).thenReturn(dummy);
        therapyScheduleService.setTherapyEventDAO(therapyEventDAO);
    }

    private static void mockPatientBasesTherapistConstraint_NoConstraints() throws PersistenceException {

        PatientBasedTherapistConstraintDAO dao = Mockito.mock(PatientBasedTherapistConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(new ArrayList<>());
        therapyScheduleService.setPatientBasedTherapistConstraintDAO(dao);
    }

    /**
     * 2 patients have differnt preferences of therapists
     * for the group event it should lead to split groups
     *
     * @throws PersistenceException
     */
    private static void mockPatientBasesTherapistConstraint_ConstraintsForPatient3And5SingleAndGroup() throws PersistenceException {

        List<PatientBasedTherapistConstraint> constraints = new ArrayList<>();

        PatientBasedTherapistConstraint constraint = new PatientBasedTherapistConstraint();
        constraint.setId(1);
        constraint.setTherapist(therapists.get(0));
        constraint.setTherapyType(therapyTypes.get(0));
        constraints.add(constraint);
        PatientBasedTherapistConstraint constraint2 = new PatientBasedTherapistConstraint();
        constraint2.setId(2);
        constraint2.setTherapist(therapists.get(1));
        constraint2.setTherapyType(therapyTypes.get(4));
        constraints.add(constraint2);

        List<PatientBasedTherapistConstraint> constraints2 = new ArrayList<>();

        PatientBasedTherapistConstraint constraint21 = new PatientBasedTherapistConstraint();
        constraint21.setId(1);
        constraint21.setTherapist(therapists.get(0));
        constraint21.setTherapyType(therapyTypes.get(0));
        constraints2.add(constraint21);
        PatientBasedTherapistConstraint constraint22 = new PatientBasedTherapistConstraint();
        constraint22.setId(2);
        constraint22.setTherapist(therapists.get(2));
        constraint22.setTherapyType(therapyTypes.get(4));
        constraints2.add(constraint22);


        PatientBasedTherapistConstraintDAO dao = Mockito.mock(PatientBasedTherapistConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(constraints2);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(new ArrayList<>());
        therapyScheduleService.setPatientBasedTherapistConstraintDAO(dao);
    }

    private static void mockPatientBasesTherapistConstraint_OneConstraintForPatient1TherapyType1() throws PersistenceException {

        List<PatientBasedTherapistConstraint> constraints = new ArrayList<>();

        PatientBasedTherapistConstraint constraint = new PatientBasedTherapistConstraint();
        constraint.setId(1);
        constraint.setTherapist(therapists.get(0));
        constraint.setTherapyType(therapyTypes.get(0));
        constraints.add(constraint);


        PatientBasedTherapistConstraintDAO dao = Mockito.mock(PatientBasedTherapistConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(new ArrayList<>());
        therapyScheduleService.setPatientBasedTherapistConstraintDAO(dao);
    }

    private static void mockPatientBasesTherapistConstraint_OneConstraintForPatient1TherapyType5() throws PersistenceException {

        List<PatientBasedTherapistConstraint> constraints = new ArrayList<>();

        PatientBasedTherapistConstraint constraint = new PatientBasedTherapistConstraint();
        constraint.setId(1);
        constraint.setTherapist(therapists.get(0));
        constraint.setTherapyType(therapyTypes.get(4));
        constraints.add(constraint);


        PatientBasedTherapistConstraintDAO dao = Mockito.mock(PatientBasedTherapistConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(new ArrayList<>());
        therapyScheduleService.setPatientBasedTherapistConstraintDAO(dao);
    }

    private static void mockPatientBasesTherapistConstraint_AllPatientsWantSingleTherapist() throws PersistenceException {

        List<PatientBasedTherapistConstraint> constraints = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            PatientBasedTherapistConstraint constraint = new PatientBasedTherapistConstraint();
            constraint.setId(i);
            constraint.setTherapist(therapists.get(0));
            constraint.setTherapyType(therapyTypes.get(i));
            constraints.add(constraint);
        }


        PatientBasedTherapistConstraintDAO dao = Mockito.mock(PatientBasedTherapistConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(constraints);
        therapyScheduleService.setPatientBasedTherapistConstraintDAO(dao);
    }

    private static void mockPatientBasesTherapistGenderConstraint_NoConstraints() throws PersistenceException {
        Map<Integer, List<PatientBasedTherapistGenderConstraint>> constraintsForTherapies = new HashMap<>();
        for (int j = 0; j < 10; j++) {
            List<PatientBasedTherapistGenderConstraint> constraints = new ArrayList<>();
            constraintsForTherapies.put(j, constraints);
            for (int i = 3; i < 5; i++) {
                PatientBasedTherapistGenderConstraint constraint = new PatientBasedTherapistGenderConstraint();
                constraint.setId(j * 10 + i);
                constraint.setGender((i % 2 == 0) ? Gender.MALE : Gender.FEMALE);
                constraint.setTherapyType(therapyTypes.get(i));
                constraints.add(constraint);
            }

        }

        PatientBasedTherapistGenderConstraintDAO dao = Mockito.mock(PatientBasedTherapistGenderConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(new ArrayList<>());
        therapyScheduleService.setPatientBasedTherapistGenderConstraintDAO(dao);
    }

    /**
     * 2 patients have different gender preferences, which should lead to split groups
     *
     * @throws PersistenceException
     */
    private static void mockPatientBasesTherapistGenderConstraint_ConstraintsForPatient2And4SingleAndGroup() throws PersistenceException {

        List<PatientBasedTherapistGenderConstraint> constraints = new ArrayList<>();
        PatientBasedTherapistGenderConstraint constraint = new PatientBasedTherapistGenderConstraint();
        constraint.setId(1);
        constraint.setGender(Gender.MALE);
        constraint.setTherapyType(therapyTypes.get(0));
        constraints.add(constraint);
        PatientBasedTherapistGenderConstraint constraint2 = new PatientBasedTherapistGenderConstraint();
        constraint2.setId(2);
        constraint2.setGender(Gender.MALE);
        constraint2.setTherapyType(therapyTypes.get(4));
        constraints.add(constraint2);

        List<PatientBasedTherapistGenderConstraint> constraints2 = new ArrayList<>();
        PatientBasedTherapistGenderConstraint constraint21 = new PatientBasedTherapistGenderConstraint();
        constraint21.setId(1);
        constraint21.setGender(Gender.FEMALE);
        constraint21.setTherapyType(therapyTypes.get(0));
        constraints2.add(constraint21);
        PatientBasedTherapistGenderConstraint constraint22 = new PatientBasedTherapistGenderConstraint();
        constraint22.setId(2);
        constraint22.setGender(Gender.FEMALE);
        constraint22.setTherapyType(therapyTypes.get(4));
        constraints2.add(constraint22);


        PatientBasedTherapistGenderConstraintDAO dao = Mockito.mock(PatientBasedTherapistGenderConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(constraints2);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(new ArrayList<>());
        therapyScheduleService.setPatientBasedTherapistGenderConstraintDAO(dao);
    }

    private static void mockPatientBasesTherapistGenderConstraint_OneConstraintForPatient1TherapyType1() throws PersistenceException {

        List<PatientBasedTherapistGenderConstraint> constraints = new ArrayList<>();
        PatientBasedTherapistGenderConstraint constraint = new PatientBasedTherapistGenderConstraint();
        constraint.setId(1);
        constraint.setGender(Gender.MALE);
        constraint.setTherapyType(therapyTypes.get(0));
        constraints.add(constraint);


        PatientBasedTherapistGenderConstraintDAO dao = Mockito.mock(PatientBasedTherapistGenderConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(new ArrayList<>());
        therapyScheduleService.setPatientBasedTherapistGenderConstraintDAO(dao);
    }

    private static void mockPatientBasesTherapistGenderConstraint_OneConstraintForPatient1TherapyType5() throws PersistenceException {

        List<PatientBasedTherapistGenderConstraint> constraints = new ArrayList<>();
        PatientBasedTherapistGenderConstraint constraint = new PatientBasedTherapistGenderConstraint();
        constraint.setId(1);
        constraint.setGender(Gender.MALE);
        constraint.setTherapyType(therapyTypes.get(4));
        constraints.add(constraint);


        PatientBasedTherapistGenderConstraintDAO dao = Mockito.mock(PatientBasedTherapistGenderConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(new ArrayList<>());
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(new ArrayList<>());
        therapyScheduleService.setPatientBasedTherapistGenderConstraintDAO(dao);
    }

    private static void mockPatientBasesTherapistGenderConstraint_AllPreferencesForMale() throws PersistenceException {

        List<PatientBasedTherapistGenderConstraint> constraints = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            PatientBasedTherapistGenderConstraint constraint = new PatientBasedTherapistGenderConstraint();
            constraint.setId(i);
            constraint.setGender(Gender.MALE);
            constraint.setTherapyType(therapyTypes.get(i));
            constraints.add(constraint);
        }


        PatientBasedTherapistGenderConstraintDAO dao = Mockito.mock(PatientBasedTherapistGenderConstraintDAO.class);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(1)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(2)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(3)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(4)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(5)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(6)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(7)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(8)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(9)), anyBoolean())).thenReturn(constraints);
        Mockito.when(dao.findAllByPatient(argThat(patientIdEquals(10)), anyBoolean())).thenReturn(constraints);
        therapyScheduleService.setPatientBasedTherapistGenderConstraintDAO(dao);
    }

    @Before
    public void setUpBefore() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        therapyScheduleService = new TherapyScheduleServiceImpl();

        setUpAuxLists();

    }

    @Test
    public void scheduleWithOnlyConflictFreeEvents_shouldReturnValidModel() throws PersistenceException, PlanningException, ValidationException, ServiceException {

        //for all scheduling Tests
        setUpAuxLists();
        mockRooms();
        mockTherapyEventsForPlanning();
        mockTimeBlocks();


        TherapyPlan plan = new TherapyPlan();
        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd");
        plan.setDateFrom(df.parseDateTime("2015-02-19"));


        List<TherapyEvent> scheduled = therapyScheduleService.schedule(plan);
        assertThat(scheduled.size(), equalTo(30));
    }

    @Test
    public void createEventsWithoutPreferences_shouldReturnEvents() throws PersistenceException, ValidationException, PlanningException, ServiceException {

        mockTherapyTypes();
        mockTherapies();
        mockTherapists();
        mockTherapistProfession();
        mockPatientBaseTherapyTypeOccurenceConstraints();
        mockTherapistAvailableBlocksIsAlwaysAvailable();
        mockPatientBasesTherapistConstraint_NoConstraints();
        mockPatientBasesTherapistGenderConstraint_NoConstraints();


        TherapyPlan plan = new TherapyPlan();
        Map<TherapyEvent, List<Therapy>> createdEvents = therapyScheduleService.createEventsForWeek(plan);
        assertThat(createdEvents.size(), equalTo(43));
    }

    @Test
    public void createEventsWithoutPreferences_GroupSizeMinimized_GroupEventsSplittedDueToSize() throws PersistenceException, ValidationException, PlanningException, ServiceException {

        for (int i = 0; i < 5; i++) {
            therapyTypes.get(i).setMaxGroupSize(5);
        }
        mockTherapyTypes();
        mockTherapies();
        mockTherapists();
        mockTherapistProfession();
        mockPatientBaseTherapyTypeOccurenceConstraints();
        mockTherapistAvailableBlocksIsAlwaysAvailable();
        mockPatientBasesTherapistConstraint_NoConstraints();
        mockPatientBasesTherapistGenderConstraint_NoConstraints();


        TherapyPlan plan = new TherapyPlan();
        Map<TherapyEvent, List<Therapy>> createdEvents = therapyScheduleService.createEventsForWeek(plan);
        assertThat(createdEvents.size(), equalTo(46));
    }

    @Test
    public void createEventsWithNonConflictingTherapistPreferences_shouldReturnEvents_GroupEventSplitted() throws PersistenceException, ValidationException, PlanningException, ServiceException {


        mockTherapyTypes();
        mockTherapies();
        mockTherapists();
        mockTherapistProfession();
        mockPatientBaseTherapyTypeOccurenceConstraints();
        mockTherapistAvailableBlocksIsAlwaysAvailable();
        mockPatientBasesTherapistConstraint_ConstraintsForPatient3And5SingleAndGroup();
        mockPatientBasesTherapistGenderConstraint_NoConstraints();


        TherapyPlan plan = new TherapyPlan();
        Map<TherapyEvent, List<Therapy>> createdEvents = therapyScheduleService.createEventsForWeek(plan);
        assertThat(createdEvents.size(), equalTo(44));
    }

    @Test
    public void createEventsWithNonConflictingGenderPreferences_shouldReturnEvents_GroupEventSplitted() throws PersistenceException, ValidationException, PlanningException, ServiceException {


        mockTherapyTypes();
        mockTherapies();
        mockTherapists();
        mockTherapistProfession();
        mockPatientBaseTherapyTypeOccurenceConstraints();
        mockTherapistAvailableBlocksIsAlwaysAvailable();
        mockPatientBasesTherapistConstraint_NoConstraints();
        mockPatientBasesTherapistGenderConstraint_ConstraintsForPatient2And4SingleAndGroup();


        TherapyPlan plan = new TherapyPlan();
        Map<TherapyEvent, List<Therapy>> createdEvents = therapyScheduleService.createEventsForWeek(plan);
        assertThat(createdEvents.size(), equalTo(44));
    }

    @Test
    public void createEventsWithNonConflictingTherapistAndGenderPreferences_shouldReturnEvents() throws PersistenceException, ValidationException, PlanningException, ServiceException {


        mockTherapyTypes();
        mockTherapies();
        mockTherapists();
        mockTherapistProfession();
        mockPatientBaseTherapyTypeOccurenceConstraints();
        mockTherapistAvailableBlocksIsAlwaysAvailable();
        mockPatientBasesTherapistConstraint_ConstraintsForPatient3And5SingleAndGroup();
        mockPatientBasesTherapistGenderConstraint_ConstraintsForPatient2And4SingleAndGroup();

        //splitting group event 4 2 times because of following preferences:
        // 2 different therapist preferences(both female)
        // 2 different therapist gender preferences(male and female) - as male is not set, 3 Events are needed
        TherapyPlan plan = new TherapyPlan();
        Map<TherapyEvent, List<Therapy>> createdEvents = therapyScheduleService.createEventsForWeek(plan);
        assertThat(createdEvents.size(), equalTo(45));
    }


    @Test
    public void createEventsWithGenderAndTherapistPreferenceForSameIndividualTherapyType_shouldThrowPlanningException() throws PersistenceException, ValidationException, PlanningException, ServiceException {

        mockTherapyTypes();
        mockTherapies();
        mockTherapists();
        mockTherapistProfession();
        mockPatientBaseTherapyTypeOccurenceConstraints();
        mockTherapistAvailableBlocksIsAlwaysAvailable();
        mockPatientBasesTherapistConstraint_OneConstraintForPatient1TherapyType1();
        mockPatientBasesTherapistGenderConstraint_OneConstraintForPatient1TherapyType1();

        TherapyPlan plan = new TherapyPlan();
        exception.expect(PlanningException.class);
        therapyScheduleService.createEventsForWeek(plan);

    }

    @Test
    public void createEventsWithGenderAndTherapistPreferenceForSameGroupTherapyType_shouldThrowPlanningException() throws PersistenceException, ValidationException, PlanningException, ServiceException {

        mockTherapyTypes();
        mockTherapies();
        mockTherapists();
        mockTherapistProfession();
        mockPatientBaseTherapyTypeOccurenceConstraints();
        mockTherapistAvailableBlocksIsAlwaysAvailable();
        mockPatientBasesTherapistConstraint_OneConstraintForPatient1TherapyType5();
        mockPatientBasesTherapistGenderConstraint_OneConstraintForPatient1TherapyType5();

        TherapyPlan plan = new TherapyPlan();
        exception.expect(PlanningException.class);
        therapyScheduleService.createEventsForWeek(plan);
    }

    @Test
    public void createEventsWithAllPatientsWantSingleTherapist_shouldThrowPlanningException() throws PersistenceException, ValidationException, PlanningException, ServiceException {

        mockTherapyTypes();
        mockTherapies();
        mockTherapists();
        mockTherapistProfession();
        mockPatientBaseTherapyTypeOccurenceConstraints();
        mockTherapistAvailableBlocksIsAlwaysAvailable();
        mockPatientBasesTherapistConstraint_AllPatientsWantSingleTherapist();
        mockPatientBasesTherapistGenderConstraint_NoConstraints();

        TherapyPlan plan = new TherapyPlan();
        exception.expect(PlanningException.class);
        therapyScheduleService.createEventsForWeek(plan);

    }

    @Test
    public void createEvents_TherapistGenderNotAvailable_shouldThrowPlanningException() throws PersistenceException, ValidationException, PlanningException, ServiceException {

        mockTherapyTypes();
        mockTherapies();
        mockTherapists();
        mockTherapistProfession();
        mockPatientBaseTherapyTypeOccurenceConstraints();
        mockTherapistAvailableBlocksIsAlwaysAvailable();
        mockPatientBasesTherapistConstraint_NoConstraints();
        mockPatientBasesTherapistGenderConstraint_AllPreferencesForMale();

        TherapyPlan plan = new TherapyPlan();
        exception.expect(PlanningException.class);
        therapyScheduleService.createEventsForWeek(plan);

    }

}
