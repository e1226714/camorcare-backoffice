package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.Therapy;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.TherapyValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.TherapyValidator}
 */
public class TherapyValidatorTest extends BasicValidatorTest<Therapy> {

    private static TherapyValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (TherapyValidator) context.getBean("therapyValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, Therapy.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new Therapy());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }


    //##########################Patient################################
    @Test
    public void patientNull() {
        sample.setPatient(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("patient"), equalTo(1));
    }

    @Test
    public void patientIdNull() {
        sample.getPatient().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("patient"), equalTo(1));
    }

    //##########################dateFrom################################
    @Test
    public void dateFromNull() {
        sample.setDateFrom(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("dateFrom"), equalTo(1));
        assertThat("errors", err.getFieldErrorCount("dateUntil"), equalTo(1));
    }

    @Test
    public void dateFromIsOlderThanSickLeave() {
        sample.setDateFrom(sample.getDateFrom().minus(100));
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("dateFrom"), equalTo(1));
    }


    //##########################dateUntil################################
    @Test
    public void dateUntilIsOlderThanFrom() {
        sample.setDateUntil(sample.getDateFrom().minus(100));
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("dateUntil"), equalTo(1));
    }


    //##########################dateSickLeave################################
    @Test
    public void dateSickLeaveNull() {
        sample.setDateSickLeave(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("dateSickLeave"), equalTo(1));
        assertThat("errors", err.getFieldErrorCount("dateFrom"), equalTo(1));
    }

}
