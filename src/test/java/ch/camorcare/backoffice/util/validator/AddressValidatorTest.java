package ch.camorcare.backoffice.util.validator;

import ch.camorcare.backoffice.entities.Address;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.AddressValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.AddressValidator}
 */
public class AddressValidatorTest extends BasicValidatorTest<Address> {

    private static AddressValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (AddressValidator) context.getBean("addressValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, Address.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new Address());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################Street################################
    @Test
    public void streetNull() {
        sample.setStreet(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("street"), equalTo(1));
    }

    @Test
    public void streetTooShort() {
        sample.setStreet("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("street"), equalTo(1));
    }

    @Test
    public void streetTooLong() {
        sample.setStreet("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("street"), equalTo(1));
    }

    //##########################Zip################################
    @Test
    public void zipNull() {
        sample.setZip(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("zip"), equalTo(1));
    }

    @Test
    public void zipTooShort() {
        sample.setZip("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("zip"), equalTo(1));
    }

    @Test
    public void zipTooLong() {
        sample.setZip("123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("zip"), equalTo(1));
    }

    //##########################City################################
    @Test
    public void cityNull() {
        sample.setCity(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("city"), equalTo(1));
    }

    @Test
    public void cityTooShort() {
        sample.setCity("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("city"), equalTo(1));
    }

    @Test
    public void cityTooLong() {
        sample.setCity("123456789_123456789_123456789_123456789_123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("city"), equalTo(1));
    }

    //##########################countryCode################################
    @Test
    public void countryCodeNull() {
        sample.setCountryCode(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("countryCode"), equalTo(1));
    }

    @Test
    public void countryCodeTooShort() {
        sample.setCountryCode("A");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("countryCode"), equalTo(1));
    }

    @Test
    public void countryCodeTooLong() {
        sample.setCountryCode("USA");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("countryCode"), equalTo(1));
    }

}
