package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.TherapyPlan;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.TherapyPlanValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.TherapyPlanValidator}
 */
public class TherapyPlanValidatorTest extends BasicValidatorTest<TherapyPlan> {

    private static TherapyPlanValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (TherapyPlanValidator) context.getBean("therapyPlanValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, TherapyPlan.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new TherapyPlan());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################dateFrom################################
    @Test
    public void dateFromNull() {
        sample.setDateFrom(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("dateFrom"), equalTo(1));
        assertThat("errors", err.getFieldErrorCount("dateUntil"), equalTo(1));
    }


    //##########################dateUntil################################
    @Test
    public void dateUntilNull() {
        sample.setDateUntil(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("dateUntil"), equalTo(1));
    }

    @Test
    public void dateUntilIsOlder() {
        sample.setDateUntil(sample.getDateFrom().minus(100));
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("dateUntil"), equalTo(1));
    }


}
