package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.Invoice;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.InvoiceValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.InvoiceValidator}
 */
public class InvoiceValidatorTest extends BasicValidatorTest<Invoice> {

    private static InvoiceValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (InvoiceValidator) context.getBean("invoiceValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, Invoice.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new Invoice());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################Address################################
    @Test
    public void addressNull() {
        sample.setAddress(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("address"), equalTo(1));
    }

    @Test
    public void addressIdNull() {
        sample.getAddress().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("address"), equalTo(1));
    }

    //##########################Therapy################################
    @Test
    public void therapyNull() {
        sample.setTherapy(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapy"), equalTo(1));
    }

    @Test
    public void therapyIdNull() {
        sample.getTherapy().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapy"), equalTo(1));
    }


}
