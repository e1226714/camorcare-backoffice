package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.TherapistBasedTherapyTypeOccurrenceConstraint;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.TherapistBasedTherapyTypeOccurrenceConstraintValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.TherapistBasedTherapyTypeOccurrenceConstraintValidator}
 */
public class TherapistBasedTherapyTypeOccurrenceConstraintValidatorTest extends BasicValidatorTest<TherapistBasedTherapyTypeOccurrenceConstraint> {

    private static TherapistBasedTherapyTypeOccurrenceConstraintValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (TherapistBasedTherapyTypeOccurrenceConstraintValidator) context.getBean("therapistBasedTherapyTypeOccurrenceConstraintValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, TherapistBasedTherapyTypeOccurrenceConstraint.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new TherapistBasedTherapyTypeOccurrenceConstraint());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }


    //##########################Therapist################################
    @Test
    public void therapistNull() {
        sample.setTherapist(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapist"), equalTo(1));
    }

    @Test
    public void patientIdNull() {
        sample.getTherapist().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapist"), equalTo(1));
    }

    //##########################therapyType################################
    @Test
    public void therapyTypeNull() {
        sample.setTherapyType(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyType"), equalTo(1));
    }

    @Test
    public void therapyTypeIdNull() {
        sample.getTherapyType().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyType"), equalTo(1));
    }


    //##########################therapyMode################################
    @Test
    public void therapyModeNull() {
        sample.setTherapyMode(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyMode"), equalTo(1));
    }

    //##########################maxOccurrence################################
    @Test
    public void maxOccurrencePerDayNull() {
        sample.setMaxOccurrencePerDay(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("maxOccurrencePerDay"), equalTo(1));
    }

    @Test
    public void maxOccurrencePerTooHigh() {
        sample.setMaxOccurrencePerDay(-1);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("maxOccurrencePerDay"), equalTo(1));
    }

    @Test
    public void maxOccurrencePerDayTooLow() {
        sample.setMaxOccurrencePerDay(7);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("maxOccurrencePerDay"), equalTo(1));
    }


}
