package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.TherapistValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.TherapistValidator}
 */
public class TherapistValidatorTest extends BasicValidatorTest<Therapist> {

    private static TherapistValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (TherapistValidator) context.getBean("therapistValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, Therapist.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new Therapist());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################FIRSTNAME################################
    @Test
    public void firstNameNull() {
        sample.setFirstName(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("firstName"), equalTo(1));
    }

    @Test
    public void firstNameTooShort() {
        sample.setFirstName("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("firstName"), equalTo(1));
    }

    @Test
    public void firstNameTooLong() {
        sample.setFirstName("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("firstName"), equalTo(1));
    }

    //##########################lastName################################
    @Test
    public void lastNameNull() {
        sample.setLastName(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("lastName"), equalTo(1));
    }

    @Test
    public void lastNameTooShort() {
        sample.setLastName("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("lastName"), equalTo(1));
    }

    @Test
    public void lastNameTooLong() {
        sample.setLastName("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("lastName"), equalTo(1));
    }

    //##########################Gender################################
    @Test
    public void genderNull() {
        sample.setGender(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("gender"), equalTo(1));
    }

    //##########################email################################
    @Test
    public void emailNull() {
        sample.setEmail(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("email"), equalTo(1));
    }

    @Test
    public void emailTooShort() {
        sample.setEmail("1234");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("email"), equalTo(1));
    }

    @Test
    public void emailTooLong() {
        sample.setEmail("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_+" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_+" +
                "x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("email"), equalTo(1));
    }


}
