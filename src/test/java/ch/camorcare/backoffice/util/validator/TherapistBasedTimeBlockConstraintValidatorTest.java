package ch.camorcare.backoffice.util.validator;

import ch.camorcare.backoffice.entities.TherapistBasedTimeBlockConstraint;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.TherapistBasedTimeBlockConstraintValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.TherapistBasedTimeBlockConstraintValidator}
 */
public class TherapistBasedTimeBlockConstraintValidatorTest extends BasicValidatorTest<TherapistBasedTimeBlockConstraint> {

    private static TherapistBasedTimeBlockConstraintValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (TherapistBasedTimeBlockConstraintValidator) context.getBean("therapistBasedTimeBlockConstraintValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<>();
        err = new MapBindingResult(map, TherapistBasedTimeBlockConstraint.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new TherapistBasedTimeBlockConstraint());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }


    //##########################therapist################################
    @Test
    public void therapistNull() {
        sample.setTherapist(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapist"), equalTo(1));
    }

    @Test
    public void therapistIdNull() {
        sample.getTherapist().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapist"), equalTo(1));
    }


    //##########################timeBlock################################
    @Test
    public void timeBlockNull() {
        sample.setTimeBlock(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("timeBlock"), equalTo(1));
    }

    @Test
    public void timeBlockIdNull() {
        sample.getTimeBlock().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("timeBlock"), equalTo(1));
    }




}
