package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.PatientComment;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.PatientCommentValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.PatientCommentValidator}
 */
public class PatientCommentValidatorTest extends BasicValidatorTest<PatientComment> {

    private static PatientCommentValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (PatientCommentValidator) context.getBean("patientCommentValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, PatientComment.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new PatientComment());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################content################################
    @Test
    public void contentNull() {
        sample.setContent(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("content"), equalTo(1));
    }

    @Test
    public void contentTooShort() {
        sample.setContent("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("content"), equalTo(1));
    }

    @Test
    public void contentTooLong() {
        sample.setContent("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("content"), equalTo(1));
    }

    //##########################Patient################################
    @Test
    public void patientNull() {
        sample.setPatient(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("patient"), equalTo(1));
    }

    @Test
    public void addressIdNull() {
        sample.getPatient().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("patient"), equalTo(1));
    }


}
