package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.Patient;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.PatientValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.PatientValidator}
 */
public class PatientValidatorTest extends BasicValidatorTest<Patient> {

    private static PatientValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (PatientValidator) context.getBean("patientValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, Patient.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new Patient());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################FIRSTNAME################################
    @Test
    public void firstNameNull() {
        sample.setFirstName(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("firstName"), equalTo(1));
    }

    @Test
    public void firstNameTooShort() {
        sample.setFirstName("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("firstName"), equalTo(1));
    }

    @Test
    public void firstNameTooLong() {
        sample.setFirstName("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("firstName"), equalTo(1));
    }

    //##########################lastName################################
    @Test
    public void lastNameNull() {
        sample.setLastName(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("lastName"), equalTo(1));
    }

    @Test
    public void lastNameTooShort() {
        sample.setLastName("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("lastName"), equalTo(1));
    }

    @Test
    public void lastNameTooLong() {
        sample.setLastName("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("lastName"), equalTo(1));
    }

    //##########################Gender################################
    @Test
    public void genderNull() {
        sample.setGender(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("gender"), equalTo(1));
    }

    //##########################birthdate################################
    @Test
    public void birthdateNull() {
        sample.setBirthdate(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("birthdate"), equalTo(1));
    }

    //##########################socialSecurityNumber################################
    @Test
    public void socialSecurityNumberNull() {
        sample.setSocialSecurityNumber(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("socialSecurityNumber"), equalTo(1));
    }

    @Test
    public void socialSecurityNumberTooShort() {
        sample.setSocialSecurityNumber("1234");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("socialSecurityNumber"), equalTo(1));
    }

    @Test
    public void socialSecurityNumberTooLong() {
        sample.setSocialSecurityNumber("123456789_123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("socialSecurityNumber"), equalTo(1));
    }

    //##########################phone################################
    @Test
    public void phoneNull() {
        sample.setPhone(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("phone"), equalTo(1));
    }

    @Test
    public void phoneTooShort() {
        sample.setPhone("1234");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("phone"), equalTo(1));
    }

    @Test
    public void phoneTooLong() {
        sample.setPhone("123456789_123456789_123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("phone"), equalTo(1));
    }

    //##########################email################################
    @Test
    public void emailNull() {
        sample.setEmail(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("email"), equalTo(1));
    }

    @Test
    public void emailTooShort() {
        sample.setEmail("1234");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("email"), equalTo(1));
    }

    @Test
    public void emailTooLong() {
        sample.setEmail("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_+" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_+" +
                "x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("email"), equalTo(1));
    }

    //##########################Address################################
    @Test
    public void addressNull() {
        sample.setAddress(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("address"), equalTo(1));
    }

    @Test
    public void addressIdNull() {
        sample.getAddress().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("address"), equalTo(1));
    }

    //##########################religion################################
    @Test
    public void religionNull() {
        sample.setReligion(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("religion"), equalTo(1));
    }

    @Test
    public void religionTooShort() {
        sample.setReligion("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("religion"), equalTo(1));
    }

    @Test
    public void religionTooLong() {
        sample.setReligion("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_+" +
                "x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("religion"), equalTo(1));
    }


}
