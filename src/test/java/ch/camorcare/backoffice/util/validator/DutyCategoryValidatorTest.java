package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.DutyCategory;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.DutyCategoryValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.DutyCategoryValidator}
 */
public class DutyCategoryValidatorTest extends BasicValidatorTest<DutyCategory> {


    private static DutyCategoryValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (DutyCategoryValidator) context.getBean("dutyCategoryValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, DutyCategory.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new DutyCategory());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################NAME################################
    @Test
    public void nameNull() {
        sample.setName(null);
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(1));
    }

    @Test
    public void nameTooShort() {
        sample.setName("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("name"), equalTo(1));
    }

    @Test
    public void nameTooLong() {
        sample.setName("123456789_123456789_123456789_123456789_123456789_x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("name"), equalTo(1));
    }

    //##########################Duration################################
    @Test
    public void durationTooShort() {
        sample.setDuration(0);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("duration"), equalTo(1));
    }


}
