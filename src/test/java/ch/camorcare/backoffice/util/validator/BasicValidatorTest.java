package ch.camorcare.backoffice.util.validator;

import ch.camorcare.backoffice.entities.BasicDTO;
import ch.camorcare.backoffice.util.validator.entities.EntityValidator;
import org.joda.time.DateTime;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.validation.MapBindingResult;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * tests for the basic Validation of DTO's
 */
public abstract class BasicValidatorTest<DtoClass extends BasicDTO> {

    protected EntityValidator<DtoClass> validator;
    protected MapBindingResult err;
    protected DtoClass sample;
    @Rule
    public ExpectedException exception = ExpectedException.none();

    //######################Validation methods##############
    @Test
    public void supportsCorrectClass() throws ValidationException {
        assertThat("validator supports Class", validator.supports(sample.getClass()), equalTo(true));
    }

    @Test
    public void easyValidationOk() throws ValidationException {
        validator.validate(sample);
    }

    @Test
    public void easyValidationValidationException() throws ValidationException {
        sample.setTimeCreated(null);
        sample.setTimeModified(new DateTime());
        sample.setTimeDeleted(new DateTime());
        exception.expect(ValidationException.class);
        validator.validate(sample);
    }

    //######################Validation methods null##############
    @Test
    public void easyValidationNull() throws ValidationException {
        exception.expect(NullPointerException.class);
        validator.validate(null);
    }
    @Test
    public void getErrorsNull() throws ValidationException {
        exception.expect(NullPointerException.class);
        validator.getErrors(null);
    }

    @Test
    public void validateToValidateNull() throws ValidationException {
        exception.expect(NullPointerException.class);
        validator.validate(null, err);
    }

    @Test
    public void validateErrorsNull() throws ValidationException {
        exception.expect(NullPointerException.class);
        validator.validate(sample, null);
    }


    //######################Dates OK##############
    @Test
    public void allOkWithoutTimes() {
        sample.setTimeCreated(null);
        sample.setTimeModified(null);
        sample.setTimeDeleted(null);
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    @Test
    public void allOkWithTimes() {
        sample.setTimeCreated(new DateTime());
        sample.setTimeModified(new DateTime());
        sample.setTimeDeleted(new DateTime());
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //######################Dates not OK##############
    @Test
    public void createIsMissing() {
        sample.setTimeCreated(null);
        sample.setTimeModified(new DateTime());
        sample.setTimeDeleted(new DateTime());
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("basic.timeModified"), equalTo(1));
        assertThat("errors", err.getFieldErrorCount("basic.timeDeleted"), equalTo(1));
    }

    @Test
    public void modifyIsOldest() throws InterruptedException {
        sample.setTimeModified(new DateTime());
        sample.setTimeCreated(new DateTime(sample.getTimeModified().plus(100)));
        sample.setTimeDeleted(new DateTime(sample.getTimeModified().plus(100)));
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("basic.timeModified"), equalTo(1));
    }

    @Test
    public void modifyIsMissing() {
        sample.setTimeCreated(new DateTime());
        sample.setTimeModified(null);
        sample.setTimeDeleted(new DateTime());
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("basic.timeDeleted"), equalTo(1));
    }

    @Test
    public void deleteIsOldest() throws InterruptedException {
        sample.setTimeDeleted(new DateTime());
        sample.setTimeCreated(new DateTime(sample.getTimeDeleted().plus(100)));
        sample.setTimeModified(new DateTime(sample.getTimeDeleted().plus(100)));
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("basic.timeDeleted"), equalTo(2));
    }

}
