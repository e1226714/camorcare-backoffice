package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.TherapyTypeValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.TherapyTypeValidator}
 */
public class TherapyTypeValidatorTest extends BasicValidatorTest<TherapyType> {

    private static TherapyTypeValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (TherapyTypeValidator) context.getBean("therapyTypeValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, TherapyType.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new TherapyType());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################NAME################################
    @Test
    public void nameNull() {
        sample.setName(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("name"), equalTo(1));
    }

    @Test
    public void nameTooShort() {
        sample.setName("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("name"), equalTo(1));
    }

    @Test
    public void nameTooLong() {
        sample.setName("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("name"), equalTo(1));
    }

    //##########################individualTherapyDuration################################
    @Test
    public void individualTherapyDurationTooShort() {
        sample.setSuitableForIndividuals(true);
        sample.setIndividualTherapyDuration(0);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("individualTherapyDuration"), equalTo(1));
    }

    //##########################groupTherapyDuration################################
    @Test
    public void groupTherapyDurationTooShort() {
        sample.setSuitableForGroups(true);
        sample.setGroupTherapyDuration(0);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("groupTherapyDuration"), equalTo(1));
    }



    //##########################minGroupSize################################
    @Test
    public void minGroupSizeTooLow() {
        sample.setSuitableForGroups(true);
        sample.setMinGroupSize(0);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("minGroupSize"), equalTo(1));
    }

    //##########################maxGroupSize################################
    @Test
    public void maxGroupSizeTooLow() {
        sample.setSuitableForGroups(true);
        sample.setMinGroupSize(0);
        sample.setMaxGroupSize(0);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("maxGroupSize"), equalTo(1));
    }

    @Test
    public void maxGroupSizeLowerThanMinGroupSize() {
        sample.setSuitableForGroups(true);
        sample.setMinGroupSize(10);
        sample.setMaxGroupSize(5);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("maxGroupSize"), equalTo(1));
    }



}
