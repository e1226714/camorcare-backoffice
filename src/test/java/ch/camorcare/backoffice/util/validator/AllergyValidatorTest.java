package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.Allergy;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.AllergyValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.AllergyValidator}
 */
public class AllergyValidatorTest extends BasicValidatorTest<Allergy> {

    private static AllergyValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (AllergyValidator) context.getBean("allergyValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, Allergy.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new Allergy());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################NAME################################
    @Test
    public void nameNull() {
        sample.setName(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("name"), equalTo(1));
    }

    @Test
    public void nameTooShort() {
        sample.setName("");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("name"), equalTo(1));
    }

    @Test
    public void nameTooLong() {
        sample.setName("123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_" +
                "123456789_123456789_123456789_123456789_123456789_" +
                "x");
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("name"), equalTo(1));
    }
}
