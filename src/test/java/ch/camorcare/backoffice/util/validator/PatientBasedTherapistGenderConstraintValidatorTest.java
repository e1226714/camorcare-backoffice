package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.PatientBasedTherapistGenderConstraint;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.PatientBasedTherapistGenderConstraintValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.PatientBasedTherapistGenderConstraintValidator}
 */
public class PatientBasedTherapistGenderConstraintValidatorTest extends BasicValidatorTest<PatientBasedTherapistGenderConstraint> {

    private static PatientBasedTherapistGenderConstraintValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (PatientBasedTherapistGenderConstraintValidator) context.getBean("patientBasedTherapistGenderConstraintValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<>();
        err = new MapBindingResult(map, PatientBasedTherapistGenderConstraint.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new PatientBasedTherapistGenderConstraint());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }


    //##########################Patient################################
    @Test
    public void patientNull() {
        sample.setPatient(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("patient"), equalTo(1));
    }

    @Test
    public void patientIdNull() {
        sample.getPatient().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("patient"), equalTo(1));
    }

    //##########################therapyType################################
    @Test
    public void therapyTypeNull() {
        sample.setTherapyType(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyType"), equalTo(1));
    }

    @Test
    public void therapyTypeIdNull() {
        sample.getTherapyType().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyType"), equalTo(1));
    }


    //##########################gender################################
    @Test
    public void genderNull() {
        sample.setGender(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("gender"), equalTo(1));
    }


}
