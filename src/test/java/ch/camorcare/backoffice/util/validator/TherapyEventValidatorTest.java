package ch.camorcare.backoffice.util.validator;


import ch.camorcare.backoffice.entities.TherapyEvent;
import ch.camorcare.backoffice.persistence.generator.SampleDTOGenerator;
import ch.camorcare.backoffice.util.validator.entities.TherapyEventValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Tests {@link ch.camorcare.backoffice.util.validator.entities.TherapyEventValidator}
 */
public class TherapyEventValidatorTest extends BasicValidatorTest<TherapyEvent> {

    private static TherapyEventValidator realValidator;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
        realValidator = (TherapyEventValidator) context.getBean("therapyEventValidator");
    }

    @Before
    public void setUpBefore() throws Exception {
        validator = realValidator;
        Map<String, String> map = new HashMap<String, String>();
        err = new MapBindingResult(map, TherapyEvent.class.getName());
        sample = SampleDTOGenerator.createValidDTO(new TherapyEvent());
    }

    //##########################ALL OK################################
    @Test
    public void allOk() throws ValidationException {
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    //##########################therapyPlan################################
    @Test
    public void therapyPlanNull() {
        sample.setTherapyPlan(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyPlan"), equalTo(1));
    }

    @Test
    public void therapyPlanIdNull() {
        sample.getTherapyPlan().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyPlan"), equalTo(1));
    }

    //##########################therapyType################################
    @Test
    public void therapyTypeNull() {
        sample.setTherapyType(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyType"), equalTo(1));
    }

    @Test
    public void therapyTypeIdNull() {
        sample.getTherapyType().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyType"), equalTo(1));
    }

    //##########################therapyMode################################
    @Test
    public void therapyModeNull() {
        sample.setTherapyMode(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyMode"), equalTo(1));
    }



    //##########################therapist################################
    @Test
    public void therapistNull() {
        sample.setTherapist(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapist"), equalTo(1));
    }

    @Test
    public void therapistIdNull() {
        sample.getTherapist().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapist"), equalTo(1));
    }

    @Test
    public void therapistNullWhenNoTimeBlockRoom() {
        sample.setTherapyTimeBlock(null);
        sample.setTherapyRoom(null);
        sample.setTherapist(null);
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }




    //##########################therapyRoom################################
    @Test
    public void therapyRoomNull() {
        sample.setTherapyRoom(null);
        validator.validate(sample, err);
        assertThat("errors",err.getErrorCount(), equalTo(0));
    }

    @Test
    public void therapyRoomIdNull() {
        sample.getTherapyRoom().setId(null);
        validator.validate(sample, err);
        assertThat("errors",err.getFieldErrorCount("therapyRoom"), equalTo(1));
    }

    @Test
    public void therapyTimeBlockAndRoomNull() {
        sample.setTherapyTimeBlock(null);
        sample.setTherapyRoom(null);
        validator.validate(sample, err);
        assertThat("errors",err.getErrorCount(), equalTo(0));
    }



    //##########################therapyTimeBlock################################
    @Test
    public void therapyTimeBlockNull() {
        sample.setTherapyTimeBlock(null);
        validator.validate(sample, err);
        assertThat("errors", err.getErrorCount(), equalTo(0));
    }

    @Test
    public void therapyTimeBlockIdNull() {
        sample.getTherapyTimeBlock().setId(null);
        validator.validate(sample, err);
        assertThat("errors", err.getFieldErrorCount("therapyTimeBlock"), equalTo(1));
    }

    //##########################therapyTime################################
    @Test
    public void therapyTimeNull() {
        sample.setTherapyTime(null);
        validator.validate(sample, err);
        assertThat("errors",  err.getFieldErrorCount("therapyTime"), equalTo(1));
    }

    @Test
    public void therapyTimeNullWhenNoRoomAndTimeBlock() {
        sample.setTherapyTimeBlock(null);
        sample.setTherapyRoom(null);
        sample.setTherapyTime(null);
        validator.validate(sample, err);
        assertThat("errors",  err.getErrorCount(), equalTo(0));
    }


}
