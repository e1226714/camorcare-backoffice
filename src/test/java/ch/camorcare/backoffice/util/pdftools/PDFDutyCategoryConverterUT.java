package ch.camorcare.backoffice.util.pdftools;

import ch.camorcare.backoffice.entities.DutyCategory;
import ch.camorcare.backoffice.entities.type.Day;
import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;

/**
 * Tests {@link PdfDutyCategoryConverter}.
 */
public class PDFDutyCategoryConverterUT {

    private List<Day> monWedFri;
    private List<Day> tuesThurs;
    private List<Day> fri;

    private List<DutyCategory> dutyCategories;
    private DutyCategory dutyMonWedFri;
    private DutyCategory dutyTuesThurs;
    private DutyCategory dutyFri;

    private DateTime monday;
    private DateTime tuesday;
    private DateTime wednesday;
    private DateTime thursday;
    private DateTime friday;

    private String mondayTitle;
    private String tuesdayTitle;
    private String wednesdayTitle;
    private String thursdayTitle;
    private String fridayTitle;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void before() {
        // Set up day lists
        monWedFri = new ArrayList<>();
        monWedFri.add(Day.MONDAY);
        monWedFri.add(Day.WEDNESDAY);
        monWedFri.add(Day.FRIDAY);

        tuesThurs = new ArrayList<>();
        tuesThurs.add(Day.TUESDAY);
        tuesThurs.add(Day.THURSDAY);

        fri = new ArrayList<>();
        fri.add(Day.FRIDAY);

        // Set up duty categories
        dutyCategories = new ArrayList<>();

        dutyMonWedFri = new DutyCategory();
        dutyMonWedFri.setId(1);
        dutyMonWedFri.setName("Mow the lawn");
        dutyMonWedFri.setWeekdays(monWedFri);

        dutyTuesThurs = new DutyCategory();
        dutyTuesThurs.setId(2);
        dutyTuesThurs.setName("Dusting");
        dutyTuesThurs.setWeekdays(tuesThurs);

        dutyFri = new DutyCategory();
        dutyFri.setId(3);
        dutyFri.setName("Weeding");
        dutyFri.setWeekdays(fri);

        dutyCategories.add(dutyMonWedFri);
        dutyCategories.add(dutyTuesThurs);
        dutyCategories.add(dutyFri);

        // Set up dates
        monday = new DateTime(2015, 6, 8, 9, 0);
        tuesday = new DateTime(2015, 6, 9, 9, 0);
        wednesday = new DateTime(2015, 6, 10, 9, 0);
        thursday = new DateTime(2015, 6, 11, 9, 0);
        friday = new DateTime(2015, 6, 12, 9, 0);

        // Set up titles for each day
        mondayTitle = "Mo, 8.6.";
        tuesdayTitle = "Di, 9.6.";
        wednesdayTitle = "Mi, 10.6.";
        thursdayTitle = "Do, 11.6.";
        fridayTitle = "Fr, 12.6.";
    }

    @Test
    public void convertWith_ValidDutyCategoryList_Monday() throws PdfException {
        List<List<String>> found = PdfDutyCategoryConverter.convert(dutyCategories, monday, true);
        assertThat("outer list length is five", found.size(), equalTo(5));

        assertThat("monday list length is two", found.get(0).size(), equalTo(2));
        assertThat("tuesday list length is two", found.get(1).size(), equalTo(2));
        assertThat("wednesday list length is two", found.get(2).size(), equalTo(2));
        assertThat("thursday list length is two", found.get(3).size(), equalTo(2));
        assertThat("friday list length is three", found.get(4).size(), equalTo(3));

        assertThat("monday title is 'Mo, 8.6.'", found.get(0).get(0), equalToIgnoringCase(mondayTitle));
        assertThat("tuesday title is 'Di, 9.6.'", found.get(1).get(0), equalToIgnoringCase(tuesdayTitle));
        assertThat("wednesday title is 'Mi, 10.6.'", found.get(2).get(0), equalToIgnoringCase(wednesdayTitle));
        assertThat("thursday title is 'Do, 11.6.'", found.get(3).get(0), equalToIgnoringCase(thursdayTitle));
        assertThat("friday title is 'Fr, 12.6.'", found.get(4).get(0), equalToIgnoringCase(fridayTitle));

        assertThat("monday is 'Mow the lawn'", found.get(0).get(1), equalToIgnoringCase(dutyMonWedFri.getName()));
        assertThat("tuesday is 'Dusting'", found.get(1).get(1), equalToIgnoringCase(dutyTuesThurs.getName()));
        assertThat("wednesday is 'Mow the lawn'", found.get(2).get(1), equalToIgnoringCase(dutyMonWedFri.getName()));
        assertThat("thursday is 'Dusting'", found.get(3).get(1), equalToIgnoringCase(dutyTuesThurs.getName()));
        assertThat("friday is 'Mow the lawn'", found.get(4).get(1), equalToIgnoringCase(dutyMonWedFri.getName()));
        assertThat("friday is 'Weeding'", found.get(4).get(2), equalToIgnoringCase(dutyFri.getName()));
    }

    @Test
    public void convertWith_ValidDutyCategoryList_Tuesday() throws PdfException {
        List<List<String>> found = PdfDutyCategoryConverter.convert(dutyCategories, tuesday, true);
        assertThat("outer list length is four", found.size(), equalTo(4));

        assertThat("tuesday list length is two", found.get(0).size(), equalTo(2));
        assertThat("wednesday list length is two", found.get(1).size(), equalTo(2));
        assertThat("thursday list length is two", found.get(2).size(), equalTo(2));
        assertThat("friday list length is three", found.get(3).size(), equalTo(3));

        assertThat("tuesday title is 'Di, 9.6.'", found.get(0).get(0), equalToIgnoringCase(tuesdayTitle));
        assertThat("wednesday title is 'Mi, 10.6.'", found.get(1).get(0), equalToIgnoringCase(wednesdayTitle));
        assertThat("thursday title is 'Do, 11.6.'", found.get(2).get(0), equalToIgnoringCase(thursdayTitle));
        assertThat("friday title is 'Fr, 12.6.'", found.get(3).get(0), equalToIgnoringCase(fridayTitle));

        assertThat("tuesday is 'Dusting'", found.get(0).get(1), equalToIgnoringCase(dutyTuesThurs.getName()));
        assertThat("wednesday is 'Mow the lawn'", found.get(1).get(1), equalToIgnoringCase(dutyMonWedFri.getName()));
        assertThat("thursday is 'Dusting'", found.get(2).get(1), equalToIgnoringCase(dutyTuesThurs.getName()));
        assertThat("friday is 'Mow the lawn'", found.get(3).get(1), equalToIgnoringCase(dutyMonWedFri.getName()));
        assertThat("friday is 'Weeding'", found.get(3).get(2), equalToIgnoringCase(dutyFri.getName()));
    }

    @Test
    public void convertWith_ValidDutyCategoryList_Wednesday() throws PdfException {
        List<List<String>> found = PdfDutyCategoryConverter.convert(dutyCategories, wednesday, true);
        assertThat("outer list length is three", found.size(), equalTo(3));

        assertThat("wednesday list length is two", found.get(0).size(), equalTo(2));
        assertThat("thursday list length is two", found.get(1).size(), equalTo(2));
        assertThat("friday list length is three", found.get(2).size(), equalTo(3));

        assertThat("wednesday title is 'Mi, 10.6.'", found.get(0).get(0), equalToIgnoringCase(wednesdayTitle));
        assertThat("thursday title is 'Do, 11.6.'", found.get(1).get(0), equalToIgnoringCase(thursdayTitle));
        assertThat("friday title is 'Fr, 12.6.'", found.get(2).get(0), equalToIgnoringCase(fridayTitle));

        assertThat("wednesday is 'Mow the lawn'", found.get(0).get(1), equalToIgnoringCase(dutyMonWedFri.getName()));
        assertThat("thursday is 'Dusting'", found.get(1).get(1), equalToIgnoringCase(dutyTuesThurs.getName()));
        assertThat("friday is 'Mow the lawn'", found.get(2).get(1), equalToIgnoringCase(dutyMonWedFri.getName()));
        assertThat("friday is 'Weeding'", found.get(2).get(2), equalToIgnoringCase(dutyFri.getName()));
    }

    @Test
    public void convertWith_ValidDutyCategoryList_Thursday() throws PdfException {
        List<List<String>> found = PdfDutyCategoryConverter.convert(dutyCategories, thursday, true);
        assertThat("outer list length is three", found.size(), equalTo(2));

        assertThat("thursday list length is two", found.get(0).size(), equalTo(2));
        assertThat("friday list length is three", found.get(1).size(), equalTo(3));

        assertThat("thursday title is 'Do, 11.6.'", found.get(0).get(0), equalToIgnoringCase(thursdayTitle));
        assertThat("friday title is 'Fr, 12.6.'", found.get(1).get(0), equalToIgnoringCase(fridayTitle));

        assertThat("thursday is 'Dusting'", found.get(0).get(1), equalToIgnoringCase(dutyTuesThurs.getName()));
        assertThat("friday is 'Mow the lawn'", found.get(1).get(1), equalToIgnoringCase(dutyMonWedFri.getName()));
        assertThat("friday is 'Weeding'", found.get(1).get(2), equalToIgnoringCase(dutyFri.getName()));
    }

    @Test
    public void convertWith_ValidDutyCategoryList_Friday() throws PdfException {
        List<List<String>> found = PdfDutyCategoryConverter.convert(dutyCategories, friday, true);
        assertThat("outer list length is three", found.size(), equalTo(1));

        assertThat("friday list length is three", found.get(0).size(), equalTo(3));

        assertThat("friday title is 'Fr, 12.6.'", found.get(0).get(0), equalToIgnoringCase(fridayTitle));

        assertThat("friday is 'Mow the lawn'", found.get(0).get(1), equalToIgnoringCase(dutyMonWedFri.getName()));
        assertThat("friday is 'Weeding'", found.get(0).get(2), equalToIgnoringCase(dutyFri.getName()));
    }

    @Test
    public void convertWith_EmptyDutyCategoryList_Monday() throws PdfException {
        exception.expect(PdfException.class);
        List<DutyCategory> invalidList = new ArrayList<>();
        PdfDutyCategoryConverter.convert(invalidList, monday, true);
    }

    @Test
    public void convertWith_NullDutyCategoryList_Monday() throws PdfException {
        exception.expect(PdfException.class);
        PdfDutyCategoryConverter.convert(null, monday, true);
    }

    @Test
    public void convertWith_ValidDutyCategoryList_NullDateTime() throws PdfException {
        exception.expect(PdfException.class);
        PdfDutyCategoryConverter.convert(dutyCategories, null, true);
    }

    @Test
    public void convertWith_NullParameters() throws PdfException {
        exception.expect(PdfException.class);
        PdfDutyCategoryConverter.convert(null, null, true);
    }

    @Test
    public void convertWith_ValidDutyCategoryList_Saturday() throws PdfException {
        exception.expect(PdfException.class);
        PdfDutyCategoryConverter.convert(dutyCategories, new DateTime(2015, 6, 13, 9, 0), true);
    }

    @Test
    public void convertWith_ValidDutyCategoryList_Sunday() throws PdfException {
        exception.expect(PdfException.class);
        PdfDutyCategoryConverter.convert(dutyCategories, new DateTime(2015, 6, 14, 9, 0), true);
    }

}
