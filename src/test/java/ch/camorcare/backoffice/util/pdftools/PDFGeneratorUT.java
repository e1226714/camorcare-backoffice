package ch.camorcare.backoffice.util.pdftools;

import ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate;
import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import org.joda.time.DateTime;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class PDFGeneratorUT {

    @Test
    public void GeneratePDFPlan_shouldCreatePDF() throws PdfException {

        ArrayList<PdfPlanDate> al = new ArrayList<>();
        al.add(new PdfPlanDate(0, 8F, 10F, "Doppelevent Mo"));
        al.add(new PdfPlanDate(1, 13F, 13.75F, "Einfachevent Di"));
        al.add(new PdfPlanDate(4, 17F, 18F, "Einfachevent Fr"));

        String path = System.getProperty("user.home") + "/PDFPlan.PDF";

        PdfPlanGenerator.generatePDF("Neuerraum, 1.2.2015 - 5.2.2015", al, path);

        assertTrue(new File(path).isFile());
        new File(path).delete();
    }

    @Test(expected = FileNotFoundException.class)
    public void GeneratePDFPlan_shouldThrowFileNotFoundException() throws PdfException {

        ArrayList<PdfPlanDate> al = new ArrayList<>();
        al.add(new PdfPlanDate(0, 8F, 10F, "Doppelevent Mo"));

        String path = "//PDFPlan.PDF";

        PdfPlanGenerator.generatePDF("Neuerraum, 1.2.2015 - 5.2.2015", al, path);

    }

    @Test(expected = NullPointerException.class)
    public void GeneratePDFPlan_shouldThrowNullPointerException() throws PdfException {

        ArrayList<PdfPlanDate> al = new ArrayList<>();
        al.add(new PdfPlanDate(0, 8F, 10F, "Doppelevent Mo"));

        String path = "//PDFPlan.PDF";

        PdfPlanGenerator.generatePDF(null, al, path);

    }

    @Test
    public void GeneratePDFBill_shouldCreatePDF() throws PdfException {

        String addressLine1 = "Johannes Dostal";
        String addressLine2 = "Reiterberggasse 10";
        String addressLine3 = "1230 Wien";
        String date = "16.01.2015";
        double stayPrice = 208.45;
        double therapyPrice = 527.67;

        String path = System.getProperty("user.home") + "/PDFRechnung.PDF";

        PdfBillGenerator.generatePDF(addressLine1, addressLine2, addressLine3, date, stayPrice, therapyPrice, path);

        assertTrue(new File(path).isFile());
        new File(path).delete();

    }

    @Test(expected = NullPointerException.class)
    public void GeneratePDFBill_shouldThrowNullPointerException() throws PdfException {

        String addressLine1 = "Johannes Dostal";
        String addressLine2 = "Reiterberggasse 10";
        String addressLine3 = "1230 Wien";
        String date = "16.01.2015";
        double stayPrice = 208.45;
        double therapyPrice = 527.67;

        String path = System.getProperty("user.home") + "/PDFRechnung.PDF";

        PdfBillGenerator.generatePDF(addressLine1, null, addressLine3, date, stayPrice, therapyPrice, path);

        assertTrue(new File(path).isFile());
        new File(path).delete();

    }


    @Test
    public void GeneratePDFHouseServiceGenerator_shouldCreatePDF() throws PdfException {

        DateTime dt = new DateTime("2012-10-15T07:22:05Z");

        List<String> sal = new ArrayList<>();
        sal.add("Hausdienst 1");
        sal.add("Hausdienst 2");

        List<String> sal2 = new ArrayList<>();
        sal2.add("Hausdienst 1");
        sal2.add("Hausdienst 2");
        sal2.add("Hausdienst 3");

        List<List<String>> sala = new ArrayList<>();
        sala.add(sal);
        sala.add(sal);
        sala.add(sal);
        sala.add(sal2);
        sala.add(sal2);

        String path = System.getProperty("user.home") + "/PDFHausdienste.PDF";

        PdfHouseServiceGenerator.generatePDF(dt, sala, path);

        assertTrue(new File(path).isFile());
        new File(path).delete();

    }

    @Test(expected = NullPointerException.class)
    public void GeneratePDFHouseServiceGenerator_shouldThrowNullPointerException() throws PdfException {

        DateTime dt = new DateTime("2012-10-15T07:22:05Z");

        List<String> sal = new ArrayList<>();
        sal.add("Hausdienst 1");
        sal.add("Hausdienst 2");

        List<String> sal2 = new ArrayList<>();
        sal2.add("Hausdienst 1");
        sal2.add("Hausdienst 2");
        sal2.add("Hausdienst 3");

        List<List<String>> sala = new ArrayList<>();
        sala.add(sal);
        sala.add(sal);
        sala.add(sal);
        sala.add(sal2);
        sala.add(sal2);

        String path = System.getProperty("user.home") + "/PDFHausdienste.PDF";

        PdfHouseServiceGenerator.generatePDF(null, sala, path);

        assertTrue(new File(path).isFile());
        new File(path).delete();

    }

    @Test(expected = FileNotFoundException.class)
    public void GeneratePDFHouseServiceGenerator_shouldThrowFileNotFoundException() throws PdfException {

        DateTime dt = new DateTime("2012-10-15T07:22:05Z");

        List<String> sal = new ArrayList<>();
        sal.add("Hausdienst 1");
        sal.add("Hausdienst 2");

        List<String> sal2 = new ArrayList<>();
        sal2.add("Hausdienst 1");
        sal2.add("Hausdienst 2");
        sal2.add("Hausdienst 3");

        List<List<String>> sala = new ArrayList<>();
        sala.add(sal);
        sala.add(sal);
        sala.add(sal);
        sala.add(sal2);
        sala.add(sal2);

        String path = "//PDFHausdienste.PDF";

        PdfHouseServiceGenerator.generatePDF(dt, sala, path);

        assertTrue(new File(path).isFile());
        new File(path).delete();

    }

    @Test(expected = IllegalArgumentException.class)
    public void GeneratePDFHouseServiceGenerator_shouldThrowIllegalArgumentException() throws PdfException {

        DateTime dt = new DateTime("2012-10-15T07:22:05Z");

        List<String> sal = new ArrayList<>();
        sal.add("Hausdienst 1");
        sal.add("Hausdienst 2");

        List<String> sal2 = new ArrayList<>();
        sal2.add("Hausdienst 1");
        sal2.add("Hausdienst 2");
        sal2.add("Hausdienst 3");

        List<List<String>> sala = new ArrayList<>();
        sala.add(sal);
        sala.add(sal2);

        String path = System.getProperty("user.home") + "/PDFHausdienste.PDF";

        PdfHouseServiceGenerator.generatePDF(dt, sala, path);

        assertTrue(new File(path).isFile());
        new File(path).delete();

    }

    @Test
    public void GenerateAndPrintPDFPlan_shouldOpenPrintDialog() throws PdfException {

        ArrayList<PdfPlanDate> al = new ArrayList<>();
        al.add(new PdfPlanDate(0, 8F, 10F, "Doppelevent Mo"));
        al.add(new PdfPlanDate(1, 13F, 13.75F, "Einfachevent Di"));
        al.add(new PdfPlanDate(4, 17F, 18F, "Einfachevent Fr"));

        String path = System.getProperty("user.home") + "/PDFPlan.PDF";

        PdfPlanGenerator.generatePDF("Neuerraum, 1.2.2015 - 5.2.2015", al, path);

        assertTrue(new File(path).isFile());

        PdfPrinter.printPDF(path);

        new File(path).delete();
    }

    @Test
    public void GenerateAndPrintPDFHouseServiceGenerator_shouldOpenPrintDialog() throws PdfException {

        DateTime dt = new DateTime("2012-10-15T07:22:05Z");

        List<String> sal = new ArrayList<>();
        sal.add("Hausdienst 1");
        sal.add("Hausdienst 2");

        List<String> sal2 = new ArrayList<>();
        sal2.add("Hausdienst 1");
        sal2.add("Hausdienst 2");
        sal2.add("Hausdienst 3");

        List<List<String>> sala = new ArrayList<>();
        sala.add(sal);
        sala.add(sal);
        sala.add(sal);
        sala.add(sal2);
        sala.add(sal2);

        String path = System.getProperty("user.home") + "/PDFHausdienste.PDF";

        PdfHouseServiceGenerator.generatePDF(dt, sala, path);

        assertTrue(new File(path).isFile());

        PdfPrinter.printPDF(path);
        new File(path).delete();

    }
}
