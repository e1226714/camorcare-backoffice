package ch.camorcare.backoffice.util.pdftools;

import ch.camorcare.backoffice.entities.Therapist;
import ch.camorcare.backoffice.entities.TherapyEvent;
import ch.camorcare.backoffice.entities.TherapyRoom;
import ch.camorcare.backoffice.entities.TherapyType;
import ch.camorcare.backoffice.entities.type.TherapyMode;
import ch.camorcare.backoffice.util.pdftools.entity.PdfPlanDate;
import ch.camorcare.backoffice.util.pdftools.exception.PdfException;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Tests for {@link PdfPlanDateConverter}.
 */
public class PDFPlanDateConverterUT {
    // Valid TherapyEvent objects
    private TherapyEvent firstValidTherapyEvent;
    private TherapyEvent secondValidTherapyEvent;
    // Invalid TherapyEvent objects
    private TherapyEvent firstInvalidTherapyEvent;
    private TherapyEvent secondInvalidTherapyEvent;
    private List<TherapyEvent> validList = new ArrayList<>();
    private List<TherapyEvent> invalidList = new ArrayList<>();
    // Expected titles for each PdfPlanDate
    private String firstValidPatientTitle;
    private String firstValidTherapistTitle;
    private String firstValidTherapyRoomTitle;
    private String secondValidPatientTitle;
    private String secondValidTherapistTitle;
    private String secondValidTherapyRoomTitle;
    // Variables needed for valid TherapyEvent objects
    TherapyRoom firstTherapyRoom;
    TherapyRoom secondTherapyRoom;
    Therapist firstTherapist;
    Therapist secondTherapist;
    TherapyType firstTherapyType;
    TherapyType secondTherapyType;
    DateTime firstDateTime;
    DateTime secondDateTime;
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void before() {
        // Set up valid objects
        firstValidTherapyEvent = new TherapyEvent();
        secondValidTherapyEvent = new TherapyEvent();

        firstValidTherapyEvent.setId(1);
        secondValidTherapyEvent.setId(2);

        // firstTherapyRoom
        firstTherapyRoom = new TherapyRoom();
        firstTherapyRoom.setId(1);
        firstTherapyRoom.setName("Room 1");

        // secondTherapyRoom
        secondTherapyRoom = new TherapyRoom();
        secondTherapyRoom.setId(2);
        secondTherapyRoom.setName("Room 2");

        // firstTherapist
        firstTherapist = new Therapist();
        firstTherapist.setId(1);
        firstTherapist.setLastName("Smith");

        // secondTherapist
        secondTherapist = new Therapist();
        secondTherapist.setId(2);
        secondTherapist.setLastName("Doe");

        // firstTherapyType
        firstTherapyType = new TherapyType();
        firstTherapyType.setId(1);
        firstTherapyType.setName("Yoga");

        // secondTherapyType
        secondTherapyType = new TherapyType();
        secondTherapyType.setId(2);
        secondTherapyType.setName("Massage");

        // firstDateTime
        firstDateTime = new DateTime(2015, 6, 8, 10, 0);
        // secondDateTime
        secondDateTime = new DateTime(2015, 6, 9, 13, 0);

        firstValidTherapyEvent.setTherapyRoom(firstTherapyRoom);
        firstValidTherapyEvent.setTherapist(firstTherapist);
        firstValidTherapyEvent.setTherapyType(firstTherapyType);
        firstValidTherapyEvent.setTherapyTime(firstDateTime);
        firstValidTherapyEvent.setTherapyMode(TherapyMode.GROUP);

        // secondValidTherapyEvent set methods
        secondValidTherapyEvent.setTherapyRoom(secondTherapyRoom);
        secondValidTherapyEvent.setTherapist(secondTherapist);
        secondValidTherapyEvent.setTherapyType(secondTherapyType);
        secondValidTherapyEvent.setTherapyTime(secondDateTime);
        secondValidTherapyEvent.setTherapyMode(TherapyMode.SINGLE);

        validList.add(firstValidTherapyEvent);
        validList.add(secondValidTherapyEvent);

        // Set up invalid objects
        firstInvalidTherapyEvent = new TherapyEvent();
        secondInvalidTherapyEvent = new TherapyEvent();

        firstInvalidTherapyEvent.setId(3);
        secondInvalidTherapyEvent.setId(4);

        invalidList.add(firstInvalidTherapyEvent);
        invalidList.add(secondInvalidTherapyEvent);

        // PdfPlanDate titles
        firstValidPatientTitle = firstTherapyType.getName() + ", " + firstTherapist.getLastName() + ", " + firstTherapyRoom.getName();
        firstValidTherapistTitle = firstTherapyType.getName() + ", " + firstValidTherapyEvent.getTherapyMode().name() + ", " + firstTherapyRoom.getName();
        firstValidTherapyRoomTitle = firstTherapyType.getName() + ", " + firstValidTherapyEvent.getTherapyMode().name() + ", " + firstTherapist.getLastName();

        secondValidPatientTitle = secondTherapyType.getName() + ", " + secondTherapist.getLastName() + ", " + secondTherapyRoom.getName();
        secondValidTherapistTitle = secondTherapyType.getName() + ", " + secondValidTherapyEvent.getTherapyMode().name() + ", " + secondTherapyRoom.getName();
        secondValidTherapyRoomTitle = secondTherapyType.getName() + ", " + secondValidTherapyEvent.getTherapyMode().name() + ", " + secondTherapist.getLastName();
    }

    // ######################### collection convert #################################

    @Test
    public void convertWith_ValidTherapyEventListAnd_ValidPatientCriteria() throws PdfException {
        List<PdfPlanDate> converted = PdfPlanDateConverter.convert(validList, 1);
        assertThat("converted entries size", converted.size(), greaterThanOrEqualTo(2));
        assertThat("first PdfPlanDate title is correct", converted.get(0).getDateName(), equalToIgnoringCase(firstValidPatientTitle));
        assertThat("second PdfPlanDate title is correct", converted.get(1).getDateName(), equalToIgnoringCase(secondValidPatientTitle));
    }

    @Test
    public void convertWith_ValidTherapyEventListAnd_ValidTherapistCriteria() throws PdfException {
        List<PdfPlanDate> converted = PdfPlanDateConverter.convert(validList, 2);
        assertThat("converted entries size", converted.size(), greaterThanOrEqualTo(2));
        assertThat("first PdfPlanDate title is correct", converted.get(0).getDateName(), equalToIgnoringCase(firstValidTherapistTitle));
        assertThat("second PdfPlanDate title is correct", converted.get(1).getDateName(), equalToIgnoringCase(secondValidTherapistTitle));
    }

    @Test
    public void convertWith_ValidTherapyEventListAnd_ValidTherapyRoomCriteria() throws PdfException {
        List<PdfPlanDate> converted = PdfPlanDateConverter.convert(validList, 3);
        assertThat("converted entries size", converted.size(), greaterThanOrEqualTo(2));
        assertThat("first PdfPlanDate title is correct", converted.get(0).getDateName(), equalToIgnoringCase(firstValidTherapyRoomTitle));
        assertThat("second PdfPlanDate title is correct", converted.get(1).getDateName(), equalToIgnoringCase(secondValidTherapyRoomTitle));
    }

    @Test
    public void convertWith_ValidTherapyEventListAnd_InvalidCriteria() throws PdfException {
        exception.expect(PdfException.class);
        PdfPlanDateConverter.convert(validList, 4);
    }

    @Test
    public void convertWith_NullTherapyEventListAnd_ValidCriteria() throws PdfException {
        exception.expect(PdfException.class);
        List<TherapyEvent> nullList = null;
        PdfPlanDateConverter.convert(nullList, 1);
    }

    @Test
    public void convertWith_EmptyTherapyEventListAnd_ValidCriteria() throws PdfException {
        exception.expect(PdfException.class);
        List<TherapyEvent> emptyList = new ArrayList<>();
        PdfPlanDateConverter.convert(emptyList, 1);
    }

    @Test
    public void convertWith_InvalidTherapyEventListAnd_ValidCriteria() throws PdfException {
        exception.expect(PdfException.class);
        PdfPlanDateConverter.convert(invalidList, 1);
    }

    // ######################### simple convert ######################################

    @Test
    public void convertWith_ValidTherapyEventAnd_ValidPatientCriteria() throws PdfException {
        float checkFrom = 10.00f;
        float checkTo = 10.75f;
        PdfPlanDate converted = PdfPlanDateConverter.convert(firstValidTherapyEvent, 1);
        assertThat("PdfPlanDate title is correct", converted.getDateName(), equalToIgnoringCase(firstValidPatientTitle));
        assertThat("Start time is 10;00 (expressed as 10.00)", converted.getTimeFrom(), equalTo(checkFrom));
        assertThat("End time is 10:45 (expressed as 10.75)", converted.getTimeTo(), equalTo(checkTo));
    }

    @Test
    public void convertWith_ValidTherapyEventAnd_ValidTherapistCriteria() throws PdfException {
        float checkFrom = 13.00f;
        float checkTo = 13.75f;
        PdfPlanDate converted = PdfPlanDateConverter.convert(secondValidTherapyEvent, 2);
        assertThat("PdfPlanDate title is correct", converted.getDateName(), equalToIgnoringCase(secondValidTherapistTitle));
        assertThat("Start time is 13:00 (expressed as 13.00)", converted.getTimeFrom(), equalTo(checkFrom));
        assertThat("End time is 13:45 (expressed as 13:45)", converted.getTimeTo(), equalTo(checkTo));
    }

    @Test
    public void convertWith_ValidTherapyEventAnd_ValidTherapyRoomCriteria() throws PdfException {
        float checkFrom = 13.00f;
        float checkTo = 13.75f;
        PdfPlanDate converted = PdfPlanDateConverter.convert(secondValidTherapyEvent, 3);
        assertThat("PdfPlanDate title is correct", converted.getDateName(), equalToIgnoringCase(secondValidTherapyRoomTitle));
        assertThat("Start time is 13:00 (expressed as 13.00)", converted.getTimeFrom(), equalTo(checkFrom));
        assertThat("End time is 13:45 (expressed as 13:45)", converted.getTimeTo(), equalTo(checkTo));
    }

    @Test
    public void convertWith_ValidTherapyEventAnd_InvalidCriteria() throws PdfException {
        exception.expect(PdfException.class);
        PdfPlanDateConverter.convert(firstValidTherapyEvent, 4);
    }

    @Test
    public void convertWith_NullTherapyEventAnd_ValidCriteria() throws PdfException {
        TherapyEvent nullTherapyEvent = null;
        exception.expect(PdfException.class);
        PdfPlanDateConverter.convert(nullTherapyEvent, 1);
    }

    @Test
    public void convertWith_EmptyTherapyEventAnd_ValidCriteria() throws PdfException {
        exception.expect(PdfException.class);
        PdfPlanDateConverter.convert(firstInvalidTherapyEvent, 1);
    }

    @Test
    public void convertWith_PartiallyInvalidTherapyEventAnd_ValidPatientCriteria() throws PdfException {
        TherapyEvent therapyEvent = new TherapyEvent();
        therapyEvent.setTherapist(firstTherapist);
        exception.expect(PdfException.class);
        PdfPlanDateConverter.convert(therapyEvent, 1);
    }

    @Test
    public void convertWith_PartiallyInvalidTherapyEventAnd_ValidTherapistCriteria() throws PdfException {
        TherapyEvent therapyEvent = new TherapyEvent();
        therapyEvent.setTherapyType(firstTherapyType);
        exception.expect(PdfException.class);
        PdfPlanDateConverter.convert(therapyEvent, 2);
    }

    @Test
    public void convertWith_PartiallyInvalidTherapyEventAnd_ValidTherapyRoomCriteria() throws PdfException {
        TherapyEvent therapyEvent = new TherapyEvent();
        therapyEvent.setTherapyType(firstTherapyType);
        exception.expect(PdfException.class);
        PdfPlanDateConverter.convert(therapyEvent, 3);
    }

}
