package ch.camorcare.backoffice.util.email;

import com.dumbster.smtp.SimpleSmtpServer;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.mail.MessagingException;

import static org.junit.Assert.assertTrue;

/**
 * This unit test checks to make sure that an email is able to be sent using the {@link
 * ch.camorcare.backoffice.util.email.EmailService}. In order to test this functionality a {@link
 * com.dumbster.smtp.SimpleSmtpServer} is set up, which can be found as part of the Dumbster project.
 */
public class EmailServiceUT {

	private static EmailService realEmailService;
	private final String validEmail = "dwietstruk@gmail.com";
	private SimpleSmtpServer server;

	/**
	 * Sets up the test class using Spring.
	 *
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext("TestConfig.xml");
		realEmailService = (EmailService) context.getBean("emailUtil");
	}

	/**
	 * Sends an email to a dummy SMTP server using Dumbster. Asserts that one email should be on the server after
	 * sending it, passes if <code>true</code>.
	 *
	 * @throws MessagingException
	 */
	@Test
	public void sendValidEmail() throws MessagingException {
		// Start server
		server = SimpleSmtpServer.start();
		// Send email
		realEmailService.sendTherapyPlan(validEmail, 10, "..\\qse-sepm-ws14-02\\src\\test\\resources\\TestConfig.xml");
		// Stop SimpleSmtpServer
		server.stop();
		// Check that the SimpleSmtpServer actually received an email
		assertTrue(server.getReceivedEmailSize() == 1);
	}
}
